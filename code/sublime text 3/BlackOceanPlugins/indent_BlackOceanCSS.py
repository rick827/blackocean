import sublime, sublime_plugin
import re
from sublime_plugin import TextCommand

def isBlackOceanCSS(view=None):
    if view is None:
        view = sublime.active_window().active_view()
    correct_scope = False
    for region in view.sel():
        correct_scope = correct_scope or ('source.blackoceancss' in view.scope_name(region.b))
    return correct_scope


class BocIndentCommand(sublime_plugin.TextCommand):
    def is_enabled(self):
        return isBlackOceanCSS(self.view)

    def is_visible(self):
        return False

    def run(self, *args, **kwargs):
        is_selector = True
        for region in self.view.sel():
            left_offset = min(region.a, region.b)
            is_selector = (
                self.view.score_selector(left_offset  , 'meta.selector.css.blackoceancss') > 0 or
                self.view.score_selector(left_offset-1, 'meta.selector.css.blackoceancss') > 0
            )
        snippet = "\n"
        if is_selector:snippet = "\n\t";
        self.view.run_command('insert_snippet', { 'contents': snippet })


