#pragma hdrstop
#include "eSessions.h"
#pragma package(smart_init)

// Otros ####################################################################
/*Variables*/
static eSessions *_eSessions = null;

/*Funciones*/
eSessions *__stdcall sfeSessions() {
	if ( !_eSessions ) _eSessions = new eSessions;

	return _eSessions;
 }
//###########################################################################


// class PACKAGE eSession //#################################################
/*Creacion - public*/
eSession::eSession( int sock ) {};
//###########################################################################


// class PACKAGE eSessions //################################################
/*Metodos - public*/
void   eSessions::Insert( int sock              ) {};
void   eSessions::Delete( int sock              ) {};
String eSessions::Pass  ( int sock              ) { return ""; };
void   eSessions::Pass  ( int sock, String pass ) {};
int    eSessions::Year  ( int sock              ) { return 0; };
void   eSessions::Year  ( int sock, int year    ) {};

void eSessions::ResponseAll( String response, int npass, ... ) {};

void eSessions::DisconnectAll() {};
//###########################################################################
