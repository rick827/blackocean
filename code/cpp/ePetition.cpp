#pragma hdrstop
#include "ePetition.h"
#pragma package(smart_init)

// class PACKAGE ePetition : public TObject //###############################
/*Creacion - public*/
__fastcall ePetition::ePetition(                                                        ) {
	ePetition_start;
 };
__fastcall ePetition::ePetition( int sock, String petition, bool http                   ) {
	ePetition_start;

	Sock = sock;

	if ( http ) Data = ParseHttp( petition );
	else {
		Data.Parse( petition );

		IdClient = Data["id"     ].Str;
		Section  = Data["section"].Str.LowerCase();
		Func     = Data["func"   ].Str.LowerCase();
	}
 };
__fastcall ePetition::ePetition( int sock, String idclient, String section, String func ) {
	ePetition_start;

	Sock     = sock;
	IdClient = idclient;
	Section  = Section;
	Func     = func;
 };
//###########################################################################


/*Operadores - public*/
sJson ePetition::operator []( String tag ) {
	return Data[tag];
 };
sJson ePetition::operator []( int    tag ) {
	return Data[tag];
 };
//###########################################################################


/*Metodos - public*/
void ePetition::Parse( String json ) {
	Data.Parse( json );

	IdClient = Data["id"     ].Str;
	Section  = Data["section"].Str.LowerCase();
	Func     = Data["func"   ].Str.LowerCase();
 };

void ePetition::ResponceHttp( int code, seContentType type, UTF8String body ) {
	UTF8String bod = body, tco = "", res;

	if      ( type == ctTXT  ) { tco = "text/plan; charset=UTF-8"       ; }
	else if ( type == ctSVG  ) { tco = "image/svg+xml"                  ; }
	else if ( type == ctOTF  ) { tco = "font/opentype"                  ; }
	else if ( type == ctPNG  ) { tco = "image/png"                      ; }
	else if ( type == ctJPG  ) { tco = "image/jpg"                      ; }
	else if ( type == ctICO  ) { tco = "image/ico"                      ; }
	else if ( type == ctCSS  ) { tco = "text/css"                       ; }
	else if ( type == ctJS   ) { tco = "text/javascript; charset=UTF-8" ; }
	else if ( type == ctJSON ) { tco = "application/json; charset=UTF-8"; }
	else if ( type == ctTTF  ) { tco = "application/x-font-truetype"    ; }
	else if ( type == ctWOFF ) { tco = "application/font-woff"          ; }
	else if ( type == ctHTML ) { tco = "text/html; charset=UTF-8"       ; }

	res =
		"HTTP/1.1 " + (UTF8String)code + "\r\n"
		"Server: BlackCoral/0.4.115 (Win64)\r\n"
		"Content-Language: es\r\n"
		"Connection: close\r\n"
		"Date: " + (UTF8String)DateToHttp( Date() + Time() ) + "\r\n"
		"Content-Type: " + tco + "\r\n"
		"Content-Length: " + (UTF8String)bod.Length() + "\r\n"
		"Access-Control-Allow-Origin: *\r\n"
		"\r\n" + bod
	;

	send( Sock, res.c_str(), res.Length(), 0 );
 };
void ePetition::ResponceSock( String state                                  ) {
	char    *cad;
	__int64  len;

	EncodeSock( "{"
		co"id"     co":"co+ IdClient +co","
		co"section"co":"co+ Section  +co","
		co"func"   co":"co+ Func     +co","
		co"state"  co":"co+ state    +co
	"}", &cad, len );

	send( Sock, cad, len, 0 );

	delete[] cad;
 };
void ePetition::ResponceSock( String state, String message                  ) {
	char    *cad;
	__int64  len;

	EncodeSock( "{"
		co"id"     co":"co+ IdClient +co","
		co"section"co":"co+ Section  +co","
		co"func"   co":"co+ Func     +co","
		co"state"  co":"co+ state    +co","
		+ message  +
	"}", &cad, len );

	send( Sock, cad, len, 0 );

	delete[] cad;
 };
void ePetition::ResponceHand(                                               ) {
	UTF8String  men =
		"HTTP/1.1 101 Switching Protocols\r\n"
		"Upgrade: websocket\r\n"
		"Connection: Upgrade\r\n"
		"Sec-WebSocket-Accept: " + GetHash( Data["Sec-WebSocket-Key"].Str ) + "\r\n"
		"Sec-WebSocket-protocolo: chat\r\n"
		"\r\n"
	;

	send( Sock, men.c_str(), men.Length(), 0 );
 };

void ePetition::ResponceSock( int sock, String message ) {
	char    *cad;
	__int64  len;

	EncodeSock( message, &cad, len );

	send( sock, cad, len, 0 );

	delete[] cad;
 };
//###########################################################################
