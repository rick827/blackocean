#pragma hdrstop
#include "eBase.h"
#include "eOptions.h"
#pragma package(smart_init)


// Otros ####################################################################
/*Variables*/
static eBase *_eBase = null;

/*Funciones*/
eBase *__stdcall sfeBase() {
	if ( !_eBase ) ( _eBase = new eBase )->Connect( true );

	return _eBase;
 };
//###########################################################################


// class PACKAGE eBase //####################################################
/*Creacion - public*/
__fastcall eBase:: eBase() {
	onConnect    = null;
	onError      = null;
	onDisconnect = null;

	_wait      = new TFDGUIxWaitCursor( null );
	_conection = new TFDConnection    ( null );
 };
__fastcall eBase::~eBase() {
	this->Connect( false );

	delete _conection; _conection = null;
	delete _wait     ; _wait      = null;
 };
//###########################################################################


/*Metodos - public*/
void eBase::Connect( bool connect ) {
	_conection->Connected   = false;
	_conection->LoginPrompt = false;
	_conection->Params->Clear();

	if ( onDisconnect ) onDisconnect( this );
	if ( !connect     ) return;

	try {
		sJson opc = gopc["base"]["strings"];

		for ( int i = 0; i < opc.Size; i++ )
			_conection->Params->Add( opc[i].Str );

		_conection->Connected = true;

		if ( onConnect ) onConnect( this );

		Notify( "Base de datos", "La base de datos se conecto correctamente.", mtInfo );
	}
	catch ( Exception &e ) {
		if ( onError ) onError( this, "Error: (" + e.ClassName() + ") " + e.Message );

		Notify( "Base de datos", "Error: (" + e.ClassName() + ") " + e.Message, mtError );
	}
 };

TFDQuery *eBase::NewQuery() {
	TFDQuery *que = new TFDQuery( null );

	que->Connection = _conection;

	return que;
 };
//###########################################################################
