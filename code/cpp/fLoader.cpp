#pragma hdrstop
#include "fLoader.h"
#pragma package(smart_init)


//Otros #####################################################################
/*Variables*/
static fLoader *globalTLoader = null;
//###########################################################################


//class PACKAGE fLoader : public TLayout //##################################
/*Creacion - controles - protected*/
void fLoader::CreateElipse( TEllipse **elipse ) {
	(*elipse)          = new TEllipse( this );
	(*elipse)->Parent  = this                ;
	(*elipse)->StrK    = BK::None            ;
	(*elipse)->FilC    = _color              ;
	(*elipse)->Opacity = 0.3                 ;
	(*elipse)->Height  = 0.001               ;
	(*elipse)->Width   = 0.001               ;
 };

void fLoader::CreateThis   () {
	_rback         = new TRectangle( this );
	_rback->Parent = this                  ;
	_rback->Align  = AL::Contents          ;
	_rback->StrK   = BK::None              ;
	_rback->FilC   = _fill                 ;
 };
void fLoader::CreateElipses() {
	CreateElipse( &_eloader1 );
	CreateElipse( &_eloader2 );
	CreateElipse( &_eloader3 );
 };

/*Creacion - public*/
__fastcall fLoader::fLoader( TComponent *owner ) : TLayout( owner ) {
	_fill     = 0xFF92CDCF;
	_color    = 0xFF1C1D21;
	_elipseIn = true      ;

	/*Inicio*/
	this->Align   = AL::Contents;
	this->Opacity = 0           ;

	CreateThis   ();
	CreateElipses();

	_rback->SendToBack();
 };
//###########################################################################


/*Propiedades - private*/
void __fastcall fLoader::WColor( unsigned color ) {
	_color = color;

	_eloader1->FilC = _color;
	_eloader2->FilC = _color;
	_eloader3->FilC = _color;
 };
void __fastcall fLoader::WFill ( unsigned fill  ) {
	_fill = fill;

	_rback->FilC = _fill;
 };
//###########################################################################


/*Eventos - protected*/
void __fastcall fLoader::frame_this( fAnim *sender ) {
	#define anie(x,y) x->Width=(y);x->Height=(y);x->PosY=(this->Height-(y))/2;x->PosX=(this->Width-(y))/2;
	#define anif sfSinusoidalInOut

	double per = ( Time() - _animDat ).Val;

	if ( !_animOut ) {
		if ( per >= _animIn1 ) { if ( _animDa1.Val == 0 ) _animDa1 = Time(); double pe1 = ( Time() - _animDa1 ).Val; anie( _eloader1, pe1 >= _animMax ? 50 : anif( pe1, 0, 50, _animMax ) ); }
		if ( per >= _animIn2 ) { if ( _animDa2.Val == 0 ) _animDa2 = Time(); double pe2 = ( Time() - _animDa2 ).Val; anie( _eloader2, pe2 >= _animMax ? 50 : anif( pe2, 0, 50, _animMax ) ); }
		if ( per >= _animIn3 ) { if ( _animDa3.Val == 0 ) _animDa3 = Time(); double pe3 = ( Time() - _animDa3 ).Val; anie( _eloader3, pe3 >= _animMax ? 50 : anif( pe3, 0, 50, _animMax ) );
			if ( pe3 >= _animMax ) {
				_animDa1 = 0;
				_animDa2 = 0;
				_animDa3 = 0;
				_animDat = Time();
				_animOut = true;
			}
		}
	}
	else {
		if ( per >= _animIn1 ) { if ( _animDa1.Val == 0 ) _animDa1 = Time(); double pe1 = ( Time() - _animDa1 ).Val; anie( _eloader1, pe1 >= _animMax ? 0 : anif( pe1, 50, -50, _animMax ) ); }
		if ( per >= _animIn2 ) { if ( _animDa2.Val == 0 ) _animDa2 = Time(); double pe2 = ( Time() - _animDa2 ).Val; anie( _eloader2, pe2 >= _animMax ? 0 : anif( pe2, 50, -50, _animMax ) ); }
		if ( per >= _animIn3 ) { if ( _animDa3.Val == 0 ) _animDa3 = Time(); double pe3 = ( Time() - _animDa3 ).Val; anie( _eloader3, pe3 >= _animMax ? 0 : anif( pe3, 50, -50, _animMax ) );
			if ( pe3 >= _animMax ) {
				_animDa1 = 0;
				_animDa2 = 0;
				_animDa3 = 0;
				_animDat = Time();
				_animOut = false;
			}
		}
	}
 };
void __fastcall fLoader::inilo_this( fAnim *sender ) {
	if ( onShow ) onShow( this );
 };
void __fastcall fLoader::finlo_this( fAnim *sender ) {
	fAnim::Stop( int(this) + 1 );

	this->Visible = false;

	if ( onHide  ) onHide( this );
	if ( _delete ) delete this;
 };
//###########################################################################


/*Metodos - public*/
void fLoader::ShowL( bool showl ) {
	if ( _showl != showl ) {
		if ( showl ) {
			this->Opacity = 0   ;
			this->Visible = true;

			_eloader1->Width = 0; _animIn1 = TTime( 0, 0, 0, 000 ).Val; _animDa1 = 0;
			_eloader2->Width = 0; _animIn2 = TTime( 0, 0, 0, 200 ).Val; _animDa2 = 0;
			_eloader3->Width = 0; _animIn3 = TTime( 0, 0, 0, 400 ).Val; _animDa3 = 0;
			_animOut = false    ; _animMax = TTime( 0, 0, 0, 900 ).Val; _animDat = Time();

			fAnim::Custom(                 int(this) + 1, 0, frame_this );
			fAnim::Opacity( (TShape*)this, int(this) + 2, 0.3, 1, sfCircularOut );
		}
	}

	if ( showl ) fAnim::Opacity( (TShape*)this, int(this) + 3, 0.3, 1, sfCircularOut, inilo_this );
	else         fAnim::Opacity( (TShape*)this, int(this) + 4, 0.3, 0, sfCircularOut, finlo_this );

	_showl = showl;
 };
void fLoader::HideL( bool delet ) {
	this->_delete = delet;
	this->ShowL( false );
 };
//###########################################################################
