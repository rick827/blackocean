#pragma hdrstop
#include "wStream.h"
#pragma package(smart_init)


// class PACKAGE wStream //##################################################
/*Creacion - public*/
__fastcall wStream:: wStream(             ) {
	wStream_start;
 };
__fastcall wStream:: wStream( String file ) {
	wStream_start;

	this->Load( file );
 };
__fastcall wStream::~wStream(             ) {
	FreeMemory( _buffer );
 };
//###########################################################################


/*Metodos - public*/
void __fastcall wStream::I16    ( __int16       value ) {
	_size  += 2;
	_buffer = (BYTE*)ReallocMemory( _buffer, _size );

	if ( ROT ) {
		_buffer[Position++] = ( value >> 8  ) & 0xFF;
		_buffer[Position++] =   value         & 0xFF;
	}
	else {
		_buffer[Position++] =   value         & 0xFF;
		_buffer[Position++] = ( value >> 8  ) & 0xFF;
	}
 };
void __fastcall wStream::I64    ( __int64       value ) {
	_size  += 8;
	_buffer = (BYTE*)ReallocMemory( _buffer, _size );

	if ( ROT ) {
		_buffer[Position++] = ( value >> 56 ) & 0xFF;
		_buffer[Position++] = ( value >> 48 ) & 0xFF;
		_buffer[Position++] = ( value >> 40 ) & 0xFF;
		_buffer[Position++] = ( value >> 32 ) & 0xFF;
		_buffer[Position++] = ( value >> 24 ) & 0xFF;
		_buffer[Position++] = ( value >> 16 ) & 0xFF;
		_buffer[Position++] = ( value >> 8  ) & 0xFF;
		_buffer[Position++] =   value         & 0xFF;
	}
	else {
		_buffer[Position++] =   value         & 0xFF;
		_buffer[Position++] = ( value >> 8  ) & 0xFF;
		_buffer[Position++] = ( value >> 16 ) & 0xFF;
		_buffer[Position++] = ( value >> 24 ) & 0xFF;
		_buffer[Position++] = ( value >> 32 ) & 0xFF;
		_buffer[Position++] = ( value >> 40 ) & 0xFF;
		_buffer[Position++] = ( value >> 48 ) & 0xFF;
		_buffer[Position++] = ( value >> 56 ) & 0xFF;
	}
 };
void __fastcall wStream::Int    ( int           value ) {
	_size  += 4;
	_buffer = (BYTE*)ReallocMemory( _buffer, _size );

	if ( ROT ) {
		_buffer[Position++] = ( value >> 24 ) & 0xFF;
		_buffer[Position++] = ( value >> 16 ) & 0xFF;
		_buffer[Position++] = ( value >> 8  ) & 0xFF;
		_buffer[Position++] =   value         & 0xFF;
	}
	else {
		_buffer[Position++] =   value         & 0xFF;
		_buffer[Position++] = ( value >> 8  ) & 0xFF;
		_buffer[Position++] = ( value >> 16 ) & 0xFF;
		_buffer[Position++] = ( value >> 24 ) & 0xFF;
	}
 };
void __fastcall wStream::Flo    ( float         value ) {
	_size  += 4;
	_buffer = (BYTE*)ReallocMemory( _buffer, _size );

	__int32 uva = *((__int32*)&value);

	if ( ROT ) {
		_buffer[Position++] = ( uva >> 24 ) & 0xFF;
		_buffer[Position++] = ( uva >> 16 ) & 0xFF;
		_buffer[Position++] = ( uva >> 8  ) & 0xFF;
		_buffer[Position++] =   uva         & 0xFF;
	}
	else {
		_buffer[Position++] =   uva         & 0xFF;
		_buffer[Position++] = ( uva >> 8  ) & 0xFF;
		_buffer[Position++] = ( uva >> 16 ) & 0xFF;
		_buffer[Position++] = ( uva >> 24 ) & 0xFF;
	}
 };
void __fastcall wStream::Dou    ( double        value ) {
	_size  += 8;
	_buffer = (BYTE*)ReallocMemory( _buffer, _size );

	__int64 uva = *((__int64*)&value);

	if ( ROT ) {
		_buffer[Position++] = ( uva >> 56 ) & 0xFF;
		_buffer[Position++] = ( uva >> 48 ) & 0xFF;
		_buffer[Position++] = ( uva >> 40 ) & 0xFF;
		_buffer[Position++] = ( uva >> 32 ) & 0xFF;
		_buffer[Position++] = ( uva >> 24 ) & 0xFF;
		_buffer[Position++] = ( uva >> 16 ) & 0xFF;
		_buffer[Position++] = ( uva >> 8  ) & 0xFF;
		_buffer[Position++] =   uva         & 0xFF;
	}
	else {
		_buffer[Position++] =   uva         & 0xFF;
		_buffer[Position++] = ( uva >> 8  ) & 0xFF;
		_buffer[Position++] = ( uva >> 16 ) & 0xFF;
		_buffer[Position++] = ( uva >> 24 ) & 0xFF;
		_buffer[Position++] = ( uva >> 32 ) & 0xFF;
		_buffer[Position++] = ( uva >> 40 ) & 0xFF;
		_buffer[Position++] = ( uva >> 48 ) & 0xFF;
		_buffer[Position++] = ( uva >> 56 ) & 0xFF;
	}
 };
void __fastcall wStream::Str    ( String        value ) {
	unsigned  len = value.Length();
	wchar_t  *str = value.w_str();

	//Guardar Tama�o
	_size  += 4;
	_buffer = (BYTE*)ReallocMemory( _buffer, _size + len );

	if ( ROT ) {
		_buffer[Position++] = ( len >> 24 ) & 0xFF;
		_buffer[Position++] = ( len >> 16 ) & 0xFF;
		_buffer[Position++] = ( len >> 8  ) & 0xFF;
		_buffer[Position++] =   len         & 0xFF;
	}
	else {
		_buffer[Position++] =   len         & 0xFF;
		_buffer[Position++] = ( len >> 8  ) & 0xFF;
		_buffer[Position++] = ( len >> 16 ) & 0xFF;
		_buffer[Position++] = ( len >> 24 ) & 0xFF;
	}

	//Guardar cadena
	_size += len;

	for ( unsigned i = 0; i < len; i++ )
		_buffer[Position++] = str[i];
 };
void __fastcall wStream::Sun    ( UnicodeString value ) {
	__int16  len = value.Length();
	wchar_t *str = value.c_str();

	I16( len + 1 );

	for ( __int16 i = 0; i < len; i++ )
		I16( str[i] );

	I16( 0 );
 };
void __fastcall wStream::OnlyStr( String        value ) {
	unsigned  len = value.Length();
	wchar_t  *str = value.w_str();

	_size  += len;
	_buffer = (BYTE*)ReallocMemory( _buffer, _size );

	for ( unsigned i = 0; i < len; i++ )
		_buffer[Position++] = str[i];
 };

__int16       __fastcall wStream::I16    (                    ) {
	if ( Position + 2 > _size ) { Position+= 2; return null; }

	if ( ROT ) {
		return
			( _buffer[Position++] << 8  ) |
			  _buffer[Position++]
		;
	}
	else {
		return
			  _buffer[Position++]         |
			( _buffer[Position++] << 8  )
		;
	}
 };
__int64       __fastcall wStream::I64    (                    ) {
	if ( Position + 8 > _size ) { Position+= 8; return null; }

	__int64 res = 0;

	if ( ROT ) {
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
	}
	else {
		res = ( res << 8 ) + _buffer[Position + 7];
		res = ( res << 8 ) + _buffer[Position + 6];
		res = ( res << 8 ) + _buffer[Position + 5];
		res = ( res << 8 ) + _buffer[Position + 4];
		res = ( res << 8 ) + _buffer[Position + 3];
		res = ( res << 8 ) + _buffer[Position + 2];
		res = ( res << 8 ) + _buffer[Position + 1];
		res = ( res << 8 ) + _buffer[Position    ];

		Position+= 8;
	}

	return res;
 };
int           __fastcall wStream::Int    (                    ) {
	if ( Position + 4 > _size ) { Position+= 4; return null; }

	if ( ROT ) {
		return
			( _buffer[Position++] << 24 ) |
			( _buffer[Position++] << 16 ) |
			( _buffer[Position++] << 8  ) |
			  _buffer[Position++]
		;
	}
	else {
		return
			  _buffer[Position++]         |
			( _buffer[Position++] << 8  ) |
			( _buffer[Position++] << 16 ) |
			( _buffer[Position++] << 24 )
		;
	}
 };
float         __fastcall wStream::Flo    (                    ) {
	if ( Position + 4 > _size ) { Position+= 4; return null; }

	__int32 res = 0;

	if ( ROT ) {
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
	}
	else {
		res = ( res << 8 ) + _buffer[Position + 3];
		res = ( res << 8 ) + _buffer[Position + 2];
		res = ( res << 8 ) + _buffer[Position + 1];
		res = ( res << 8 ) + _buffer[Position    ];

		Position+= 4;
	}

	return *(float*)&res;
 };
double        __fastcall wStream::Dou    (                    ) {
	if ( Position + 8 > _size ) { Position+= 8; return null; }

	__int64 res = 0;

	if ( ROT ) {
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
		res = ( res << 8 ) + _buffer[Position++];
	}
	else {
		res = ( res << 8 ) + _buffer[Position + 7];
		res = ( res << 8 ) + _buffer[Position + 6];
		res = ( res << 8 ) + _buffer[Position + 5];
		res = ( res << 8 ) + _buffer[Position + 4];
		res = ( res << 8 ) + _buffer[Position + 3];
		res = ( res << 8 ) + _buffer[Position + 2];
		res = ( res << 8 ) + _buffer[Position + 1];
		res = ( res << 8 ) + _buffer[Position    ];

		Position+= 8;
	}

	return *( double* )&res;
 };
String        __fastcall wStream::Str    (                    ) {
	if ( Position + 4 > _size ) { Position+= 4; return null; }

	unsigned len;

	if ( ROT ) {
		len =
			( _buffer[Position++] << 24 ) |
			( _buffer[Position++] << 16 ) |
			( _buffer[Position++] << 8  ) |
			  _buffer[Position++]
		;
	}
	else {
		len =
			  _buffer[Position++]         |
			( _buffer[Position++] << 8  ) |
			( _buffer[Position++] << 16 ) |
			( _buffer[Position++] << 24 )
		;
	}

	//Leer cadena
	String res = "";

	for ( unsigned i = 0; i < len && i < _size; i++ )
		res+= (wchar_t)_buffer[Position++];

	return res;
 };
UnicodeString __fastcall wStream::Sun    (                    ) {
	__int16       len = I16();
	UnicodeString res = "";

	for ( __int16 i = 0; Position < _size && i < len; i++, Position+= 2 )
		if ( _buffer[Position]!='\0' || _buffer[Position + 1]!='\0' )
			res+= wchar_t( ( _buffer[Position] << 8 ) | _buffer[Position + 1] );


	return res;
 };
String        __fastcall wStream::OnlyStr( unsigned long size ) {
	if ( size == 0 ) size = _size;

	Position = 0;

	String res = "";

	for ( unsigned i = 0; i < size; i++ )
		res += (wchar_t)_buffer[Position++];

	return res;
 };

void __fastcall wStream::Save ( String file                         ) {
	HANDLE hfi = CreateFile(
		file.w_str()                ,
		GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ             ,
		NULL                        ,
		CREATE_ALWAYS               ,
		FILE_ATTRIBUTE_NORMAL       ,
		NULL
	);

	if ( hfi == INVALID_HANDLE_VALUE ) {
		MessageBox( GetHandle(), LastError().w_str(), L"Error al crear el archivo", MB_ICONERROR );
		return;
	}

	unsigned long bwr = 0;

	if ( !WriteFile( hfi, _buffer, _size, &bwr, null ) )
		MessageBox( GetHandle(), LastError().w_str(), L"Error al escribir el archivo", MB_ICONERROR );

	else if ( ( unsigned long )_size != bwr )
		MessageBox( GetHandle(), LastError().w_str(), L"Error, diferencias al escribir", MB_ICONERROR );

	CloseHandle( hfi );
 };
void __fastcall wStream::Load ( String file                         ) {
	HANDLE hfi = CreateFile(
		file.w_str()                                ,
		GENERIC_READ                                ,
		FILE_SHARE_READ                             ,
		null                                        ,
		OPEN_EXISTING                               ,
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
		null
	);

	if ( hfi == INVALID_HANDLE_VALUE ) {
		MessageBox( GetHandle(), LastError().w_str(), L"Error al abrir el archivo", MB_ICONERROR );
		return;
	}

	LARGE_INTEGER siz;

	if ( !GetFileSizeEx( hfi, &siz ) ) {
		MessageBox( GetHandle(), LastError().w_str(), L"Error al obtener el tama�o", MB_ICONERROR );
		CloseHandle( hfi );
		return;
	}

	if ( _size )
		FreeMemory( _buffer );

	Position = 0;
	_size    = siz.LowPart;
	_buffer  = (BYTE*)GetMemory( _size );

	OVERLAPPED ove = {0};

	if( ReadFile( hfi, _buffer, _size, null, &ove ) ) {
		MessageBox( GetHandle(), LastError().w_str(), L"Error al leer el archivo", MB_ICONERROR );
		CloseHandle( hfi );
		return;
	}

	CloseHandle( hfi );
 };
void __fastcall wStream::Copy ( wStream *stream, unsigned long size ) {
	if ( !stream || size == 0 ) return;

	if ( this->_size < this->Position + size ) {
		this->_size   = this->Position + size;
		this->_buffer = (BYTE*)ReallocMemory( this->_buffer, this->_size );
	}

	for ( unsigned long i = 0; i < size && stream->Position < stream->_size; i++ )
		this->_buffer[Position + i] = stream->_buffer[stream->Position + i];

	stream->Position+= size;
	this  ->Position+= size;
 };
void __fastcall wStream::Clear(                                     ) {
	FreeMemory( _buffer );

	_size    = 0;
	_buffer  = (BYTE*)GetMemory( 0 );
	Position = 0;
 };
//###########################################################################