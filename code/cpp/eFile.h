#ifndef eFileH
#define eFileH

#include <classes.hpp>
#include "ePetition.h"

class PACKAGE eFile {
	/*Variables*/protected:
	UTF8String  _file;
	ePetition  *_petition;
	//###########################################################################


	/*Metodos*/public:
	void Load( String file, bool json                                    );
	void Load( HINSTANCE instance, String file, String module, bool json );

	/*Metodos*/private:
	void Load( TStream *stream, bool json );
	//###########################################################################
 };

#endif
