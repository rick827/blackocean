#pragma hdrstop
#include "fLine.h"
#pragma package(smart_init)


// OTROS ####################################################################
/*Definiciones*/
#define playCursor _rcursor->Visible=true;SetInterval("cursor"+String((int)this),0.5,showw_cursor);
#define stopCursor StopInterval("cursor"+String((int)this));

#define playDThis SetInterval("dthis"+String((int)this),0.25,click_edit);_senderDblDown=sender;_pointsDblDown.x=x;_pointsDblDown.y=y;
#define stopDThis StopInterval("dthis"+String((int)this));_senderDblDown=null;

#define playDSelect SetInterval("dselect"+String((int)this),0.12,click_select);_senderSelDown=sender;_pointxSelDown=pom.x-fom.left-bor.left-this->AbsoluteRect.left;_shiftsSelDown=shift;
#define stopDSelect StopInterval("dselect"+String((int)this));_senderSelDown=null;

#define toleranceDrag 5
#define widthInsert   ( _insert ? _widthLetter : _widthCursor )

#define charSpecial(x) ( x==' ' || x=='.' || x==':' || x=='/' || x=='[' || x==']' || x=='(' || x==')' || x=='<' || x=='>' || x=='%' || x=='-' || x=='\\' )
//###########################################################################


// class fLine : public TLayout //###########################################
/*Creacion - private*/
void fLine::CreateThis  () {
	this->HitTest      = true;
	this->TabStop      = true;
	this->CanFocus     = true;
	this->ClipChildren = true;
	this->Cursor       = crIBeam;
	this->OnResize     = resiz_this;
	this->OnEnter      = enter_this;
	this->OnExit       = exitt_this;
	this->OnMouseWheel = wheel_this;
	this->OnMouseDown  = moudw_this;
	this->OnKeyDown    = keydw_this;
 };
void fLine::CreateCursor() {
	_rcursor          = new TRectangle( this );
	_rcursor->Parent  = this                  ;
	_rcursor->PosX    = 0                     ;
	_rcursor->PosY    = 0                     ;
	_rcursor->Width   = widthInsert           ;
	_rcursor->Height  = this->Height          ;
	_rcursor->HitTest = false                 ;
	_rcursor->FilC    = _fill                 ;
	_rcursor->StrK    = BK::None              ;
	_rcursor->Visible = false                 ;
 };
void fLine::CreateText  () {
	_twords                = new TText( this )                 ;
	_twords->Parent        = this                              ;
	_twords->PosX          = 0                                 ;
	_twords->PosY          = 0                                 ;
	_twords->Height        = this->Height                      ;
	_twords->Width         = this->Width + ( _widthLetter * 2 );
	_twords->Font->Family  = fontFamily                        ;
	_twords->Font->Size    = fontSize                          ;
	_twords->Color         = _color                            ;
	_twords->HitTest       = false                             ;
	_twords->Text          = ""                                ;
	_twords->WordWrap      = false                             ;
	_twords->HorzTextAlign = TTextAlign::Leading               ;
 };
void fLine::CreateSelect() {
	_rselect              = new TRectangle( this );
	_rselect->Parent      = this                  ;
	_rselect->PosX        = 0                     ;
	_rselect->PosY        = 0                     ;
	_rselect->Height      = this->Height          ;
	_rselect->Width       = 0                     ;
	_rselect->FilC        = _fill                 ;
	_rselect->StrK        = BK::None              ;
	_rselect->Opacity     = 0.15                  ;
	_rselect->Visible     = false                 ;
	_rselect->Cursor      = crArrow;
	_rselect->OnMouseDown = moudw_select;
 };
void fLine::CreateScroll() {
	_rscroll          = new TRectangle( null );
	_rscroll->Parent  = this                  ;
	_rscroll->HitTest = false                 ;
	_rscroll->FilC    = _fill                 ;
	_rscroll->StrK    = BK::None              ;
	_rscroll->PosX    = 0                     ;
	_rscroll->PosY    = this->Height - 1      ;
	_rscroll->Height  = 1                     ;
	_rscroll->Width   = this->Width           ;
	_rscroll->Opacity = 0.3                   ;
	_rscroll->Visible = false                 ;
 };

/*Creacion - public*/
__fastcall fLine:: fLine( TComponent *owner ) : TLayout( owner ) {
	/*Variables - private*/
	_readOnly = false;

	/*Variables - public*/
	NoRender = false;

	_insert      = false;
	_widthLetter = 6.59765625;
	_widthCursor = 1.0;

	_position = 0;
	_startPos = 0;
	_finalPos = snan;

	_senderDblDown = null;

	_undoIndex = 0;
	_undoDelay = false;

	/*Variables - propiedades - private*/
	_fill   = claBlack;
	_color  = claBlack;
	_text   = "";
	_family = fontFamily;
	_size   = fontSize;

	/*Variables - Eventos - public*/
	onKeyEnter  = null;
	onKeyChange = null;

	/*Inicio*/
	CreateThis  ();
	CreateCursor();
	CreateText  ();
	CreateSelect();
	CreateScroll();

	_rselect->BringToFront();

	WSize( _size );
 };
__fastcall fLine::~fLine(                   ) {
	stopCursor;
	StopInterval;
	StopInterval;
 };
//###########################################################################


/*Propiedades - private*/
void fLine::WFill  ( unsigned fill   ) {
	_fill          = fill;
	_rcursor->FilC = fill;
	_rselect->FilC = fill;
	_rscroll->FilC = fill;
 };
void fLine::WColor ( unsigned color  ) {
	_color         = color;
	_twords->Color = color;
 };
void fLine::WText  ( String   text   ) {
	_text = text;

	Render( false );
 };
void fLine::WFamily( String   family ) {
	_family               = family;
	_twords->Font->Family = family;

	TBitmap *
	bmp                       = new TBitmap;
	bmp->Canvas->Font->Size   = _size;
	bmp->Canvas->Font->Family = _family;
	_widthLetter              = bmp->Canvas->TextWidth( "00" ) - bmp->Canvas->TextWidth( "0" );
	delete bmp;

	Render( false );
 };
void fLine::WSize  ( float    size   ) {
	_size               = size;
	_twords->Font->Size = size;

	TBitmap *
	bmp                       = new TBitmap;
	bmp->Canvas->Font->Size   = _size;
	bmp->Canvas->Font->Family = _family;
	_widthLetter              = bmp->Canvas->TextWidth( "00" ) - bmp->Canvas->TextWidth( "0" );
	delete bmp;

	Render( false );
 };
//###########################################################################


/*Intervalos - private*/
void __fastcall fLine::showw_cursor( TObject *sender ) {
	if ( NoRender ) {
		stopCursor;

		_rcursor->Visible = false;
	}
	else _rcursor->Visible = !_rcursor->Visible;
 };

void __fastcall fLine::timer_undo( TObject *sender ) {
	_undoDelay = false;
 };
//###########################################################################


/*Eventos - private*/
void __fastcall fLine::resiz_this( TObject *sender                                                           ) {
	_rcursor->Height = this->Height;
	_rselect->Height = this->Height;
	_rscroll->PosY   = this->Height - 1;
	_twords ->Height = this->Height;
	_twords ->Width  = this->Width + ( _widthLetter * 200 );

	Render( false );
 };
void __fastcall fLine::enter_this( TObject *sender                                                           ) {
	playCursor;

	if ( _finalPos != snan )
		_rselect->Visible = true;

	if ( onEnter )
		onEnter( this );
 };
void __fastcall fLine::exitt_this( TObject *sender                                                           ) {
	stopCursor;

	_rcursor->Visible = false;
	_rselect->Visible = false;

	if ( onExit )
		onExit( this );
 };
void __fastcall fLine::wheel_this( TObject *sender, TShiftState shift, int wheelDelta, bool &handled         ) {
	if ( _senderDblDown ) return;

	//_position-= ( wheelDelta / 10 );
	_position+= wheelDelta;

	Render( false );
 };
void __fastcall fLine::moudw_this( TObject *sender, TMouseButton button, TShiftState shift, float x, float y ) {
	if ( !_senderDblDown ) {
		varPom;
		float px1 = pom.x;
		float py1 = pom.y;
		float po1 ;
		float po2 ;

		stopDThis;

		if ( button != TMouseButton::mbLeft ) {
        	ViewPopUp( pom.x, pom.y );
		}
		else {
			ClickEdit( pom.x - fom.left - bor.left - this->AbsoluteRect.left, shift );

			while ( whileDown ) {
				GetCursorPos( &pom );

				po1 = pom.x;
				po2 = pom.y;

				if (
					( px1 > po1 + toleranceDrag || px1 < po1 - toleranceDrag ) ||
					( py1 > po2 + toleranceDrag || py1 < po2 - toleranceDrag )
				) {
					moudw_edit( sender, button, shift, x, y );
					return;
				}

				Application->ProcessMessages();
			}

			playDThis;
		}
	}
	else dblcl_edit( sender );
 };
void __fastcall fLine::keydw_this( TObject *sender, WORD &key, System::WideChar &keyChar, TShiftState shift  ) {
	playCursor;

	switch ( key ) {
		case 45 /*Insert*/          : KeyInsert( !_insert ); break;
		case 13 /*Enter*/           : KeyEnter (          ); break;
		case 0  /*Caracter*/        : KeyAdd   ( keyChar  ); break;
		case 8  /*Borrar*/          : KeyDelete( true     ); break;
		case 35 /*Fin*/             : KeyFin   ( shift    ); break;
		case 36 /*Inicio*/          : KeyStart ( shift    ); break;
		case 46 /*Suprimr*/         : KeySuprim(          ); break;
		case 67 /*Copiar*/          : KeyCopy  (          ); break;
		case 86 /*Pegar*/           : KeyPaste (          ); break;
		case 88 /*Cortar*/          : KeyCut   (          ); break;
		case 90 /*Deshacer*/        : KeyUndo  ( shift    ); break;
		case 37 /*Flecha Izquierda*/: KeyLeft  ( shift    ); break;
		case 39 /*Flecha derecha*/  : KeyRight ( shift    ); break;
		case 65 /*Seleccionar Todo*/: KeyAll   (          ); break;
	}
 };

void __fastcall fLine::click_edit( TObject *sender                                                           ) {
	stopDThis;
	playCursor;
 };
void __fastcall fLine::dblcl_edit( TObject *sender                                                           ) {
	stopDThis;
	playCursor;

	_startPos = GetAjust( _pointsDblDown.x );

	int      len = _text.Length();
	wchar_t *cad = _text.w_str();

	for ( int x = _startPos; x >= 0; x-- ) {
		if ( charSpecial( cad[x] ) || x == 0 ) {
			_finalPos = x == 0 ? 0 : x + 1;
			break;
		}
	}

	for ( int x = _startPos; x < len; x++ ) {
		if ( charSpecial( cad[x] ) || x == len - 1 ) {
			_startPos = x == len - 1 ? x + 1 : x;
			break;
		}
	}

	Render( true );
 };
void __fastcall fLine::moudw_edit( TObject *sender, TMouseButton button, TShiftState shift, float x, float y ) {
	stopDThis;
	stopCursor;

	_senderDblDown    = sender;
	_rcursor->Visible = true;
	_rselect->Visible = true;

	varPom;
	TTime  ti1 = Time();
	double tma = TTime( 0, 0, 0, 100 ).Val;
	bool   rec ;
	float  px2, pos = _position, poo = ( GetAjust( x ) * _widthLetter ) - _position, po1 = poo;
	float  max = ( _text.Length() * _widthLetter ) + widthInsert; if ( max > this->Width ) max = this->Width;

	while ( whileDown ) {
		GetCursorPos( &pom );

		px2 = pom.x - fom.left - bor.left - this->AbsoluteRect.left;

		if ( px2 < 0 || px2 > max ) {
			if ( ( Time() - ti1 ).Val >= tma ) {
				ti1       = Time();
				rec       = ( px2 > max && px2 > po1 ) || px2 >= 0;
				_position+= rec ? px2 - max : px2;

				Render( false );

				_rselect->Visible = true;
				_rselect->PosX    = rec ? ( po1 = poo - ( _position - pos ) ) : px2;
				_rselect->Width   = rec ? max - _rselect->PosX : ( po1 = poo - ( _position - pos ) ) - px2;
			}
		}
		else {
			_rselect->PosX  = px2 > po1 ? po1       : px2;
			_rselect->Width = px2 > po1 ? px2 - po1 : po1 - px2;
		}

		_rcursor->PosX = px2 > max ? max : px2;

		Application->ProcessMessages();
	}

	_startPos = GetAjust( _rcursor->PosX );
	_finalPos = GetAjust( po1            );
	if ( _startPos == _finalPos ) _finalPos = snan;

	playCursor;

	Render( true );

	_senderDblDown = null;
 };

void __fastcall fLine::click_select( TObject *sender                                                           ) {
	stopDSelect;
	ClickEdit( _pointxSelDown, _shiftsSelDown );

	this->SetFocus();
 };
void __fastcall fLine::dblcl_select( TObject *sender                                                           ) {
	stopDSelect;

	TPoint pom;

	GetCursorPos(&pom);

	ViewPopUp( pom.x, pom.y );
 };
void __fastcall fLine::moudw_select( TObject *sender, TMouseButton button, TShiftState shift, float x, float y ) {
	if ( !_senderSelDown ) {
		varPom;
		float px1 = pom.x, px2;
		float py1 = pom.y, py2;

        stopDSelect;

		if ( button != TMouseButton::mbLeft ) {
			ViewPopUp( pom.x, pom.y );
		}
		else {
			while ( whileDown ) {
				GetCursorPos( &pom );

				px2 = pom.x, py2 = pom.y;

				if (
					( px1 > px2 + toleranceDrag || px1 < px2 - toleranceDrag ) ||
					( py1 > py2 + toleranceDrag || py1 < py2 - toleranceDrag )
				) {
					wDragDrop::DoDragText( tdMove, true, null, 1, Selected() );
					return;
				}

				Application->ProcessMessages();
			}

			playDSelect;
		}
	}
	else dblcl_select( sender );
 };
//###########################################################################

/*Metodos - public*/
void   fLine::Render  ( bool reposition      ) {
	if ( NoRender ) {
		_rcursor->Visible = false;
		_rselect->Visible = false;
		_rscroll->Visible = false;
		_twords ->PosX    = 0;
		_twords ->Text    = _text;
		return;
	}

	int      len = _text.Length();
	float    max = ( ( len * _widthLetter ) - this->Width ) + widthInsert;
	wchar_t *cad = _text.w_str();
	String   str = "";
	int      ini = int( _position / _widthLetter ); if ( ini < 0 ) ini = 0;
	float    pic, pif;

	/*Ajustes*/
	if ( _startPos > len ) _startPos = len; if ( _startPos < 0 ) _startPos = 0;
	if ( _position > max ) _position = max; if ( _position < 0 ) _position = 0;
	pic = ( _startPos * _widthLetter ) - _position;
	if ( reposition ) {
		if      ( pic + widthInsert > this->Width ) { _position+= ( pic + widthInsert ) - this->Width; pic = this->Width - widthInsert; }
		else if ( pic               < 0           ) { _position+= pic                                ; pic = 0                        ; }
	}

	/*Letras*/
	ini = int( _position / _widthLetter ); if ( ini < 0 ) ini = 0;
	int fin = ini + ( this->Width / _widthLetter ) + 2;
	for ( int i = ini; i < fin && i < len; i++ ) str+= cad[i]; _twords->Text = str;

	/*Posicion*/
	_twords ->PosX = ( ini * _widthLetter ) - _position;
	_rcursor->PosX = pic;

	if ( _finalPos != snan ) {
		pif               = ( _finalPos * _widthLetter ) - _position;
		_rselect->PosX    = pif < pic ? pif : pic;
		_rselect->Width   = pif < pic ? pic - pif : pif - pic;
		_rselect->Visible = this->IsFocused;
	}
	else _rselect->Visible = false;

	/*Scroll*/
	_rscroll->Width   = ( this->Width * this->Width ) / ( ( len * _widthLetter ) + widthInsert );
	_rscroll->PosX    = ( _position * this->Width ) / ( ( len * _widthLetter ) + widthInsert );
	_rscroll->Visible = _rscroll->Width < this->Width;
 };
void   fLine::Select  ( int start, int final ) {
	_startPos = start;
	_finalPos = final;

	Render( false );
 };
String fLine::Selected(                      ) {
	String ret = "";
	int    ini = _startPos < _finalPos ? _startPos : _finalPos;
	int    fin = _startPos < _finalPos ? _finalPos : _startPos;

	if ( _finalPos != snan ) {
		wchar_t *cad = _text.w_str();
		int      len = _text.Length();

		for ( int x = ini; x < len; x++ ) {
			if ( x >= ini && x < fin ) {
				ret+= (String)cad[x];
			}
		}
	}

	return ret;
 };
//###########################################################################


/*Funciones - private*/
void fLine::KeyInsert( bool    insert    ) {
	_insert = insert;

	fAnim::Width  ( _rcursor, (__int64)_rcursor + 1, 0.3, _insert ? _widthLetter : widthInsert, sfSinusoidalOut );
	fAnim::Opacity( _rcursor, (__int64)_rcursor + 2, 0.3, _insert ? 0.4          : 1          , sfSinusoidalOut );

	Render( true );
 };
void fLine::KeyEnter (                   ) {
	if ( onKeyEnter ) onKeyEnter( this );
 };
void fLine::KeyAdd   ( wchar_t keyChar   ) {
	if ( _readOnly ) return;

	SetUndo( true );

	bool sel = _rselect->Visible;

	if ( sel ) KeyDelete( false );

	if ( !sel && _insert && _startPos >= 0 && _startPos < _text.Length() ) _text.w_str()[_startPos++] = keyChar;
	else                                                                   _text.Insert( keyChar, ++_startPos );

	Render( true );

	if ( onKeyChange ) onKeyChange( this );
 };
void fLine::KeyDelete( bool    saveUndo  ) {
	if ( _readOnly ) return;
	if ( saveUndo  ) SetUndo( true );

	if ( _finalPos != snan ) {
		int ini = _startPos < _finalPos ? _startPos : _finalPos;
		int fin = _startPos < _finalPos ? _finalPos : _startPos;
		int len = _text.Length();

		for ( int x = len; x >= 0; x-- ) {
			if ( x >= ini && x < ( _insert ? fin + 1 : fin ) ) {
				_text.Delete( x + 1, 1 );
			}
		}

		_startPos = ini;
		_finalPos = snan;
	}
	else _text.Delete( _startPos--, 1 );

	Render( true );

	if ( onKeyChange ) onKeyChange( this );
 };
void fLine::KeyStart ( TShiftState shift ) {
	if ( shift.Contains( ssShift ) && shift.Contains( ssCtrl ) ) {}
	else if ( shift.Contains( ssShift ) ) {
		if ( _finalPos == snan ) _finalPos = _startPos;

		_startPos = 0;
	}
	else if ( shift.Contains( ssCtrl ) ) {}
	else {
		_finalPos = snan;
		_startPos = 0;
	}

	Render( true );
 };
void fLine::KeyFin   ( TShiftState shift ) {
	if ( shift.Contains( ssShift ) && shift.Contains( ssCtrl ) ) {}
	else if ( shift.Contains( ssShift ) ) {
		if ( _finalPos == snan ) _finalPos = _startPos;

		_startPos = _text.Length();
	}
	else if ( shift.Contains( ssCtrl ) ) {}
	else {
		_finalPos = snan;
		_startPos = _text.Length();
	}

	Render( true );
 };
void fLine::KeyLeft  ( TShiftState shift ) {
	if ( shift.Contains( ssShift ) && _finalPos == snan ) _finalPos = _startPos;

	if ( shift.Contains( ssCtrl ) ) {
		if ( !shift.Contains( ssShift ) && _finalPos != snan ) _finalPos = snan;

		int      pos = 0;
		wchar_t *cad = _text.w_str();

		if ( _startPos-2>=0 && charSpecial( cad[_startPos-2] ) ) _startPos--;

		for ( pos = _startPos - 1; pos>=0; pos-- ) {
			if ( charSpecial( cad[pos - 1] ) ) {
				_startPos = pos + 1;
				break;
			}
		}

		if ( pos < 0 ) _startPos = 0;
	}

	if ( !shift.Contains( ssShift ) && _finalPos != snan ) {
		if ( _finalPos < _startPos ) _startPos = _finalPos;

		_finalPos = snan;
	}
	else _startPos--;

	if ( _startPos == _finalPos ) _finalPos = snan;

	Render( true );
 };
void fLine::KeyRight ( TShiftState shift ) {
	if ( shift.Contains( ssShift ) && _finalPos == snan ) _finalPos = _startPos;

	if ( shift.Contains( ssCtrl ) ) {
    	if ( !shift.Contains( ssShift ) && _finalPos != snan ) _finalPos = snan;

		int      len = _text.Length();
		int      pos = 0;
		wchar_t *cad = _text.w_str();

		if ( _startPos+1<len && charSpecial( cad[_startPos+1]) ) _startPos++;

		for ( pos = _startPos; pos<len; pos++ ) {
			if ( ( pos+1<len && charSpecial( cad[pos+1] ) ) ) {
				_startPos = pos;
				break;
			}
		}

		if ( pos == len ) _startPos = len;
	}

	if ( !shift.Contains( ssShift ) && _finalPos != snan ) {
		if ( _finalPos > _startPos ) _startPos = _finalPos;

		_finalPos = snan;
	}
	else _startPos++;

	if ( _startPos == _finalPos ) _finalPos = snan;

	Render( true );
 };
void fLine::KeyAll   (                   ) {
	_startPos = _text.Length();
	_finalPos = 0;

	Render( false );
 };
void fLine::KeySuprim(                   ) {
	if ( _readOnly ) return;

	SetUndo( true );

	if ( _rselect->Visible ) KeyDelete( false );
	else {
		_text.Delete( _startPos + 1, 1 );

		Render( true );
	}

	if ( onKeyChange ) onKeyChange( this );
 };
void fLine::KeyCopy  (                   ) {
	SetClipBoardString( _finalPos != snan ? Selected() : _text );
 };
void fLine::KeyPaste (                   ) {
	if ( _readOnly         ) return;
	if ( _finalPos != snan ) KeyDelete( false );

	SetUndo( true );

	String ret = GetClipBoardString();

	ret = StringReplace( ret, "\r", ""    , TReplaceFlags() << rfReplaceAll );
	ret = StringReplace( ret, "\n", ""    , TReplaceFlags() << rfReplaceAll );
	ret = StringReplace( ret, "\t", "    ", TReplaceFlags() << rfReplaceAll );

	_text.Insert( ret, _startPos + 1 );

	_startPos+= ret.Length();

	Render( true );

	if ( onKeyChange ) onKeyChange( this );
 };
void fLine::KeyCut   (                   ) {
	KeyCopy();

	if ( _readOnly         ) return;
	if ( _finalPos != snan ) KeyDelete( true );
	if ( onKeyChange       ) onKeyChange( this );
 };
void fLine::KeyUndo  ( TShiftState shift ) {
	if ( _readOnly        ) return;
	if ( !_undoTexts.Size ) return;

	if ( _undoIndex == _undoTexts.Size ) {
		_undoTexts+= _text;
		_undoStart+= _startPos;
		_undoFinal+= _finalPos;
	}

	if (
		(  shift.Contains( ssShift ) && _undoIndex + 1 >= _undoTexts.Size )
		||
		( !shift.Contains( ssShift ) && _undoIndex - 1 < 0 )
	) return;

	if ( shift.Contains( ssShift ) ) _undoIndex++; else _undoIndex--;

	_text     = _undoTexts[_undoIndex];
	_startPos = _undoStart[_undoIndex];
	_finalPos = _undoFinal[_undoIndex];

	Render( false );

	if ( onKeyChange ) onKeyChange( this );
 };

float fLine::GetAjust( float x    ) {
	float ind = int( ( x + _position ) / _widthLetter );

	if ( ind > _text.Length() - 1 ) ind = _text.Length();
	if ( ind < 0                  ) ind = 0;

	float y = ( ind *_widthLetter ) - _position;

	if ( x > y + ( _widthLetter / 2 ) ) return ind + 1;
	else                                return ind;
 };
void  fLine::SetUndo ( bool timer ) {
	if ( _undoIndex < _undoTexts.Size ) {
		for ( int i = _undoTexts.Size; i >= _undoIndex; i-- ) {
			_undoTexts-= i;
			_undoStart-= i;
			_undoFinal-= i;
		}
	}

	if ( !_undoDelay ) {
		_undoIndex++;
		_undoDelay = true;
		_undoTexts+= _text;
		_undoStart+= _startPos;
		_undoFinal+= _finalPos;
	}

	SetTimeout( "undo", 1, timer_undo );
 };

void fLine::ClickEdit( float x, TShiftState shift ) {
	_rcursor->Visible = true;
	_startPos         = GetAjust( x );

	if ( _finalPos != snan && _rselect->Visible && shift.Contains( ssShift ) )
		_finalPos = GetAjust( _startPos < _finalPos ? _rselect->PosX + _rselect->Width : _rselect->PosX );

	else _finalPos = snan;

	Render( true );

	playCursor;
 };
void fLine::ViewPopUp( float x, float y           ) {
	switch (
		ShowPopUp( x, y, 5,
			1, L"Copiar",
			2, L"Cortar",
			3, L"Pegar",
			snan, L"",
			4, L"Seleccionar todo"
		)
	) {
		case 1: KeyCopy (); break;
		case 2: KeyCut  (); break;
		case 3: KeyPaste(); break;
		case 4: KeyAll  (); break;
	}
 };
//###########################################################################
