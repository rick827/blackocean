#pragma hdrstop
#include "wRegistry.h"
#pragma package(smart_init)

//class PACKAGE wRegistry //#################################################
/*Creacion - public*/
wRegistry::wRegistry(                                                                         ) {
	wRegistry_start;
 };
wRegistry::wRegistry( HKEY hkey, String key,                     bool create, bool viewErrors ) {
	wRegistry_start;

	HKey       = hkey      ;
	Key        = key       ;
	ViewErrors = viewErrors;

	OpenKey( create );
 };
wRegistry::wRegistry( HKEY hkey, String key, unsigned long pass, bool create, bool viewErrors ) {
	wRegistry_start;

	HKey       = hkey      ;
	Key        = key       ;
	Pass       = pass      ;
	ViewErrors = viewErrors;

	OpenKey( create );
 };
//###########################################################################


/*Metodos - public*/
bool wRegistry::OpenKey  (                        bool create ) {
	long ret;

	if ( !create ) ret = RegOpenKeyEx  ( HKey, Key.w_str(), 0, Pass, &HKey                                            );
	else           ret = RegCreateKeyEx( HKey, Key.w_str(), 0, null, REG_OPTION_NON_VOLATILE, Pass, null, &HKey, null );

	if ( ret == /*ERROR_SUCCESS*/0L ) return true;
	else {
		if ( ViewErrors )
			MessageBox( GetHandle(), LastError().w_str(), L"Error", MB_ICONERROR );

		return false;
	}
 };
bool wRegistry::OpenKey  (            String key, bool create ) {
	Key = key;

	return OpenKey( create );
 };
bool wRegistry::OpenKey  ( HKEY hkey, String key, bool create ) {
	HKey = hkey;
	Key  = key ;

	return OpenKey( create );
 };
void wRegistry::CloseKey (                                    ) {
	RegCloseKey( HKey );

	HKey = null;
 };
bool wRegistry::RenameKey(            String key, String name ) {
	if ( key != "" ) {
		if ( RegRenameKey( HKey, key.w_str(), name.w_str() ) == ERROR_SUCCESS )
			return true;

		if ( ViewErrors )
			MessageBox( GetHandle(), LastError().w_str(), L"", MB_ICONERROR );

		return false;
	}
	else if ( ViewErrors )
		MessageBox( GetHandle(), L"La clave esta vacia", L"Error", MB_ICONERROR );

	return false;
 };
bool wRegistry::DeleteKey(            String key              ) {
	if ( key != "" ) {
		if ( RegDeleteKeyEx( HKey, key.w_str(), Pass, 0 ) == ERROR_SUCCESS )
			return true;

		if ( ViewErrors )
			MessageBox( GetHandle(), LastError().w_str(), L"", MB_ICONERROR );

		return false;
	}
	else if ( ViewErrors )
		MessageBox( GetHandle(), L"La clave esta vacia", L"Error", MB_ICONERROR );

	return false;
 };
bool wRegistry::DeleteKey( HKEY hkey, String key              ) {
	HKey = hkey;
	Key  = key ;

	return DeleteKey( key );
 };
bool wRegistry::DeleteVal(            String key              ) {
	if ( key != "" ) {
		if ( RegDeleteValue( HKey, key.w_str() ) == ERROR_SUCCESS )
			return true;

		if ( ViewErrors )
			MessageBox( GetHandle(), LastError().w_str(), L"", MB_ICONERROR );

		return false;
	}
	else if ( ViewErrors )
		MessageBox( GetHandle(), L"La clave esta vacia", L"Error", MB_ICONERROR );

	return false;
 };
bool wRegistry::DeleteVal( HKEY hkey, String key              ) {
	HKey = hkey;
	Key  = key ;

	return DeleteVal( key );
 };

int    wRegistry::Int( String name ) {
	BYTE  ret[4];
	DWORD siz = 4;

	ZeroMemory( &ret, siz );

	if ( RegGetValue( HKey, null, name.w_str(), REG_DWORD, null, ret, &siz ) > 2 ) {
		return
			( ret[3] << 24 )|
			( ret[2] << 16 )|
			( ret[1] << 8  )|
			( ret[0]       )
		;
	}

	return 0;
 };
String wRegistry::Str( String name ) {
	DWORD siz = 0;

	if ( RegGetValue( HKey, null, name.w_str(), REG_SZ, null, null, &siz ) > 2 ) {
		BYTE   *val = new BYTE[siz];
		String  ret = "";

		if ( RegGetValue( HKey, null, name.w_str(), REG_SZ, null, val, &siz ) > 2 )
			ret = ( wchar_t* )val;

		delete []val;

		return ret;
	}

	return "";
 };
double wRegistry::Dou( String name ) {
	BYTE  buf[8];
	DWORD siz = 8;

	if ( RegGetValue( HKey, null, name.w_str(), REG_QWORD, null, buf, &siz ) > 2 ) {
		__int64 ret = 0;

		ret = ( ret << 8 ) + buf[0];
		ret = ( ret << 8 ) + buf[1];
		ret = ( ret << 8 ) + buf[2];
		ret = ( ret << 8 ) + buf[3];
		ret = ( ret << 8 ) + buf[4];
		ret = ( ret << 8 ) + buf[5];
		ret = ( ret << 8 ) + buf[6];
		ret = ( ret << 8 ) + buf[7];

		return *( double* )&ret;
	}

	return 0;
 };

bool wRegistry::Int( String name, int    value ) {
	BYTE buf[ 4 ] = {
		BYTE(   value         & 0xFF ),
		BYTE( ( value >> 8  ) & 0xFF ),
		BYTE( ( value >> 16 ) & 0xFF ),
		BYTE( ( value >> 24 ) & 0xFF )
	 };

	if ( RegSetValueEx( HKey, name.w_str(), 0, REG_DWORD, buf, 4 ) == ERROR_SUCCESS )
		return true;

	if ( ViewErrors )
		MessageBox( GetHandle(), LastError().w_str(), L"Error", MB_ICONERROR );

	return false;
 };
bool wRegistry::Str( String name, String value ) {
	int   siz = value.Length();
	BYTE *buf = (BYTE*)value.w_str();

	if ( RegSetValueEx( HKey, name.w_str(), 0, REG_SZ, buf, siz == 0 ? 0 : ( siz + 1 ) * 2 ) > 2 )
		return true;

	if ( ViewErrors )
		MessageBox( GetHandle(), LastError().w_str(), L"Error", MB_ICONERROR );

	return false;
 };
bool wRegistry::Dou( String name, double value ) {
	__int64 abu = *(__int64*)&value;

	BYTE buf[ 8 ] = {
		BYTE( ( abu >> 56 ) & 0xFF ),
		BYTE( ( abu >> 48 ) & 0xFF ),
		BYTE( ( abu >> 40 ) & 0xFF ),
		BYTE( ( abu >> 32 ) & 0xFF ),
		BYTE( ( abu >> 24 ) & 0xFF ),
		BYTE( ( abu >> 16 ) & 0xFF ),
		BYTE( ( abu >> 8  ) & 0xFF ),
		BYTE(   abu         & 0xFF )
	 };

	if ( RegSetValueEx( HKey, name.w_str(), 0, REG_QWORD, buf, 8 ) == ERROR_SUCCESS )
		return true;

	if ( ViewErrors )
		MessageBox( GetHandle(), LastError().w_str(), L"Error", MB_ICONERROR );

	return false;
 };

int    wRegistry::DefInt( String name, int    def ) {
	BYTE  ret[4];
	DWORD siz = 4;

	ZeroMemory( &ret, siz );

	if ( RegGetValue( HKey, null, name.w_str(), REG_DWORD, null, ret, &siz ) > 2 ) {
		return
			( ret[3] << 24 )|
			( ret[2] << 16 )|
			( ret[1] << 8  )|
			( ret[0]       )
		;
	}

	return def;
 };
String wRegistry::DefStr( String name, String def ) {
	DWORD siz = 0;

	if ( RegGetValue( HKey, null, name.w_str(), REG_SZ, null, null, &siz ) > 2 ) {
		BYTE *val = new BYTE[siz];

		if ( RegGetValue( HKey, null, name.w_str(), REG_SZ, null, val, &siz ) > 2 )
			def = (wchar_t*)val;

		delete []val;
	}

	return def;
 };
double wRegistry::DefDou( String name, double def ) {
	BYTE  buf[8];
	DWORD siz = 8;

	if ( RegGetValue( HKey, null, name.w_str(), REG_QWORD, null, buf, &siz ) > 2 ) {
		__int64 ret = 0;

		ret = ( ret << 8 ) + buf[0];
		ret = ( ret << 8 ) + buf[1];
		ret = ( ret << 8 ) + buf[2];
		ret = ( ret << 8 ) + buf[3];
		ret = ( ret << 8 ) + buf[4];
		ret = ( ret << 8 ) + buf[5];
		ret = ( ret << 8 ) + buf[6];
		ret = ( ret << 8 ) + buf[7];

		return *(double*)&ret;
	}

	return def;
 };
//###########################################################################
