#pragma hdrstop
#include "eFile.h"
#pragma package(smart_init)

// class PACKAGE eFile //##################################################
/*Metodos - public*/
void eFile::Load( String file, bool json                                    ) {
	TMemoryStream *mem = new TMemoryStream;

	mem->LoadFromFile( file );

	Load( mem, json );

	delete mem;
 };
void eFile::Load( HINSTANCE instance, String file, String module, bool json ) {
	TResourceStream *mem = new TResourceStream( (int)instance, file, module.w_str() );

	Load( mem, json );

	delete mem;
 };

/*Metodos - private*/
void eFile::Load( TStream *stream, bool json ) {
	stream->Position = 0;

	_file = "";
	_file.SetLength( stream->Size );

	stream->Read( _file.c_str(), stream->Size );

	if ( json ) {
		int         pos = 0;
		int         len = _file.Length();
		char       *cad = _file.c_str();
		UTF8String  res = "";

		for ( ;pos<len; ) {
			if       ( cad[pos]=='"' ) { res+= "\\\""; pos+= 1; }
			else if ( cad[pos]=='\\' ) { res+= "\\\\"; pos+= 1; }
			else if ( cad[pos]=='/' && pos+1<len && cad[pos+1]=='/' ) {
				for ( pos+=2; pos<len && cad[pos]!='\n'; pos++);
			}

			if ( cad[pos]=='\n' || cad[pos]=='\r' || cad[pos]=='\t' ) pos++;
			else res+= cad[pos++];
		}

		_file = res;
	}
 };
//###########################################################################