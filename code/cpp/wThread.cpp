#pragma hdrstop
#include "wThread.h"
#pragma package(smart_init)


// Otros //##################################################################
/*Funciones*/
DWORD CALLBACK wThreadOnThreadRun( LPVOID lpParameter                                   ) {
	wThread *hil = (wThread*)lpParameter;

	if ( hil ) {
		try {
			if ( hil->onThread ) hil->onThread( hil );
		}
		catch ( ... ) {
			hil->Error = "(" + hil->HID + ")";
		}
	}

	return 0;
 };
VOID  CALLBACK wThreadOnTimerRun ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) {
	wThread *hil = (wThread*)idEvent;

	DWORD exc;
	BOOL  res = GetExitCodeThread( hil->HThread, &exc );

	if ( res && exc != STILL_ACTIVE )
		hil->Stop();
 };
VOID  CALLBACK wThreadOnTimerSyn ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) {
	KillTimer( hwnd, idEvent );

	wThread *hil = (wThread*)( idEvent - 1 );

	if ( hil->onSync ) hil->onSync( hil );

	ReleaseSemaphore( hil->HMutex, 1, 0 );
 };
//###########################################################################


// class PACKAGE wThread //##################################################
/*Creacion - public*/
wThread::wThread(                                     ) {
	wThread_start;
 };
wThread::wThread( sfThread onthread                   ) {
	wThread_start;

	onThread = onthread;

	Play();
 };
wThread::wThread( sfThread onthread, sfThread onfinal ) {
	wThread_start;

	onThread = onthread;
	onFinal  = onfinal ;

	Play();
 };
//###########################################################################


/*Metodos - public*/
void wThread::Play(                 ) {
	if ( HThread ) return;

	Cancel  = false;
	HThread = CreateThread( null, 0, wThreadOnThreadRun, this, 0, null );
	HTimer  = SetTimer( _handle, (UINT)this, 50, wThreadOnTimerRun );
 };
void wThread::Paus( bool pause      ) {
	if ( !HThread ) return;

	if ( pause ) {
		if ( !_hpause ) {
			_hpause = true;
			HMutex  = CreateSemaphoreW( 0, 1, 1, 0 );

			if ( HMutex != null ) {
				WaitForSingleObject( HMutex, INFINITE );
				WaitForSingleObject( HMutex, INFINITE );
			}
        }
	}
	else {
		if ( _hpause ) {
			_hpause = false;

			ReleaseSemaphore( HMutex, 1, 0 );
        }
	}
 };
void wThread::Stop(                 ) {
	if ( onFinal )
		onFinal( this );

	KillTimer( _handle, (UINT)this     );
	KillTimer( _handle, (UINT)this + 1 );
	CloseHandle( HThread );

	delete this;
 };
void wThread::Sync( sfThread onsync ) {
	onSync = onsync;

	if ( HThread ) {
		HMutex = CreateSemaphoreW( 0, 1, 1, 0 );

		if ( HMutex != null ) {
			WaitForSingleObject( HMutex, INFINITE );

			if ( SetTimer( _handle, (UINT)this + 1, 0, wThreadOnTimerSyn ) == 0 ) {
				MessageBox( _handle, LastError().w_str(), L"Error en hilo", MB_ICONERROR );
				ReleaseSemaphore( HMutex, 1, 0 );
				CloseHandle( HMutex );

				HMutex = null;
			}

			WaitForSingleObject( HMutex, INFINITE );
		}
		else MessageBox( _handle, LastError().w_str(), L"Error en hilo", MB_ICONERROR );
	}
	else if ( _handle == null && onSync ) onSync( this );
 };
//###########################################################################