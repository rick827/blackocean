#ifndef eBaseH
#define eBaseH

#include <classes.hpp>
#include <dateutils.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.VCLUI.Wait.hpp>
#include <FireDAC.Phys.MSSQL.hpp>
#include <FireDAC.Phys.MSSQLDef.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>

#include "sDef.h"
#include "eGeneral.h"

class PACKAGE eBase : public TObject {
	/*Variables*/private:
	TFDGUIxWaitCursor *_wait;
	TFDConnection     *_conection;

	/*Variables - eventos*/public:
	TNotifyEvent onConnect;
	sfNotify     onError;
	TNotifyEvent onDisconnect;
	//###########################################################################


	/*Creacion*/public:
	__fastcall  eBase();
	__fastcall ~eBase();
	//###########################################################################


	/*Metodos*/public:
	void Connect( bool connect );

	TFDQuery *NewQuery();
	//###########################################################################
 };

extern "C++" __declspec(dllexport)eBase *__stdcall sfeBase();

#endif
