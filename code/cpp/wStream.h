#ifndef wStreamH
#define wStreamH

#include <classes.hpp>
#include "wGeneral.h"

class PACKAGE wStream {
	/*Definiciones*/
	#define wStream_start \
		_size    = 0;                     \
		_buffer  = (BYTE*)GetMemory( 0 ); \
		ROT      = true;                  \
		Position = 0;
	//###########################################################################


	/*Variables*/private:
	unsigned long  _size  ;
	BYTE          *_buffer;

	/*Variables*/public:
	bool     ROT     ;
	unsigned Position;
	//###########################################################################


	/*Creacion*/public:
	__fastcall  wStream(             );
	__fastcall  wStream( String file );
	__fastcall ~wStream(             );
	//###########################################################################


	/*Propiedades*/public:
	__property unsigned long  Size   = { read = _size   };
	__property BYTE          *Buffer = { read = _buffer };
	//###########################################################################


	/*Metodos*/public:
	void __fastcall I16    ( __int16       value );
	void __fastcall I64    ( __int64       value );
	void __fastcall Int    ( int           value );
	void __fastcall Flo    ( float         value );
	void __fastcall Dou    ( double        value );
	void __fastcall Str    ( String        value );
	void __fastcall Sun    ( UnicodeString value );
	void __fastcall OnlyStr( String        value );

	__int16       __fastcall I16    (                        );
	__int64       __fastcall I64    (                        );
	int           __fastcall Int    (                        );
	float         __fastcall Flo    (                        );
	double        __fastcall Dou    (                        );
	String        __fastcall Str    (                        );
	UnicodeString __fastcall Sun    (                        );
	String        __fastcall OnlyStr( unsigned long size = 0 );

	void __fastcall Save ( String file                         );
	void __fastcall Load ( String file                         );
	void __fastcall Copy ( wStream *stream, unsigned long size );
	void __fastcall Clear(                                     );
	//###########################################################################
 };

#endif
