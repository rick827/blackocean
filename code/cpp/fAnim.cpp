#pragma hdrstop
#include "fAnim.h"
#pragma package(smart_init)


//Otros //###################################################################
/*Variables*/
static TList__1<String > *fAnimIDS = new TList__1<String >;
static TList__1<__int64> *fAnimPOI = new TList__1<__int64>;

/*Callback's*/
VOID CALLBACK wfAnimOnFrameHeight ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameHeight (); };
VOID CALLBACK wfAnimOnFrameWidth  ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameWidth  (); };
VOID CALLBACK wfAnimOnFrameTop    ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameTop    (); };
VOID CALLBACK wfAnimOnFrameLeft   ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameLeft   (); };
VOID CALLBACK wfAnimOnFrameMarginT( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameMarginT(); };
VOID CALLBACK wfAnimOnFrameOpacity( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameOpacity(); };
VOID CALLBACK wfAnimOnFrameFontC  ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameFontC  (); };
VOID CALLBACK wfAnimOnFrameFillC  ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameFillC  (); };
VOID CALLBACK wfAnimOnFrameStrokeC( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameStrokeC(); };
VOID CALLBACK wfAnimOnFrameCustom ( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime ) { ((fAnim*)idEvent)->onFrameCustom (); };
//###########################################################################


// class PACKAGE fAnim //####################################################
/*Creacion - public*/
fAnim::fAnim() {};
//###########################################################################


/*Metodos - public*/
void fAnim::onFrameHeight () {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Height = this->_finalP;

		this->Final();
	}
	else this->parent->Height = this->_onFrame( this->Percent, this->_startP, this->_finalP - this->_startP, this->Max );
 };
void fAnim::onFrameWidth  () {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Width = this->_finalP;

		this->Final();
	}
	else this->parent->Width = this->_onFrame( this->Percent, this->_startP, this->_finalP - this->_startP, this->Max );
 };
void fAnim::onFrameTop    () {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Position->Y = this->_finalP;

		this->Final();
	}
	else this->parent->Position->Y = this->_onFrame( this->Percent, this->_startP, this->_finalP - this->_startP, this->Max );
 };
void fAnim::onFrameLeft   () {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Position->X = this->_finalP;

		this->Final();
	}
	else this->parent->Position->X = this->_onFrame( this->Percent, this->_startP, this->_finalP - this->_startP, this->Max );
 };
void fAnim::onFrameMarginT() {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Margins->Top = this->_finalP;

		this->Final();
	}
	else this->parent->Margins->Top = this->_onFrame( this->Percent, this->_startP, this->_finalP - this->_startP, this->Max );
 };
void fAnim::onFrameOpacity() {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Opacity = this->_finalP;

		this->Final();
	}
	else this->parent->Opacity = this->_onFrame( this->Percent, this->_startP, this->_finalP - this->_startP, this->Max );
 };
void fAnim::onFrameFontC  () {
	this->Percent = ( Time() - this->Date ).Val;

    TText *tex = (TText*)this->parent;

	if ( this->Percent >= this->Max ) {
		tex->Color = (unsigned)LABaRGB( this->_finalL, this->_finalA, this->_finalB, this->_finalH );

		this->Final();
	}
	else {
		double l, a, b, h;

		l = this->_onFrame( this->Percent, this->_startL, this->_finalL - this->_startL, this->Max );
		a = this->_onFrame( this->Percent, this->_startA, this->_finalA - this->_startA, this->Max );
		b = this->_onFrame( this->Percent, this->_startB, this->_finalB - this->_startB, this->Max );
		h = this->_onFrame( this->Percent, this->_startH, this->_finalH - this->_startH, this->Max );

		tex->Color = (unsigned)LABaRGB( l, a, b, h );
	}
 };
void fAnim::onFrameFillC  () {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Fill->Color = (unsigned)LABaRGB( this->_finalL, this->_finalA, this->_finalB, this->_finalH );

		this->Final();
	}
	else {
		double l, a, b, h;

		l = this->_onFrame( this->Percent, this->_startL, this->_finalL - this->_startL, this->Max );
		a = this->_onFrame( this->Percent, this->_startA, this->_finalA - this->_startA, this->Max );
		b = this->_onFrame( this->Percent, this->_startB, this->_finalB - this->_startB, this->Max );
		h = this->_onFrame( this->Percent, this->_startH, this->_finalH - this->_startH, this->Max );

		this->parent->Fill->Color = (unsigned)LABaRGB( l, a, b, h );
	}
 };
void fAnim::onFrameStrokeC() {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->Percent >= this->Max ) {
		this->parent->Stroke->Color = (unsigned)LABaRGB( this->_finalL, this->_finalA, this->_finalB, this->_finalH );

		this->Final();
	}
	else {
		double l, a, b, h;

		l = this->_onFrame( this->Percent, this->_startL, this->_finalL - this->_startL, this->Max );
		a = this->_onFrame( this->Percent, this->_startA, this->_finalA - this->_startA, this->Max );
		b = this->_onFrame( this->Percent, this->_startB, this->_finalB - this->_startB, this->Max );
		h = this->_onFrame( this->Percent, this->_startH, this->_finalH - this->_startH, this->Max );

		this->parent->Stroke->Color = (unsigned)LABaRGB( l, a, b, h );
	}
 };
void fAnim::onFrameCustom () {
	this->Percent = ( Time() - this->Date ).Val;

	if ( this->_onCustom )
		this->_onCustom( this );
 };

void fAnim::Final( bool exefinal ) {
	KillTimer( this->_handle, (unsigned)this );

	for ( int i = 0; i < fAnimPOI->Count; i++ ) {
		if ( fAnimPOI->Items[i] == (__int64)this ) {
			fAnimIDS->Delete( i );
			fAnimPOI->Delete( i );
			break;
		}
    }

	if ( exefinal && this->_onFinal )
		this->_onFinal( this );

	delete this;
 };

void fAnim::Height ( TShape *parent, String id, float duration, double   final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	fAnim *ani; Configure;

	ani->_startP = parent->Height;
	ani->_finalP = final         ;
	ani->Date    = Time()        ;

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameHeight );
 };
void fAnim::Width  ( TShape *parent, String id, float duration, double   final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	fAnim *ani; Configure;

	ani->_startP = parent->Width;
	ani->_finalP = final        ;
	ani->Date    = Time()       ;

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameWidth );
 };
void fAnim::Top    ( TShape *parent, String id, float duration, double   final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	fAnim *ani; Configure;

	ani->_startP = parent->Position->Y;
	ani->_finalP = final              ;
	ani->Date    = Time()             ;

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameTop );
 };
void fAnim::Left   ( TShape *parent, String id, float duration, double   final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	fAnim *ani; Configure;

	ani->_startP = parent->Position->X;
	ani->_finalP = final              ;
	ani->Date    = Time()             ;

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameLeft );
 };
void fAnim::MarginT( TShape *parent, String id, float duration, double   final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	fAnim *ani; Configure;

	ani->_startP = parent->Position->X;
	ani->_finalP = final              ;
	ani->Date    = Time()             ;

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameMarginT );
 };
void fAnim::Opacity( TShape *parent, String id, float duration, double   final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	fAnim *ani; Configure;

	ani->_startP = parent->Opacity;
	ani->_finalP = final          ;
	ani->Date    = Time()         ;

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameOpacity );
 };
void fAnim::FontC  ( TText  *parent, String id, float duration, unsigned final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	double l, a, b, h;

	fAnim *ani; Configure;

	RGBaLAB( parent->Color, l, a, b, h );
	ani->_startL = l;
	ani->_startA = a;
	ani->_startB = b;
	ani->_startH = h;

	RGBaLAB( final, l, a, b, h );
	ani->_finalL = l;
	ani->_finalA = a;
	ani->_finalB = b;
	ani->_finalH = h;

	ani->Date = Time();

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameFontC );
 };
void fAnim::FillC  ( TShape *parent, String id, float duration, unsigned final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	double l, a, b, h;

	fAnim *ani; Configure;

	RGBaLAB( parent->Fill->Color, l, a, b, h );
	ani->_startL = l;
	ani->_startA = a;
	ani->_startB = b;
	ani->_startH = h;

	RGBaLAB( final, l, a, b, h );
	ani->_finalL = l;
	ani->_finalA = a;
	ani->_finalB = b;
	ani->_finalH = h;

	ani->Date = Time();

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameFillC );
 };
void fAnim::StrokeC( TShape *parent, String id, float duration, unsigned final, sfWave onframe, sfAnim onfinal ) {
	if ( !parent ) return;

	double l, a, b, h;

	fAnim *ani; Configure;

	RGBaLAB( parent->Stroke->Color, l, a, b, h );
	ani->_startL = l;
	ani->_startA = a;
	ani->_startB = b;
	ani->_startH = h;

	RGBaLAB( final, l, a, b, h );
	ani->_finalL = l;
	ani->_finalA = a;
	ani->_finalB = b;
	ani->_finalH = h;

	ani->Date = Time();

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameStrokeC );
 };
void fAnim::Custom (                 String id, float duration                , sfAnim onframe, sfAnim onfinal ) {
	if ( id != "" ) {
		for ( int i = 0; i < fAnimIDS->Count; i++ ) {
			if ( fAnimIDS->Items[i] == id )
				((fAnim*)fAnimPOI->Items[i])->Final( false );
		}
	}

	fAnim *
	ani            = new fAnim                         ;
	ani->_id       = id                                ;
	ani->_handle   = GetHandle()                       ;
	ani->_onCustom = onframe                           ;
	ani->_onFinal  = onfinal                           ;
	ani->Max       = duration * TTime( 0, 0, 1, 0 ).Val;

	fAnimIDS->Add( id           );
	fAnimPOI->Add( (__int64)ani );

	ani->Date = Time();

	SetTimer( ani->_handle, (unsigned)ani, 0, wfAnimOnFrameCustom );
 };
void fAnim::Stop   ( String id                                                                                 ) {
	for ( int i = fAnimPOI->Count - 1; i >= 0; i-- ) {
		if ( id == "" || fAnimIDS->Items[i] == id )
			((fAnim*)fAnimPOI->Items[i])->Final( false );
    }
 };
//###########################################################################
