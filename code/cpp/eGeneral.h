#ifndef eGeneralH
#define eGeneralH

#include <dateutils.hpp>

#include "sJson.h"

/*Respuestas*/
extern "C++" __declspec(dllexport)String _stdcall Response404();
extern "C++" __declspec(dllexport)String _stdcall Response409();
extern "C++" __declspec(dllexport)String _stdcall Response200();

extern "C++" __declspec(dllexport)String _stdcall DateToHttp( TDateTime date      );
extern "C++" __declspec(dllexport)String _stdcall DecodeURL ( String all          );
extern "C++" __declspec(dllexport)sJson  _stdcall ParseHttp ( UTF8String petition );
//###########################################################################


/*Socket*/
extern "C++" __declspec(dllexport)SOCKET     _stdcall CreateServer( AnsiString host, int port            );
extern "C++" __declspec(dllexport)SOCKET     _stdcall CreateClient( AnsiString host, int port            );
extern "C++" __declspec(dllexport)String     _stdcall GetHash     ( String key, bool charSpecial = true  );
extern "C++" __declspec(dllexport)void       _stdcall EncodeSock  ( String all, char **cat, __int64 &let );
extern "C++" __declspec(dllexport)UTF8String _stdcall DecodeSock  ( byte *petition                       );
//###########################################################################


/*Otros*/
extern "C++" __declspec(dllexport)void       _stdcall Notify   ( String title, String message, seMessageType type );
extern "C++" __declspec(dllexport)UTF8String _stdcall LoadEFile( String file, bool json                           );
extern "C++" __declspec(dllexport)UTF8String _stdcall LoadEFile( String file, String module, bool json            );
//###########################################################################

#endif
