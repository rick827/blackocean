#pragma hdrstop
#include "sGeneral.h"
#pragma package(smart_init)

/*Funciones - OTROS*/
String __stdcall ByteAStr( double bytes ) {
	int    i;
	String siz[6] = { " B", " KB", " MB", " GB", " TB", " EB" };

	for ( i = 0; i < 6 && bytes > 999; i++ )
		bytes = bytes / 1024;

	return FormatFloat( ",0.00", bytes ) + siz[i];
 };

/*Funciones - COLORES*/
int __stdcall RGBaSAF( int color                    ) {
	return RGBaSAF( RRGB( color ), GRGB( color ), BRGB( color ) );
 };
int __stdcall RGBaSAF( int red, int green, int blue ) {
	int
	temporary = red   % 51; if ( temporary > 25 ) { temporary = red   + 51 - temporary; } else { temporary = red   - temporary; } red   = temporary;
	temporary = green % 51; if ( temporary > 25 ) { temporary = green + 51 - temporary; } else { temporary = green - temporary; } green = temporary;
	temporary = blue  % 51; if ( temporary > 25 ) { temporary = blue  + 51 - temporary; } else { temporary = blue  - temporary; } blue  = temporary;

	return RCOL( red, green, blue );
 };

//HEX
String __stdcall RGBaHEX( int color                               ) {
	return
		IntToHex( RRGB( color ), 2 ) +
		IntToHex( GRGB( color ), 2 ) +
		IntToHex( BRGB( color ), 2 )
	;
 };
String __stdcall RGBaHEX( int red, int green, int blue            ) {
	return
		IntToHex( red  , 2 ) +
		IntToHex( green, 2 ) +
		IntToHex( blue , 2 )
	;
 };
String __stdcall RGBaHEX( int red, int green, int blue, int alpha ) {
	return
		IntToHex( alpha, 2 ) +
		IntToHex( red  , 2 ) +
		IntToHex( green, 2 ) +
		IntToHex( blue , 2 )
	;
 };
String __stdcall BGRaHEX( int color                               ) {
	return
		IntToHex( BRGB( color ), 2 ) +
		IntToHex( GRGB( color ), 2 ) +
		IntToHex( RRGB( color ), 2 )
	;
 };
String __stdcall BGRaHEX( int red, int green, int blue            ) {
	return
		IntToHex( blue , 2 ) +
		IntToHex( green, 2 ) +
		IntToHex( red  , 2 )
	;
 };
String __stdcall BGRaHEX( int red, int green, int blue, int alpha ) {
	return
		IntToHex( alpha, 2 ) +
		IntToHex( blue , 2 ) +
		IntToHex( green, 2 ) +
		IntToHex( red  , 2 )
	;
 };

//HSL
double __stdcall HUEaRGB( double  pos, double  qua, double  tem                                                          ) {
	if ( tem < 0.0       ) tem += 1.0;
	if ( tem > 1.0       ) tem -= 1.0;
	if ( tem < 1.0 / 6.0 ) return pos + ( qua - pos ) * 6.0 * tem;
	if ( tem < 1.0 / 2.0 ) return qua;
	if ( tem < 2.0 / 3.0 ) return pos + ( qua - pos ) * ( 2.0 / 3.0 - tem ) * 6.0;

	return pos;
 };
void   __stdcall RGBaHSL( double  red, double  green, double  blue, double &Hue, double &Sat, double &Lum                ) {
	red  /= 255.0;
	green/= 255.0;
	blue /= 255.0;

	double max = red > green ? ( red > blue ? red : blue ) : ( green > blue ? green : blue );
	double min = red < green ? ( red < blue ? red : blue ) : ( green < blue ? green : blue );

	Lum = ( max + min ) / 2.0;

	if ( max == min )
		Hue = Sat = 0.0;

	else {
		double dec = max - min;

		Sat = Lum > 0.5 ? dec / ( 2.0 - max - min ) : dec / ( max + min );

		if      ( max == red   ) Hue = ( green - blue  ) / dec + ( green < blue ? 6.0 : 0.0 );
		else if ( max == green ) Hue = ( blue  - red   ) / dec + 2.0;
		else if ( max == blue  ) Hue = ( red   - green ) / dec + 4.0;

		Hue/= 6.0;
	}
 };
void   __stdcall RGBaHSL( int     red,                              double &Hue, double &Sat, double &Lum, double &Alpha ) {
	RGBaHSL( double( RRGB( red ) ), double( GRGB( red ) ), double( BRGB( red ) ), Hue, Sat, Lum );

	Alpha = double( ARGB( red ) ) / 255.0;
 };
void   __stdcall RGBaHSL( int     red,                              double &Hue, double &Sat, double &Lum                ) {
	RGBaHSL( double( RRGB( red ) ), double( GRGB( red ) ), double( BRGB( red ) ), Hue, Sat, Lum );
 };
void   __stdcall HSLaRGB( double &Red, double &Green, double &Blue, double  hue, double  sat, double  lum                ) {
	if ( sat == 0 )
		Red = Green = Blue = lum;

	else {
		double qua = lum < 0.5 ? lum * ( 1.0 + sat ) : lum + sat - lum * sat;
		double pos = 2.0 * lum - qua;

		Red   = HUEaRGB( pos, qua, hue + 1.0 / 3.0 );
		Green = HUEaRGB( pos, qua, hue             );
		Blue  = HUEaRGB( pos, qua, hue - 1.0 / 3.0 );
	}

	Red   = sround( Red   * 255.0 );
	Green = sround( Green * 255.0 );
	Blue  = sround( Blue  * 255.0 );
 };
int    __stdcall HSLaRGB(                                           double  hue, double  sat, double  lum, double  alpha ) {
	double red, green, blue;

	HSLaRGB( red, green, blue, hue, sat, lum );

	return
		( ( ( int )sround( alpha * 255.0 ) & 0xFF ) << 24 ) +
		( ( ( int )red                     & 0xFF ) << 16 ) +
		( ( ( int )green                   & 0xFF ) << 8  ) +
		(   ( int )blue                    & 0xFF         )
	;
 };

//HSV
void __stdcall RGBaHSV( double  red, double  green, double  blue, double &Hue, double &Sat, double &Val                ) {
	red  /= 255.0;
	green/= 255.0;
	blue /= 255.0;

	double minimum = red < green ? ( red < blue ? red : blue ) : ( green < blue ? green : blue );
	double maximum = red > green ? ( red > blue ? red : blue ) : ( green > blue ? green : blue );
	double subtrac = maximum - minimum;

	Val = maximum;

	if ( subtrac == 0 ) {
		Hue = 0;
		Sat = 0;
	}
	else {
		Sat = subtrac / maximum;

		double degRed   = ( ( ( maximum - red   ) / 6.0 ) + ( subtrac / 2.0 ) ) / subtrac;
		double degGreen = ( ( ( maximum - green ) / 6.0 ) + ( subtrac / 2.0 ) ) / subtrac;
		double degBlue  = ( ( ( maximum - blue  ) / 6.0 ) + ( subtrac / 2.0 ) ) / subtrac;

		if      ( red   == maximum ) Hue =                 degBlue  - degGreen;
		else if ( green == maximum ) Hue = ( 1.0 / 3.0 ) + degRed   - degBlue ;
		else if ( blue  == maximum ) Hue = ( 2.0 / 3.0 ) + degGreen - degRed  ;

		if ( Hue < 0 ) Hue += 1;
		if ( Hue > 1 ) Hue -= 1;
	}
 };
void __stdcall RGBaHSV( int     red,                              double &Hue, double &Sat, double &Val, double &Alpha ) {
	RGBaHSV( (double)RRGB( red ), (double)GRGB( red ), (double)BRGB( red ), Hue, Sat, Val );

	Alpha = double(ARGB( red )) / 255.0;
 };
void __stdcall RGBaHSV( int     red,                              double &Hue, double &Sat, double &Val                ) {
	RGBaHSV( (double)RRGB( red ), (double)GRGB( red ), (double)BRGB( red ), Hue, Sat, Val );
 };
void __stdcall HSVaRGB( double &Red, double &Green, double &Blue, double  hue, double  sat, double  val                ) {
	if ( sat == 0 ) {
		Red   = val * 255.0;
		Green = val * 255.0;
		Blue  = val * 255.0;
	}
	else {
		double thue   = hue * 6.0; if ( thue == 6.0 ) thue = 0.0;
		double value0 = (double)(int)thue;
		double value1 = val * ( 1.0 - sat                               );
		double value2 = val * ( 1.0 - sat *         ( thue - value0 )   );
		double value3 = val * ( 1.0 - sat * ( 1.f - ( thue - value0 ) ) );

		if      ( value0 == 0.0 ) { Red = val   ; Green = value3; Blue = value1; }
		else if ( value0 == 1.0 ) { Red = value2; Green = val   ; Blue = value1; }
		else if ( value0 == 2.0 ) { Red = value1; Green = val   ; Blue = value3; }
		else if ( value0 == 3.0 ) { Red = value1; Green = value2; Blue = val   ; }
		else if ( value0 == 4.0 ) { Red = value3; Green = value1; Blue = val   ; }
		else                      { Red = val   ; Green = value1; Blue = value2; }

		Red   = sround( Red   * 255.0 );
		Green = sround( Green * 255.0 );
		Blue  = sround( Blue  * 255.0 );
	}
 };
int  __stdcall HSVaRGB(                                           double  hue, double  sat, double  val, double  alpha ) {
	double red, green, blue;

	HSVaRGB( red, green, blue, hue, sat, val );

	return ACOL( sround( alpha * 255.0 ), red, green, blue );
 };

//XYZ
void __stdcall RGBaXYZ( double  red, double  green, double  blue, double &X, double &Y, double &Z                ) {
	red  /= 255.0;
	green/= 255.0;
	blue /= 255.0;

	if ( red   > 0.04045 ) red   = pow( ( ( red   + 0.055 ) / 1.055 ), 2.4 ); else red   = red   / 12.92;
	if ( green > 0.04045 ) green = pow( ( ( green + 0.055 ) / 1.055 ), 2.4 ); else green = green / 12.92;
	if ( blue  > 0.04045 ) blue  = pow( ( ( blue  + 0.055 ) / 1.055 ), 2.4 ); else blue  = blue  / 12.92;

	red  *= 100.0;
	green*= 100.0;
	blue *= 100.0;

	X = red * 0.4124 + green * 0.3576 + blue * 0.1805;
	Y = red * 0.2126 + green * 0.7152 + blue * 0.0722;
	Z = red * 0.0193 + green * 0.1192 + blue * 0.9505;
 };
void __stdcall RGBaXYZ( int color,                                double &X, double &Y, double &Z, double &Alpha ) {
	RGBaXYZ( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), X, Y, Z );

	Alpha = double(ARGB( color )) / 255.0;
 };
void __stdcall RGBaXYZ( int color,                                double &X, double &Y, double &Z                ) {
	RGBaXYZ( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), X, Y, Z );
 };
void __stdcall XYZaRGB( double &Red, double &Green, double &Blue, double  x, double  y, double  z                ) {
	x/= 100.0;
	y/= 100.0;
	z/= 100.0;

	Red   = x *  3.2406 + y * -1.5372 + z * -0.4986;
	Green = x * -0.9689 + y *  1.8758 + z *  0.0415;
	Blue  = x *  0.0557 + y * -0.2040 + z *  1.0570;

	if ( Red   > 0.0031308 ) Red   = 1.055 * pow( Red  , ( 1.0 / 2.4 ) ) - 0.055; else Red   = 12.92 * Red  ;
	if ( Green > 0.0031308 ) Green = 1.055 * pow( Green, ( 1.0 / 2.4 ) ) - 0.055; else Green = 12.92 * Green;
	if ( Blue  > 0.0031308 ) Blue  = 1.055 * pow( Blue , ( 1.0 / 2.4 ) ) - 0.055; else Blue  = 12.92 * Blue ;

	Red   = sround( Red   * 255.0 );
	Green = sround( Green * 255.0 );
	Blue  = sround( Blue  * 255.0 );
 };
int  __stdcall XYZaRGB(                                           double  x, double  y, double  z, double  alpha ) {
	double red, green, blue;

	XYZaRGB( red, green, blue, x, y, z );

	return ACOL( sround( alpha * 255.0 ), red, green, blue );
 };

//LAB
void __stdcall LABaXYZ( double &X, double &Y, double &Z,          double  lum, double  red, double  blue                ) {
	double y =     ( lum  + 16.0  ) / 116.0;
	double x =     ( red  / 500.0 ) + y    ;
	double z = y - ( blue / 200.0 )        ;

	if ( pow( y, 3.0 ) > 0.008856 ) y = pow( y, 3.0 ); else y = ( y - 16.0 / 116.0 ) / 7.787;
	if ( pow( x, 3.0 ) > 0.008856 ) x = pow( x, 3.0 ); else x = ( x - 16.0 / 116.0 ) / 7.787;
	if ( pow( z, 3.0 ) > 0.008856 ) z = pow( z, 3.0 ); else z = ( z - 16.0 / 116.0 ) / 7.787;

	X = x * 95.047 ;
	Y = y * 100.0  ;
	Z = z * 108.883;
 };
void __stdcall RGBaLAB( double red, double green, double blue,    double &Lum, double &Red, double &Blue                ) {
	double x, y, z;

	RGBaXYZ( red, green, blue, x, y, z );

	double tx = x / 95.047 ;
	double ty = y / 100.0  ;
	double tz = z / 108.883;

	if ( tx > 0.008856 ) tx = pow( tx, ( 1.0 / 3.0 ) ); else tx = ( 7.787 * tx ) + ( 16.0 / 116.0 );
	if ( ty > 0.008856 ) ty = pow( ty, ( 1.0 / 3.0 ) ); else ty = ( 7.787 * ty ) + ( 16.0 / 116.0 );
	if ( tz > 0.008856 ) tz = pow( tz, ( 1.0 / 3.0 ) ); else tz = ( 7.787 * tz ) + ( 16.0 / 116.0 );

	Lum  = ( 116.0 * ty ) - 16.0;
	Red  = 500.0 * ( tx - ty );
	Blue = 200.0 * ( ty - tz );
 };
void __stdcall RGBaLAB( int color,                                double &Lum, double &Red, double &Blue, double &Alpha ) {
	RGBaLAB( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), Lum, Red, Blue );

	Alpha = double(ARGB( color )) / 255.0;
 };
void __stdcall RGBaLAB( int color,                                double &Lum, double &Red, double &Blue                ) {
	RGBaLAB( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), Lum, Red, Blue );
 };
void __stdcall LABaRGB( double &Red, double &Green, double &Blue, double  lum, double  red, double  blue                ) {
	double x, y, z;

	LABaXYZ( x, y, z, lum, red, blue );
	XYZaRGB( Red, Green, Blue, x, y, z );
 };
int  __stdcall LABaRGB(                                           double  lum, double  red, double  blue                ) {
	double x, y, z;

	LABaXYZ( x, y, z, lum, red, blue );

	return XYZaRGB( x, y, z, 255.0 );
 };
int  __stdcall LABaRGB(                                           double  lum, double  red, double  blue, double  alpha ) {
	double x, y, z;

	LABaXYZ( x, y, z, lum, red, blue );

	return XYZaRGB( x, y, z, alpha );
 };

//CMY
void __stdcall RGBaCMY( double red, double green, double blue,    double &Cyan, double &Magenta, double &Yellow                ) {
	Cyan    = 1.0 - ( red   / 255.0 );
	Magenta = 1.0 - ( green / 255.0 );
	Yellow  = 1.0 - ( blue  / 255.0 );
 };
void __stdcall RGBaCMY( int color,                                double &Cyan, double &Magenta, double &Yellow, double &Alpha ) {
	RGBaCMY( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), Cyan, Magenta, Yellow );

	Alpha = double(ARGB( color )) / 255.0;
 };
void __stdcall RGBaCMY( int color,                                double &Cyan, double &Magenta, double &Yellow                ) {
	RGBaCMY( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), Cyan, Magenta, Yellow );
 };
void __stdcall CMYaRGB( double &Red, double &Green, double &Blue, double  cyan, double  magenta, double  yellow                ) {
	Red   = ( 1.0 - cyan    ) * 255.0;
	Green = ( 1.0 - magenta ) * 255.0;
	Blue  = ( 1.0 - yellow  ) * 255.0;
 };
int  __stdcall CMYaRGB(                                           double  cyan, double  magenta, double  yellow, double  alpha ) {
	double red, green, blue;

	CMYaRGB( red, green, blue, cyan, magenta, yellow );

	return ACOL(
		sround( alpha * 255.0 ),
		sround( red   * 255.0 ),
		sround( green * 255.0 ),
		sround( blue  * 255.0 )
	);
 };

//CMYK
void __stdcall RGBaCMYK( double red, double  green, double  blue,  double &Cyan, double &Magenta, double &Yellow, double &Key                ) {
	Cyan    = 1.0 - ( red   / 255.0 );
	Magenta = 1.0 - ( green / 255.0 );
	Yellow  = 1.0 - ( blue  / 255.0 );
	Key     = 1.0;

	if ( Cyan    <  Key ) Key = Cyan   ;
	if ( Magenta <  Key ) Key = Magenta;
	if ( Yellow  <  Key ) Key = Yellow ;
	if ( Key     == 1.0 ) {
		Cyan    = 0.0;
		Magenta = 0.0;
		Yellow  = 0.0;
	}
	else {
		Cyan    = ( Cyan    - Key ) / ( 1.0 - Key );
		Magenta = ( Magenta - Key ) / ( 1.0 - Key );
		Yellow  = ( Yellow  - Key ) / ( 1.0 - Key );
	}
 };
void __stdcall RGBaCMYK( int color,                                double &Cyan, double &Magenta, double &Yellow, double &Key, double &Alpha ) {
	RGBaCMYK( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), Cyan, Magenta, Yellow, Key );

	Alpha = double(ARGB( color )) / 255.0;
 };
void __stdcall RGBaCMYK( int color,                                double &Cyan, double &Magenta, double &Yellow, double &Key                ) {
	RGBaCMYK( (double)RRGB( color ), (double)GRGB( color ), (double)BRGB( color ), Cyan, Magenta, Yellow, Key );
 };
void __stdcall CMYKaRGB( double &Red, double &Green, double &Blue, double  cyan, double  magenta, double  yellow, double  key                ) {
	Red   = ( 1.0 - ( cyan    * ( 1.0 - key ) + key ) ) * 255.0;
	Green = ( 1.0 - ( magenta * ( 1.0 - key ) + key ) ) * 255.0;
	Blue  = ( 1.0 - ( yellow  * ( 1.0 - key ) + key ) ) * 255.0;
 };
int  __stdcall CMYKaRGB(                                           double  cyan, double  magenta, double  yellow, double  key, double  alpha ) {
	double red, green, blue;

	CMYKaRGB( red, green, blue, cyan, magenta, yellow, key );

	return ACOL(
		sround( alpha * 255.0 ),
		sround( red   * 255.0 ),
		sround( green * 255.0 ),
		sround( blue  * 255.0 )
	);
 };
//###########################################################################


/*Funciones - Encriptación*/
#define rdc(x,n  ) ((x&0xFFFFFFFF)>>n)
#define rot(x,n  ) (rdc(x,n)|(x<<(32-n)))
#define fn0(x    ) (rot(x,7)^rot(x,18)^rdc(x,3))
#define fn1(x    ) (rot(x,17)^rot(x,19)^rdc(x,10))
#define sn0(x    ) (rot(x,2)^rot(x,13)^rot(x,22))
#define sn1(x    ) (rot(x,6)^rot(x,11)^rot(x,25))
#define chh(x,y,z) ((x&y)^(~x&z))
#define maj(x,y,z) ((x&y)^(x&z)^(y&z))

__int32 MD5tai[16] = {0};
__int32 MD5blo[16] = {0};
__int32 MD5sa1[4 ] = {128, 32768, 8388608, -2147483648};
__int32 MD5sa2[4 ] = {0, 8, 16, 24};
__int32 MD5sta[4 ] = {0};
__int32 MD5buf[4 ] = {0};
bool    MD5scr = false;
int     MD5lec = 0;
int     MD5len = 0;

String  fenc( String cad ) {
	String utf = "";
	String enc = "";
	int    str = 0 ;
	int    end = 0 ;
	int    j   = cad.Length();

	for( int i = 0; i < j; i++ ) {
		int c = cad.w_str()[i];

		if      ( c < 128  ) { end++; continue; }
		else if ( c < 2048 ) { enc = (char)(__int32)( ( c >> 6  ) | 192, (   c & 63 ) | 128 ); }
		else                 { enc = (char)(__int32)( ( c >> 12 ) | 224, ( ( c >> 6 ) & 63  ) | 128, ( c & 63 ) | 128 ); }

		if ( end > str ) {
			for ( int i = str; i < end; i++ ) {
				utf+= cad[i];
			}
		}

		utf+= enc;
		str = end = i + 1;
	}

	if ( end > str ) {
		for ( int i = str; i < j; i++ ) {
			utf+= cad.w_str()[i];
		}
	}

	return utf;
 };
void    fgbl( String cad ) {
	int I;

	for ( int i = 16; i--; ) {
		I      = i << 2;
		MD5blo[i] = cad.w_str()[I] + ( cad.w_str()[I+1] << 8 ) + ( cad.w_str()[I+2] << 16 ) + ( cad.w_str()[I+3] << 24 );
	}
 };
__int32 frou( __int32 q, __int32 a, __int32 b, __int32 x, __int32 s1, __int32 s2, __int32 t ) {
	return ( ( ( a+= q + x + t ) << s1 | unsigned((unsigned)a >> (unsigned)s2) ) + b ) << 0;
 };
void    fros( __int32 a, __int32 b, __int32 c, __int32 d, __int32 *k ) {
	__int32 bc, da;

	if ( MD5scr ) {
		a = frou( ( ( c ^ d ) & b ) ^ d, a, b, k[0], 7 , 25, -680876936  );
		d = frou( ( ( b ^ c ) & a ) ^ c, d, a, k[1], 12, 20, -389564586  );
		c = frou( ( ( a ^ b ) & d ) ^ b, c, d, k[2], 17, 15,  606105819  );
		b = frou( ( ( d ^ a ) & c ) ^ a, b, c, k[3], 22, 10, -1044525330 );
	}
	else {
		a = k[0] - 680876937;
		a = ( ( a << 7 | unsigned((unsigned)a >> 25) ) - 271733879 ) << 0;
		d = k[1] - 117830708 + ( ( 2004318071 & a ) ^ -1732584194 );
		d = ( ( d << 12 | unsigned((unsigned)d >> 20) ) + a ) << 0;
		c = k[2] - 1126478375 + ( ( ( a ^ -271733879 ) & d ) ^ -271733879 );
		c = ( ( c << 17 | unsigned((unsigned)c >> 15) ) + d ) << 0;
		b = k[3] - 1316259209 + ( ( ( d ^ a ) & c ) ^ a );
		b = ( ( b << 22 | unsigned((unsigned)b >> 10) ) + c ) << 0;
	}

	a = frou( ( ( c ^ d ) & b ) ^ d, a, b, k[4 ], 7 , 25, -176418897  );
	d = frou( ( ( b ^ c ) & a ) ^ c, d, a, k[5 ], 12, 20,  1200080426 );
	c = frou( ( ( a ^ b ) & d ) ^ b, c, d, k[6 ], 17, 15, -1473231341 );
	b = frou( ( ( d ^ a ) & c ) ^ a, b, c, k[7 ], 22, 10, -45705983   );
	a = frou( ( ( c ^ d ) & b ) ^ d, a, b, k[8 ], 7 , 25,  1770035416 );
	d = frou( ( ( b ^ c ) & a ) ^ c, d, a, k[9 ], 12, 20, -1958414417 );
	c = frou( ( ( a ^ b ) & d ) ^ b, c, d, k[10], 17, 15, -42063      );
	b = frou( ( ( d ^ a ) & c ) ^ a, b, c, k[11], 22, 10, -1990404162 );
	a = frou( ( ( c ^ d ) & b ) ^ d, a, b, k[12], 7 , 25,  1804603682 );
	d = frou( ( ( b ^ c ) & a ) ^ c, d, a, k[13], 12, 20, -40341101   );
	c = frou( ( ( a ^ b ) & d ) ^ b, c, d, k[14], 17, 15, -1502002290 );
	b = frou( ( ( d ^ a ) & c ) ^ a, b, c, k[15], 22, 10,  1236535329 );

	a = frou( ( ( b ^ c ) & d ) ^ c, a, b, k[1 ], 5 , 27, -165796510  );
	d = frou( ( ( a ^ b ) & c ) ^ b, d, a, k[6 ], 9 , 23, -1069501632 );
	c = frou( ( ( d ^ a ) & b ) ^ a, c, d, k[11], 14, 18,  643717713  );
	b = frou( ( ( c ^ d ) & a ) ^ d, b, c, k[0 ], 20, 12, -373897302  );
	a = frou( ( ( b ^ c ) & d ) ^ c, a, b, k[5 ], 5 , 27, -701558691  );
	d = frou( ( ( a ^ b ) & c ) ^ b, d, a, k[10], 9 , 23,  38016083   );
	c = frou( ( ( d ^ a ) & b ) ^ a, c, d, k[15], 14, 18, -660478335  );
	b = frou( ( ( c ^ d ) & a ) ^ d, b, c, k[4 ], 20, 12, -405537848  );
	a = frou( ( ( b ^ c ) & d ) ^ c, a, b, k[9 ], 5 , 27,  568446438  );
	d = frou( ( ( a ^ b ) & c ) ^ b, d, a, k[14], 9 , 23, -1019803690 );
	c = frou( ( ( d ^ a ) & b ) ^ a, c, d, k[3 ], 14, 18, -187363961  );
	b = frou( ( ( c ^ d ) & a ) ^ d, b, c, k[8 ], 20, 12,  1163531501 );
	a = frou( ( ( b ^ c ) & d ) ^ c, a, b, k[13], 5 , 27, -1444681467 );
	d = frou( ( ( a ^ b ) & c ) ^ b, d, a, k[2 ], 9 , 23, -51403784   );
	c = frou( ( ( d ^ a ) & b ) ^ a, c, d, k[7 ], 14, 18,  1735328473 );
	b = frou( ( ( c ^ d ) & a ) ^ d, b, c, k[12], 20, 12, -1926607734 );

	bc = b ^ c; a = frou( bc ^ d, a, b, k[5 ], 4 , 28, -378558     ); d = frou( bc ^ a, d, a, k[8 ], 11, 21, -2022574463 );
	da = d ^ a; c = frou( da ^ b, c, d, k[11], 16, 16,  1839030562 ); b = frou( da ^ c, b, c, k[14], 23, 9 , -35309556   );
	bc = b ^ c; a = frou( bc ^ d, a, b, k[1 ], 4 , 28, -1530992060 ); d = frou( bc ^ a, d, a, k[4 ], 11, 21,  1272893353 );
	da = d ^ a; c = frou( da ^ b, c, d, k[7 ], 16, 16, -155497632  ); b = frou( da ^ c, b, c, k[10], 23, 9 , -1094730640 );
	bc = b ^ c; a = frou( bc ^ d, a, b, k[13], 4 , 28,  681279174  ); d = frou( bc ^ a, d, a, k[0 ], 11, 21, -358537222  );
	da = d ^ a; c = frou( da ^ b, c, d, k[3 ], 16, 16, -722521979  ); b = frou( da ^ c, b, c, k[6 ], 23, 9 ,  76029189   );
	bc = b ^ c; a = frou( bc ^ d, a, b, k[9 ], 4 , 28, -640364487  ); d = frou( bc ^ a, d, a, k[12], 11, 21, -421815835  );
	da = d ^ a; c = frou( da ^ b, c, d, k[15], 16, 16,  530742520  ); b = frou( da ^ c, b, c, k[2 ], 23, 9 , -995338651  );

	a = frou( c ^ ( b | ~d ), a, b, k[0 ], 6 , 26, -198630844  );
	d = frou( b ^ ( a | ~c ), d, a, k[7 ], 10, 22,  1126891415 );
	c = frou( a ^ ( d | ~b ), c, d, k[14], 15, 17, -1416354905 );
	b = frou( d ^ ( c | ~a ), b, c, k[5 ], 21, 11, -57434055   );
	a = frou( c ^ ( b | ~d ), a, b, k[12], 6 , 26,  1700485571 );
	d = frou( b ^ ( a | ~c ), d, a, k[3 ], 10, 22, -1894986606 );
	c = frou( a ^ ( d | ~b ), c, d, k[10], 15, 17, -1051523    );
	b = frou( d ^ ( c | ~a ), b, c, k[1 ], 21, 11, -2054922799 );
	a = frou( c ^ ( b | ~d ), a, b, k[8 ], 6 , 26,  1873313359 );
	d = frou( b ^ ( a | ~c ), d, a, k[15], 10, 22, -30611744   );
	c = frou( a ^ ( d | ~b ), c, d, k[6 ], 15, 17, -1560198380 );
	b = frou( d ^ ( c | ~a ), b, c, k[13], 21, 11,  1309151649 );
	a = frou( c ^ ( b | ~d ), a, b, k[4 ], 6 , 26, -145523070  );
	d = frou( b ^ ( a | ~c ), d, a, k[11], 10, 22, -1120210379 );
	c = frou( a ^ ( d | ~b ), c, d, k[2 ], 15, 17,  718787259  );
	b = frou( d ^ ( c | ~a ), b, c, k[9 ], 21, 11, -343485551  );

	MD5buf[0] = a;
	MD5buf[1] = b;
	MD5buf[2] = c;
	MD5buf[3] = d;
 };
void    fcir( __int32 *kal ) {
	fros( 0, 0, 0, 0, kal );

	MD5sta[0] = ( MD5buf[0] + 1732584193 ) << 0;
	MD5sta[1] = ( MD5buf[1] - 271733879  ) << 0;
	MD5sta[2] = ( MD5buf[2] - 1732584194 ) << 0;
	MD5sta[3] = ( MD5buf[3] + 271733878  ) << 0;
 };
void    fadd( __int32 *kal ) {
	fros( MD5sta[0], MD5sta[1], MD5sta[2], MD5sta[3], kal );

	MD5sta[0] = ( MD5buf[0] + MD5sta[0] ) << 0;
	MD5sta[1] = ( MD5buf[1] + MD5sta[1] ) << 0;
	MD5sta[2] = ( MD5buf[2] + MD5sta[2] ) << 0;
	MD5sta[3] = ( MD5buf[3] + MD5sta[3] ) << 0;
 };
void    fup5( String cad ) {
	int i, I;

	cad+= "";
	MD5scr = false;
	MD5lec = MD5len = cad.Length();

	if ( MD5len > 63 ) {
		fgbl( cad.SubString( 0, 64 ) );
		fcir( MD5blo );

		MD5scr = true;

		for ( i = 128; i <= MD5len; i+= 64 ) {
			fgbl( cad.SubString( (i - 64)+1, 64 ) );
			fadd( MD5blo );
		}

		cad    = cad.SubString( (i - 64)+1, cad.Length() );
		MD5len = cad.Length();
	}

	MD5tai[0 ] = MD5tai[1 ] = MD5tai[2 ] = MD5tai[3 ] =
	MD5tai[4 ] = MD5tai[5 ] = MD5tai[6 ] = MD5tai[7 ] =
	MD5tai[8 ] = MD5tai[9 ] = MD5tai[10] = MD5tai[11] =
	MD5tai[12] = MD5tai[13] = MD5tai[14] = MD5tai[15] = 0;

	for ( i = 0; i < MD5len; i++ ) {
		I = i & 3;

		if ( I == 0 ) MD5tai[i >> 2] = cad.w_str()[i];
		else          MD5tai[i >> 2]|= cad.w_str()[i] << MD5sa2[I];
	}

	MD5tai[i >> 2]|= MD5sa1[i & 3];

	if ( i > 55 ) {
		if ( MD5scr ) fadd( MD5tai );
		else {
			fcir( MD5tai );

			MD5scr = true;
		}

		__int32 kal[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, MD5lec << 3, 0};
		fadd( kal );
	}

	MD5tai[14] = MD5lec << 3;

	if ( MD5scr ) fadd( MD5tai );
	else          fcir( MD5tai );
 };

String __stdcall AHexStr( __int32 n ) {
	String  s = "";
	__int32 v;

	for ( int i = 7; i >= 0; i-- ) {
		v = rdc(n,(i*4)) & 0xF;
		s += IntToHex( v, 1 );
	}

	return s;
 };
String __stdcall SHA256 ( String cadena  ) {
	typedef __int32 aint;

	aint K[64] = {
		0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
		0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
		0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
		0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
		0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
		0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
		0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
		0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
	};

	aint H[8] = { 0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19 };

	UTF8String sut = cadena;
	aint       tam = sut.Length() + 1;
	float      l   = tam / 4.f + 2.f;
	int        N   = ceil( l / 16.f );
	aint     **M;

	sut+= (char)0x80;

	M = new aint*[N];

	for ( int i = 0; i < N; i++ ) {
		M[i] = new aint[16];

		for ( int j = 0; j < 16; j++ ) {
			uint8_t *tem = sut.c_str();

			aint c1 = i * 64 + j * 4    ; if ( c1 >= tam ) c1 = 0; else c1 = tem[c1] << 24;
			aint c2 = i * 64 + j * 4 + 1; if ( c2 >= tam ) c2 = 0; else c2 = tem[c2] << 16;
			aint c3 = i * 64 + j * 4 + 2; if ( c3 >= tam ) c3 = 0; else c3 = tem[c3] << 8 ;
			aint c4 = i * 64 + j * 4 + 3; if ( c4 >= tam ) c4 = 0; else c4 = tem[c4]      ;

			M[i][j] = c1 | c2 | c3 | c4;
		}
	}

	M[N - 1][14] = ( ( tam - 1 ) * 8 ) / pow( 2, 32 );
	M[N - 1][14] = floor( M[N - 1][14] );
	M[N - 1][15] = ( ( tam - 1 ) * 8 ) & 0xFFFFFFFF;

	aint W[ 64 ];
	aint a, b, c, d, e, f, g, h;

	for ( int i = 0; i < N; i++ ) {
		for ( int t = 0 ; t < 16; t++ ) W[t] = M[i][t];
		for ( int t = 16; t < 64; t++ ) W[t] = ( fn1(W[t-2]) + W[t - 7] + fn0(W[t-15]) + W[t - 16] ) & 0xFFFFFFFF;

		a = H[0];
		b = H[1];
		c = H[2];
		d = H[3];
		e = H[4];
		f = H[5];
		g = H[6];
		h = H[7];

		for ( int t = 0; t < 64; t++ ) {
			aint T1 = h + sn1( e ) + chh( e, f, g ) + K[t] + W[t];
			aint T2 =     sn0( a ) + maj( a, b, c );

			h = g;
			g = f;
			f = e;
			e = ( d + T1 ) & 0xFFFFFFFF;
			d = c;
			c = b;
			b = a;
			a = ( T1 + T2 ) & 0xFFFFFFFF;
		}

		H[0] = ( H[0] + a ) & 0xFFFFFFFF;
		H[1] = ( H[1] + b ) & 0xFFFFFFFF;
		H[2] = ( H[2] + c ) & 0xFFFFFFFF;
		H[3] = ( H[3] + d ) & 0xFFFFFFFF;
		H[4] = ( H[4] + e ) & 0xFFFFFFFF;
		H[5] = ( H[5] + f ) & 0xFFFFFFFF;
		H[6] = ( H[6] + g ) & 0xFFFFFFFF;
		H[7] = ( H[7] + h ) & 0xFFFFFFFF;
	}

	return
		  AHexStr( H[0] )
		+ AHexStr( H[1] )
		+ AHexStr( H[2] )
		+ AHexStr( H[3] )
		+ AHexStr( H[4] )
		+ AHexStr( H[5] )
		+ AHexStr( H[6] )
		+ AHexStr( H[7] )
	;
 };
String __stdcall MD5    ( String cadena ) {
	fup5( fenc( cadena ) );

	String  rec     = "";
	__int32 res[32] = {0};
	__int32 hex[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
	__int32
	tmp    = MD5sta[0];
	res[1] = hex[tmp & 15];

	res[0] = hex[( tmp >>= 4 ) & 15];
	res[3] = hex[( tmp >>= 4 ) & 15];
	res[2] = hex[( tmp >>= 4 ) & 15];
	res[5] = hex[( tmp >>= 4 ) & 15];
	res[4] = hex[( tmp >>= 4 ) & 15];
	res[7] = hex[( tmp >>= 4 ) & 15];
	res[6] = hex[( tmp >>= 4 ) & 15];

	tmp     = MD5sta[1];
	res[9 ] = hex[tmp & 15];
	res[8 ] = hex[( tmp >>= 4 ) & 15];
	res[11] = hex[( tmp >>= 4 ) & 15];
	res[10] = hex[( tmp >>= 4 ) & 15];
	res[13] = hex[( tmp >>= 4 ) & 15];
	res[12] = hex[( tmp >>= 4 ) & 15];
	res[15] = hex[( tmp >>= 4 ) & 15];
	res[14] = hex[( tmp >>= 4 ) & 15];

	tmp     = MD5sta[2];
	res[17] = hex[tmp & 15];
	res[16] = hex[( tmp >>= 4 ) & 15];
	res[19] = hex[( tmp >>= 4 ) & 15];
	res[18] = hex[( tmp >>= 4 ) & 15];
	res[21] = hex[( tmp >>= 4 ) & 15];
	res[20] = hex[( tmp >>= 4 ) & 15];
	res[23] = hex[( tmp >>= 4 ) & 15];
	res[22] = hex[( tmp >>= 4 ) & 15];

	tmp     = MD5sta[3];
	res[25] = hex[tmp & 15];
	res[24] = hex[( tmp >>= 4 ) & 15];
	res[27] = hex[( tmp >>= 4 ) & 15];
	res[26] = hex[( tmp >>= 4 ) & 15];
	res[29] = hex[( tmp >>= 4 ) & 15];
	res[28] = hex[( tmp >>= 4 ) & 15];
	res[31] = hex[( tmp >>= 4 ) & 15];
	res[30] = hex[( tmp >>= 4 ) & 15];

	for ( int i = 0; i < 32; i++ )
		rec+= (char)res[i];

	return rec;
 };
//###########################################################################


/*Funciones - Animacion*/
/*Definiciones*/
#define PI 3.14159265

/*Funciones*/
Variant __stdcall GetPropValueSub( TObject *object, String property, bool preferStrings ) {
	if ( object ) {
		try {
			int pos = property.Pos( "." );

			if ( pos == 0 ) return GetPropValue( object, property, preferStrings );
			else {
				String sub = property.SubString( 1, pos - 1 );

				if ( GetPropInfo( object, sub ) != null ) {
					TObject *osu = GetObjectProp( object, sub );

					if ( osu ) return GetPropValueSub( osu, property.SubString( pos + 1, property.Length() ), preferStrings );
				}
			}
		}
		catch ( ... ) {}
	}

	return NULL;
 };
void    __stdcall SetPropValueSub( TObject *object, String property, Variant &value     ) {
	if ( object ) {
		try {
			int pos = property.Pos( "." );

			if ( pos == 0 ) SetPropValue( object, property, value );
			else {
				String sub = property.SubString( 1, pos - 1 );

				if ( GetPropInfo( object, sub ) != NULL ) {
					TObject *osu = GetObjectProp( object, sub );

					if ( osu ) SetPropValueSub( osu, property.SubString( pos + 1, property.Length() ), value );
				}
			}
		}
		catch ( ... ) {}
	}
 };

double __stdcall sfBackIn          ( double progres, double start, double final_start, double max ) {
	return final_start * ( progres /= max ) * progres * ( 2.70158 * progres - 1.70158 ) + start;
 };
double __stdcall sfBackOut         ( double progres, double start, double final_start, double max ) {
	return final_start * ( ( progres = progres / max - 1.0 ) * progres * ( 2.70158 * progres + 1.70158 ) + 1.0 ) + start;
 };
double __stdcall sfBackInOut       ( double progres, double start, double final_start, double max ) {
	if ( ( progres /= max / 2.0 ) < 1 )
		return final_start / 2.0 * ( progres * progres * ( 3.5949095 * progres - 2.5949095 ) ) + start;

	return final_start / 2.0 * ( ( progres -= 2.0 ) * progres * ( 3.5949095 * progres + 2.5949095 ) + 2.0 ) + start;
 };
double __stdcall sfBounceOut       ( double progres, double start, double final_start, double max ) {
	if      ( ( progres /= max ) < ( 1.0 / 2.75 ) ) return final_start * ( 7.5625 * progres                         * progres            ) + start;
	else if ( progres          < ( 2.0 / 2.75 ) ) return final_start * ( 7.5625 * ( progres -= ( 1.5   / 2.75 ) ) * progres + 0.75     ) + start;
	else if ( progres          < ( 2.5 / 2.75 ) ) return final_start * ( 7.5625 * ( progres -= ( 2.25  / 2.75 ) ) * progres + 0.9375   ) + start;
	else                                    return final_start * ( 7.5625 * ( progres -= ( 2.625 / 2.75 ) ) * progres + 0.984375 ) + start;
 };
double __stdcall sfBounceIn        ( double progres, double start, double final_start, double max ) {
	return final_start - sfBounceOut( max - progres, 0, final_start, max ) + start;
 };
double __stdcall sfBounceInOut     ( double progres, double start, double final_start, double max ) {
	if ( progres < max / 2.0 )
		return sfBounceIn( progres * 2.0, 0, final_start, max ) * 0.5 + start;

	return sfBounceOut( progres * 2.0 - max, 0, final_start, max ) * 0.5 + final_start * 0.5 + start;
 };
double __stdcall sfCircularIn      ( double progres, double start, double final_start, double max ) {
	progres = 1.0 - ( progres /= max ) * progres;

	if ( progres < 0 ) progres = 0;

	return -final_start * ( sqrt( progres ) - 1.0 ) + start;
 };
double __stdcall sfCircularOut     ( double progres, double start, double final_start, double max ) {
	progres = 1.0 - ( progres = ( progres / max ) - 1.0 ) * progres;

	if ( progres < 0 ) progres = 0;

	return final_start * sqrt( progres ) + start;
 };
double __stdcall sfCircularInOut   ( double progres, double start, double final_start, double max ) {
	if ( ( progres /= max / 2.0 ) < 1.0 ) {
		progres = 1.0 - progres * progres;

		if ( progres < 0 ) progres = 0;

		return -final_start / 2.0 * ( sqrt( progres ) - 1.0 ) + start;
	}

	progres = 1.0 - progres * ( progres -= 2.0 );

	if ( progres < 0 ) progres = 0;

	return final_start / 2.0 * ( sqrt( progres ) + 1.0 ) + start;
 };
double __stdcall sfCubicIn         ( double progres, double start, double final_start, double max ) {
	return final_start * ( progres /= max ) * progres * progres + start;
 };
double __stdcall sfCubicOut        ( double progres, double start, double final_start, double max ) {
	return final_start * ( ( progres = progres / max - 1.0 ) * progres * progres + 1.0 ) + start;
 };
double __stdcall sfCubicInOut      ( double progres, double start, double final_start, double max ) {
	if ( ( progres /= max / 2.0 ) < 1 )
		return final_start / 2.0 * progres * progres * progres + start;

	return final_start / 2.0 * ( ( progres -= 2.0 ) * progres * progres + 2.0 ) + start;
 };
double __stdcall sfElasticIn       ( double progres, double start, double final_start, double max ) {
	if ( progres == 0            ) return start;
	if ( ( progres /= max ) == 1.0 ) return start+final_start;

	double p = max * 0.3;
	double a = final_start;
	double s = p / 4.0;
	double postFix = a * powf( 2.0, 10.0 * ( progres -= 1.0 ) );

	return -( postFix * sin( ( progres * max - s ) * ( 2.0 * PI ) / p ) ) + start;
 };
double __stdcall sfElasticOut      ( double progres, double start, double final_start, double max ) {
	if ( progres          == 0.0 ) return start;
	if ( ( progres /= max ) == 1.0 ) return start + final_start;

	double p = max * 0.3;
	double a = final_start;
	double s = p / 4.0;

	return ( a * powf( 2.0, -10.0 * progres ) * sin( ( progres * max - s ) * ( 2.0 * PI ) / p ) + final_start + start );
 };
double __stdcall sfElasticInOut    ( double progres, double start, double final_start, double max ) {
	if ( progres                == 0.0 ) return start;
	if ( ( progres /= max / 2.0 ) == 2.0 ) return start+final_start;

	double p = max * ( 0.3 * 1.5 );
	double a = final_start;
	double s = p / 4.0;

	if ( progres < 1.0 ) {
		double postFix = a * powf( 2.0, 10.0 * ( progres -= 1.0 ) );

		return -0.5 * ( postFix * sin( ( progres * max - s ) * ( 2.0 * PI ) / p ) ) + start;
	}

	double postFix = a * powf( 2.0, -10.0 * ( progres -= 1.0 ) );

	return postFix * sin( ( progres * max - s ) * ( 2.0 * PI ) / p ) * 0.5 + final_start + start;
 };
double __stdcall sfExponentialIn   ( double progres, double start, double final_start, double max ) {
	return ( progres == 0.0 ) ? start : final_start * powf( 2.0, 10.0 * ( progres / max - 1.0 ) ) + start;
 };
double __stdcall sfExponentialOut  ( double progres, double start, double final_start, double max ) {
	return ( progres == max ) ? start + final_start : final_start * ( -powf( 2.0, -10.0 * progres / max ) + 1.0 ) + start;
 };
double __stdcall sfExponentialInOut( double progres, double start, double final_start, double max ) {
	if ( progres                == 0.0 ) return start;
	if ( progres                == max   ) return start + final_start;
	if ( ( progres /= max / 2.0 ) <  1.0 ) return final_start / 2.0 * powf( 2.0, 10.0 * ( progres - 1.0 ) ) + start;

	return final_start / 2.0 * ( -powf( 2.0, -10.0 * --progres ) + 2.0 ) + start;
 };
double __stdcall sfLinear          ( double progres, double start, double final_start, double max ) {
	return final_start * progres / max + start;
 };
double __stdcall sfQuadraticIn     ( double progres, double start, double final_start, double max ) {
	return final_start * ( progres /= max ) * progres + start;
 };
double __stdcall sfQuadraticOut    ( double progres, double start, double final_start, double max ) {
	return -final_start * ( progres /= max ) * ( progres - 2.0 ) + start;
 };
double __stdcall sfQuadraticInOut  ( double progres, double start, double final_start, double max ) {
	if ( ( progres /= max / 2.0 ) < 1.0 )
		return final_start / 2.0 * progres * progres + start;

	return -final_start / 2.0 * ( ( --progres ) * ( progres - 2.0 ) - 1.0 ) + start;
 };
double __stdcall sfQuarticIn       ( double progres, double start, double final_start, double max ) {
	return final_start * ( progres /= max ) * progres * progres * progres + start;
 };
double __stdcall sfQuarticOut      ( double progres, double start, double final_start, double max ) {
	return -final_start * ( ( progres = progres / max - 1.0 ) * progres * progres * progres - 1.0 ) + start;
 };
double __stdcall sfQuarticInOut    ( double progres, double start, double final_start, double max ) {
	if ( ( progres /= max / 2.0 ) < 1.0 )
		return final_start / 2.0 * progres * progres * progres * progres + start;

	return -final_start / 2.0 * ( ( progres -= 2.0 ) * progres * progres * progres - 2.0 ) + start;
 };
double __stdcall sfQuinticIn       ( double progres, double start, double final_start, double max ) {
	return final_start * ( progres /= max ) * progres * progres * progres * progres + start;
 };
double __stdcall sfQuinticOut      ( double progres, double start, double final_start, double max ) {
	return final_start * ( ( progres = progres / max - 1.0 ) * progres * progres * progres * progres + 1.0 ) + start;
 };
double __stdcall sfQuinticInOut    ( double progres, double start, double final_start, double max ) {
	if ( ( progres /= max / 2.0 ) < 1.0 )
		return final_start / 2.0 * progres * progres * progres * progres * progres + start;

	return final_start / 2.0 * ( ( progres -= 2.0 ) * progres * progres * progres * progres + 2.0 ) + start;
 };
double __stdcall sfSinusoidalIn    ( double progres, double start, double final_start, double max ) {
	return -final_start * cos( progres / max * ( PI / 2.0 ) ) + final_start + start;
 };
double __stdcall sfSinusoidalOut   ( double progres, double start, double final_start, double max ) {
	return final_start * sin( progres / max * ( PI / 2.0 ) ) + start;
 };
double __stdcall sfSinusoidalInOut ( double progres, double start, double final_start, double max ) {
	return -final_start / 2.0 * ( cos( PI * progres / max ) - 1.0 ) + start;
 };
//###########################################################################


/*Otros*/
String __stdcall Cifrar   ( String Cadena ) {
	String OCadena[11] = { "", "’L§’", "Ä±VW", "§čĮö", "ü", "§üŻ", "ęö’", "’õ", "’±§", "Ā3Ņ§", "’Ä±" };

	int o = 0;
	while ( o == 0 ) o = Random( 10 );

	Cadena = StringReplace( Cadena, " ", "Äų ų", TReplaceFlags() << rfReplaceAll );

	Cadena = StringReplace( Cadena, "a", "ÅĶ¶ %¶", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "b", " ģ«ę ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "c", "Ē¶ļ §±", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "d", "’ ž’Ē", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "e", "Ö ü ’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "f", "źųĘ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "g", "ė ¶ļå", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "h", "Ā| Ā", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "i", "Ųō ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "j", " ŠŌć", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "k", "Ī³ā³ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "l", "’ @", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "m", "ėų Ćš", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "n", "ü¤ ’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "ń", "Ļ’’Ū $", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "o", "’’ę„ „„", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "p", "ų’ļ ’’’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "q", "¶ū’ ųÕ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "r", " ’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "s", "’ ü", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "t", "’ ’äė", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "u", " ųš ±§", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "v", "Ć’’ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "w", "ü  ü ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "x", "ż’ Ćże", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "y", "$ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "z", "’ ’’±", TReplaceFlags() << rfReplaceAll );

	Cadena = StringReplace( Cadena, "A", "Ć ųš", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "B", "ū’ ^Ćź", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "C", "’~ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "D", "ĒŠ« ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "E", " ¶ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "F", " ž Š", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "G", "¶ ž", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "H", "¶ ½", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "I", "« ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "J", "ĆŲ Ę", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "K", "ų ŠĪ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "L", "§ ²±°", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "M", "ū’’ ė", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "N", "ŠĪ  ’’ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "Ń", "ė ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "O", "Ū­ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "P", "$# Ē", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "Q", "Ä ¼Š", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "R", "’ ’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "S", "ūĘ ¶", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "T", "", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "U", "£ų’Ō", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "V", "±", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "W", "÷Ę±§", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "X", "Ā §", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "Y", "ų£Ć $’Ä", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "Z", "Š÷ ’öÄ", TReplaceFlags() << rfReplaceAll );

	Cadena = StringReplace( Cadena, "1", "~Ö Ē¼", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "2", "÷ ’^Ć", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "3", "÷#Ā ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "4", "ø Ź", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "5", "’ė š^Ć±", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "6", "ųĘ $§±", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "7", "Ā~ ¾Æ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "8", "’ ō", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "9", "¶’ Ę", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "0", " »¤", TReplaceFlags() << rfReplaceAll );

	Cadena = StringReplace( Cadena, "[", "Ųß Ū", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "{", "’ Ę", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "}", "^å Ć", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "]", "Ŗ’ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "-", "×© ’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "_", "÷’÷ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, ".", "|ž’’ŗ ’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, ":", "’ž ’’", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, ",", "’’’ ", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, ";", "¾³’ %", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "+", "ž ’ ’ü", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "*", " ¼ ¼Š", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "?", "ōų", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "æ", "¼Š § ų", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, "/", " ± ¾Æ", TReplaceFlags() << rfReplaceAll );

	Cadena = StringReplace( Cadena, "\\", "¼ ¼Š±", TReplaceFlags() << rfReplaceAll );
	Cadena = StringReplace( Cadena, " ", OCadena[ o ], TReplaceFlags() << rfReplaceAll );
	Cadena = Cadena.SubString( 1, 10 ) + String( o ) + Cadena.SubString( 11, Cadena.Length() );

	return Cadena;
 };
String __stdcall Descifrar( String Cadena ) {
	String reg = Cadena;
	String OCadena[11] = { "", "’L§’", "Ä±VW", "§čĮö", "ü", "§üŻ", "ęö’", "’õ", "’±§", "Ā3Ņ§", "’Ä±" };

	int o = sai( reg.SubString( 11, 1 ) );

	if ( o == 0 ) return "";

	reg = reg.SubString( 1, 10 ) + reg.SubString( 12, reg.Length() );
	reg = StringReplace( reg, OCadena[ o ], " ", TReplaceFlags() << rfReplaceAll );

	reg = StringReplace( reg, "Äų ų", " ", TReplaceFlags() << rfReplaceAll );

	reg = StringReplace( reg, "ÅĶ¶ %¶", "a", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ģ«ę ", "b", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ē¶ļ §±", "c", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ ž’Ē", "d", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ö ü ’", "e", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "źųĘ ", "f", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ė ¶ļå", "g", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ā| Ā", "h", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ųō ", "i", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ŠŌć", "j", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ī³ā³ ", "k", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ @", "l", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ėų Ćš", "m", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ü¤ ’", "n", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ļ’’Ū $", "ń", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’’ę„ „„", "o", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ų’ļ ’’’", "p", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "¶ū’ ųÕ", "q", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ’", "r", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ ü", "s", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ ’äė", "t", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ųš ±§", "u", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ć’’ ", "v", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ü  ü ", "w", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ż’ Ćże", "x", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "$ ", "y", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ ’’±", "z", TReplaceFlags() << rfReplaceAll );

	reg = StringReplace( reg, "Ć ųš", "A", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ū’ ^Ćź", "B", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’~ ", "C", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ĒŠ« ", "D", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ¶ ", "E", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ž Š", "F", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "¶ ž", "G", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "¶ ½", "H", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "« ", "I", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ĆŲ Ę", "J", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ų ŠĪ", "K", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "§ ²±°", "L", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ū’’ ė", "M", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ŠĪ  ’’ ", "N", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ė ", "Ń", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ū­ ", "O", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "$# Ē", "P", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ä ¼Š", "Q", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ ’", "R", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ūĘ ¶", "S", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "", "T", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "£ų’Ō", "U", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "±", "V", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "÷Ę±§", "W", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ā §", "X", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ų£Ć $’Ä", "Y", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Š÷ ’öÄ", "Z", TReplaceFlags() << rfReplaceAll );

	reg = StringReplace( reg, "~Ö Ē¼", "1", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "÷ ’^Ć", "2", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "÷#Ā ", "3", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ø Ź", "4", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ė š^Ć±", "5", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ųĘ $§±", "6", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ā~ ¾Æ", "7", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ ō", "8", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "¶’ Ę", "9", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " »¤", "0", TReplaceFlags() << rfReplaceAll );

	reg = StringReplace( reg, "Ųß Ū", "[", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ Ę", "{", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "^å Ć", "}", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "Ŗ’ ", "]", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "×© ’", "-", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "÷’÷ ", "_", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "|ž’’ŗ ’", ".", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’ž ’’", ":", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "’’’ ", ",", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "¾³’ %", ";", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ž ’ ’ü", "+", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ¼ ¼Š", "*", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "ōų", "?", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, "¼Š § ų", "æ", TReplaceFlags() << rfReplaceAll );
	reg = StringReplace( reg, " ± ¾Æ", "/", TReplaceFlags() << rfReplaceAll );

	reg = StringReplace( reg, "¼ ¼Š±", "\\", TReplaceFlags() << rfReplaceAll );

	return reg == Cadena ? ( String )"" : reg;
 };
//###########################################################################