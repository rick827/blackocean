#ifndef fGeneralH
#define fGeneralH
                          
#include <Shobjidl.h>
#include <FMX.Graphics.hpp>
#include <FMX.Platform.Win.hpp>

#include <sDef.h>

/*Imagen*/
extern "C++" __declspec( dllexport )TMemoryStream *__stdcall HBMaMEM( HBITMAP  bitmap                 );
extern "C++" __declspec( dllexport )HBITMAP        __stdcall BMPaHBM( TBitmap *bitmap                 );
extern "C++" __declspec( dllexport )SHDRAGIMAGE   *__stdcall BMPaDBM( TBitmap *bitmap, long x, long y );
//###########################################################################


/*General*/
extern "C++" __declspec( dllexport )HWND __stdcall GetApplicationHandle();
//###########################################################################

#endif

