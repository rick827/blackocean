#ifndef eOptionsH
#define eOptionsH

#include "sJson.h"
#include "wThread.h"

class PACKAGE eOptions : public TObject {
	/*Variables*/private:
	wThread *_hwatch;
	sJson    _options;
	int      _debug;
	//###########################################################################


	/*Creacion*/public:
	__fastcall eOptions();
	//###########################################################################


	/*Hilos*/private:
	void __fastcall thini_watch( wThread *hilo );
	//###########################################################################


	/*Operadores*/public:
	sJson operator [] ( int    index              );
	sJson operator [] ( String tag                );
	sJson operator () ( int    index, sJson value );
	//###########################################################################


	/*Propiedades*/private:
	bool RDebug();

	/*Propiedades*/public:
	__property bool Debug = { read=RDebug };
	//###########################################################################


	/*Metodos*/public:
	void Load();
	void Save();
	//###########################################################################
 };

extern "C++" __declspec(dllexport)eOptions *__stdcall sfeOptions();

#endif
