#pragma hdrstop
#include "eSection.h"
#pragma package(smart_init)


/*********************************( Otros )*********************************/
//###########################################################################
#include "eOptions.h"
#include "ePetition.h"


/*****************************( class eSection )*****************************/
/*Creacion - public*/
eSection::eSection() {
	this->Load();
 };
//###########################################################################


/*Hilos - private*/
void __fastcall eSection::thini_watch( wThread *hilo ) {
	String dir = GetAppDirectory() + "\\web";

	if ( !DirectoryExists( dir ) ) return;

	while ( 1 ) {
		WatchPath( dir );

		Sleep( 300 );

		this->Load();
	}
 };
//###########################################################################


/*Metodos - public*/
void eSection::Router( ePetition *_petition ) {
	try {
		if      ( pe.Func == "execjs" ) ExecJS( _petition );
		else if ( pe.Func == "getter" ) Getter( _petition );
		else if ( pe.Func == "insert" ) Insert( _petition );
		else if ( pe.Func == "update" ) Update( _petition );
		else if ( pe.Func == "delete" ) Delete( _petition );
		else sene( "Error en el modulo", "101", "No se encontr� la funci�n, la funci�n no existe o est� mal escrita." );
	}
	catch ( Exception &e ) {
		Notify( "Error en DesktopMV", e.Message + " (" + e.ClassName() + ")", mtError );
	}
 };
void eSection::ExecJS( ePetition *_petition ) { sene( "Error en seccion", "265", "Funcion no implementada." ); };
void eSection::Getter( ePetition *_petition ) { sene( "Error en seccion", "265", "Funcion no implementada." ); };
void eSection::Insert( ePetition *_petition ) { sene( "Error en seccion", "265", "Funcion no implementada." ); };
void eSection::Update( ePetition *_petition ) { sene( "Error en seccion", "265", "Funcion no implementada." ); };
void eSection::Delete( ePetition *_petition ) { sene( "Error en seccion", "265", "Funcion no implementada." ); };

void eSection::Load() {
	if ( gopc.Debug && !_hwatch ) {
		_hwatch = new wThread( &thini_watch );
	}
 };
//###########################################################################
