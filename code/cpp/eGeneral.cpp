#pragma hdrstop
#include "eGeneral.h"
#include "iostream.h"
#pragma package(smart_init)

// Otros ####################################################################
/*Definiciones*/
#define SpecGUID "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define Rotate32(x,y  ) (((x)<<(y))|((x)>>(32-(y))))
#define RotateRB(x    ) (((x)>>24)|(((x)>>8)&0xFF00)|(((x)<<8)&0xFF0000)|((x)<<24))
#define NumberF1(x,y,z) ((z)^((x)&((y)^(z))))
#define NumberF2(x,y,z) ((x)^(y)^(z))
#define NumberF3(x,y,z) (((x)&(y))|((z)&((x)|(y))))

/*Tipos*/
typedef StaticArray<Byte,20> TSHA1Digest;

/*Clases*/
struct TSHA1Context{public: int Hi,Lo,Index; StaticArray<unsigned,5>Hash; StaticArray<Byte,64>Buffer;};

/*Socket - Funciones*/
void   sha1Compress ( TSHA1Context &Data ) {
	DWORD A, B, C, D, E, T, W[80] = {0};

	Move( &Data.Buffer, W, sizeof(Data.Buffer) );

	for ( int i = 0; i <= 15; i++ )
		W[i] = RotateRB( W[i] );

	for ( int i = 16; i <= 79; i++ )
		W[i] = Rotate32( W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1 );

	A = Data.Hash[0];
	B = Data.Hash[1];
	C = Data.Hash[2];
	D = Data.Hash[3];
	E = Data.Hash[4];

	for ( int i = 0; i <= 19; i++ ) {
		T = Rotate32( A, 5 ) + NumberF1( B, C, D ) + E + W[i] + 0x5A827999;
		E = D;
		D = C;
		C = Rotate32( B, 30 );
		B = A;
		A = T;
	}

	for ( int i = 20; i <= 39; i++ ) {
		T = Rotate32( A, 5 ) + NumberF2( B, C, D ) + E + W[i] + 0x6ED9EBA1;
		E = D;
		D = C;
		C = Rotate32( B, 30 );
		B = A;
		A = T;
	}

	for ( int i = 40; i <= 59; i++ ) {
		T = Rotate32( A, 5 ) + NumberF3( B, C, D ) + E + W[i] + 0x8F1BBCDC;
		E = D;
		D = C;
		C = Rotate32( B, 30 );
		B = A;
		A = T;
	}

	for ( int i = 60; i <= 79; i++ ) {
		T = Rotate32( A, 5 ) + NumberF2( B, C, D ) + E + W[i] + 0xCA62C1D6;
		E = D;
		D = C;
		C = Rotate32( B, 30 );
		B = A;
		A = T;
	}

	Data.Hash[0]+= A;
	Data.Hash[1]+= B;
	Data.Hash[2]+= C;
	Data.Hash[3]+= D;
	Data.Hash[4]+= E;

	ZeroMemory( &W, sizeof(W) );
	ZeroMemory( &Data.Buffer, sizeof(Data.Buffer) );
 };
void   sha1Init     ( TSHA1Context &Context ) {
	Context.Hi    = 0;
	Context.Lo    = 0;
	Context.Index = 0;

	ZeroMemory( &Context.Buffer, sizeof(Context.Buffer) );

	Context.Hash[0] = 0x67452301;
	Context.Hash[1] = 0xEFCDAB89;
	Context.Hash[2] = 0x98BADCFE;
	Context.Hash[3] = 0x10325476;
	Context.Hash[4] = 0xC3D2E1F0;
 };
void   sha1UpdateLen( TSHA1Context &Context, int Len ) {
	for ( int k = 0, i; k <= 7; k++ ) {
		i          = Context.Lo;
		Context.Lo+= Len;

		if ( Context.Lo < i )
			Context.Hi++;
	}
 };
void   sha1Update   ( TSHA1Context &Context, wchar_t *Buffer, int Len ) {
	sha1UpdateLen( Context, Len );

	for ( int i = 0; Len > 0; ) {
		Context.Buffer[Context.Index] = Buffer[i++];
		Context.Index++;
		Len--;

		if ( Context.Index == 64 ) {
			Context.Index = 0;
			sha1Compress( Context );
		}
	}
 };
void   sha1Final    ( TSHA1Context &Context, TSHA1Digest &Digest ) {
	Context.Buffer[Context.Index] = 0x80;

	if ( Context.Index >= 56 )
		sha1Compress( Context );

	((DWORD&)Context.Buffer[56]) = RotateRB( Context.Hi );
	((DWORD&)Context.Buffer[60]) = RotateRB( Context.Lo );

	sha1Compress( Context );

	Context.Hash[0] = RotateRB( Context.Hash[0] );
	Context.Hash[1] = RotateRB( Context.Hash[1] );
	Context.Hash[2] = RotateRB( Context.Hash[2] );
	Context.Hash[3] = RotateRB( Context.Hash[3] );
	Context.Hash[4] = RotateRB( Context.Hash[4] );

	Move( Context.Hash, Digest, sizeof(Digest) );

	ZeroMemory( &Context, sizeof(Context) );
 };
String base64Encode ( String Input, bool charSpecial = true ) {
	String     Final         = "";
	UTF8String Base64Out[65] = {
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
		"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
		"+", "/", "="
	 };

	if ( !charSpecial ) {
		Base64Out[62] = "a";
		Base64Out[63] = "b";
		Base64Out[64] = "c";
    }

	for ( int Count = 1, Len = Input.Length(); Count <= Len; ) {
		Final+= Base64Out[((Byte)Input[Count] & 0xFC) >> 2];

		if ( Count + 1 <= Len ) {
			Final+= Base64Out[(((Byte)Input[Count] & 0x03) << 4) + (((Byte)Input[Count+1] & 0xF0) >> 4)];

			if ( Count + 2 <= Len ) {
				Final+= Base64Out[(((Byte)Input[Count + 1] & 0x0F) << 2) + (((Byte)Input[Count + 2] & 0xC0) >> 6)];
				Final+= Base64Out[(Byte)Input[Count + 2] & 0x3F];
			}
			else {
				Final+= Base64Out[((Byte)Input[Count + 1] & 0x0F) << 2];
				Final+= "=";
			}
		}
		else {
			Final+= Base64Out[((Byte)Input[Count] & 0x03) << 4];
			Final = "==";
		}

		Count+= 3;
	}

	return Final;
 };

void GetMetod   ( sJson jso, char *cad, int &pos, int tam ) {
	String te1 = "";

	for ( ;pos<tam && cad[pos]!=' '; pos++ )
		te1+= cad[pos];

	jso.Add( vlText, "metod", te1 );
 };
void GetPath    ( sJson jso, char *cad, int &pos, int tam ) {
	/*/mpublico/login/ls/%%20%7B%7D HTTP/1.1*/
	String rut = "";

	for ( pos++; pos<tam && cad[pos]!=' '; pos++ ) rut+= cad[pos];

	rut = DecodeURL( rut );

	jso.Add( vlText, "url", rut );

	/*/mpublico/login/ls/2+3\/5*/
	int      pot  = 0;
	int      let  = rut.Length();
	wchar_t *cat  = rut.w_str();
	String   te1  = "";
	String   path = "";

	for ( ;pot<let; ) {
		if ( cat[pot]=='/' ) {
			pot++;
			te1 = "";

			for ( ;pot<let; pot++ ) {
				if ( cat[pot] == '\\' && pot+1 < let && cat[pot+1] == '/' ) {
					te1 += "/";
					pot ++;
				}
				else if ( cat[pot] == '/' ) break;
				else te1 += cat[pot];
			}

			if ( path != "" ) path += ",";

			path+= co + te1 + co;
		}
	}

	jso.Add( vlArray, "path", "[" + path + "]" );
 };
void GetProtocol( sJson jso, char *cad, int &pos, int tam ) {
	String pro = "";

	for ( pos++; pos<tam && cad[pos]!='\r'; pos++ ) pro+= cad[pos];

	jso.Add( vlText, "protocol", pro );
 };
void GetData    ( sJson jso, char *cad, int &pos, int tam ) {
	String te1;
	String te2;

	for (; pos < tam; pos++ ) {
		if ( cad[pos] == '\n' ) {
			pos++;
			te1 = "";

			if ( cad[pos] == '\r' && pos+1 < tam && cad[pos+1] == '\n' ) {
				pos += 2;
				te2 = "";

				for (; pos < tam; pos++ ) te2 += cad[pos];
			}
			else {
				for (; pos < tam && cad[pos] != ':'; pos++ ) te1 += cad[pos];
				pos += 2;
				te2 = "";
				for (; pos < tam && cad[pos] != '\r'; pos++ ) te2 += cad[pos];
			}

			if ( te1 == "" ) te1 = "body";

			jso.Add( vlText, te1, te2 );
		}
	}

	if ( jso["Upgrade"].Str == "websocket" || jso["Sec-WebSocket-Key"].Type != vlTemporary )
		jso.Add( vlText, "protocol", "SOCKET" );
 };

/*Otros - Funciones*/
UTF8String Load( TStream *stream, bool json ) {
	stream->Position = 0;

	UTF8String res = "";

	res.SetLength( stream->Size );
	stream->Read( res.c_str(), stream->Size );

	if ( json ) {
		int         pos = 0;
		int         len = res.Length();
		char       *cad = res.c_str();
		UTF8String  reg = "";

		for ( ;pos<len; ) {
			if      ( cad[pos]=='"' ) { reg+= "\\\""; pos+= 1; }
			else if ( cad[pos]=='\\' ) { res+= "\\\\"; pos+= 1; }
			else if ( cad[pos]=='/' && pos+1<len && cad[pos+1]=='/' ) {
				for ( pos+=2; pos<len && cad[pos]!='\n'; pos++);
			}

			if ( cad[pos]=='\n' || cad[pos]=='\r' || cad[pos]=='\t' ) pos++;
			else reg+= cad[pos++];
		}

		res = reg;
	}

	return res;
 };
//###########################################################################


/*Http*/
String _stdcall Response404() {
	return
		"<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">"
		"<html>"
			"<head>"
				"<meta charset=\"utf-8\" />"
				"<title>404 No Encontrado</title>"
			"</head>"
			"<body>"
				"<h1>No Encontrado</h1>"
				"<p>El recurso solicitado no se ha encontrado o no existe.</p>"
				"<hr>"
				"<address>BlackCoral v0.4.115 Servidor de AND en el puerto 80</address>"
			"</body>"
		"</html>"
	;
 };
String _stdcall Response409() {
	return
		"<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">"
		"<html>"
			"<head>"
				"<meta charset=\"utf-8\" />"
				"<title>409 Conflicto</title>"
			"</head>"
			"<body>"
				"<h1>Conflicto</h1>"
				"<p>La solicitud no pudo ser procesada debido a un conflicto con el estado actual del recurso.</p>"
				"<hr>"
				"<address>BlackCoral v0.4.115 Servidor de AND en el puerto 80</address>"
			"</body>"
		"</html>"
	;
 };
String _stdcall Response200() {
	return
		"<!DOCTYPE html>"
		"<html lang=\"en\">"
		"<head>"
			"<meta charset=\"UTF-8\">"
			"<meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, user-scalable=0\"/>"
			"<link rel=\"shortcut icon\" href=\"./favicon.ico\"/>"
		"</head>"
		"</html>"
	;
 };

String _stdcall DateToHttp( TDateTime date      ) {
	String sse[ 7  ] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };
	String sme[ 12 ] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	unsigned short dia, sem, mes, ano, hor, min, seg, mil;

	sem = DayOfTheWeek( date );
	DecodeDateTime( date, ano, mes, dia, hor, min, seg, mil );

	return
		sse[ sem - 1 ] + ", " + FormatFloat( "00", dia ) + " " + sme[ mes - 1 ] + " " + ( String )ano + " " +
		FormatFloat( "00", hor ) + ":" + FormatFloat( "00", min ) + ":" + FormatFloat( "00", seg ) + " GTM"
	;
 };
String _stdcall DecodeURL ( String all          ) {
	wchar_t *cad = all.w_str();
	int      tam = all.Length();
	int      pos = 0;
	String   reg = "";

	for ( ;pos < tam; ) {
		if (
			cad[pos] == '%' &&
			pos+2 < tam &&
			( ( cad[pos+1] >= 48 && cad[pos+1] <= 57 ) || ( cad[pos+1] >= 65 && cad[pos+1] <= 70 ) || ( cad[pos+1] >= 97 && cad[pos+1] <= 102 ) ) &&
			( ( cad[pos+2] >= 48 && cad[pos+2] <= 57 ) || ( cad[pos+2] >= 65 && cad[pos+2] <= 70 ) || ( cad[pos+2] >= 97 && cad[pos+2] <= 102 ) )
		) {
			char te1 = cad[pos+1];
			char te2 = cad[pos+2];

			if ( te1 == 67 && te2 == 51 ) {
				pos += 3;

				te1 = cad[pos+1];
				te2 = cad[pos+2];

				te1 += 4;
			}

			//Minusculas
			if ( te1 < 97 && te1 >= 65 ) te1+= 32;
			if ( te2 < 97 && te2 >= 65 ) te2+= 32;

			//Letras
			if ( te1 >= 97 && te1 <= 102 ) te1-= 87;
			if ( te2 >= 97 && te2 <= 102 ) te2-= 87;

			//Numeros
			if ( te1 >= 48 && te1 <= 57 ) te1-= 48;
			if ( te2 >= 48 && te2 <= 57 ) te2-= 48;

			te1 = char( ( te1 << 4 ) | te2 );

			if ( te1 == '/' ) reg+= "\\/";
			else              reg+= String( te1 );

			pos+= 3;
		}
		else reg+= cad[pos++];
	}

	return reg;
 };
sJson  _stdcall ParseHttp ( UTF8String petition ) {
	int    pos = 0;
	int    tam = petition.Length();
	char  *cad = petition.c_str() ;
	sJson  jso;

	jso.Type = vlObject;

	GetMetod   ( jso, cad, pos, tam );
	GetPath    ( jso, cad, pos, tam );
	GetProtocol( jso, cad, pos, tam );
	GetData    ( jso, cad, pos, tam );

	return jso;
 };
//###########################################################################


/*Socket*/
SOCKET     _stdcall CreateServer( AnsiString host, int port            ) {
	/*Inicio*/
	if ( host == "" ) host = "127.0.0.1";
	if ( port <= 0  ) port = 80;

	//Declaraciones
	WSADATA             wsa;
	struct sockaddr_in  ads;
	struct hostent     *tho;

	//Inicializamos la DLL de Sockets
	if ( WSAStartup( MAKEWORD( 2, 2 ), &wsa ) ) {
		MessageBox( GetActiveWindow(), ( "La DLL de sockets no se inicio correctamente(" + LastError() + ")" ).w_str(), L"Error en el servidor", MB_ICONERROR );
		return null;
	}

	//IP que usar� nuestro servidor...
	tho = (struct hostent*)gethostbyname( host.c_str() );
	if( !tho ) {
		MessageBox( GetActiveWindow(), ( "No se ha encontrado servidor(" + LastError() + ")" ).w_str(), L"Error en el servidor", MB_ICONERROR );
		WSACleanup();
		return null;
	}

	//Creamos el socket...
	SOCKET sserver = socket( AF_INET, SOCK_STREAM, 0 );
	if ( sserver == INVALID_SOCKET ) {
		MessageBox( GetActiveWindow(), ( "Error al crear socket(" + LastError() + ")" ).w_str(), L"Error en el servidor", MB_ICONERROR );
		WSACleanup();
		return null;
	}

	//Asociamos puerto al socket
	memset( &ads         , 0          , sizeof(ads)   );
	memcpy( &ads.sin_addr, tho->h_addr, tho->h_length );

	ads.sin_port   = htons( port );
	ads.sin_family = tho->h_addrtype;

	if ( bind( sserver, (struct sockaddr*)&ads, sizeof(ads) ) == SOCKET_ERROR ) {
		MessageBox( GetActiveWindow(), ( "Error al asociar puerto al socket(" + LastError() + ")" ).w_str(), L"Error en el servidor", MB_ICONERROR );
		closesocket( sserver );
		WSACleanup();
		return null;
	}

	//Habilitar conexiones
	if ( listen( sserver, 1 ) == SOCKET_ERROR ) {
		MessageBox( GetActiveWindow(), ( "Error al habilitar conexiones entrantes(" + LastError() + ")" ).w_str(), L"Error en el servidor", MB_ICONERROR );
		closesocket( sserver );
		WSACleanup();
		return null;
	}

	return sserver;
 };
SOCKET     _stdcall CreateClient( AnsiString host, int port            ) {
	if ( host == "" ) host = "127.0.0.1";
	if ( port <= 0  ) port = 80;

	//Declaraciones
	WSADATA            wsa;
	SOCKET             soc;
	struct sockaddr_in ser;

	//Inicializamos la DLL de Sockets
	if ( WSAStartup( MAKEWORD( 2, 2 ), &wsa ) ) {
		MessageBox( GetActiveWindow(), ( "La DLL de sockets no se inicio correctamente(" + LastError() + ")" ).w_str(), L"Error en el servidor", MB_ICONERROR );
		return null;
	}

	//Creamos el socket...
	soc = socket( AF_INET, SOCK_STREAM, 0 );
	if ( soc == INVALID_SOCKET ) {
		MessageBox( GetActiveWindow(), ( "Error al crear socket(" + LastError() + ")" ).w_str(), L"Error en el servidor", MB_ICONERROR );
		WSACleanup();
		return null;
	}

	//inet_addr( AnsiString( port ).c_str() );

	ser.sin_addr.s_addr = inet_addr( host.c_str() );
	ser.sin_family      = AF_INET;
	ser.sin_port        = htons( port );

	connect( soc, (struct sockaddr*)&ser, sizeof(ser) );

	return soc;
 };
String     _stdcall GetHash     ( String key, bool charSpecial         ) {
	String       Hash   ;
	String       Tmp    ;
	TSHA1Context Context;
	TSHA1Digest  Digest ;

	Tmp = key + SpecGUID;

	sha1Init  ( Context );
	sha1Update( Context, Tmp.w_str(), Tmp.Length() );
	sha1Final ( Context, Digest );

	for ( int i = 0; i <= 19; i++ )
		Hash = Hash + AnsiChar( Digest[i] );

	return base64Encode( Hash, charSpecial );
 };
void       _stdcall EncodeSock  ( String all, char **cat, __int64 &let ) {
	/*Empaquetado, sin m�scara*/
	UTF8String res = all;

	/*Empaquetado, sin m�scara*/
	int      ini = 0;
	char    *cad = res.c_str();
	__int64  len = res.Length();

	let = len + 2;

	if ( len<=125 ) {
		let    = len + 2;
		(*cat) = new char[let];
		(*cat)[0] = 129;
		(*cat)[1] = len;
		ini    = 2;
	}
	else if ( len>=126 && len<=65535 ) {
		let    = len + 4;
		(*cat)    = new char[let];
		(*cat)[0] = 129;
		(*cat)[1] = 126;
		(*cat)[2] = ( len >> 8 ) & 255;
		(*cat)[3] = ( len      ) & 255;
		ini    = 4;
	}
	else {
		let    = len + 10;
		(*cat)    = new char[let];
		(*cat)[0] = 129;
		(*cat)[1] = 127;
		(*cat)[2] = ( len >> 56 ) & 255;
		(*cat)[3] = ( len >> 48 ) & 255;
		(*cat)[4] = ( len >> 40 ) & 255;
		(*cat)[5] = ( len >> 32 ) & 255;
		(*cat)[6] = ( len >> 24 ) & 255;
		(*cat)[7] = ( len >> 16 ) & 255;
		(*cat)[8] = ( len >>  8 ) & 255;
		(*cat)[9] = ( len       ) & 255;
		ini    = 10;
	}

	for ( int i = 0; i<len; i++ )
		(*cat)[ini++] = cad[i];
 };
UTF8String _stdcall DecodeSock  ( byte  *petition                     ) {
	char      *buf = new char[maxbuf];
	int        len = 0;
	UTF8String res;

	ZeroMemory( buf, maxbuf );

	if ( petition[1] & 0x80 ) {
		len = petition[1] & 0x7F; //payload longitud de datos

		if ( len<126 ) {
			for ( int i = 0; i<len; i++ )
				buf[i] = petition[6+i] ^ petition[i%4+2];//Primera 3 4 5 6 bytes de la m�scara, comenzaron los primeros 7 bytes de datos

			buf[len] = '\0';
		}
		else if( len==126 ) {
			len = (unsigned char)petition[2] * 256 + (unsigned char)petition[3];//Longitud de los datos reales, lo que representa dos bytes

			for ( int i = 0; i<len; i++ )
				buf[i] = petition[8+i] ^ petition[i%4+4]; //Los primeros 5 6 7 8 bytes de la m�scara, el primer byte de los datos 9

			buf[len] = '\0';
		}
	}

	res = buf;

	delete []buf;

	return res;
 };
//###########################################################################


/*Otros*/
void       _stdcall Notify   ( String title, String message, seMessageType type ) {
	String tip = "";

	if      ( type == seMessageType::mtAccept  ) tip = "[Accept]" ;
	else if ( type == seMessageType::mtError   ) tip = "[Error]"  ;
	else if ( type == seMessageType::mtInfo    ) tip = "[Info]"   ;
	else if ( type == seMessageType::mtWarning ) tip = "[Earning]";

	std::cout << AnsiString( AccentABat( title   ) ).c_str() << "\n";
	std::cout << AnsiString( AccentABat( message ) ).c_str() << "\n";
	std::cout << AnsiString( AccentABat( tip     ) ).c_str() << "\n\n";

	if ( type == seMessageType::mtError ) {
        MessageBox( GetActiveWindow(), message.w_str(), title.w_str(), MB_ICONERROR );
    }
 };
UTF8String _stdcall LoadEFile( String file, bool json                           ) {
	TMemoryStream *mem = new TMemoryStream;

	mem->LoadFromFile( file );

	UTF8String res = Load( mem, json );

	delete mem;
	return res;
 };
UTF8String _stdcall LoadEFile( String file, String module, bool json            ) {
	TResourceStream *mem = new TResourceStream( (int)HInstance, file, module.w_str() );

	UTF8String res = Load( mem, json );

	delete mem;
	return res;
 };
//###########################################################################
