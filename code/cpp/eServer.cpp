#pragma hdrstop
#include "eServer.h"
#include "ePetition.h"
#include "eModules.h"
#include "eSessions.h"
#include "eOptions.h"
#pragma package(smart_init)

// Otros ####################################################################
/*Definiciones*/
#define HIP       Str[0]
#define HSServer  i64[0]
#define HSClient  i64[1]
#define HSBuffer  i64[2]

/*Variables*/
static eServer *_eServer = null;

/*Funciones*/
eServer *__stdcall sfeServer() {
	if ( !_eServer ) _eServer = new eServer;

	return _eServer;
 }
//###########################################################################


// class PACKAGE eServer : public TObject //#################################
/*Creacion - public*/
__fastcall eServer:: eServer() {
	_cancel = false;
 };
__fastcall eServer::~eServer() {
	this->Stop();
 };
//###########################################################################


/*Hilos - private*/
void __fastcall eServer::thini_sock( wThread *hilo ) {
	sSField *lis = (sSField*)hilo->HSBuffer;

	while ( !hilo->Cancel ) {
		if ( !lis->Size ) hilo->Paus( true );
		else {
			while ( lis->Size ) {
				ePetition *pes = new ePetition( hilo->HSClient, (*lis)[0].Value, false );

				gmod.Sock( pes );

				(*lis)-= 0;

				delete pes;
			}
		}
	}
 };
void __fastcall eServer::thini_http( wThread *hilo ) {
	char *buf = new char[maxbuf];

	ZeroMemory( buf, maxbuf );
	recv( hilo->HSClient, buf, maxbuf, 0 );

	ePetition *petition = new ePetition( hilo->HSClient, buf, true );

	if      ( pe["protocol"].Str.Pos( "HTTP" ) ) gmod.Http( petition );
	else if ( pe["protocol"].Str == "SOCKET"   ) {
		sSField  lso;
		wThread *hil;

		pe.ResponceHand();

		gses.Insert( hilo->HSClient );

		hil           = new wThread;
		hil->HSBuffer = (__int64)new sSField( lso );
		hil->HSClient = hilo->HSClient;
		hil->onThread = thini_sock;
		hil->Play();

		/*Escucha*/
		while( 1 ) {
			ZeroMemory( buf, maxbuf );
			recv( hilo->HSClient, buf, maxbuf, 0 );

			//Desconectar
			if ( (Byte)buf[0]==0x88 || (Byte)buf[0]==0x00 )
				break;

			//Enrutar
			else {
				lso+= DecodeSock( buf );

				hil->Paus( false );
			}
		}

		hil->Cancel = true;
		hil->Paus( false );

		gses.Delete( hilo->HSClient );
	}

	delete[] buf;
	delete   petition;
 };
//###########################################################################


/*Metodos - public*/
void eServer::Play() {
	_cancel = false;

	/*Inicio*/
	String hos = gopc["server"]["host"].Str;
	int    por = gopc["server"]["port"].Num;

	if ( hos == "" ) hos = "127.0.0.1";
	if ( por <= 0  ) por = 80;

	//Declaraciones
	WSADATA             wsaData;
	struct sockaddr_in  ad_server;
	struct sockaddr_in  ad_client;
	int                 iport    = por;
	AnsiString          shost    = hos;
	struct hostent     *thost    ;

	//Inicializamos la DLL de Sockets
	if ( WSAStartup( MAKEWORD( 2, 2 ), &wsaData ) ) {
		Notify( "Error en el servidor", "La DLL de sockets no se inicio correctamente(" + LastError() + ")", seMessageType::mtError );
		return;
	}

	//IP que usar� nuestro servidor...
	thost = (struct hostent*)gethostbyname( shost.c_str() );
	if( !thost ) {
		Notify( "Error en el servidor", "No se ha encontrado servidor(" + LastError() + ")", seMessageType::mtError );
		WSACleanup();
		return;
	}

	//Creamos el socket...
	SOCKET sserver = socket( AF_INET, SOCK_STREAM, 0 );
	if ( sserver == INVALID_SOCKET ) {
		Notify( "Error en el servidor", "Error al crear socket(" + LastError() + ")", seMessageType::mtError );
		WSACleanup();
		return;
	}

	//Asociamos puerto al socket
	memset( &ad_server         , 0           , sizeof(ad_server) );
	memcpy( &ad_server.sin_addr, thost->h_addr, thost->h_length    );

	ad_server.sin_port   = htons( iport );
	ad_server.sin_family = thost->h_addrtype;

	if ( bind( sserver, (struct sockaddr*)&ad_server, sizeof(ad_server) ) == SOCKET_ERROR ) {
		Notify( "Error en el servidor", "Error al asociar puerto al socket(" + LastError() + ")", seMessageType::mtError );
		closesocket( sserver );
		WSACleanup();
		return;
	}

	//Habilitar conexiones
	if ( listen( sserver, 1 ) == SOCKET_ERROR ) {
		Notify( "Error en el servidor", "Error al habilitar conexiones entrantes(" + LastError() + ")", seMessageType::mtError );
		closesocket( sserver );
		WSACleanup();
		return;
	}

	// Aceptamos conexiones entrantes
	Notify( "Servidor", "Servidor en l�nea", seMessageType::mtInfo );

	while ( !_cancel ) {
		int    stsize  = sizeof(struct sockaddr);
		SOCKET sclient = accept( sserver, (struct sockaddr*)&ad_client, &stsize );

		if ( !_cancel ) {
			if ( sclient == INVALID_SOCKET ) {
				Notify( "Error en el servidor", "Error al aceptar conexi�n entrante(" + LastError() + ")", seMessageType::mtError );
			}
			else {
				wThread*
				hil           = new wThread;
				hil->onThread = thini_http;
				hil->HIP      = inet_ntoa( ad_client.sin_addr );
				hil->HSServer = sserver;
				hil->HSClient = sclient;
				hil->Play();
			}
		}
	}

	closesocket( sserver );
 };
void eServer::Stop() {};
//###########################################################################
