#pragma hdrstop
#include "wTaskBar.h"
#pragma package(smart_init)


/*Otros*/
static ITaskbarList3 *wTaskBarPPROG;
static ITaskbarList3 *wTaskBarPICON;
static HICON          wTaskBarHICON;
//###########################################################################


// class PACKAGE wTaskBar //#################################################
/*Creacion - public*/
wTaskBar::wTaskBar() {};
//###########################################################################


/*Metodos - icon - public*/
void wTaskBar::SetIcon( HICON hicon, String description ) {
	if ( hicon && wTaskBarHICON ) CloseHandle( wTaskBarHICON );

	wTaskBarHICON = hicon;

	if ( !wTaskBarPICON )
		CoCreateInstance( CLSID_TaskbarList, null, CLSCTX_ALL, IID_ITaskbarList3, (void**)&wTaskBarPICON );

	wTaskBarPICON->SetOverlayIcon( GetGlobalHandle(), wTaskBarHICON, description.w_str() );
 };
void wTaskBar::DelIcon(                                 ) {
	if ( wTaskBarHICON ) wTaskBar::SetIcon( null );
	if ( wTaskBarPICON ) wTaskBarPICON->Release();

	wTaskBarPICON = null;
 };

/*Metodos - progreso - public*/
void wTaskBar::SetProgress( seTypeProg type, int vmax, int vmin, int vprogress ) {
	if ( !wTaskBarPPROG )
		CoCreateInstance( CLSID_TaskbarList, NULL, CLSCTX_ALL, IID_ITaskbarList3, (void**)&wTaskBarPPROG );

	int max = vmax      - vmin;
	int pro = vprogress - vmin;

	wTaskBarPPROG->SetProgressValue( GetGlobalHandle(), max == 0 ? 0 : ( pro * 100 ) / max, max );
	wTaskBarPPROG->SetProgressState( GetGlobalHandle(), (TBPFLAG)type );
 };
void wTaskBar::DelProgress(                                                    ) {
	wTaskBar::SetProgress( tpNoProgress, 0, 0, 0 );

	if ( wTaskBarPPROG ) wTaskBarPPROG->Release();

	wTaskBarPPROG = null;
 };

/*Metodos - jumpList - public*/
void wTaskBar::AddLink( String category, String title, String path, String params, String iconPath, int iconIndex, seTypeLink type ) {
	int ind = -1;

	for ( int i = 0; i < _categorys.Size; i++ ) {
		if ( _categorys[i].Value == category ) {
			ind = i;
			break;
		}
	}

	if ( ind == -1 ) _categorys+= category;

	_linkCategory+= category ;
	_linkTitle   += title    ;
	_linkPath    += path     ;
	_linkParams  += params   ;
	_linkIcon    += iconPath ;
	_linkIndex   += iconIndex;
	_linkType    += type     ;
 };

bool wTaskBar::CreateJump( String applicationID, bool viewFrequent, bool viewRecent ) {
	ICustomDestinationList *jumplist;
	IObjectArray           *objects ;
	UINT                    minSlots;

	if ( SUCCEEDED( CoCreateInstance( CLSID_DestinationList, null, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&jumplist) ) ) )
		if ( SUCCEEDED( jumplist->BeginList( &minSlots, IID_PPV_ARGS(&objects) ) ) ) {
			for ( int i = 0; i < _categorys.Size; i++ )
				CreateCategory( jumplist, _categorys[i] );

			if ( viewFrequent ) jumplist->AppendKnownCategory( KDC_FREQUENT );
			if ( viewRecent   ) jumplist->AppendKnownCategory( KDC_RECENT   );

			objects->Release();

			if ( SUCCEEDED( jumplist->CommitList() ) ) {
				jumplist->Release();

				return true;
            }
		}

	return false;
 };
bool wTaskBar::DeleteJump( String applicationID                                     ) {
	ICustomDestinationList *jumpList;

	CoCreateInstance( CLSID_DestinationList, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&jumpList) );

	if ( applicationID != "" ) {
		jumpList->DeleteList( applicationID.w_str() );
	}
	else {
		unsigned      minSlots;
		IObjectArray *object;

		jumpList->BeginList( &minSlots, IID_PPV_ARGS(&object) );

		object->Release();
		jumpList->CommitList();
	}

	jumpList->Release();

	return true;
 };
//###########################################################################


/*Funciones - private*/
bool wTaskBar::CreateCategory( ICustomDestinationList *jumplist, String title ) {
	IObjectCollection *lis;
	IObjectArray      *arr;
	bool               ret = false;

	if ( SUCCEEDED( CoCreateInstance( CLSID_EnumerableObjectCollection, null, CLSCTX_INPROC, IID_PPV_ARGS(&lis) ) ) ) {
		for ( int i = 0; i < _linkCategory.Size; i++ ) {
			if ( _linkCategory[i].Value == title ) {
				ret = CreateLink( lis, i );

				if ( !ret ) break;
			}
		}

		if ( ret && SUCCEEDED( lis->QueryInterface( &arr ) ) ) {
			if ( title == "[tareas]" ) jumplist->AddUserTasks( arr );
			else                       jumplist->AppendCategory( title.w_str(), arr );

			arr->Release();
		}

		lis->Release();
	}

	return ret;
 };
bool wTaskBar::CreateLink    ( IObjectCollection *list, int index             ) {
	bool ret = false;

	if ( _linkType[index].Value == tlShellLink ) {
		IShellLink     *lnk; 
		IShellLink     *add; 
		IPropertyStore *prs;
		PROPVARIANT     prv;
		PROPERTYKEY     kti;
		String          tit = _linkTitle[index];
		String          pat = StringReplace( _linkPath[index], "[application]", ParamStr( 0 ), TReplaceFlags()<< rfReplaceAll );
		String          par = _linkParams[index];
		String          dir = ExtractFileDir( pat );
		String          ico = StringReplace( _linkIcon[index], "[application]", ParamStr( 0 ), TReplaceFlags()<< rfReplaceAll );
		int             iic = _linkIndex[index];
		
		CLSIDFromString( L"{F29F85E0-4FF9-1068-AB91-08002B27B3D9}", &(kti.fmtid) );

		kti.pid = 2;

		if ( SUCCEEDED( CoCreateInstance(CLSID_ShellLink, null, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&lnk) ) ) )
			if ( SUCCEEDED( lnk->SetPath( pat.w_str() ) ) )                                        //-> Path del link
				if ( SUCCEEDED( lnk->SetArguments( par.w_str() ) ) )                               //-> Parametros del link
					if ( SUCCEEDED( lnk->SetIconLocation( ico.w_str(), iic ) ) )                   //-> Path e Indice del icono
						if ( SUCCEEDED( lnk->SetWorkingDirectory( dir.w_str() ) ) )                //-> Directorio de trabajo
							if ( SUCCEEDED( lnk->QueryInterface( &prs ) ) )                        //
								if ( SUCCEEDED( InitPropVariantFromString( tit.w_str(), &prv ) ) ) //-> Titulo del link
									if ( SUCCEEDED( prs->SetValue( kti, prv ) ) )
										if ( SUCCEEDED( prs->Commit() ) ) {
											list->AddObject( lnk );
											PropVariantClear( &prv );

											ret = true;
										}
	}
	else {
		PROPERTYKEY     usm;
		PROPVARIANT     var;
		IPropertyStore *sep;

		CLSIDFromString( L"{9F4C2855-9F79-4B39-A8D0-E1D42DE1D5F3}", &usm.fmtid );

		usm.pid = 6;

		if ( SUCCEEDED( CoCreateInstance( CLSID_ShellLink, null, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&sep) ) ) ) {
			var.vt      = VT_BOOL     ;
			var.boolVal = VARIANT_TRUE;

			if ( SUCCEEDED( sep->SetValue( usm, var ) ) ) {
				sep->Commit();
				list->AddObject( sep );
				PropVariantClear( &var );

				ret = true;
			}

			sep->Release();
		}
	}

	return ret;
 };
//###########################################################################