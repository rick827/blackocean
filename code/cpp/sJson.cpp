#pragma hdrstop
#include "sJson.h"
#pragma package(smart_init)

// class PACKAGE sJson //####################################################
/*Creacion - public*/
sJson:: sJson(                   ) {
	sJson_start;

	(*_count) = 1;
 };
sJson:: sJson( const String file ) {
	sJson_start;

	(*_count) = 1;

	Parse( file );
 };
sJson:: sJson( const sJson *json ) {
	_child = json->_child;
	_count = json->_count;
	_temp  = json->_temp;
	_json  = json->_json;
	_file  = json->_file;
	_value = json->_value;
	_tag   = json->_tag;
	_type  = json->_type;

	(*_count)++;
 };
sJson:: sJson( const sJson &json ) {
	_child = json._child;
	_count = json._count;
	_temp  = json._temp;
	_json  = json._json;
	_file  = json._file;
	_value = json._value;
	_tag   = json._tag;
	_type  = json._type;

	(*_count)++;
 };
sJson::~sJson(                   ) {
	Delete();
 };
//###########################################################################


/*Propiedades - protected*/
void sJson::WJson ( String      json  ) {
	(*_json) = json;
 };
void sJson::WValue( String      value ) {
	(*_value) = value;
 };
void sJson::WTag  ( String      tag   ) {
	(*_tag) = tag;
 };
void sJson::WType ( seValueType type  ) {
	(*_type) = type;
 };
void sJson::WStr  ( String      str   ) {
	(*_value) = str;
 };
void sJson::WBoo  ( bool        boo   ) {
	(*_value) = boo ? "true" : "false";
 };
void sJson::WNum  ( double      num   ) {
	(*_value) = (String)num;
 };

/*Propiedades - protected*/
int         sJson::RSize () {
	return _child->Count;
 };
String      sJson::RJson () {
	return (*_json);
 };
String      sJson::RValue() {
	return (*_value);
 };
String      sJson::RTag  () {
	return (*_tag);
 };
seValueType sJson::RType () {
	return (*_type);
 };
String      sJson::RStr  () {
	return (*_value);
 };
bool        sJson::RBoo  () {
	return (*_value) == "true";
 };
double      sJson::RNum  () {
	return sad( (*_value) );
 };
//###########################################################################


/*Operadores - public*/
sJson sJson::operator = ( bool   value ) {
	(*_value) = value ? "true" : "false";

	return this;
 };
sJson sJson::operator = ( double value ) {
	(*_value) = (String)sad( value );

	return this;
 };
sJson sJson::operator = ( String value ) {
	(*_value) = value;

	return this;
 };
sJson sJson::operator = ( sJson  value ) {
	Delete();

	sJson_start;

	Clone( value );

	(*_count)++;

	return this;
 };

bool sJson::operator == ( bool   value ) {
	return value ? (*_value) == "true" : false;
 };
bool sJson::operator == ( double value ) {
	return sad( (*_value) ) == value;
 };
bool sJson::operator == ( String value ) {
	return (*_value) == value;
 };
bool sJson::operator != ( bool   value ) {
	return value ? (*_value) == "false" : true;
 };
bool sJson::operator != ( double value ) {
	return (*_value) != value;
 };
bool sJson::operator != ( String value ) {
	return (*_value) != value;
 };

sJson sJson::operator += ( sJson  value              ) {
	_child->Add( (__int64)new sJson( value ) );

	return this;
 };
sJson sJson::operator -= ( int    index              ) {
	if ( index >= 0 && index < _child->Count ) {
		delete (sJson*)_child->Items[index];

		_child->Delete( index );
	}

	return this;
 };
sJson sJson::operator [] ( int    index              ) {
	if ( index >= 0 && index < _child->Count )
		return (sJson*)_child->Items[index];

	return GetTemporary();
 };
sJson sJson::operator [] ( String tag                ) {
	for ( int x = 0; x < _child->Count; x++ ) {
		if ( *(((sJson*)_child->Items[x])->_tag) == tag )
			return (sJson*)_child->Items[x];
	}

	return GetTemporary();
 };
sJson sJson::operator () ( int    index, sJson value ) {
	if ( index >= _child->Count + 1 ) index = _child->Count;
	if ( index <  0                 ) index = 0;

	_child->Insert( index, (__int64)new sJson( value ) );

	return this;
 };
//###########################################################################


/*Metodos - public*/
void sJson::Parse( String file              ) {
	(*_json) = "";
	(*_file) = "";

	Clear();

	//Archivo
	if ( file == "" ) return;

	if ( FileExists( file ) ) {
		wStream *st = new wStream( (*_file) = file );
		(*_json) = st->OnlyStr();
		delete st;
	}
	else (*_json) = file;

	//Parseando
	if ( (*_json) != "" ) {
		int      pos = 0;
		int      len = (*_json).Length();
		wchar_t *cad = (*_json).w_str();

		(*_type) = vlNothing;

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]=='{' ) { (*_type) = vlObject; break; }
			else if ( cad[pos]=='[' ) { (*_type) = vlArray ; break; }
		}

		if ( (*_type) == vlObject ) ViewObject( cad, pos, len );
		else                        ViewArray ( cad, pos, len );
	}
 };
void sJson::Clear(                          ) {
	for ( int x = 0; x < _child->Count; x++ )
		delete (sJson*)_child->Items[x];

	_child->Clear();
 };
void sJson::Save ( String file, bool format ) {
	(*_json) = AJson( this, format, this->Type == vlObject, "", 0 );

	if ( file != "" ) {
		wStream *st = new wStream;
		st->OnlyStr( (*_json) );
		st->Save( (*_file) = file );
		delete st;
	}
 };
void sJson::Clone( sJson json               ) {
	_child = json._child;
	_count = json._count;
	_temp  = json._temp;
	_json  = json._json;
	_file  = json._file;
	_value = json._value;
	_tag   = json._tag;
	_type  = json._type;
 };

sJson sJson::Add( seValueType type, String tag, String value ) {
	if ( (*this)[tag].Type != vlTemporary ) {
		(*this)[tag].Type  = type;
		(*this)[tag].Value = value;

		return (*this)[tag];
	}

	sJson *jso = new sJson;

	(*jso->_type ) = type ;
	(*jso->_tag  ) = tag  ;
	(*jso->_value) = value;

	if ( ( type == vlArray || type == vlObject ) && value != "" )
		jso->Parse( value );

	(*this)+= jso;

	return jso;
 };
//###########################################################################


/*Funciones - protected*/
String sJson::GetString( wchar_t *cad, int &pos, int len ) {
	char   com = cad[pos];
	String res = "";

	for ( pos++; pos<len; ) {
		if ( cad[pos]=='\\' && pos+1<len && cad[pos+1]==com ) {
			pos+= 2;
			res+= "\\" + String(com);
		}
		else if ( cad[pos]==com ) {
			pos++;
			break;
		}
		else res+= cad[pos++];
	}

	return res;
 };

sJson sJson::GetTemporary() {
	if ( !_temp ) {
		_temp       = new sJson;
		_temp->Type = vlTemporary;
	}

	return _temp;
 };

void sJson::AddString( wchar_t *cad, int &pos, int len, String tag ) {
	Add( vlText, tag, GetString( cad, pos, len ) );
 };
void sJson::AddBool  ( wchar_t *cad, int &pos, int len, String tag ) {
	Add( vlBool, tag, cad[pos]=='t' ? "true" : "false" );

	if ( cad[pos] == 't' ) pos+= 4;
	else                   pos+= 5;
 };
void sJson::AddNull  ( wchar_t *cad, int &pos, int len, String tag ) {
	Add( vlNull, tag, "null" );

	pos+= 4;
 };
void sJson::AddArray ( wchar_t *cad, int &pos, int len, String tag ) {
	sJson js = Add( vlArray, tag, "" );

	for ( int ope = -1; pos<len; pos++ ) {
		(*(js._value))+= cad[pos];

		if      ( cad[pos]=='[' ) ope++;
		else if ( cad[pos]==']' ) {
			if ( ope <= 0 ) break;
			else            ope--;
		}
	}

	pos++;

	js.Parse( (*js._value) );
 };
void sJson::AddObject( wchar_t *cad, int &pos, int len, String tag ) {
	sJson js = Add( vlObject, tag, "" );

	for ( int ope = -1; pos<len; pos++ ) {
		(*(js._value))+= cad[pos];

		if      ( cad[pos]=='{' ) ope++;
		else if ( cad[pos]=='}' ) {
			if ( ope <= 0 ) break;
			else            ope--;
		}
	}

	pos++;

	js.Parse( (*js._value) );
 };
void sJson::AddNumber( wchar_t *cad, int &pos, int len, String tag ) {
	String val = "";

	for ( ;pos<len && ( cad[pos]==' ' || cad[pos]=='\t' || cad[pos]=='\r' || cad[pos]=='\n' ); pos++ );
	for ( ;pos<len && cad[pos]!=' ' && cad[pos]!=',' && cad[pos]!='}' && cad[pos]!=']' && cad[pos]!='\r' && cad[pos]!='\n' && cad[pos]!='\t'; pos++ )
		val += cad[pos];

	Add( vlNumber, tag, (String)sad( val ) );
 };

void sJson::ViewObject( wchar_t *cad, int &pos, int len ) {
	for ( ;pos<len && cad[pos]!='{'; pos++ );

	do {
		pos++;

		for ( ;pos<len && ( cad[pos]==' ' || cad[pos]=='\t' || cad[pos]=='\r' || cad[pos]=='\n' ); ) pos++;

		if ( cad[pos]=='}' ) break;

		//Etiqueta
		String tag = "";

		if ( cad[pos]=='"' || cad[pos]=='\'' ) {
			tag = GetString( cad, pos, len );
		}
		else for ( ;pos<len && cad[pos]!=' ' && cad[pos]!='\t' && cad[pos]!='\r' && cad[pos]!='\n' && cad[pos]!=':'; pos++ )
			tag += cad[pos];

		for ( ;pos<len && cad[pos]!=':'; ) pos++; pos++;
		for ( ;pos<len && ( cad[pos]==' ' || cad[pos]=='\t' || cad[pos]=='\r' || cad[pos]=='\n' ); ) pos++;

		//Valor
		if      ( cad[pos]=='"' || cad[pos]=='\'' ) AddString( cad, pos, len, tag );
		else if ( cad[pos]=='t' || cad[pos]=='f'  ) AddBool  ( cad, pos, len, tag );
		else if ( cad[pos]=='n'                   ) AddNull  ( cad, pos, len, tag );
		else if ( cad[pos]=='['                   ) AddArray ( cad, pos, len, tag );
		else if ( cad[pos]=='{'                   ) AddObject( cad, pos, len, tag );
		else                                        AddNumber( cad, pos, len, tag );

		for ( ;pos<len && cad[pos]!=',' && cad[pos]!='}'; pos++ );
	}
	while ( cad[pos]==',' );

	if ( cad[pos]=='}' ) pos++;
 };
void sJson::ViewArray ( wchar_t *cad, int &pos, int len ) {
	for ( ;pos<len && cad[pos]!='['; pos++ )  pos++;

	do {
		pos++;

		for ( ;pos<len && ( cad[pos]==' ' || cad[pos]=='\t' || cad[pos]=='\r' || cad[pos]=='\n' ); ) pos++;

		if ( cad[pos]==']' ) break;

		if      ( cad[pos]=='"' || cad[pos]=='\'' ) AddString( cad, pos, len, (String)_child->Count );
		else if ( cad[pos]=='t' || cad[pos]=='f'  ) AddBool  ( cad, pos, len, (String)_child->Count );
		else if ( cad[pos]=='n'                   ) AddNull  ( cad, pos, len, (String)_child->Count );
		else if ( cad[pos]=='['                   ) AddArray ( cad, pos, len, (String)_child->Count );
		else if ( cad[pos]=='{'                   ) AddObject( cad, pos, len, (String)_child->Count );
		else                                        AddNumber( cad, pos, len, (String)_child->Count );

		for ( ;pos<len && cad[pos]!=',' && cad[pos]!=']'; pos++ );
	}
	while ( cad[pos]==',' );

	if ( cad[pos]==']' ) pos++;
 };

String sJson::AJson( sJson json, bool format, bool isObject, String slevel, int ilevel ) {
	String lin = "\n", tab = "\t", esp = " ";

	if ( !format ) { lin = ""; tab = ""; esp = ""; }

	if ( json.Type == vlText ) {
		return slevel + ( isObject ? co + json.Tag + co + ":" + esp + co + json.Value + co : co + json.Value + co );
	}
	else if ( json.Type == vlNumber || json.Type == vlBool || json.Type == vlNull ) {
		return slevel + ( isObject ? co + json.Tag + co + ":" + esp + json.Value : json.Value );
	}
	else if ( json.Type == vlArray || json.Type == vlObject ) {
		bool   arr = json.Type == vlArray;
		int    num = json.Size;
		String res = slevel + ( isObject && ilevel ? co + json.Tag + co + ":" + esp : (String)"" ) + ( arr ? "[" : "{" );

		for ( int i = 0; i < num; i++ ) {
			res+= lin + AJson( json[i], format, !arr, slevel + tab, ilevel + 1 );

			if ( i + 1 < num ) res+= ",";
		}

		if ( num ) res+= lin + slevel;

		res+= arr ? "]" : "}";

		return res;
	}

	return "";
 };

void sJson::Delete() {
	if ( _count && --(*_count) == 0 ) {
		delete _child; _child = null;
		delete _count; _count = null;
		delete _temp ; _temp  = null;
		delete _json ; _json  = null;
		delete _file ; _file  = null;
		delete _value; _value = null;
		delete _tag  ; _tag   = null;
		delete _type ; _type  = null;
	}
 };
//###########################################################################