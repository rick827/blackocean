#ifndef wRegistryH
#define wRegistryH

#include "wGeneral.h"
#include "Winreg.h"

class PACKAGE wRegistry {
	/*Definiciones*/
	#define wRegistry_start \
		ViewErrors  = false;             \
		HKey        = HKEY_CURRENT_USER; \
		Pass        = KEY_ALL_ACCESS;    \
		Transaction = null;
	//###########################################################################


	/*Variables*/public:
	bool           ViewErrors ;
	HKEY           HKey       ;
	unsigned long  Pass       ;
	String         Key        ;
	void          *Transaction;
	//###########################################################################


	/*Creacion*/public:
	wRegistry(                                                                         );
	wRegistry( HKEY hkey, String key,                     bool create, bool viewErrors );
	wRegistry( HKEY hkey, String key, unsigned long pass, bool create, bool viewErrors );
	//###########################################################################


	/*Metodos*/public:
	bool OpenKey  (                        bool create );
	bool OpenKey  (            String key, bool create );
	bool OpenKey  ( HKEY hkey, String key, bool create );
	void CloseKey (                                    );
	bool RenameKey(            String key, String name );
	bool DeleteKey(            String key              );
	bool DeleteKey( HKEY hkey, String key              );
	bool DeleteVal(            String key              );
	bool DeleteVal( HKEY hkey, String key              );

	int    Int( String name );
	String Str( String name );
	double Dou( String name );

	bool Int( String name, int    value );
	bool Str( String name, String value );
	bool Dou( String name, double value );

	int    DefInt( String name, int    def = 0  );
	String DefStr( String name, String def = "" );
	double DefDou( String name, double def = 0  );
	//###########################################################################
 };

#endif
