#ifndef eServerH
#define eServerH

#include "sFields.h"
#include "wThread.h"

class PACKAGE eServer : public TObject {
	/*Variables*/private:
	bool _cancel;
	//###########################################################################


	/*Creacion*/public:
	__fastcall  eServer();
	__fastcall ~eServer();
	//###########################################################################


	/*Hilos*/private:
	void __fastcall thini_sock( wThread *hilo );
	void __fastcall thini_http( wThread *hilo );
	//###########################################################################


	/*Metodos*/public:
	void Play();
	void Stop();
	//###########################################################################
 };

extern "C++" __declspec(dllexport)eServer *__stdcall sfeServer();

#endif
