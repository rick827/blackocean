#ifndef eSectionH
#define eSectionH

#include <classes.hpp>
#include "sDef.h"
#include "wThread.h"

class PACKAGE eSection {
	/*Variables*/public:
	wThread *_hwatch;
	//###########################################################################


	/*Creacion*/public:
	eSection();
	//###########################################################################


	/*Hilos*/private:
	void __fastcall thini_watch( wThread *hilo );
	//###########################################################################


	/*Metodos*/public:
	virtual void Router( ePetition *petition );
	virtual void ExecJS( ePetition *petition );
	virtual void Getter( ePetition *petition );
	virtual void Insert( ePetition *petition );
	virtual void Update( ePetition *petition );
	virtual void Delete( ePetition *petition );

	virtual void Load();
	//###########################################################################
 };

#endif
