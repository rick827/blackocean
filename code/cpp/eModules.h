#ifndef eModulesH
#define eModulesH

#include "sFields.h"
#include "wGeneral.h"
#include "eGeneral.h"
#include "ePetition.h"

class PACKAGE eModule {
	/*Variables*/private:
	HINSTANCE  _instance;
	sfLoad     _fload;
	sfPetition _fhttp;
	sfPetition _fsock;
	String     _name;
	String     _path;
	//###########################################################################


	/*Creacion*/public:
	eModule( String path );
   ~eModule(             );
	//###########################################################################


	/*Propiedades*/public:
    __property String Name = { read=_name };
	//###########################################################################


	/*Metodos*/public:
	void Http( ePetition *petition );
	void Sock( ePetition *petition );
	//###########################################################################
 };
class PACKAGE eModules : public TObject {
	/*Variables*/private:
	sIField  _pmodules;
	eModule *_mpublic;
	//###########################################################################


	/*Creacion*/public:
	__fastcall  eModules();
	__fastcall ~eModules();
	//###########################################################################


	/*Metodos*/public:
	void Load (                     );
	void Http ( ePetition *petition );
	void Sock ( ePetition *petition );
	void Clear(                     );
	//###########################################################################


	/*Funciones*/private:
	void LoadModules( String path );
	//###########################################################################
 };

extern "C++" __declspec(dllexport)eModules *__stdcall sfeModules();

#endif
