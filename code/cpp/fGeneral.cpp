#pragma hdrstop
#include "fGeneral.h"
#pragma package(smart_init)

/*Otros*/
static HWND fGeneralAPH = null;
//###########################################################################


/*Imagen*/
TMemoryStream *__stdcall HBMaMEM( HBITMAP  bitmap                 ) {
	TMemoryStream     *res           = new TMemoryStream;
	HDC                hDC           = CreateDC( L"DISPLAY", NULL, NULL, NULL );
	int                iBits         = GetDeviceCaps( hDC, BITSPIXEL ) * GetDeviceCaps( hDC, PLANES );
	WORD               wBitCount     ;
	DWORD              dwPaletteSize = 0;
	DWORD              dwBmBitsSize  = 0;
	DWORD              dwDIBSize     = 0;
	BITMAP             Bitmap        ;
	BITMAPFILEHEADER   bmfHdr        ;
	BITMAPINFOHEADER   bmpInfo       ;
	LPBITMAPINFOHEADER bmpIHea       ;
	HANDLE             hDib          ;
	HANDLE             hPal          ;
	HANDLE             hOldPal       = NULL;

	DeleteDC( hDC );

	if      ( iBits <= 1 ) wBitCount = 1;
	else if ( iBits <= 4 ) wBitCount = 4;
	else if ( iBits <= 8 ) wBitCount = 8;
	else                   wBitCount = 24;

	GetObject( bitmap, sizeof( Bitmap ), (LPSTR)&Bitmap );

	bmpInfo.biSize          = sizeof( BITMAPINFOHEADER );
	bmpInfo.biWidth         = Bitmap.bmWidth;
	bmpInfo.biHeight        = Bitmap.bmHeight;
	bmpInfo.biPlanes        = 1;
	bmpInfo.biBitCount      = wBitCount;
	bmpInfo.biCompression   = BI_RGB;
	bmpInfo.biSizeImage     = 0;
	bmpInfo.biXPelsPerMeter = 0;
	bmpInfo.biYPelsPerMeter = 0;
	bmpInfo.biClrImportant  = 0;
	bmpInfo.biClrUsed       = 0;

	dwBmBitsSize = ( ( Bitmap.bmWidth * wBitCount + 31 ) / 32 ) * 4 * Bitmap.bmHeight;
	hDib         = GlobalAlloc( GHND, dwBmBitsSize + dwPaletteSize + sizeof( BITMAPINFOHEADER ) );
	bmpIHea      = (LPBITMAPINFOHEADER)GlobalLock( hDib );
	(*bmpIHea)   = bmpInfo;
	hPal         = GetStockObject( DEFAULT_PALETTE );

	if ( hPal ) {
		hDC     = GetDC( NULL );
		hOldPal = SelectPalette( hDC, (HPALETTE)hPal, FALSE );

		RealizePalette( hDC );
	}

	GetDIBits( hDC, bitmap, 0, (UINT)Bitmap.bmHeight, (LPSTR)bmpIHea + sizeof( BITMAPINFOHEADER ) + dwPaletteSize, (BITMAPINFO*)bmpIHea, DIB_RGB_COLORS );

	if ( hOldPal ) {
		SelectPalette ( hDC, (HPALETTE)hOldPal, true );
		RealizePalette( hDC                          );
		ReleaseDC     ( NULL, hDC                    );
	}

	bmfHdr.bfType      = 0x4D42;
	dwDIBSize          = sizeof( BITMAPFILEHEADER ) + sizeof( BITMAPINFOHEADER ) + dwPaletteSize + dwBmBitsSize;
	bmfHdr.bfSize      = dwDIBSize;
	bmfHdr.bfReserved1 = 0;
	bmfHdr.bfReserved2 = 0;
	bmfHdr.bfOffBits   = (DWORD)sizeof( BITMAPFILEHEADER ) + (DWORD)sizeof( BITMAPINFOHEADER ) + dwPaletteSize;

	res->Write( &bmfHdr, sizeof( BITMAPFILEHEADER ) );
	res->Write( (LPSTR)bmpIHea, dwDIBSize );

	res->Position = 0;

	GlobalUnlock( hDib );
	GlobalFree  ( hDib );

	return res;
 };
HBITMAP        __stdcall BMPaHBM( TBitmap *bitmap                 ) {
	HBITMAP     result;
	Pointer     bmpPOI;
	TBitmapInfo bmpINF;
	TBitmapData bmpDAT;

	SecureZeroMemory( &bmpINF, sizeof( bmpINF ) );

	bmpINF.bmiHeader.biSize        = sizeof( TBitmapInfoHeader );
	bmpINF.bmiHeader.biPlanes      = 1;
	bmpINF.bmiHeader.biBitCount    = 32;
	bmpINF.bmiHeader.biCompression = BI_RGB;
	bmpINF.bmiHeader.biWidth       = bitmap->Width;
	bmpINF.bmiHeader.biHeight      = bitmap->Height * -1;

	if ( bmpINF.bmiHeader.biWidth  <= 0 ) bmpINF.bmiHeader.biWidth  =  1;
	if ( bmpINF.bmiHeader.biHeight >= 0 ) bmpINF.bmiHeader.biHeight = -1;

	result = CreateDIBSection( 0, &bmpINF, DIB_RGB_COLORS, &bmpPOI, 0, 0 );

	if ( bmpPOI != NULL ) {
		if ( bitmap->Map( TMapAccess::ReadWrite, bmpDAT ) ) {
			try {
				for ( int i = 0; i <= bitmap->Height - 1; i++ ) {
					Move(
						&( *PAlphaColorArray( bmpDAT.Data ) )[ i * ( bmpDAT.Pitch / 4 ) ],
						&( *PAlphaColorArray( bmpPOI      ) )[ i * bitmap->Width        ],
						bitmap->Width * 4
					);
				}
			}
			__finally {
				bitmap->Unmap( bmpDAT );
			}
		}
	}

	return result;
 };
SHDRAGIMAGE   *__stdcall BMPaDBM( TBitmap *bitmap, long x, long y ) {
	SHDRAGIMAGE *
	res                   = new SHDRAGIMAGE  ;
	res->sizeDragImage.cx = bitmap->Width    ;
	res->sizeDragImage.cy = bitmap->Height   ;
	res->ptOffset.x       = x                ;
	res->ptOffset.y       = y                ;
	res->hbmpDragImage    = BMPaHBM( bitmap );
	res->crColorKey       = RGB( 0, 0, 0 )   ;

	return res;
 };
//###########################################################################


/*General*/
HWND __stdcall GetApplicationHandle() {
	if ( !fGeneralAPH )
		fGeneralAPH = Platform::Win::ApplicationHWND();

	return fGeneralAPH;
 };
//###########################################################################
