#ifndef wDialogH
#define wDialogH

#include <System.Classes.hpp>
#include <Shobjidl.h>

#include "sFields.h"
#include "wGeneral.h"

class PACKAGE wDialog {
	/*Variables*/public:
	int             Index     ;
	String          Path      ;
	String          Title     ;
	String          Filter    ;
	String          Folder    ;
	String          Extension ;
	String          AllFiles  ;
	String          AllFormats;
	seOptionsDialog Options   ;
	sSField         Paths     ;
	//###########################################################################


	/*Creacion*/public:
	wDialog();
	//###########################################################################


	/*Metodos*/public:
	bool RunDI();
	bool RunOP();
	bool RunSA();
	//###########################################################################


	/*Funciones*/private:
	HRESULT LoadPaths ( IFileOpenDialog *dialog, sSField paths, String &path );
	HRESULT LoadFilter( IFileDialog     *dialog, String  filter              );
	HRESULT LoadFolder( IFileDialog     *dialog, String  directory           );
	//###########################################################################
 };

#endif
