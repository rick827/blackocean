#ifndef eQueryH
#define eQueryH

#include <classes.hpp>
#include "sDef.h"
#include "eBase.h"
#include "ePetition.h"

class PACKAGE eQuery {
	/*Creacion*/public:
	eQuery();
	//###########################################################################


	/*Metodos*/public:
	void Query( ePetition *petition );
	//###########################################################################


	/*Funciones*/private:
	String Select( ePetition *petition );
	String Update( ePetition *petition );
	String Insert( ePetition *petition );
	String Delete( ePetition *petition );
	String From  ( ePetition *petition );
	String Where ( ePetition *petition );
	String Order ( ePetition *petition );
	String Group ( ePetition *petition );
	String Error ( Exception *exeption );

	void Open( ePetition *petition );
	void Exec( ePetition *petition );
	//###########################################################################
 };

#endif
