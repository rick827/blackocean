#pragma hdrstop
#include "wGeneral.h"
#pragma package(smart_init)

#if defined( __WIN64__ )
//#pragma comment( lib, "Shell64.lib" )
#else
#pragma comment( lib, "Shell32.lib" )
#endif

//Otros //###################################################################
/*Variables*/
int   fGeneralPOS = 0;
int   fGeneralIND = 0;
int   fGeneralTAM = 0;
BYTE *fGeneralICO = 0;
int   fGeneralTGR = 0;
BYTE *fGeneralIGR = 0;

static HWND               wGeneralHAN = null;
static TList__1<String > *wGeneralIDS = new TList__1<String >;
static TList__1<__int64> *wGeneralPOI = new TList__1<__int64>;

/*Clases*/
class SetTimerIntervalClass { public: String id; TNotifyEvent function; bool interval; TObject *sender; };

/*Funciones*/
VOID CALLBACK onSetTimerInterval( HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime                ) {
	SetTimerIntervalClass *res = (SetTimerIntervalClass*)idEvent;

	res->function( res->sender );

	if ( !res->interval )
		StopInterval( res->id );
 };
BOOL CALLBACK onEnumResNameProc ( HMODULE hModule, LPCTSTR lpszType, LPTSTR lpszName, LONG_PTR lParam ) {
	if ( fGeneralPOS++ != fGeneralIND ) return true;

	LPICONDIR      IconDir     ;
	LPICONDIR      IconDGR     ;
	LPICONDIRENTRY IconEntry   ;
	LPICONDIRENTRY IconEntryGR ;
	LPICONIMAGE    resIconImage;
	LPGRPICONDIR   grpIconDir  ;
	DWORD          ImageOffset ;
	HRSRC          hRes        = FindResource( hModule, lpszName, lpszType );
	int            IconsCount  = 0;
	int            indImagenGR = -1;
	int            tamImagenGR = -1;

	fGeneralTAM = sizeof( ICONDIR ) - sizeof( ICONDIRENTRY );

	if ( hRes ) {
		grpIconDir = ( LPGRPICONDIR )LoadResource( hModule, hRes );
		IconsCount = grpIconDir->idCount;

		for ( int n = 0; n < IconsCount; n++ ) {
			if ( tamImagenGR != 0 && ( tamImagenGR < grpIconDir->idEntries[n].bWidth || grpIconDir->idEntries[n].bWidth == 0 ) ) {
				tamImagenGR = grpIconDir->idEntries[n].bWidth;
				indImagenGR = n;
				fGeneralTGR = sizeof( ICONDIRENTRY ) + grpIconDir->idEntries[n].dwBytesInRes;
			}

			fGeneralTAM += sizeof( ICONDIRENTRY ) + grpIconDir->idEntries[n].dwBytesInRes;
		}
	}

	if ( IconsCount == 0 ) return false;

	// Preparo la Cabecera General grpIconDir
	fGeneralICO = new BYTE[fGeneralTAM];

	setmem( fGeneralICO, 0, fGeneralTAM );

	IconDir             = ( LPICONDIR )fGeneralICO;
	IconDir->idReserved = 0;
	IconDir->idType     = 1;
	IconDir->idCount    = IconsCount;
	IconEntry           = &IconDir->idEntries[0];

	if ( indImagenGR > -1 ) {
		fGeneralTGR += sizeof( ICONDIR ) - sizeof( ICONDIRENTRY );
		fGeneralIGR  = new BYTE[fGeneralTGR];

		setmem( fGeneralIGR, 0, fGeneralTGR );

		IconDGR             = ( LPICONDIR )fGeneralIGR;
		IconDGR->idReserved = 0;
		IconDGR->idType     = 1;
		IconDGR->idCount    = 1;
		IconEntryGR         = &IconDGR->idEntries[0];
	}

	// Localizar el ICON resource en el ejecutable y sus Imagenes.
	hRes        = null;
	ImageOffset = sizeof( ICONDIR ) + ( sizeof( ICONDIRENTRY ) * ( IconDir->idCount - 1 ) );

	for ( int n = 0; n < IconsCount; n++ ) {
		int
		nID                        = grpIconDir->idEntries[n].nID;
		hRes                       = FindResource( hModule, MAKEINTRESOURCE( nID ), RT_ICON );
		resIconImage               = ( ICONIMAGE* )LoadResource( hModule, hRes );
		IconEntry[n].bWidth        = resIconImage->icHeader.biWidth;
		IconEntry[n].bHeight       = resIconImage->icHeader.biHeight / 2;
		IconEntry[n].bColorCount   = resIconImage->icHeader.biBitCount;
		IconEntry[n].bReserved     = 0;
		IconEntry[n].wPlanes       = 1;
		IconEntry[n].wBitCount     = resIconImage->icHeader.biBitCount;
		IconEntry[n].dwBytesInRes  = SizeofResource( hModule, hRes );
		IconEntry[n].dwImageOffset = ImageOffset;

		memcpy( ( BYTE* )( fGeneralICO + ImageOffset ), resIconImage, IconEntry[n].dwBytesInRes );

		if ( n == indImagenGR ) {
			IconEntryGR[0].bWidth        = resIconImage->icHeader.biWidth;
			IconEntryGR[0].bHeight       = resIconImage->icHeader.biHeight / 2;
			IconEntryGR[0].bColorCount   = resIconImage->icHeader.biBitCount;
			IconEntryGR[0].bReserved     = 0;
			IconEntryGR[0].wPlanes       = 1;
			IconEntryGR[0].wBitCount     = resIconImage->icHeader.biBitCount;
			IconEntryGR[0].dwBytesInRes  = SizeofResource( hModule, hRes );
			IconEntryGR[0].dwImageOffset = sizeof( ICONDIR );

			memcpy( ( BYTE* )( fGeneralIGR + sizeof( ICONDIR ) ), resIconImage, IconEntryGR[0].dwBytesInRes );
		}

		ImageOffset += IconEntry[n].dwBytesInRes;
	}

	return true;
 };
//###########################################################################


/*Funciones - FILES*/
DWORD     __stdcall GetFileSize( String file                                                          ) {
	DWORD  response = 0;
	void  *hfile    = CreateFile( file.w_str(), 0, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0 );

	if ( hfile > 0 )
		response = GetFileSize( hfile, null );

	CloseHandle( hfile );

	return response;
 };
TDateTime __stdcall GetDateFile( String file                                                          ) {
	TDateTime  _creationDate;
	HWND       _hfile;
	FILETIME   _fec1;
	FILETIME   _localfiletime;
	SYSTEMTIME _systime;

	_hfile = ( HWND )FileOpen( file, fmOpenRead );

	GetFileTime( _hfile , &_fec1, NULL, NULL );
	FileClose( ( int )_hfile );

	try {
		FileTimeToLocalFileTime( &_fec1         , &_localfiletime );
		FileTimeToSystemTime   ( &_localfiletime, &_systime       );

		_creationDate = TDateTime( _systime.wYear, _systime.wMonth, _systime.wDay, _systime.wHour, _systime.wMinute, _systime.wSecond, _systime.wMilliseconds );
	}
	catch ( ... ) {
		_creationDate = 0;
	}

	return _creationDate;
 };
void      __stdcall GetDateFile( String file, TDateTime &create, TDateTime &access, TDateTime &modify ) {
	HWND       _hfile;
	FILETIME   _fec1;
	FILETIME   _fec2;
	FILETIME   _fec3;
	FILETIME   _localfiletime;
	SYSTEMTIME _systime;

	_hfile = ( HWND )FileOpen( file, fmOpenRead );

	GetFileTime( _hfile , &_fec1, &_fec2, &_fec3 );
	FileClose  ( ( int )_hfile );

	try {
		FileTimeToLocalFileTime( &_fec1         , &_localfiletime );
		FileTimeToSystemTime   ( &_localfiletime, &_systime       );

		create = TDateTime( _systime.wYear, _systime.wMonth, _systime.wDay, _systime.wHour, _systime.wMinute, _systime.wSecond, _systime.wMilliseconds );
	}
	catch ( ... ) {
		create = 0;
	}

	try {
		FileTimeToLocalFileTime( &_fec2         , &_localfiletime );
		FileTimeToSystemTime   ( &_localfiletime, &_systime       );

		access = TDateTime( _systime.wYear, _systime.wMonth, _systime.wDay, _systime.wHour, _systime.wMinute, _systime.wSecond, _systime.wMilliseconds );
	}
	catch ( ... ) {
		access = 0;
	}

	try {
		FileTimeToLocalFileTime( &_fec3         , &_localfiletime );
		FileTimeToSystemTime   ( &_localfiletime, &_systime       );

		modify = TDateTime( _systime.wYear, _systime.wMonth, _systime.wDay, _systime.wHour, _systime.wMinute, _systime.wSecond, _systime.wMilliseconds );
	}
	catch ( ... ) {
		modify = 0;
	}
 };

bool __stdcall CopyFiles   ( String source, String destiny ) {
	source  += "\0\0";
	destiny += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow()        ;
	ShFileOpStruct.wFunc  = FO_COPY                  ;
	ShFileOpStruct.pFrom  = source.w_str()           ;
	ShFileOpStruct.pTo    = destiny.w_str()          ;
	ShFileOpStruct.fFlags = FOF_ALLOWUNDO | FOF_NO_UI;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };
bool __stdcall MoveFiles   ( String source, String destiny ) {
	source  += "\0\0";
	destiny += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow()        ;
	ShFileOpStruct.wFunc  = FO_MOVE                  ;
	ShFileOpStruct.pFrom  = source.w_str()           ;
	ShFileOpStruct.pTo    = destiny.w_str()          ;
	ShFileOpStruct.fFlags = FOF_ALLOWUNDO | FOF_NO_UI;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };
bool __stdcall RecycleFiles( String source                 ) {
	source += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow()        ;
	ShFileOpStruct.wFunc  = FO_DELETE                ;
	ShFileOpStruct.pFrom  = source.w_str()           ;
	ShFileOpStruct.pTo    = NULL                     ;
	ShFileOpStruct.fFlags = FOF_ALLOWUNDO | FOF_NO_UI;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };
bool __stdcall DeleteFiles ( String source                 ) {
	source += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow();
	ShFileOpStruct.wFunc  = FO_DELETE        ;
	ShFileOpStruct.pFrom  = source.w_str()   ;
	ShFileOpStruct.pTo    = NULL             ;
	ShFileOpStruct.fFlags = FOF_NO_UI        ;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };

String __stdcall ExtractExtension( String file ) {
	// C:\Users\Aion\Desktop\Arrel.txt
	String   reg = "";
	int      tam = file.Length() - 1;
	wchar_t *cad = file.w_str();

	for ( ;tam >= 0; tam-- ) {
		if ( cad[tam] == '.' ) {
			break;
		}
		else reg = String( cad[tam] ) + reg;
	}

	return reg;
 };
String __stdcall ExtractName     ( String file ) {
	// C:\Users\Aion\Desktop\Arrel.txt
	String   reg = "";
	int      tam = file.Length() - 1;
	wchar_t *cad = file.w_str();

	for ( ;tam >= 0; tam-- ) {
		if ( cad[tam] == '\\' || cad[tam] == '/' ) {
			break;
		}
		else reg = String( cad[tam] ) + reg;
	}

	return reg;
 };
String __stdcall ExtractPath     ( String file ) {
	// C:\Users\Aion\Desktop\Arrel.txt
	String   reg = "";
	int      pos = 0;
	int      tam = file.Length() - 1;
	wchar_t *cad = file.w_str();

	for ( ;tam >= 0 && cad[tam]!='\\' && cad[tam]!='/'; tam-- );
	for ( ;pos < tam; reg += cad[pos++] );

	return reg;
 };
bool   __stdcall FileExist       ( String file ) {
	DWORD fattri = GetFileAttributes( file.w_str() );

	return ( fattri != INVALID_FILE_ATTRIBUTES && !( fattri & FILE_ATTRIBUTE_DIRECTORY ) );
 };
bool   __stdcall PathExist       ( String path ) {
	DWORD fattri = GetFileAttributes( path.w_str() );

	return ( fattri != INVALID_FILE_ATTRIBUTES && ( fattri & FILE_ATTRIBUTE_DIRECTORY ) );
 };

String __stdcall CompressString( float fSize, String fFamily, float width, String path ) {
	SIZE  siz;
	HFONT fon = CreateFont( fSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH, fFamily.w_str() );
	HDC   hdc = GetDC( null );
	//bool  bno = false;

	SelectObject( hdc, fon );
	GetTextExtentPoint32( hdc, path.w_str(), path.Length(), &siz );

	if ( siz.cx > width ) {
		String rut = ExtractPath( path );
		String nom = ExtractName( path );

		do {
			if ( rut != "" ) {
				path = rut + "[...]\\" + nom;
				rut  = rut.SubString( 1, rut.Length() - 1 );
			}
			//else if ( nom != "" && !bno ) {
			//	bno  = true;
			//	path = nom;
			//}
			else if ( nom != "" ) {
				path = "[...]" + nom;
				nom  = nom.SubString( 2, nom.Length() );
			}
			else break;

			GetTextExtentPoint32( hdc, path.w_str(), path.Length(), &siz );
		}
		while ( siz.cx > width );
	}

	ReleaseDC( null, hdc );

	return path;
 };
//###########################################################################


/*Funciones - DIRECTORIOS*/
bool                     __stdcall CopyPath   ( String source, String destiny ) {
	source  += "\0\0";
	destiny += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow()        ;
	ShFileOpStruct.wFunc  = FO_COPY                  ;
	ShFileOpStruct.pFrom  = source.w_str()           ;
	ShFileOpStruct.pTo    = destiny.w_str()          ;
	ShFileOpStruct.fFlags = FOF_ALLOWUNDO | FOF_NO_UI;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };
bool                     __stdcall MovePath   ( String source, String destiny ) {
	source  += "\0\0";
	destiny += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow()        ;
	ShFileOpStruct.wFunc  = FO_MOVE                  ;
	ShFileOpStruct.pFrom  = source.w_str()           ;
	ShFileOpStruct.pTo    = destiny.w_str()          ;
	ShFileOpStruct.fFlags = FOF_ALLOWUNDO | FOF_NO_UI;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };
bool                     __stdcall RecyclePath( String source                 ) {
	source += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow()        ;
	ShFileOpStruct.wFunc  = FO_DELETE                ;
	ShFileOpStruct.pFrom  = source.w_str()           ;
	ShFileOpStruct.pTo    = NULL                     ;
	ShFileOpStruct.fFlags = FOF_ALLOWUNDO | FOF_NO_UI;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };
bool                     __stdcall DeletaPath ( String source                 ) {
	source += "\0\0";

	SHFILEOPSTRUCT ShFileOpStruct;

	memset( &ShFileOpStruct, 0, sizeof( SHFILEOPSTRUCT ) );

	ShFileOpStruct.hwnd   = GetActiveWindow();
	ShFileOpStruct.wFunc  = FO_DELETE        ;
	ShFileOpStruct.pFrom  = source.w_str()   ;
	ShFileOpStruct.pTo    = NULL             ;
	ShFileOpStruct.fFlags = FOF_NO_UI        ;

	return SHFileOperation( &ShFileOpStruct ) == 0;
 };
sSField                  __stdcall TravelPath ( String source                 ) {
	WIN32_FIND_DATA fdata   ;
	sSField         response;
	HANDLE          hfile   = FindFirstFile( ( source + "\\*.*" ).w_str(), &fdata );

	if ( hfile != INVALID_HANDLE_VALUE ) {
		do {
			response+= String( fdata.cFileName );
		}
		while( FindNextFile( hfile, &fdata ) );
	}

	FindClose( hfile );

	return response;
 };
FILE_NOTIFY_INFORMATION *__stdcall WatchPath  ( String source                 ) {
	FILE_NOTIFY_INFORMATION *dinfor;
	char                     buffer[ 256 * ( sizeof( FILE_NOTIFY_INFORMATION ) + MAX_PATH * sizeof( WCHAR ) ) ] = { 0  };
	DWORD                    bytesReturned = 0;
	HRESULT                  response         ;

	void *hdirectory = CreateFile(
		source.w_str(),
		FILE_LIST_DIRECTORY | STANDARD_RIGHTS_READ,
		FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE,
		NULL,
		OPEN_EXISTING,
		FILE_FLAG_BACKUP_SEMANTICS,
		NULL
	);

	if ( !hdirectory || hdirectory == INVALID_HANDLE_VALUE ) {
		//No s� a podido vigilar el archivo de opciones
		return null;
	}

	response = ReadDirectoryChangesW(
		hdirectory,
		buffer,
		sizeof( buffer ) / sizeof( *buffer ),
		true,
		FILE_NOTIFY_CHANGE_FILE_NAME  | FILE_NOTIFY_CHANGE_DIR_NAME    | FILE_NOTIFY_CHANGE_ATTRIBUTES | FILE_NOTIFY_CHANGE_SIZE    |
		FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_LAST_ACCESS | FILE_NOTIFY_CHANGE_CREATION   | FILE_NOTIFY_CHANGE_SECURITY,
		&bytesReturned,
		NULL,
		NULL
	) == TRUE;

	if ( response && bytesReturned ) {
		dinfor = (FILE_NOTIFY_INFORMATION*)buffer;

		// switch ( response->Action ) {
		// 	case FILE_ACTION_ADDED           :
		// 	case FILE_ACTION_REMOVED         :
		// 	case FILE_ACTION_MODIFIED        :
		// 	case FILE_ACTION_RENAMED_OLD_NAME:
		// 	case FILE_ACTION_RENAMED_NEW_NAME:
		// }
	}
	else {
		//"Fallo al inicio del monitoreo del archivo de opciones"
		return null;
	}

	CloseHandle( hdirectory );

	return dinfor;
 };
//###########################################################################


/*Funciones - PORTAPAPELES*/
void   __stdcall SetClipBoardString( AnsiString text ) {
	if ( OpenClipboard( null ) ) {
		if ( EmptyClipboard() ) {
			HGLOBAL  hbuffer;
			char    *cbuffer;

			hbuffer = GlobalAlloc( GMEM_DDESHARE, text.Length() + 1 );
			cbuffer = ( char* )GlobalLock( hbuffer );

			//strcpy_s( cbuffer, text.Length(), text.c_str() );
			strcpy( cbuffer, text.c_str() );

			GlobalUnlock    ( hbuffer          );
			SetClipboardData( CF_TEXT, hbuffer );
		}

		CloseClipboard();
	}
 };
String __stdcall GetClipBoardString(                 ) {
	String response;

	if ( OpenClipboard( null ) ) {
		void *hdata  = GetClipboardData( CF_TEXT );
		char *buffer = (char*)GlobalLock( hdata );

		GlobalUnlock  ( hdata );
		CloseClipboard(       );

		if( buffer != NULL ) response = buffer;
	}

	return response;
 };
//###########################################################################


/*Funciones - BITMAP*/
void    __stdcall OverHBitmap( HBITMAP base, HBITMAP up, RECT rup, bool alphablend ) {
	HDC     device1 = CreateCompatibleDC( null );
	HDC     device2 = CreateCompatibleDC( null );
	HBITMAP object1 = ( HBITMAP )SelectObject( device1, up   );
	HBITMAP object2 = ( HBITMAP )SelectObject( device2, base );
	BITMAP  sizeofb ; GetObject( up, sizeof( BITMAP ), &sizeofb );

	if ( alphablend ) {
		BLENDFUNCTION fblend;

		fblend.AlphaFormat         = AC_SRC_ALPHA;
		fblend.BlendFlags          = 0;
		fblend.BlendOp             = AC_SRC_OVER;
		fblend.SourceConstantAlpha = 255;

		AlphaBlend(
			device2, rup.left, rup.top, rup.right      , rup.bottom      ,
			device1, 0       , 0      , sizeofb.bmWidth, sizeofb.bmHeight,
			fblend
		);
	}
	else {
		TransparentBlt(
			device2, rup.left, rup.top, rup.right      , rup.bottom      ,
			device1, 0       , 0      , sizeofb.bmWidth, sizeofb.bmHeight,
			RGB( 1, 11, 111 )
		);
    }

	SelectObject( device1, object1 );

	//base = ( HBITMAP )SelectObject( device2, object2 );
	SelectObject( device2, object2 );

	DeleteDC( device1 );
	DeleteDC( device2 );
 };
HBITMAP __stdcall GetIconFile( String file, SIZE size                              ) {
	HBITMAP                 response = null;
	IShellItem             *child    = null;
	IShellItemImageFactory *factory  = null;

	if ( SHCreateItemFromParsingName( file.w_str(), 0, IID_IShellItem, (void**)&child ) == S_OK ) {
		if ( child->QueryInterface( IID_IShellItemImageFactory, (void**)&factory ) == S_OK ) {
			factory->GetImage( size, SIIGBF_ICONONLY | SIIGBF_ICONONLY, &response );
			factory->Release();
		}

		child->Release();
	}

	return response;
 };
HICON   __stdcall HBMPaHICON ( HBITMAP bitmap                                      ) {
	BITMAP      size ; GetObject( bitmap, sizeof( BITMAP ), &size );
	HICON       icon;
	HBITMAP     mask = CreateCompatibleBitmap( GetDC( null ), size.bmWidth, size.bmHeight );
	ICONINFO    info = {0};

	info.fIcon    = true  ;
	info.hbmColor = bitmap;
	info.hbmMask  = mask  ;
	icon          = ::CreateIconIndirect( &info );

	DeleteObject( mask );

	return icon;
 };

TMemoryStream *__stdcall GetIconStream( String path, int index ) {
	TMemoryStream *mem = new TMemoryStream;
	HMODULE        hmo = LoadLibrary( path.w_str() );

	if ( hmo ) {
		fGeneralPOS = 0;
		fGeneralIND = index;
		fGeneralTAM = 0;
		fGeneralICO = null;

		EnumResourceNames( hmo, RT_GROUP_ICON, onEnumResNameProc, 0 );
		FreeLibrary      ( hmo );

		//mem->Position = 0;
		//mem->Write( fGeneralICO, fGeneralTAM );

		mem->Position = 0;
		mem->Write( fGeneralIGR, fGeneralTGR );

		delete []fGeneralICO;
	}

	return mem;
 };
//###########################################################################


/*Funciones - APLICACION*/
String __stdcall Run  ( String exe, String parameters            ) {
	switch ( ( int )ShellExecute( null, L"open", exe.w_str(), parameters.w_str(), null, SW_NORMAL ) ){
		case 0                     : return "El sistema no tiene suficiente memoria o recursos.";
		case ERROR_FILE_NOT_FOUND  : return "Archivo no encontrado.";
		case ERROR_PATH_NOT_FOUND  : return "Directorio no encontrado.";
		case ERROR_BAD_FORMAT      : return "El archivo. exe no es v�lido (no es Win32 o Corrupci�n exe).";
		case SE_ERR_ACCESSDENIED   : return "El sistema operativo deniega el acceso al archivo especificado.";
		case SE_ERR_ASSOCINCOMPLETE: return "La asociaci�n de nombre de archivo est� incompleto o no v�lido.";
		case SE_ERR_DDEBUSY        : return "La transacci�n DDE no se pudo completar debido a otras operaciones DDE estaban siendo procesados.";
		case SE_ERR_DDEFAIL        : return "La transacci�n DDE no se pudo completar.";
		case SE_ERR_DDETIMEOUT     : return "La transacci�n DDE no se pudo completar porque la solicitud ha agotado el tiempo.";
		case SE_ERR_DLLNOTFOUND    : return "La DLL especificada no se encontr�.";
		//case SE_ERR_FNF            : return "El archivo especificado no se encontr�.";
		case SE_ERR_NOASSOC        : return "No hay ninguna aplicaci�n asociada con la extensi�n de nombre de archivo dado. "
											"Este error tambi�n ser� devuelto si se intenta imprimir un archivo que no se puede imprimir.";
		case SE_ERR_OOM            : return "No hab�a suficiente memoria para completar la operaci�n.";
		//case SE_ERR_PNF            : return "La ruta especificada no se encontr�.";
		case SE_ERR_SHARE          : return "Se produjo una violaci�n de acceso.";
	}

	return "";
 };
bool   __stdcall RunAs( String exe, String parameters, bool wait ) {
	SHELLEXECUTEINFO shex;

	ZeroMemory( &shex, sizeof( shex ) );

	shex.cbSize       = sizeof( SHELLEXECUTEINFO );
	shex.fMask        = SEE_MASK_NOCLOSEPROCESS   ;
	shex.hwnd         = 0                         ;
	shex.lpVerb       = L"runas"                  ;
	shex.lpFile       = exe.w_str()               ;
	shex.lpDirectory  = 0                         ;
	shex.nShow        = SW_HIDE                   ;
	shex.hInstApp     = 0                         ;
	shex.lpParameters = parameters.w_str()        ;

	if ( !wait ) return ShellExecuteExW( &shex ) == TRUE;
	else {
		ShellExecuteEx( &shex );
		WaitForSingleObject( shex.hProcess, INFINITE );
		return true;
	}
 };
//###########################################################################


/*Funciones - LINK*/
void   __stdcall SolveLink   ( AnsiString path, PInfoLink info                                         ) {
	IShellLink *pShellLink = null;
	HRESULT     response   = CoCreateInstance( CLSID_ShellLink, null, CLSCTX_INPROC_SERVER, IID_IShellLink, ( void** )&pShellLink );

	if ( SUCCEEDED( response ) ) {
		IPersistFile *pPersistFile = null;

		response = pShellLink->QueryInterface( IID_IPersistFile, ( void** )&pPersistFile );

		if ( SUCCEEDED( response ) ) {
			wchar_t loadPath[ MAX_PATH ];

			MultiByteToWideChar( CP_ACP, 0, path.c_str(), -1, loadPath, MAX_PATH );

			response = pPersistFile->Load( loadPath, STGM_READ );

			if ( SUCCEEDED( response ) ) {
				WIN32_FIND_DATA WFDD;

				/*response = */pShellLink->Resolve( null, SLR_NO_UI );

				//if ( NOERROR == response ){
					wchar_t resultPath[ MAX_PATH ];
					pShellLink->GetPath( resultPath, MAX_PATH, &WFDD, SLGP_UNCPRIORITY );
					info->Destiny = resultPath;

					pShellLink->GetArguments( resultPath, MAX_PATH );
					info->Arguments = resultPath;

					int index;
					pShellLink->GetIconLocation( resultPath, MAX_PATH, &index );
					info->PathIcon  = resultPath;
					info->IndexIcon = index;

					pShellLink->GetShowCmd( &index );
					info->VisualMode = index;

					pShellLink->GetWorkingDirectory( resultPath, MAX_PATH );
					info->WorkPath = resultPath;

					pShellLink->GetDescription( resultPath, MAX_PATH );
					info->Description = resultPath;

					if ( !FileExist( info->Destiny ) ) {
						String pat = info->WorkPath;
						pat = pat + "\\" + ExtractName( info->Destiny );
                        pat = StringReplace( pat, "\\\\", "\\", TReplaceFlags() << rfReplaceAll );

						if ( FileExist( pat ) ) info->Destiny = pat;
					}

					if ( !FileExist( info->PathIcon ) ) {
						String pat = info->WorkPath;
						pat = pat + "\\" + ExtractName( info->PathIcon );
						pat = StringReplace( pat, "\\\\", "\\", TReplaceFlags() << rfReplaceAll );

						if ( FileExist( pat ) ) info->PathIcon = pat;
					}
				//}
			}

			pPersistFile->Release();
		}

		pShellLink->Release();
	}
 };
bool   __stdcall CreateLink  ( String path, AnsiString pathLink, String description, String parameters ) {
	IShellLink *plink    ;
	HRESULT     response = CoCreateInstance( CLSID_ShellLink, null, CLSCTX_INPROC_SERVER, IID_IShellLink, (LPVOID*)&plink );

	if ( SUCCEEDED( response ) ) {
		IPersistFile *pfile;

		plink->SetPath       ( path.w_str()        );
		plink->SetDescription( description.w_str() );

		response = plink->QueryInterface( IID_IPersistFile, (LPVOID*)&pfile );

		if ( SUCCEEDED( response ) ) {
			WCHAR savePath[MAX_PATH];

			MultiByteToWideChar( CP_ACP, 0, pathLink.c_str(), -1, savePath, MAX_PATH );

			response = pfile->Save( savePath, TRUE );

			pfile->Release();
		}

		plink->Release();
	}

	return SUCCEEDED( response );
 };
String __stdcall DestinyLink ( AnsiString path                                                         ) {
	String      response   ;
	IShellLink *pShellLink = null;
	HRESULT     oresult    = CoCreateInstance( CLSID_ShellLink, null, CLSCTX_INPROC_SERVER, IID_IShellLink, ( void** )&pShellLink );

	if ( SUCCEEDED( oresult ) ) {
		IPersistFile *pPersistFile = null;

		oresult = pShellLink->QueryInterface( IID_IPersistFile, ( void** )&pPersistFile );

		if ( SUCCEEDED( oresult ) ) {
			wchar_t loadPath[ MAX_PATH ];

			MultiByteToWideChar( CP_ACP, 0, path.c_str(), -1, loadPath, MAX_PATH );

			oresult = pPersistFile->Load( loadPath, STGM_READ );

			if ( SUCCEEDED( oresult ) ) {
				WIN32_FIND_DATA WFDD;

				oresult = pShellLink->Resolve( null, SLR_NO_UI );

				if ( NOERROR == oresult ) {
					wchar_t resultPath[ MAX_PATH ];

					pShellLink->GetPath( resultPath, MAX_PATH, & WFDD, SLGP_UNCPRIORITY );

					response = resultPath;
				}
			}

			pPersistFile->Release();
		}

		pShellLink->Release();
	}

	return response;
 };
void   __stdcall PinToTaskBar( String path, bool unpin                                                 ) {
	// wchar_t   verbsName[ MAX_PATH ];
	// HINSTANCE dinstance = LoadLibrary( L"Shell32.dll" );

	// LoadString( dinstance, unpin == false ? 5386/*Pin*/ : 5387/*Unpin*/, verbsName, MAX_PATH );
	// FreeLibrary( dinstance );

	// CComPtr<IShellDispatch > pShellDisp;
	// CComPtr<Folder         > folder_ptr;
	// CComPtr<FolderItem     > folder_item_ptr;
	// CComPtr<FolderItemVerbs> folder_item_verbs_ptr;

	// HRESULT retorno = CoCreateInstance( CLSID_Shell, null, CLSCTX_SERVER,IID_IDispatch, (LPVOID *) &pShellDisp );

	// do  {
	// 	IF_FAILED_OR_NULL_BREAK( retorno, pShellDisp );
	// 	//retorno = pShellDisp->NameSpace( _variant_t( ExtraerRuta( path ) ), &folder_ptr );
	// 	retorno = pShellDisp->NameSpace( CComVariant( ExtraerRuta( path ) ), &folder_ptr );

	// 	IF_FAILED_OR_NULL_BREAK( retorno, folder_ptr );
	// 	retorno = folder_ptr->ParseName( CComBSTR( ExtraerNombre( path ) ), &folder_item_ptr );

	// 	IF_FAILED_OR_NULL_BREAK( retorno, folder_item_ptr );
	// 	retorno = folder_item_ptr->Verbs( &folder_item_verbs_ptr );

	// 	IF_FAILED_OR_NULL_BREAK( retorno, folder_item_verbs_ptr );

	// 	long count = 0;

	// 	folder_item_verbs_ptr->get_Count( &count );

	// 	for ( long i = 0; i < count ; ++i ) {
	// 		FolderItemVerb *item_verb = null;

	// 		//retorno = folder_item_verbs_ptr->Item( _variant_t( i ), &item_verb );
	// 		retorno = folder_item_verbs_ptr->Item( CComVariant( i ), &item_verb );

	// 		if ( SUCCEEDED( retorno ) && item_verb ) {
	// 			CComBSTR name;

	// 			item_verb->get_Name( &name );

	// 			if ( _wcsnicmp( name, verbsName, wcslen( verbsName ) ) == 0 ) {
	// 				item_verb->DoIt();
	// 				break;
	// 			}
	// 		}
	// 	}
	// }
	// while ( 0 );

	// CoUninitialize();
 };
//###########################################################################


/*Funciones - GENERALES*/
HWND   __stdcall GetHandle      ( HWND handle ) {
	if ( handle ) wGeneralHAN = handle;

	return wGeneralHAN;
 };
HWND   __stdcall GetGlobalHandle( HWND handle ) {
	if ( handle ) wGeneralHAN = handle;

	return wGeneralHAN;
 };
String __stdcall GetAppPath     (             ) {
	if ( appPath == "" ) {
		wchar_t resultPath[ MAX_PATH + 1 ];

		GetModuleFileName( null, resultPath, MAX_PATH );

		appPath = resultPath;
	}

	return appPath;
 };
String __stdcall GetAppDirectory(             ) {
	if ( appDirectory == "" )
		appDirectory = ExtractPath( GetAppPath() );

	return appDirectory;
 };
String __stdcall GetAppExtension(             ) {
	if ( appExtension == "" )
		appExtension = ExtractName( GetAppPath() );

	return appExtension;
 };
String __stdcall GetAppName     (             ) {
	if ( appName == "" ) {
		appName = GetAppExtension();
		appName = StringReplace( appName, ExtractExtension( appName ), "", TReplaceFlags() << rfReplaceAll );
	}

	return appName;
 };
String __stdcall GetTemporary   (             ) {

	if ( appTemporary == "" ) {
		wchar_t cad[MAX_PATH];

		GetTempPathW( MAX_PATH, cad );

		appTemporary = String( cad );
	}

	return appTemporary;
 };
String __stdcall GetPathIcon    ( String path ) {
	int     ind;
	String  pat = path;
	String  ext = ExtractFileExt( path ).LowerCase();
	wchar_t exp[MAX_PATH];

	ExpandEnvironmentStrings( pat.w_str(), exp, MAX_PATH );

	pat = (String)exp;
	ind = -1000;

	if ( pat.Pos( "*" ) ) {
		pat = StringReplace( pat, "*", "", TReplaceFlags() << rfReplaceAll ).LowerCase();
		ext = pat;
	}

	/*Archivo*/
	if ( pat == ext || ( FileExists( pat ) && ext != ".exe" && ext != ".ico" ) ) {
		wRegistry *reg = new wRegistry;

		reg->HKey = HKEY_CLASSES_ROOT;
		reg->Pass = KEY_READ;

		if ( reg->OpenKey( ext, false ) ) {
			pat = reg->DefStr( "", "" );

			if ( pat != "" ) {
				reg->CloseKey();
				reg->OpenKey( HKEY_CLASSES_ROOT, pat + "\\DefaultIcon", false );

				pat = reg->DefStr( "", "" );

				if ( pat.Pos( "," ) ) {
					ind = sai( pat.SubString( pat.Pos( "," ) + 1, pat.Length() ) );
					pat = pat.SubString( 1, pat.Pos( "," ) -1 );
				}

				pat = StringReplace( pat, "\"", "", TReplaceFlags() << rfReplaceAll );
			}

			delete reg;

			if ( pat != "" ) {
				SecureZeroMemory( &exp, MAX_PATH );
				ExpandEnvironmentStrings( pat.w_str(), exp, MAX_PATH );
				pat = (String)exp;

				if ( !FileExists( pat ) ) goto salto1;

				if ( ind != -1000 ) path = pat + "|" + (String)ind;
				else                path = pat;
			}
			else {
				salto1:
				SecureZeroMemory( &exp, MAX_PATH );
				ExpandEnvironmentStrings( L"%SystemRoot%\\system32\\imageres.dll", exp, MAX_PATH );
				path = (String)exp + "|2";
			}
		}
		else return "";
	}

	/*Carpeta*/
	else if ( DirectoryExists( path ) ) {
		if ( FileExists( path + "\\desktop.ini" ) ) {
			TIniFile *ini = new TIniFile( path + "\\desktop.ini" );

			ind = -1;
			pat = ini->ReadString( ".ShellClassInfo", "IconResource", "" );

			delete ini;

			if ( pat.Pos( "," ) ) {
				ind = sai( pat.SubString( pat.Pos( "," ) + 1, pat.Length() ) );
				pat = pat.SubString( 1, pat.Pos( "," ) - 1 );
			}

			SecureZeroMemory( &exp, MAX_PATH );
			ExpandEnvironmentStrings( pat.w_str(), exp, MAX_PATH );

			pat = (String)exp;

			if ( !FileExists( pat ) ) goto salto2;
		}
		else {
			salto2:
			SecureZeroMemory( &exp, MAX_PATH );
			ExpandEnvironmentStrings( L"%SystemRoot%\\system32\\imageres.dll", exp, MAX_PATH );
			pat = (String)exp;
			ind = 3;
		}

		path = pat + "|" + ( String )ind;
	}

	/*Otros*/
	else path = pat;

	return path;
 };
String __stdcall AccentABat     ( String text ) {
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );

	text = StringReplace( text, "�", "�" , TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�"  , TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�" , TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�" , TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�" , TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�" , TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�" , TReplaceFlags() << rfReplaceAll );

	return text;
 };
String __stdcall BatAAccent     ( String text ) {
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );

	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );
	text = StringReplace( text, "�", "�", TReplaceFlags() << rfReplaceAll );

	return text;
 };

sSField __stdcall GetTagsGroup( String allString ) {
	int       pos = 0;
	int       siz = allString.Length();
	wchar_t  *cad = allString.w_str();
	String    tem ;
	sSField   res ;

	for ( ;pos<siz && ( pos==0 || cad[pos]=='|' ); pos++ ) {
		if ( cad[pos]=='|' ) pos++;

		for ( tem = ""; pos<siz && cad[pos]!='|'; pos++ )
			tem += cad[pos];

		pos--;

		if ( tem != "" ) res+= tem;
	}

	return res;
 };

String __stdcall LastError() {
	LPWSTR string;

	if (
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			null,
			GetLastError(),
			0,
			( LPTSTR )&string,
			0,
			NULL
		) == 0
	) return "Fallo";

	else return string;
 };

void __stdcall SetTimeout  ( String id, float time, TNotifyEvent function, TObject *sender ) {
	StopInterval( id );

	SetTimerIntervalClass *
	ti           = new SetTimerIntervalClass;
	ti->id       = id;
	ti->sender   = sender;
	ti->function = function;
	ti->interval = false;

	wGeneralIDS->Add(          id );
	wGeneralPOI->Add( (__int64)ti );

	SetTimer( GetHandle(), (int)ti, time * 1000, onSetTimerInterval );
 };
void __stdcall SetInterval ( String id, float time, TNotifyEvent function, TObject *sender ) {
	StopInterval( id );

	SetTimerIntervalClass *
	ti           = new SetTimerIntervalClass;
	ti->id       = id;
	ti->sender   = sender;
	ti->function = function;
	ti->interval = true;

	wGeneralIDS->Add(          id );
	wGeneralPOI->Add( (__int64)ti );

	SetTimer( GetHandle(), (int)ti, time * 1000, onSetTimerInterval );
 };
void __stdcall StopInterval( String id                                                     ) {
	for ( int i = 0; i < wGeneralIDS->Count; i++ ) {
		if ( wGeneralIDS->Items[i] == id ) {
			KillTimer( GetHandle(), wGeneralPOI->Items[i] );

			delete (SetTimerIntervalClass*)wGeneralPOI->Items[i];

			wGeneralIDS->Delete( i );
			wGeneralPOI->Delete( i );
			break;
		}
	}
 };

bool __stdcall ChangeIconExe( wchar_t *pathIcon, wchar_t *exe, wchar_t *nameRes, bool deletePrevious ) {
	HANDLE hFile;
	LPBYTE lpBuffer;
	LPBYTE lpResLock;
	bool   result = true;

	if( !pathIcon || !*pathIcon ) return false;
	if( !exe      || !*exe      ) return false;
	if( !nameRes  || !*nameRes  ) return false;

	// Abrimos el icono.ico en modo de lectura binaria
	DWORD dwFileSize, dwBytesRead;

	hFile = CreateFile( pathIcon, GENERIC_READ, 0, null, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, null );

	if ( INVALID_HANDLE_VALUE != hFile ) {
		dwFileSize = GetFileSize( hFile, null );
		lpBuffer   = new BYTE[dwFileSize];

		ReadFile( hFile, lpBuffer, dwFileSize, &dwBytesRead, null );
		CloseHandle( hFile );
	}

	// Convierto el Buffer a formato Icono con cabeceras...
	ICONDIR *iconDir = (ICONDIR*)lpBuffer;

	// Crea espacio para las cabeceras del icono recurso
	int         headerSize    = sizeof( GRPICONDIR ) + ( sizeof( GRPICONDIRENTRY ) * ( iconDir->idCount - 1 ) );
	BYTE       *headerIconRec = new BYTE[headerSize];
	GRPICONDIR *grpIconDir    = (GRPICONDIR*)headerIconRec;

	// Copio la cabecera com�n con iconDir
	memcpy( grpIconDir, iconDir, sizeof( GRPICONDIR ) - sizeof( GRPICONDIRENTRY ) );

	// Abrimos el recurso del exe
	HANDLE hUpdateRes = BeginUpdateResource( exe, deletePrevious );

	for( int n = 0; n < grpIconDir->idCount; n++ ){
		// Copio las cabeceras
		memcpy( &grpIconDir->idEntries[n], &iconDir->idEntries[n], sizeof( GRPICONDIRENTRY ) );
		grpIconDir->idEntries[n].nID = n + 100;

		// Localizo la imagen del icono a pasar al .exe
		BYTE *iconImage = (BYTE*)iconDir + iconDir->idEntries[n].dwImageOffset;
		lpResLock       = (BYTE*)LockResource( iconImage );

		// Abrir el fichero donde a�adir el icono.
		if ( hUpdateRes != NULL ) {
			// Actualizar el resource destino
			result &= UpdateResource(
				hUpdateRes
				, RT_ICON
				, MAKEINTRESOURCE( grpIconDir->idEntries[n].nID )
				, MAKELANGID( LANG_NEUTRAL, SUBLANG_NEUTRAL )
				, lpResLock
				, iconDir->idEntries[n].dwBytesInRes
			);
		}
	}

	// Y la grabamos comp "indice" o cabecera de grupo de iconos
	result &= UpdateResource(
		hUpdateRes
		, RT_GROUP_ICON
		, nameRes
		, MAKELANGID( LANG_NEUTRAL, SUBLANG_NEUTRAL )
		, grpIconDir
		, headerSize
	);

	// Escribir los cambios y cerrar.
	EndUpdateResource( hUpdateRes, false );

	delete lpBuffer;
	delete headerIconRec;
	return result;
 };

int __stdcall ShowPopUp( float x, float y, int number, ... ) {
	HMENU    hme = CreatePopupMenu();
	int      ind;
	wchar_t *cad;
	va_list  lis;

	va_start( lis, number );

	for ( int i = 0; i < number; i++ ) {
		ind = va_arg( lis, int );
		cad = va_arg( lis, wchar_t* );

		MENUITEMINFO mii;

		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask  = MIIM_ID | MIIM_TYPE ;
		mii.wID    = ind                 ;

		if ( ind != snan ) {
			mii.fType      = MFT_STRING;
			mii.dwTypeData = cad       ;
		}
		else mii.fType = MFT_SEPARATOR;

		InsertMenuItem( hme, ind, false, &mii );
	}

	return TrackPopupMenu( hme, TPM_RETURNCMD, x, y, 0, GetHandle(), null );
 };
//###########################################################################
