unit sField;

interface
  uses System.Generics.Collections;

type
  sField<T> = class(TList<T>)
  public
    // Anchors constructor/destructor
    constructor Create;
    destructor Destroy; override;

    class procedure Cleanup( var L: sField<T> ); static;
  end;

  // Herencias
  sFieldByt = class( sField<BYTE      > ) end;
  sFieldInt = class( sField<integer   > ) end;
  sFieldDou = class( sField<double    > ) end;
  sFieldStr = class( sField<string    > ) end;
  sFieldAns = class( sField<AnsiString> ) end;
  sFieldUTF = class( sField<UTF8String> ) end;
  sFieldI64 = class( sField<int64     > ) end;

implementation

class procedure sField<T>.Cleanup(var L: sField<T>);
begin
  L.Free;
end;

constructor sField<T>.Create;
begin
  inherited;
end;

destructor sField<T>.Destroy;
begin
  inherited;
end;

end.
