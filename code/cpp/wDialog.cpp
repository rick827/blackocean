#pragma hdrstop
#include "wDialog.h"
#pragma package(smart_init)


// class PACKAGE wDialog //##################################################
/*Creacion - public*/
wDialog::wDialog() {
	Index      = 0;
	Path       = "";
	Title      = "";
	Filter     = "";
	Folder     = "";
	Extension  = "";
	AllFiles   = "";
	AllFormats = "";
	Options    = seOptionsDialog();
 };
//###########################################################################


/*Metodos - public*/
bool wDialog::RunDI() {
	IShellItem  *child  ;
	IFileDialog *dialog ;
	PWSTR        filter = null;
	bool         result = false;

	if ( SUCCEEDED( CoInitializeEx( null, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE ) ) ) {
		if ( SUCCEEDED( CoCreateInstance( CLSID_FileOpenDialog, null, CLSCTX_INPROC_SERVER, IID_PPV_ARGS( &dialog ) ) ) ) {
			if ( SUCCEEDED( dialog->SetTitle( Title.w_str() ) ) )                                                           // Titulo
				if ( SUCCEEDED( dialog->SetOptions( Options | odSeleccionarCarpetas ) ) )                                   // Opciones
					if ( SUCCEEDED( dialog->SetFileName( Path.w_str() ) ) )                                                 // Carpeta
						if ( SUCCEEDED( LoadFolder( dialog, Folder ) ) )                                                    // Carpeta Default
							if ( SUCCEEDED( dialog->Show( GetHandle() ) ) )                                                 // Mostrar Dialogo
								if ( SUCCEEDED( dialog->GetResult( &child ) ) ) {
									if ( SUCCEEDED( child->GetDisplayName( SIGDN_FILESYSPATH, &filter ) ) ) {
										Path   = filter;
										result = true;
									}

									child->Release();
								}

			dialog->Release();
		}

		CoUninitialize();
	}

	return result;
 };
bool wDialog::RunOP() {
	IFileOpenDialog *dialog ;
	unsigned int     index  ;
	bool             result = false;

	if ( SUCCEEDED( CoInitializeEx( null, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE ) ) ) {
		if ( SUCCEEDED( CoCreateInstance( CLSID_FileOpenDialog, null, CLSCTX_INPROC_SERVER, IID_PPV_ARGS( &dialog ) ) ) ) {
			if ( SUCCEEDED( dialog->SetTitle( Title.w_str() ) ) )                                                           // Titulo
				if ( SUCCEEDED( LoadFilter( dialog, Filter ) ) )                                                            // Filtro
					if ( SUCCEEDED( dialog->SetFileTypeIndex( Index + 1 ) ) || Filter == "" )                               // Indice Filtro
						if ( SUCCEEDED( dialog->SetOptions( Options ) ) )                                                   // Opciones
							if ( SUCCEEDED( dialog->SetFileName( Path.w_str() ) ) )                                         // Path
								if ( SUCCEEDED( LoadFolder( dialog, Folder ) ) )                                            // Carpeta Default
									if ( SUCCEEDED( dialog->Show( GetHandle() ) ) )                                         // Mostrar dialog
										if ( SUCCEEDED( LoadPaths( dialog, Paths, Path ) ) )                                // Path Regresulto
										{
											if ( SUCCEEDED( dialog->GetFileTypeIndex( &index ) ) ) {
												Index       = index - 1;
												sSField fie = GetTagsGroup( Filter );

												if ( Index >= 0 && Index < fie.Size ) {
													Extension = fie[Index];
													Extension = StringReplace( Extension, "*", "", TReplaceFlags() << rfReplaceAll );
													Extension = StringReplace( Extension, ".", "", TReplaceFlags() << rfReplaceAll );
												}
											}

											result = true;
										}

			dialog->Release();
		}

		CoUninitialize();
	}

	return result;
 };
bool wDialog::RunSA() {
	IShellItem      *child  ;
	IFileSaveDialog *dialog ;
	PWSTR            filter = null;
	bool             result = false;
	unsigned         index  ;

	if ( SUCCEEDED( CoInitializeEx( null, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE ) ) ) {
		if ( SUCCEEDED( CoCreateInstance( CLSID_FileSaveDialog, null, CLSCTX_INPROC_SERVER, IID_PPV_ARGS( &dialog ) ) ) ) {
			if ( SUCCEEDED( dialog->SetTitle( Title.w_str() ) ) )                                // Titulo
				if ( SUCCEEDED( LoadFilter( dialog, Filter ) ) )                                 // Filtro
					if ( SUCCEEDED( dialog->SetFileTypeIndex( Index + 1 ) ) || Filter == "" )    // Indice Filtro
						if ( SUCCEEDED( dialog->SetOptions( Options ) ) )                        // Opciones
							if ( SUCCEEDED( dialog->SetFileName( Path.w_str() ) ) )              // Path
								if ( SUCCEEDED( LoadFolder( dialog, Folder ) ) )                 // Carpeta Default
									if ( SUCCEEDED( dialog->Show( GetHandle() ) ) )              // Mostrar Dialogo
										if ( SUCCEEDED( dialog->GetResult( &child ) ) ) {
											if ( SUCCEEDED( child->GetDisplayName( SIGDN_FILESYSPATH, &filter ) ) ) {
												Path = filter;

												if ( SUCCEEDED( dialog->GetFileTypeIndex( &index ) ) ) {
													Index       = index - 1;
													sSField fie = GetTagsGroup( Filter );

													if ( Index > -1 && Index < fie.Size ) {
														Extension = fie[Index];
														Extension = StringReplace( Extension, "*", "", TReplaceFlags() << rfReplaceAll );
													}

													if ( ExtractFileExt( Path ).LowerCase() != Extension.LowerCase() )
														Path+= Extension;
												}

												result = true;
											}

											child->Release();
										}

			dialog->Release();
		}

		CoUninitialize();
	}

	return result;
 };
//###########################################################################


/*Funciones - private*/
HRESULT wDialog::LoadPaths ( IFileOpenDialog *dialog, sSField paths, String &path ) {
	if ( !dialog ) return null;

	path = "";

	paths.Clear();

	IShellItemArray *list   = null;
	HRESULT          result = dialog->GetResults( &list );

	if ( SUCCEEDED( result ) ) {
		unsigned long number;

		result = list->GetCount( &number );

		if ( SUCCEEDED( result ) ) {
			for ( unsigned long i = 0; i < number; i++ ) {
				IShellItem*
				points = null;
				result = list->GetItemAt( i, &points );

				if ( SUCCEEDED( result ) ) {
					PWSTR
					_path  = null;
					result = points->GetDisplayName( SIGDN_FILESYSPATH, &_path );

					if ( SUCCEEDED( result ) ) paths += _path;

					points->Release();
				}
			}

			if ( paths.Size )
				path = paths[ 0 ].Value;
		}
	}

	list->Release();

	return result;
 };
HRESULT wDialog::LoadFilter( IFileDialog     *dialog, String  filter              ) {
	/*
		*.bp|Paleta de Colores|*.ase|Adobe Swatch Exchange
	*/
	int      pos = 0;
	int      len = filter.Length();
	wchar_t *str = filter.w_str();
	sSField  ext ;
	sSField  nam ;
	String   tem = "";
	bool     bex = true;

	for ( ;pos<len; pos++ ) {
		if ( str[pos] == '|' || pos+1>=len ) {
			if ( !( bex = !bex ) ) ext+= tem;
			else                   nam+= tem;

			tem = "";
		}
		else tem+= str[pos];
	}

	if ( ext.Size ) {
		int                tot    = ext.Size;
		String             all    = "";
		COMDLG_FILTERSPEC *filest = new COMDLG_FILTERSPEC[ ext.Size + int(AllFormats != "" ? 1 : 0) + int(AllFiles != "" ? 1 : 0) ];

		for ( int i = 0; i < ext.Size; i++ ) {
			all              += ( all != "" ? ";" : "" ) + ext[i].Value;
			filest[i].pszSpec = ext[i].Value.w_str();
			filest[i].pszName = nam[i].Value.w_str();
		}

		if ( AllFormats != "" ) {
			filest[tot  ].pszSpec = all.w_str();        // Extencion
			filest[tot++].pszName = AllFormats.w_str(); // Nombre
		}

		if ( AllFiles != "" ) {
			filest[tot  ].pszSpec = L"*.*";           // Extencion
			filest[tot++].pszName = AllFiles.w_str(); // Nombre
		}

		return dialog->SetFileTypes( tot, filest );
    }
	else return S_OK;
 };
HRESULT wDialog::LoadFolder( IFileDialog     *dialog, String  directory           ) {
	if ( !dialog         ) return null;
	if ( directory == "" ) return S_OK;

	typedef HRESULT ( WINAPI *sSHCIFPN )( PCWSTR pszPath, IBindCtx *pbc, REFIID riid, void **ppv );

	HRESULT     result   = null;
	HINSTANCE   instance = LoadLibrary( L"shell32.dll" );
	IShellItem *child    = null;
	sSHCIFPN    shcifpn  ;

	if ( instance ) {
		shcifpn = ( sSHCIFPN )GetProcAddress( instance, "SHCreateItemFromParsingName" );

		if ( shcifpn ) {
			if ( SUCCEEDED( result = shcifpn( directory.w_str(), null, IID_PPV_ARGS( &child ) ) ) ) {
				result = dialog->SetDefaultFolder( child );

				child->Release();
			}
		}

		FreeLibrary( instance );
	}

	return result;
 };
//###########################################################################