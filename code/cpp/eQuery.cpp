#pragma hdrstop
#include "eQuery.h"
#pragma package(smart_init)


/*********************************( Otros )*********************************/
/*Definiciones*/
#define numberdata 300
//###########################################################################



/*********************************( eQuery )*********************************/
/*Creacion - public*/
eQuery::eQuery() {};
//###########################################################################


/*Metodos - public*/
void eQuery::Query( ePetition *_petition ) {
	if      ( pe["select"].Str != "" ) Open( _petition );
	else if ( pe["update"].Str != "" ) {}
	else if ( pe["insert"].Str != "" ) {}
	else if ( pe["delete"].Str != "" ) {}
	else sene( "Error en consulta", "1", "No se encontr� la funci�n o la funci�n est� mal escrita." );
 };
//###########################################################################


/*Funciones - private*/
String eQuery::Select( ePetition *_petition ) {
	/*select : 'sum(vo_pri) as num, vo_pan'*/
	return " select " + pe["select"].Str + " ";
 };
String eQuery::Update( ePetition *_petition ) { return ""; };
String eQuery::Insert( ePetition *_petition ) { return ""; };
String eQuery::Delete( ePetition *_petition ) { return ""; };
String eQuery::From  ( ePetition *_petition ) {
	/*from: 'mv_do_vo_votos'*/
	String fro = pe["from"].Str;
	String res = "";

	if ( fro == "" ) throw EArgumentException( "La propiedad �from� no puede estar vac�a" );
	else {
		res = "from " + fro;
	}

	return " " + res + " ";
 };
String eQuery::Where ( ePetition *_petition ) {
	/*where: `vo_clave = '1'`*/
	String whe = pe["where"].Str;
	String res = "";

	if ( whe != "" ) {
		res = "where " + whe;
	}

	return " " + res + " ";
 };
String eQuery::Order ( ePetition *_petition ) {
	/*vo_pri asc*/
	String ord = pe["order"].Str;
	String res = "";

	if ( ord != "" ) {
		res = "order by " + ord;
	}

	return " " + res + " ";
 };
String eQuery::Group ( ePetition *_petition ) {
	/*vo_pri, vo_pan*/
	String gru = pe["group"].Str;
	String res = "";

	if ( gru != "" ) {
		res = "group by " + gru;
    }

	return " " + res + " ";
 };
String eQuery::Error ( Exception *_exeption ) {
	/*[FireDAC][Phys][ODBC][Microsoft][SQL Server Native Client 11.0][SQL Server]Incorrect syntax near 'vo_pri'.*/
	wchar_t *cad = _exeption->Message.w_str();
	int      len = _exeption->Message.Length();
	int      pos = len - 1;
	String   res = "";

	for ( ;pos >= 0; pos-- ) {
		if ( cad[pos]==']' ) {
			break;
		}
		else res = String(cad[pos]) + res;
	}

	return res;
 };

void eQuery::Open( ePetition *_petition ) {
	TFDQuery *que = gbas.NewQuery();
	int       gst = pe["start"].Num;
	int       gco = pe["count"].Num;

	try {
		que->Open( Select(_petition) + From(_petition) + Where(_petition) + Group(_petition) + Order(_petition) );
		que->Last(); const int len = que->RecordCount;

		if ( gco <= 0   ) gco = len;
		if ( gco >  len ) gco = len;

		if ( gst < 0   ) gst = 0;
		if ( gst > len ) gst = len;

		que->RecNo = gst;

		if ( len == 0 ) {
			pe.ResponceSock( "ok",
				co"start"co ":0,"
				co"count"co ":0,"
				co"final"co ":0,"
				co"datas"co ":[]"
			);
		}

		String res = "";
		for ( int i = 0; i < numberdata && i < gco; i++ ) {
			String tem = "";

			for ( int ii = 0; ii < que->FieldCount; ii++ ) {
				TField *fie = (*que->Fields)[ii];

				tem+= ","co+ fie->FieldName.LowerCase() +co":";

				if      ( fie->DataType == ftInteger ) tem+= String( fie->AsInteger );
				else if ( fie->DataType == ftFloat   ) tem+= String( fie->AsFloat   );
				else if ( fie->DataType == ftString  ) tem+=co+ fie->AsString +co;
			}

			if ( tem.w_str()[0] == ',' ) tem.w_str()[0] = ' ';

			res+= ",{" + tem + "}";

			que->Next();
		}

		if ( res.w_str()[0] == ',' ) res.w_str()[0] = ' ';

		res = "[" + res + "]";

		pe.ResponceSock( "ok",
			co"start"co ":" + (String)gst + ","
			co"count"co ":" + (String)gco + ","
			co"final"co ":" + (String)len + ","
			co"datas"co ":" + res
		);
	}
	catch( Exception &e ) {
		Notify( "Error en VotosMV::Getter_casillas", e.Message + " (" + e.ClassName() + ")", mtError );
		sene( "Error en consulta", "2", +Error( &e )+ );
	}

	delete que;
 };
void eQuery::Exec( ePetition *_petition ) {};
//###########################################################################
