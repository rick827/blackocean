#pragma hdrstop
#include "eBase.h"
#include "eOptions.h"
#pragma package(smart_init)


// Otros ####################################################################
/*Variables*/
static eOptions *_eOptions = null;

/*Funciones*/
eOptions *__stdcall sfeOptions() {
	if ( !_eOptions ) ( _eOptions = new eOptions )->Load();

	return _eOptions;
 };
//###########################################################################


// class PACKAGE eOptions : public TObject //################################
/*Creacion - public*/
__fastcall eOptions::eOptions() {
	_hwatch = null;
	_debug  = null;
 };
//###########################################################################


/*Hilos - private*/
void __fastcall eOptions::thini_watch( wThread *hilo ) {
	String dir = GetAppDirectory();
	String nam = GetAppDirectory() + ".json";

	while ( 1 ) {
		FILE_NOTIFY_INFORMATION *arc = WatchPath( dir );

		Sleep( 300 );

		if ( arc && (String)arc->FileName == nam ) {
			_debug = null;

			this->Load();
			gbas .Connect( true );
		}
	}
 };
//###########################################################################


/*Operadores - public*/
sJson eOptions::operator [] ( int    index              ) {
	return _options[index];
 };
sJson eOptions::operator [] ( String tag                ) {
	return _options[tag];
 };
sJson eOptions::operator () ( int    index, sJson value ) {
	return _options( index, value );
 };
//###########################################################################


/*Propiedades - private*/
bool eOptions::RDebug() {
	if ( !_debug ) _debug = _options["debug"]["debug"].Boo ? 2 : 1;

	return _debug == 2;
 };
//###########################################################################


/*Metodos - public*/
void eOptions::Load() {
	_debug = null;

	_options.Parse( GetAppDirectory() + "\\options.json" );

	if ( !_hwatch ) _hwatch = new wThread( &thini_watch );
 };
void eOptions::Save() {
	_options.Save( GetAppDirectory() + "\\options.json", true );
 };
//###########################################################################
