#ifndef wDragDropH
#define wDragDropH

#include <Shlobj.h>
#include <Shobjidl.h>
#include <classes.hpp>

#include "sFields.h"
#include "wGeneral.h"

class PACKAGE wDragDrop {
	/*Variables*/private:
	HWND         _handle    ;
	IDropTarget *_dropTarget;
	//###########################################################################


	/*Creacion*/public:
	wDragDrop( HWND handle, sfDrag oninit, sfDrag onover, sfDrag ondrop, sfDrag onleave );
   ~wDragDrop(                                                                          );
	//###########################################################################


	/*Funciones*/private:
	static HANDLE  STRaHAN( const TCHAR *text, int size = -1                            );
	static void    STRaDAT( String text, IDataObject *dataobject                        );
	static HBITMAP STRaBMP( String text, SIZE &size                                     );
	static void    FILaBMP( SHDRAGIMAGE *image, IDataObject *pDataObject, sSField files );

	/*Funciones*/public:
	static DWORD DoDragText( DWORD effects, bool viewimage, SHDRAGIMAGE *image, int ntexts, ...             );
	static DWORD DoDragFile( DWORD effects, bool viewimage, SHDRAGIMAGE *image, String  files, String extra );
	static DWORD DoDragFile( DWORD effects, bool viewimage, SHDRAGIMAGE *image, sSField files, String extra );
	//###########################################################################
 };

#endif
