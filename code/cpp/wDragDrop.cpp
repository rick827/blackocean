#pragma hdrstop
#include "wDragDrop.h"
#pragma package(smart_init)

// Otros //##################################################################
/*Definiciones*/
#define E_WDRAGDROP (HRESULT)0x8000AAAA

/*Clase*/
class wEnumFORMATETC : public IEnumFORMATETC {
	/*Variables*/private:
	int     _index    ;
	int     _reference;
	sIField _formats  ;
	//###########################################################################


	/*Creacion*/public:
	wEnumFORMATETC( sIField formats ) {
		_reference = 0;
		_index     = 0;

		for ( int i = 0; i < formats.Size; i++ ) {
			FORMATETC *sou = (FORMATETC*)formats[i].Value;
			FORMATETC
			*fom = new FORMATETC;
			*fom = *sou;

			if ( sou->ptd ) {
				fom->ptd    = (DVTARGETDEVICE*)CoTaskMemAlloc( sizeof( DVTARGETDEVICE ) );
				*(fom->ptd) = *(sou->ptd);
			}

			_formats+= (int)fom;
		}
	 };
	//###########################################################################


	/*Metodos - IUnknown*/public:
	ULONG   __stdcall AddRef        (                               ) {
		return ++_reference;
	 };
	HRESULT __stdcall QueryInterface( REFIID riid, void **ppvObject ) {
		(*ppvObject) = riid == IID_IUnknown || riid == IID_IEnumFORMATETC ? this : null;

		if ( (*ppvObject) )
			( (LPUNKNOWN)(*ppvObject) )->AddRef();

		return (*ppvObject) == null ? E_NOINTERFACE : S_OK;
	 };
	ULONG   __stdcall Release       (                               ) {
		int ref = --_reference;

		if ( ref == 0 )
			delete this;

		return ref;
	 };

	/*Metodos - IEnumFORMATETC*/public:
	HRESULT __stdcall Clone( IEnumFORMATETC **ppenum                           ) {
		if ( !ppenum ) return E_POINTER;

		(*ppenum) = new wEnumFORMATETC( _formats );

		( (wEnumFORMATETC*)(*ppenum) )->AddRef();

		( (wEnumFORMATETC*)(*ppenum) )->_index = _index;

		return S_OK;
	 };
	HRESULT __stdcall Next ( ULONG celt, FORMATETC *rgelt, ULONG *pceltFetched ) {
		if ( pceltFetched                                   ) (*pceltFetched) = 0;
		if ( celt <= 0 || !rgelt || _index >= _formats.Size ) return S_FALSE;
		if ( !pceltFetched && celt != 1                     ) return S_FALSE;

		ULONG tem = celt;

		while ( _index < _formats.Size && tem-- > 0 )
			(*rgelt++) = ( *(FORMATETC*)_formats[_index++].Value );

		if ( pceltFetched ) (*pceltFetched) = celt - tem;

		return tem == 0 ? S_OK : S_FALSE;
	 };
	HRESULT __stdcall Reset(                                                   ) {
		_index = 0;

		return S_OK;
	 };
	HRESULT __stdcall Skip ( ULONG celt                                        ) {
		if ( ( _index + (int)celt ) >= _formats.Size ) return S_FALSE;

		_index += celt;

		return S_OK;
	 };
	//###########################################################################
 };
class wDropSource    : public IDropSource    {
	/*Variables*/protected:
	int _reference;
	//###########################################################################


	/*Creacion*/public:
	wDropSource() {
		_reference = 0;
	 };
	//###########################################################################


	/*Metodos - IUnknown*/public:
	ULONG   __stdcall AddRef        (                               ) {
		return ++_reference;
	 };
	HRESULT __stdcall QueryInterface( REFIID riid, void **ppvObject ) {
		(*ppvObject) = riid == IID_IUnknown || riid == IID_IEnumFORMATETC ? this : null;

		if ( (*ppvObject) )
			( (LPUNKNOWN)(*ppvObject) )->AddRef();

		return (*ppvObject) == null ? E_NOINTERFACE : S_OK;
	 };
	ULONG   __stdcall Release       (                               ) {
		int ref = --_reference;

		if ( ref == 0 )
			delete this;

		return ref;
	 };
	//###########################################################################


	/*Metodos - IDropSource*/public:
	HRESULT __stdcall GiveFeedback     ( DWORD dwEffect                         ) {
		return DRAGDROP_S_USEDEFAULTCURSORS;
	 };
	HRESULT __stdcall QueryContinueDrag( BOOL fEscapePressed, DWORD grfKeyState ) {
		if ( fEscapePressed || ( grfKeyState & MK_RBUTTON ) )
			return DRAGDROP_S_CANCEL;

		if ( !( grfKeyState & MK_LBUTTON ) )
			return DRAGDROP_S_DROP;

		return S_OK;
	 };
	//###########################################################################
 };
class wDataObject    : public IDataObject    {
	/*Variables*/protected:
	int  _reference     ;
	BOOL _swappedButtons;

	sIField _formats;
	sIField _mediums;

	wDropSource *_dropSource;
	//###########################################################################


	/*Creacion*/public:
	wDataObject( wDropSource *dropSource ) {
		_reference      = 0                                ;
		_dropSource     = dropSource                       ;
		_swappedButtons = GetSystemMetrics( SM_SWAPBUTTON );
	 };
   ~wDataObject(                         ) {
		for ( int i = 0; i < _formats.Size; i++ ) {
			delete (FORMATETC*)_formats[i].Value;

			ReleaseStgMedium( (STGMEDIUM*)_mediums[i].Value );

			delete (STGMEDIUM*)_mediums[i].Value;
		}

		_formats.Clear();
		_mediums.Clear();
	 };
	//###########################################################################


	/*Metodos - IUnknown*/public:
	ULONG   __stdcall AddRef        (                               ) {
		return ++_reference;
	 };
	HRESULT __stdcall QueryInterface( REFIID riid, void **ppvObject ) {
		(*ppvObject) = riid == IID_IUnknown || riid == IID_IDataObject ? this : null;

		if ( (*ppvObject) )
			( (LPUNKNOWN)(*ppvObject) )->AddRef();

		return !(*ppvObject) ? E_NOINTERFACE : S_OK;
	 };
	ULONG   __stdcall Release       (                               ) {
		int ref = --_reference;

		if ( ref == 0 )
			delete this;

		return ref;
	 };
	//###########################################################################


	/*Metodos - IDataObject*/public:
	HRESULT __stdcall DAdvise              ( FORMATETC *pformatetc, DWORD advf, IAdviseSink *pAdvSink, DWORD *pdwConnection ) {
		return OLE_E_ADVISENOTSUPPORTED;
	 };
	HRESULT __stdcall DUnadvise            ( DWORD dwConnection                                                             ) {
		return E_NOTIMPL;
	 };
	HRESULT __stdcall EnumDAdvise          ( IEnumSTATDATA **ppenumAdvise                                                   ) {
		//return OLE_E_ADVISENOTSUPPORTED;
		return E_WDRAGDROP;
	 };
	HRESULT __stdcall EnumFormatEtc        ( DWORD dwDirection, IEnumFORMATETC **ppenumFormatEtc                            ) {
		if ( !ppenumFormatEtc ) return E_POINTER;

		switch ( dwDirection ) {
			case DATADIR_GET:
				(*ppenumFormatEtc) = new wEnumFORMATETC( _formats );
				( (wEnumFORMATETC*)(*ppenumFormatEtc) )->AddRef();

				return S_OK;

			default:
				(*ppenumFormatEtc) = null;

				return E_NOTIMPL;
		}
	 };
	HRESULT __stdcall GetCanonicalFormatEtc( FORMATETC *pformatectIn, FORMATETC *pformatetcOut                              ) {
		return !pformatetcOut ? E_INVALIDARG : DATA_S_SAMEFORMATETC;
	 };
	HRESULT __stdcall GetData              ( FORMATETC *pformatetcIn, STGMEDIUM *pmedium                                    ) {
		if ( !pformatetcIn || !pmedium ) return E_INVALIDARG;

		ZeroMemory( pmedium, sizeof( STGMEDIUM ) );

		int siz = _formats.Size;

		for ( int i = 0; i < siz; i++ ) {
			FORMATETC *fom = (FORMATETC*)_formats[i].Value;
			STGMEDIUM *med = (STGMEDIUM*)_mediums[i].Value;

			if (
				pformatetcIn->tymed     & fom->tymed    &&
				pformatetcIn->dwAspect == fom->dwAspect &&
				pformatetcIn->cfFormat == fom->cfFormat
			) {
				CopyMedium( pmedium, *med, *fom );

				return S_OK;
			}
		}

		return DV_E_FORMATETC;
	 };
	HRESULT __stdcall GetData              ( sSField text                                                                   ) {
		STGMEDIUM medium;
		HGLOBAL   global;

		int siz = _formats.Size;

		for ( int i = 0; i < siz; i++ ) {
			FORMATETC *fom = (FORMATETC*)_formats[i].Value;
			STGMEDIUM *med = (STGMEDIUM*)_mediums[i].Value;

			if ( fom->tymed & TYMED_HGLOBAL && fom->dwAspect == DVASPECT_CONTENT && fom->cfFormat == CF_UNICODETEXT ) {
				ZeroMemory( &medium, sizeof( STGMEDIUM ) );
				CopyMedium( &medium, *med, *fom );

				global = medium.hGlobal;
				text  += (String)(wchar_t*)GlobalLock( global );

				GlobalUnlock    ( global  );
				ReleaseStgMedium( &medium );
			}
		}

		return S_OK;
	 };
	HRESULT __stdcall GetDataHere          ( FORMATETC *pformatetc, STGMEDIUM *pmedium                                      ) {
		return E_NOTIMPL;
	 };
	HRESULT __stdcall QueryGetData         ( FORMATETC *pformatetc                                                          ) {
		if ( !pformatetc ) return E_INVALIDARG;

		if ( !( pformatetc->dwAspect & DVASPECT_CONTENT ) )
			return DV_E_DVASPECT;

		HRESULT res = DV_E_TYMED;
		int     siz = _formats.Size;

		for ( int i = 0; i < siz; i++ ) {
			FORMATETC *fom = (FORMATETC*)_formats[i].Value;

			if ( !( pformatetc->tymed & fom->tymed ) ) {
				res = DV_E_TYMED;

				continue;
			}

			if ( pformatetc->cfFormat == fom->cfFormat )
				return S_OK;

			res = DV_E_CLIPFORMAT;
		}

		return res;
	 };
	HRESULT __stdcall SetData              ( FORMATETC *pformatetc, STGMEDIUM *pmedium, BOOL fRelease                       ) {
		if ( !pformatetc || !pmedium )
			return E_INVALIDARG;

		FORMATETC *fom = new FORMATETC;
		STGMEDIUM *med = new STGMEDIUM;

		DeepCopyFormatEtc( fom, pformatetc );

		_formats += (int)fom;
		(*med)    = (*pmedium);

		if ( !fRelease )
			CopyMedium( med, (*pmedium), (*pformatetc) );

		_mediums += (int)med;

		return S_OK;
	 };
	//###########################################################################


	/*Funciones*/private:
	void CopyMedium       ( STGMEDIUM *pMedDest, STGMEDIUM &MedSrc, FORMATETC &FmtSrc ) {
		switch( MedSrc.tymed ) {
			case TYMED_HGLOBAL : pMedDest->hGlobal       = (HGLOBAL      )OleDuplicateData( MedSrc.hGlobal      , FmtSrc.cfFormat, null ); break;
			case TYMED_GDI     : pMedDest->hBitmap       = (HBITMAP      )OleDuplicateData( MedSrc.hBitmap      , FmtSrc.cfFormat, null ); break;
			case TYMED_MFPICT  : pMedDest->hMetaFilePict = (HMETAFILEPICT)OleDuplicateData( MedSrc.hMetaFilePict, FmtSrc.cfFormat, null ); break;
			case TYMED_ENHMF   : pMedDest->hEnhMetaFile  = (HENHMETAFILE )OleDuplicateData( MedSrc.hEnhMetaFile , FmtSrc.cfFormat, null ); break;
			case TYMED_FILE    : pMedDest->lpszFileName  = (LPOLESTR     )OleDuplicateData( MedSrc.lpszFileName , FmtSrc.cfFormat, null ); break;
			case TYMED_ISTREAM : pMedDest->pstm          = MedSrc.pstm; MedSrc.pstm->AddRef();                                             break;
			case TYMED_ISTORAGE: pMedDest->pstg          = MedSrc.pstg; MedSrc.pstg->AddRef();                                             break;
		}

		pMedDest->tymed          = MedSrc.tymed;
		pMedDest->pUnkForRelease = null        ;

		if ( MedSrc.pUnkForRelease != null ) {
			pMedDest->pUnkForRelease = MedSrc.pUnkForRelease;

			MedSrc.pUnkForRelease->AddRef();
		}
	 };
	void DeepCopyFormatEtc( FORMATETC *destiny, FORMATETC *source                     ) {
		*destiny = *source;

		if ( source->ptd ) {
			destiny->ptd    = (DVTARGETDEVICE*)CoTaskMemAlloc( sizeof( DVTARGETDEVICE ) );
			*(destiny->ptd) = *(source->ptd);
		}
	 };
	//###########################################################################
 };
class wDropTarget    : public IDropTarget    {
	/*Declaraciones*/private:
	bool               _accept    ;
	bool               _rbutton   ;
	HWND               _handle    ;
	LONG               _numref    ;
	sSField            _texts     ;
	sSField            _files     ;
	IDropTargetHelper *_dropHelper;

	/*Eventos*/public:
	sfDrag onInit ;
	sfDrag onOver ;
	sfDrag onDrop ;
	sfDrag onLeave;
	//###########################################################################


	/*Creacion*/public:
	wDropTarget( HWND handle ) {
		/*Declaraciones - private*/
		_accept     = true  ;
		_rbutton    = false ;
		_handle     = handle;
		_numref     = 1     ;
		_dropHelper = null  ;

		/*Eventos - public*/
		onInit  = null;
		onOver  = null;
		onDrop  = null;
		onLeave = null;

		/*Inicio*/
		CoCreateInstance( CLSID_DragDropHelper, null, CLSCTX_INPROC_SERVER, IID_PPV_ARGS( &_dropHelper ) );
	 };
   ~wDropTarget(             ) {
		if ( _dropHelper != null )
			_dropHelper->Release();
	 };
	//###########################################################################


	/*Metodos*/private:
	void    IniMenu   ( HMENU hmenu, LPTSTR iname, int nid ) {
		MENUITEMINFO mii;

		mii.cbSize = sizeof( MENUITEMINFO );
		mii.fMask  = MIIM_ID | MIIM_TYPE   ;
		mii.wID    = nid                   ;

		if ( iname != null ) {
			mii.fType      = MFT_STRING;
			mii.dwTypeData = iname     ;
		}
		else mii.fType = MFT_SEPARATOR;

		InsertMenuItem( hmenu, nid, false, &mii );
	 };
	HRESULT GetMenu   ( DWORD &effects, POINTL point       ) {
		int   id;
		HMENU hmenu;

		hmenu = CreatePopupMenu();

		if ( effects & DROPEFFECT_MOVE ) IniMenu( hmenu, L"Mover aqu�"     , DROPEFFECT_MOVE );
		if ( effects & DROPEFFECT_COPY ) IniMenu( hmenu, L"Copiar aqu�"    , DROPEFFECT_COPY );
		if ( effects & DROPEFFECT_LINK ) IniMenu( hmenu, L"Crear link aqu�", DROPEFFECT_LINK );

		IniMenu( hmenu, null       , 100 );
		IniMenu( hmenu, L"Cancelar", 0   );

		id = TrackPopupMenu( hmenu, TPM_RETURNCMD, point.x, point.y, 0, _handle, null );

		if ( id == 0 ) {
			DestroyMenu( hmenu );

			effects = DROPEFFECT_NONE;

			return E_FAIL;
		}

		effects = id;

		DestroyMenu( hmenu );

		return S_OK;
	 };
	void    GetEffects( DWORD &effects, DWORD keystate     ) {
		if ( keystate & MK_CONTROL ) {
			if ( keystate & MK_SHIFT ) effects = DROPEFFECT_LINK;
			else                       effects = DROPEFFECT_COPY;
		}
		//else effects = DROPEFFECT_MOVE;
	 };
	void    GetTexts  ( IDataObject *dataobject            ) {
		STGMEDIUM medium;
		FORMATETC format = { CF_UNICODETEXT, null, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };

		if ( SUCCEEDED( dataobject->GetData( &format, &medium ) ) ) {
			HGLOBAL global = medium.hGlobal;

			_texts += (wchar_t*)GlobalLock( global );

			GlobalUnlock    ( global  );
			ReleaseStgMedium( &medium );
		}
	 };
	void    GetFiles  ( IDataObject *dataobject            ) {
		STGMEDIUM medium;
		FORMATETC format = { CF_HDROP, null, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };

		if ( SUCCEEDED( dataobject->GetData( &format, &medium ) ) ) {
			wchar_t  path[MAX_PATH];
			HDROP    hdro = reinterpret_cast<HDROP>( medium.hGlobal );
			unsigned numn = 0;
			unsigned coun = DragQueryFile( hdro, 0xFFFFFFFF, null, 0 );

			if ( coun > 0 ) {
				_files.Clear();

				for ( unsigned i = 0; i < coun; i++ ) {
					numn = DragQueryFile( hdro, i, path, MAX_PATH );

					if ( numn > 0 && numn < MAX_PATH )
						_files += path;
				}

				ReleaseStgMedium( &medium );
			}
		}
	 };

	/*Metodos - IUnknown*/public:
	ULONG   __stdcall AddRef        (                               ) {
		return InterlockedIncrement( &_numref );
	 };
	HRESULT __stdcall QueryInterface( REFIID riid, void **ppvObject ) {
		(*ppvObject) = null;

		if ( IsEqualIID( riid, IID_IUnknown ) || IsEqualIID( riid, IID_IDropTarget ) ) (*ppvObject) = static_cast<IDropTarget*>( this );
		else return E_NOINTERFACE;

		AddRef();

		return S_OK;
	 };
	ULONG   __stdcall Release       (                               ) {
		if ( InterlockedDecrement( &_numref ) == 0 ) {
			delete this;
			return 0;
		}

		return _numref;
	 };

	/*Metodos - IDropTarget*/public:
	HRESULT __stdcall DragEnter( IDataObject *pDataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect ) {
		if ( _dropHelper ) _dropHelper->DragEnter( _handle, pDataObj, (LPPOINT)&pt, *pdwEffect );

		wDataObject *dob = pDataObj->EnumDAdvise( null ) == E_WDRAGDROP ? (wDataObject*)pDataObj : null;

		_texts.Clear();
		_files.Clear();

		if ( dob ) dob->GetData( _texts );
		else
		GetTexts( pDataObj );
		GetFiles( pDataObj );

		_accept = true;

		GetEffects( *pdwEffect, grfKeyState );

		if ( onInit   ) onInit( _texts, _files, pt, *pdwEffect, _accept );
		if ( !_accept ) (*pdwEffect) = DROPEFFECT_NONE;

		_rbutton = ( grfKeyState & MK_RBUTTON );

		return S_OK;
	 };
	HRESULT __stdcall DragLeave(                                                                       ) {
		if ( _dropHelper ) _dropHelper->DragLeave();

		if ( onLeave ) {
			TPoint pointc  ; GetCursorPos( &pointc );
			DWORD  effects = DROPEFFECT_NONE;
			POINTL point   = { pointc.x, pointc.y };

			onLeave( _texts, _files, point, effects, _accept );
		}

		return S_OK;
	 };
	HRESULT __stdcall DragOver ( DWORD grfKeyState, POINTL pt, DWORD *pdwEffect                        ) {
		if ( _dropHelper ) _dropHelper->DragOver( (LPPOINT)&pt, *pdwEffect );

		GetEffects( *pdwEffect, grfKeyState );

		if ( onOver   ) onOver( _texts, _files, pt, *pdwEffect, _accept );
		if ( !_accept ) (*pdwEffect) = DROPEFFECT_NONE;

		return S_OK;
	 };
	HRESULT __stdcall Drop     ( IDataObject *pDataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect ) {
		if ( _dropHelper ) _dropHelper->Drop( pDataObj, (LPPOINT)&pt, *pdwEffect );

		if ( _rbutton ) {
			if ( GetMenu( *pdwEffect, pt ) == E_FAIL )
				return E_FAIL;
		}
		else {
			(*pdwEffect) = DROPEFFECT_MOVE;

			GetEffects( *pdwEffect, grfKeyState );
		}

		if ( onDrop ) onDrop( _texts, _files, pt, *pdwEffect, _accept );

		return S_OK;
	 };
	//###########################################################################
 };
//###########################################################################


//class wDragDrop //#########################################################
/*Creacion - public*/
wDragDrop:: wDragDrop( HWND handle, sfDrag oninit, sfDrag onover, sfDrag ondrop, sfDrag onleave ) {
	if ( !handle ) return;

	_handle = handle;

	RevokeDragDrop( handle );

	_dropTarget = new wDropTarget( handle );

	((wDropTarget*)_dropTarget)->onInit  = oninit;
	((wDropTarget*)_dropTarget)->onOver  = onover;
	((wDropTarget*)_dropTarget)->onDrop  = ondrop;
	((wDropTarget*)_dropTarget)->onLeave = onleave;

	CoLockObjectExternal( _dropTarget, true, true );

	long re;
	if ( ( re = RegisterDragDrop( handle, _dropTarget ) ) != S_OK ) {
		if      ( re == DRAGDROP_E_INVALIDHWND       ) MessageBoxA( handle, "DRAGDROP_E_INVALIDHWND"      , "Error", MB_ICONERROR );
		else if ( re == DRAGDROP_E_ALREADYREGISTERED ) MessageBoxA( handle, "DRAGDROP_E_ALREADYREGISTERED", "Error", MB_ICONERROR );
		else if ( re == E_OUTOFMEMORY                ) MessageBoxA( handle, "E_OUTOFMEMORY"               , "Error", MB_ICONERROR );
		else                                           MessageBoxA( handle, "Error"                       , "Error", MB_ICONERROR );

		_dropTarget->Release();

		_dropTarget = null;
	}
 };
wDragDrop::~wDragDrop(                                                                          ) {
	if ( _dropTarget ) {
		RevokeDragDrop( _handle );

		_dropTarget->Release();
	}
 };
//###########################################################################


/*Funciones - private*/
HANDLE  wDragDrop::STRaHAN( const TCHAR *text, int size                                 ) {
	TCHAR *res;

	if ( size == -1 ) size = wcslen( text );

	res = (TCHAR*)GlobalAlloc( GMEM_FIXED, ( size + 1 ) * sizeof( TCHAR ) );

	memcpy( res, text, size * sizeof( TCHAR ) );

	res[size] = '\0';

	return res;
 };
void    wDragDrop::STRaDAT( String text, IDataObject *dataobject                        ) {
	FORMATETC fmtetc = { CF_UNICODETEXT, 0, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };
	STGMEDIUM stgmed = { TYMED_HGLOBAL, {0}, 0 };

	stgmed.hGlobal = STRaHAN( text.w_str() );

	dataobject->AddRef();
	dataobject->SetData( &fmtetc, &stgmed, true );
 };
HBITMAP wDragDrop::STRaBMP( String text, SIZE &size                                     ) {
	text = " " + text + " ";

	HDC     hdc = GetDC( null );
	HDC     mem = CreateCompatibleDC( hdc ); GetTextExtentPoint32( mem, text.w_str(), text.Length(), &size );
	HBITMAP res = CreateCompatibleBitmap( hdc, size.cx, size.cy );

	SelectObject( mem, res );

	SetBkColor  ( mem, RGB( 50, 100, 255 ) );
	SetTextColor( mem, RGB( 255, 255, 255 ) );
	TextOut     ( mem, 0, 0, text.w_str(), text.Length() );

	SelectObject( mem, res );
	DeleteDC    ( mem );
	ReleaseDC   ( null, hdc );

	return res;
 };
void    wDragDrop::FILaBMP( SHDRAGIMAGE *image, IDataObject *pDataObject, sSField files ) {
	IDragSourceHelper *pDragHelper;

	if ( FAILED( CoCreateInstance( CLSID_DragDropHelper, null, CLSCTX_INPROC_SERVER, IID_IDragSourceHelper, (LPVOID*)&pDragHelper ) ) )
		pDragHelper = null;

	if ( pDragHelper ) {
		if ( !image ) {
			SIZE        siz = {150, 150};
			HBITMAP     ico = null;
			HBITMAP     tem = null;
			SHDRAGIMAGE ima = {0};
			wchar_t     pat[MAX_PATH];

			for ( int i = 0; i < files.Size; i++ ) {
				_wfullpath( pat, files[i].Value.w_str(), MAX_PATH );

				tem = GetIconFile( pat, siz );

				if ( ico == null )
					ico = tem;

				else OverHBitmap( ico, tem, TRect( 0, 0, siz.cx, siz.cy ) );
			}

			if ( ico ) {
				if ( files.Size > 1 ) {
					SIZE    si;
					HBITMAP bm = STRaBMP( files.Size, si );

					OverHBitmap(
						ico,
						bm,
						TRect( ( siz.cx - si.cx ) / 2, ( ( siz.cy - si.cy ) / 2 ) - si.cy, si.cx, si.cy ),
						false
					);
				}

				ima.sizeDragImage = siz;
				ima.hbmpDragImage = ico;
				ima.crColorKey    = RGB( 100, 50, 10 );
				ima.ptOffset      = TPoint( siz.cx / 2, siz.cy / 2 );
			}

			pDragHelper->InitializeFromBitmap( &ima, pDataObject );
		}
		else pDragHelper->InitializeFromBitmap( image, pDataObject );
	}
 };

/*Funciones - public*/
DWORD wDragDrop::DoDragText( DWORD effects, bool viewimage, SHDRAGIMAGE *image, int ntexts, ...             ) {
	DWORD              res = DROPEFFECT_NONE;
	wDropSource       *dso = new wDropSource; dso->AddRef();
	wDataObject       *dob = new wDataObject( dso );
	IDragSourceHelper *dhe = null;
	wchar_t           *str ;
	String             tex = "";
	va_list            lis;

	va_start( lis, ntexts );

	for ( int i = 0; i < ntexts; i++ ) {
		str = va_arg( lis, wchar_t* );

		if ( i == 0 ) tex = (String)str;

		STRaDAT( (String)str, dob );
	}

	if ( viewimage ) {
		if ( FAILED( CoCreateInstance( CLSID_DragDropHelper, null, CLSCTX_INPROC_SERVER, IID_IDragSourceHelper, (LPVOID*)&dhe ) ) )
			dhe = null;

		if ( dhe && !image ) {
			SIZE        siz;
			SHDRAGIMAGE ima;

			ima.hbmpDragImage = STRaBMP( tex, siz );
			ima.sizeDragImage = siz                 ;
			ima.crColorKey    = RGB( 0, 0, 0 )      ;
			ima.ptOffset      = TPoint( siz.cx / 2, siz.cy / 2 );

			dhe->InitializeFromBitmap( &ima, dob );
		}
		else if ( dhe && image ) dhe->InitializeFromBitmap( image, dob );

	}

	res = ::DoDragDrop( dob, dso, effects, &res ) == DRAGDROP_S_DROP ? DROPEFFECT_NONE : res;

	if ( dhe ) dhe->Release();
	if ( dso ) dso->Release();
	if ( dob ) dob->Release();

	return res;
 };
DWORD wDragDrop::DoDragFile( DWORD effects, bool viewimage, SHDRAGIMAGE *image, String  files, String extra ) {
	return wDragDrop::DoDragFile( effects, viewimage, image, GetTagsGroup( files ), extra );
 };
DWORD wDragDrop::DoDragFile( DWORD effects, bool viewimage, SHDRAGIMAGE *image, sSField files, String extra ) {
	DWORD res = DROPEFFECT_NONE;

	for ( int i = files.Size - 1; i >= 0; i-- ) {
		if ( !FileExists( files[i].Value ) && !DirectoryExists( files[i].Value ) )
			files -= i;
	}

	if ( files.Size ) {
		wDropSource      *dso = new wDropSource();
		PITEMID_CHILD    *ppc = (PITEMID_CHILD   *)CoTaskMemAlloc( sizeof( PITEMID_CHILD    ) * files.Size );
		PIDLIST_ABSOLUTE *ppa = (PIDLIST_ABSOLUTE*)CoTaskMemAlloc( sizeof( PIDLIST_ABSOLUTE ) * files.Size );
		IShellFolder     *sfo = null;
		wDataObject      *dob = null;
		wchar_t           exp[MAX_PATH];

		for ( int i = 0; i < files.Size; i++ ) {
			_wfullpath( exp, files[i].Value.w_str(), MAX_PATH );

			ppa[i] = ILCreateFromPath( exp );
		}

		SHBindToParent( ppa[0], IID_PPV_ARGS( &sfo ), null );

		for ( int i = 0; i < files.Size; i++ )
			ppc[i] = ILFindLastID( ppa[i] );

		sfo->GetUIObjectOf( null, files.Size, (LPCITEMIDLIST*)ppc, IID_IDataObject, null, (void**)&dob );

		if ( extra != "" ) STRaDAT( extra, dob      );
		if ( viewimage   ) FILaBMP( image, dob, files );

		::DoDragDrop( dob, dso, effects, &res );

		for ( int i = 0; i < files.Size; i++ )
		CoTaskMemFree( ppa[i] );
		CoTaskMemFree( ppa    );
		CoTaskMemFree( ppc    );
	}

	return res;
 };
//###########################################################################
