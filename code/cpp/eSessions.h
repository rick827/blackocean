#ifndef eSessionsH
#define eSessionsH

#include "sFields.h"

class PACKAGE eSession  {
	/*Variables*/public:
	int    Sock;
	int    Year;
	String Pass;
	//###########################################################################


	/*Creacion*/public:
	eSession( int sock );
	//###########################################################################
 };
class PACKAGE eSessions {
	/*Variables*/private:
	sIField _sessions;
	//###########################################################################


	/*Metodos*/public:
	void   Insert( int sock              );
	void   Delete( int sock              );
	String Pass  ( int sock              );
	void   Pass  ( int sock, String pass );
	int    Year  ( int sock              );
	void   Year  ( int sock, int year    );

	void ResponseAll( String response, int npass, ... );

	void DisconnectAll();
	//###########################################################################
 };

extern "C++" __declspec(dllexport)eSessions *__stdcall sfeSessions();

#endif
