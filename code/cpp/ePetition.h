#ifndef ePetitionH
#define ePetitionH

#include "sFields.h"
#include "sJson.h"
#include "eGeneral.h"

class PACKAGE ePetition : public TObject {
	/*Definiciones*/
	#define ePetition_start \
		Sock     = 0;       \
		Section  = "";      \
		Func     = "";      \
		State    = "";      \
		IdClient = "";
	//###########################################################################


	/*Variables*/public:
	int    Sock;
	String IdClient;
	String Section;
	String Func;
	String State;
	sJson  Data;
	//###########################################################################


	/*Creacion*/public:
	__fastcall ePetition(                                                        );
	__fastcall ePetition( int sock, String petition, bool http                   );
	__fastcall ePetition( int sock, String idclient, String section, String func );
	//###########################################################################


	/*Operadores*/public:
	sJson operator []( String tag );
	sJson operator []( int    tag );
	//###########################################################################


	/*Metodos*/public:
	void Parse( String json );

	void ResponceHttp( int code, seContentType type, UTF8String body );
	void ResponceSock( String state                                  );
	void ResponceSock( String state, String message                  );
	void ResponceHand(                                               );

	static void ResponceSock( int sock, String message );
	//###########################################################################
 };

#endif
