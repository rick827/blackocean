#ifndef wTaskBarH
#define wTaskBarH

#pragma link "Shlwapi.lib"

#include <Shobjidl.h>
#include <propvarutil.h>

#include "sFields.h"
#include "wGeneral.h"

class PACKAGE wTaskBar {
	/*Variables*/private:
	sSField _categorys   ;
	sSField _linkCategory;
	sSField _linkTitle   ;
	sSField _linkPath    ;
	sSField _linkParams  ;
	sSField _linkIcon    ;
	sIField _linkIndex   ;
	sIField _linkType    ;
	//###########################################################################


	/*Creacion*/public:
	wTaskBar();
	//###########################################################################


	/*Metodos - icon*/public:
	static void SetIcon( HICON hicon, String description = "" );
	static void DelIcon(                                      );

	/*Metodos - progreso*/public:
	static void SetProgress( seTypeProg type, int vmax, int vmin, int vprogress );
	static void DelProgress(                                                    );

	/*Metodos - jumpList*/public:
	void AddLink( String category, String title, String path, String params, String iconPath, int iconIndex, seTypeLink type = tlShellLink );

		   bool CreateJump( String applicationID = "", bool viewFrequent = false, bool viewRecent = false );
	static bool DeleteJump( String applicationID = ""                                                     );
	//###########################################################################


	/*Funciones*/private:
	bool CreateCategory( ICustomDestinationList *jumplist, String title );
	bool CreateLink    ( IObjectCollection *list, int index             );
	//###########################################################################
 };

#endif
