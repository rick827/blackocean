#pragma hdrstop
#include "eModules.h"
#include "eOptions.h"
#pragma package(smart_init)


// Otros ####################################################################
/*Variables*/
static eModules *_eModules = null;

/*Funciones*/
eModules *__stdcall sfeModules() {
	if ( !_eModules ) ( _eModules = new eModules )->Load();

	return _eModules;
 };
//###########################################################################


// class PACKAGE eModule //##################################################
/*Creacion - public*/
eModule:: eModule( String path ) {
	_instance = null;
	_fload    = null;
	_fhttp    = null;
	_fsock    = null;
	_path     = path;
	_name     = "";

	/*Inicio*/
	_instance = FileExists( _path ) ? LoadLibrary( _path.w_str() )                               : null;
	_fload    = _instance           ? (sfLoad    )GetProcAddress( (HINSTANCE)_instance, "Load" ) : null;
	_fhttp    = _instance           ? (sfPetition)GetProcAddress( (HINSTANCE)_instance, "Http" ) : null;
	_fsock    = _instance           ? (sfPetition)GetProcAddress( (HINSTANCE)_instance, "Sock" ) : null;

	if ( _fload ) _fload( &_name );
 };
eModule::~eModule(             ) {
	if( _instance )
		FreeLibrary( _instance );
 };
//###########################################################################


/*Metodos - public*/
void eModule::Http( ePetition *petition ) {
	if ( _fhttp ) _fhttp( petition );
	else petition->ResponceHttp( 409, ctHTML, Response409() );
 };
void eModule::Sock( ePetition *petition ) {
	if ( _fsock ) _fsock( petition );
	else pe.me( "error", co"error"co":"co"No se carg� el modulo"co );
 };
//###########################################################################


// class PACKAGE eModules : public TObject //################################
/*Creacion - public*/
__fastcall eModules:: eModules() {
	_mpublic = null;
 };
__fastcall eModules::~eModules() {
	this->Clear();
 };
//###########################################################################


/*Metodos - public*/
void eModules::Load (                     ) {
	this->Clear();
	this->LoadModules( gopc["modules"]["path"].Str );
 };
void eModules::Http ( ePetition *petition ) {
	String   nom = pe["path"][0].Str.LowerCase();
	eModule *mod = null;

	for ( int i = 0; i < _pmodules.Size; i++ ) {
		eModule *mop = (eModule*)_pmodules[i].Value;

		if ( mop->Name == nom ) {
			mod = mop;
			break;
		}
	}

	if ( !mod ) mod = _mpublic;
	if (  mod ) mod->Http( petition ); else pe.ResponceHttp( 404, ctHTML, Response404() );
 };
void eModules::Sock ( ePetition *petition ) {
	String   nom = pe.Module.LowerCase();
	eModule *mod = null;

	if ( nom == "all" )
		for ( int i = 0; i < _pmodules.Size; i++ )
			((eModule*)_pmodules[i].Value)->Sock( petition );

	else {
		for ( int i = 0; i < _pmodules.Size; i++ ) {
			eModule *mop = (eModule*)_pmodules[i].Value;

			if ( mop->Name == nom ) {
				mod = mop;
				break;
			}
		}

		if ( mod ) mod->Sock( petition );
		else pe.me( "error", co"error"co":"co"No se encontr� el modulo"co );
	}
 };
void eModules::Clear(                     ) {
	_mpublic = null;

	for ( int i = 0; i < _pmodules.Size; i++ )
		delete (eModule*)_pmodules[i].Value;

	_pmodules.Clear();
 };
//###########################################################################


/*Funciones - private*/
void eModules::LoadModules( String path ) {
	sSField fil = TravelPath( path );
	String  pat, ext, exm = ";" + gopc["modules"]["extensions"].Str.LowerCase() + ";";

	for ( int i = 0; i < fil.Size; i++ ) {
		if ( fil[i].Value == "." || fil[i].Value == ".." ) continue;

		pat = StringReplace( path + "\\" + fil[i], "\\\\", "\\", TReplaceFlags() << rfReplaceAll );

		if ( FileExists( pat ) ) {
			if ( exm.Pos( ";" + ExtractFileExt( pat ).LowerCase() + ";" ) ) {
				eModule *mod = new eModule( pat );

				_pmodules+= (int)mod;

				if ( mod->Name == "mpublic" ) _mpublic = mod;
			}
		}
		else if ( DirectoryExists( pat ) ) LoadModules( pat );
	}
 };
//###########################################################################
