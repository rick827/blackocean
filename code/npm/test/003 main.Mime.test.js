require('../lib/main.js');
const as=require('assert');

describe('funcion Mime', ()=>{
	it('tipo js'  , ()=>as.equal( Mime('js'  ), 'application/javascript' ));
	it('tipo bh'  , ()=>as.equal( Mime('BH'  ), 'text/html'              ));
	it('tipo bj'  , ()=>as.equal( Mime('bj'  ), 'application/javascript' ));
	it('tipo css' , ()=>as.equal( Mime('css' ), 'text/css'               ));
	it('tipo ico' , ()=>as.equal( Mime('ico' ), 'image/x-icon'           ));
	it('tipo jpg' , ()=>as.equal( Mime('jpG' ), 'image/jpeg'             ));
	it('tipo png' , ()=>as.equal( Mime('PNg' ), 'image/png'              ));
	it('tipo svg' , ()=>as.equal( Mime('.svg'), 'image/svg+xml'          ));
	it('tipo txt' , ()=>as.equal( Mime('.txt'), 'text/plain'             ));
	it('tipo woff', ()=>as.equal( Mime('woff'), 'font/woff'              ));
	it('tipo html', ()=>as.equal( Mime('html'), 'text/html'              ));
	it('tipo json', ()=>as.equal( Mime('json'), 'application/json'       ));
	it('tipo pdf' , ()=>as.equal( Mime('pdf' ), 'application/pdf'        ));
});