require('../lib/main.js');
const as = require('assert');

describe('declaraciones', ()=>{
	describe('existen', ()=>{
		it('Existe __ids__'        , ()=> as.equal(typeof global.__ids__   , 'number') );
		it('Existe VALUE_FOLDER'   , ()=> as.equal(typeof global.VALUE_FOLDER  , 'string') );
		it('Existe VALUE_PATH'     , ()=> as.equal(typeof global.VALUE_PATH    , 'string') );
		it('Existe VALUE_FUNCTION' , ()=> as.equal(typeof global.VALUE_FUNCTION, 'string') );
		it('Existe VALUE_OBJECT'   , ()=> as.equal(typeof global.VALUE_OBJECT  , 'string') );
		it('Existe VALUE_ARRAY'    , ()=> as.equal(typeof global.VALUE_ARRAY   , 'string') );
		it('Existe VALUE_STRING'   , ()=> as.equal(typeof global.VALUE_STRING  , 'string') );
		it('Existe VALUE_NUMBER'   , ()=> as.equal(typeof global.VALUE_NUMBER  , 'string') );
		it('Existe VALUE_SPECIAL'  , ()=> as.equal(typeof global.VALUE_SPECIAL , 'string') );
		it('Existe VALUE_ASYNC'    , ()=> as.equal(typeof global.VALUE_ASYNC   , 'string') );
		it('Existe VALUE_VALID'    , ()=> as.equal(typeof global.VALUE_VALID   , 'string') );
		it('Existe VALUE_QUERY'    , ()=> as.equal(typeof global.VALUE_QUERY   , 'string') );
		it('Existe VALUE_API'      , ()=> as.equal(typeof global.VALUE_API     , 'string') );
		it('Existe VALUE_COMPILER' , ()=> as.equal(typeof global.VALUE_COMPILER, 'string') );
		it('Existe VALUE_RESOURCES', ()=> as.equal(typeof global.VALUE_RESOURCES, 'string') );
		it('Existe VALUE_APP'      , ()=> as.equal(typeof global.VALUE_APP     , 'string') );
		it('Existe VALUE_COM'      , ()=> as.equal(typeof global.VALUE_COM     , 'string') );
		it('Existe VALUE_SPA'      , ()=> as.equal(typeof global.VALUE_SPA     , 'string') );
		it('Existe VALUE_SPA'      , ()=> as.equal(typeof global.VALUE_SPA     , 'string') );
	 });
	describe('valores', ()=>{
		it('Valor __ids__'        , ()=> as.equal(global.__ids__        , 0            , 0            ));
		it('Valor VALUE_FOLDER'   , ()=> as.equal(global.VALUE_FOLDER   , 'folder'     , 'folder'     ));
		it('Valor VALUE_PATH'     , ()=> as.equal(global.VALUE_PATH     , 'path'       , 'path'       ));
		it('Valor VALUE_FUNCTION' , ()=> as.equal(global.VALUE_FUNCTION , 'function'   , 'function'   ));
		it('Valor VALUE_OBJECT'   , ()=> as.equal(global.VALUE_OBJECT   , 'object'     , 'object'     ));
		it('Valor VALUE_ARRAY'    , ()=> as.equal(global.VALUE_ARRAY    , 'array'      , 'array'      ));
		it('Valor VALUE_STRING'   , ()=> as.equal(global.VALUE_STRING   , 'string'     , 'string'     ));
		it('Valor VALUE_NUMBER'   , ()=> as.equal(global.VALUE_NUMBER   , 'number'     , 'number'     ));
		it('Valor VALUE_SPECIAL'  , ()=> as.equal(global.VALUE_SPECIAL  , 'special'    , 'special'    ));
		it('Valor VALUE_ASYNC'    , ()=> as.equal(global.VALUE_ASYNC    , 'async'      , 'async'      ));
		it('Valor VALUE_VALID'    , ()=> as.equal(global.VALUE_VALID    , 'valid'      , 'valid'      ));
		it('Valor VALUE_QUERY'    , ()=> as.equal(global.VALUE_QUERY    , 'query'      , 'query'      ));
		it('Valor VALUE_API'      , ()=> as.equal(global.VALUE_API      , 'api'        , 'api'        ));
		it('Valor VALUE_COMPILER' , ()=> as.equal(global.VALUE_COMPILER , 'compiler'   , 'compiler'   ));
		it('Valor VALUE_RESOURCES', ()=> as.equal(global.VALUE_RESOURCES, 'resources'  , 'resources'  ));
		it('Valor VALUE_APP'      , ()=> as.equal(global.VALUE_APP      , 'Application', 'Application'));
		it('Valor VALUE_COM'      , ()=> as.equal(global.VALUE_COM      , 'Component'  , 'Component'  ));
		it('Valor VALUE_SPA'      , ()=> as.equal(global.VALUE_SPA      , 'SPA'        , 'SPA'        ));
		it('Valor VALUE_KRAKEN'   , ()=> as.equal(global.VALUE_KRAKEN   , 'KRAKEN'     , 'KRAKEN'     ));
	 });
 });