require('../lib/main.js');
const as = require('assert');

describe('funcion tipode', ()=>{
	it('tipo de VALUE_FUNCTION', ()=>as.equal( tipode(function(){}), VALUE_FUNCTION ));
	it('tipo de VALUE_OBJECT'  , ()=>as.equal( tipode({})          , VALUE_OBJECT   ));
	it('tipo de VALUE_ARRAY'   , ()=>as.equal( tipode([])          , VALUE_ARRAY    ));
	it('tipo de VALUE_STRING'  , ()=>as.equal( tipode('')          , VALUE_STRING   ));
	it('tipo de VALUE_NUMBER'  , ()=>as.equal( tipode(.5)          , VALUE_NUMBER   ));
 });