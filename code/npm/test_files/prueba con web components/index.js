
const VALUE_FILE   = 'file',
	VALUE_FOLDER   = 'folder',
	VALUE_INSERT   = 'insert',
	VALUE_UPDATE   = 'update',
	VALUE_DELETE   = 'delete',
	VALUE_GETTER   = 'getter',
	VALUE_FUNCTION = 'function',
	VALUE_OBJECT   = 'object',
	VALUE_ARRAY    = 'array',
	VALUE_STRING   = 'string',
	VALUE_NUMBER   = 'number',
	VALUE_FLOAT    = 'float',
	VALUE_BOOL     = 'bool',
	VALUE_INT      = 'int',
	VALUE_PASSWORD = 'password',
	VALUE_SPECIAL  = 'special',
	VALUE_HTML     = 'html',
	VALUE_CSS      = 'css',
	VALUE_JS       = 'js',
	VALUE_TAXONOMY = 'taxonomy',
	VALUE_JSON     = 'json',
	VALUE_TEXT     = 'text',
	VALUE_BIG      = 'big',
	VALUE_MED      = 'med',
	VALUE_MIN      = 'min',
	VALUE_SYNC     = 'sync',
	VALUE_ASYNC    = 'async',
	VALUE_NULL     = 'null',
	VALUE_WARN     = 'warn',
	VALUE_ERROR    = 'error',
	VALUE_VALID    = 'valid',
	VALUE_QUERY    = 'query',
	VALUE_ACCEPT   = 'accept',
	VALUE_NOTHING  = 'nothing';

/*Extenciones*/
Object.defineProperty(Object.prototype, 'sMap'    , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return [];

	let res=[], ind=0;

	for ( let x in this )
		res.push(func.call(this, this[x], x, ind++));

	return res;
 }});
Object.defineProperty(Object.prototype, 'sReduce' , { enumerable:false, value:function( func, result ){
	if ( typeof func!=VALUE_FUNCTION ) return result;

	let ret=result, ant=null, ind=0;

	for ( let x in this ) {
		if ( ret==null ) ret = this[x];
		else             ret = func.call(this, ret, this[x], x, ant, ind++);

		ant = this[x];
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sFilter' , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return {};

	let ret={}, ind=0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++) ) {
			ret[x] = this[x];
		}
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind=0, res=null;

	for ( let x in this ) {
		res = func.call(this, this[x], x, ind++);

		if ( res ) return res;
	}
 }});
Object.defineProperty(Object.prototype, 'sForEach', { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind = 0;

	for ( let x in this ) func.call(this, this[x], x, ind++);
 }});
Object.defineProperty(Number.prototype, 'sFormat' , { enumerable:false, value:function( format       ){
	if ( format=='$' || format==',' ) {
		let num         = Math.round(parseFloat(this) * Math.pow(10, 2)) / Math.pow(10, 2);
		let regx        = /(\d+)(\d{3})/;
		let numSeparado = num.toString().split('.');
		let enteros     = numSeparado[0];
		let decimales   = numSeparado.length>1 ? '.'+numSeparado[1] : '.00';

		decimales = decimales + '00';
		decimales = decimales.substr(0, 3);

		while ( regx.test(enteros) ) {
			enteros = enteros.replace(regx, '$1,$2');
		}

		return enteros + (format=='$'?decimales:'');
	}
	else {
		let num = String(this);

		while ( num.length<format.length ) {
			num = '_' + num;
		}

		for ( let i=0; i<format.length; i++ ) {
			if ( format[i]==='0' && num[i]==='_' ) {
				num = num.replace(/_/, 0);
			}
		}

		return num;
	}
 }});
Object.defineProperty( Array.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let res=null;

	for ( let x=this.length; x--; ) {
		res = func.call(this, this[x], x);

		if ( res ) return res;
	}
 }});









class SellButton extends HTMLElement {
	constructor () {
		super();
	}

	connectedCallback () {
		let shadowRoot = this.attachShadow({mode: 'open'});

		shadowRoot.innerHTML = `
			<style>
				:host {
					--orange: #e67e22;
					--space: 1.5em;
				}
				.btn-container {
					border: 2px dashed var(--orange);
					padding: var(--space);
					text-align: center;
				}
				.btn {
					background-color: var(--orange);
					border: 0;
					border-radius: 5px;
					color: white;
					padding: var(--space);
					text-transform: uppercase;
				}
			</style>
			<div class="btn-container">
				<button class="btn">Comprar Ahora</button>
			</div>
		`;
	}
 }
window.customElements.define('sell-button', SellButton);








function MyEl() {
	return Reflect.construct(HTMLElement, [], this.constructor);
 }
MyEl.prototype = Object.create(HTMLElement.prototype);
MyEl.prototype.constructor = MyEl;
Object.setPrototypeOf(MyEl, HTMLElement);
MyEl.prototype.connectedCallback = function() {
	this.innerHTML = 'Hello world';
 };
window.customElements.define('my-el', MyEl);















/*Declaraciones*/
const tipode = ( type ) => Object.prototype.toString.call(type).replace(/object|\[|\]| /g, '').toLowerCase();

/*Extenciones*/
HTMLElement.prototype.sAdd        = function( html, after      ) {/* AGREGAR ELEMENTO */
	const edit = ( element, after ) => {
		if ( after==undefined || after!=false ) {
			while ( element.childNodes.length ) {
				this.appendChild(element.childNodes[0]);
			}
		}
		else while ( element.childNodes.length>0 ) {
			this.insertBefore(element.childNodes[0], this.childNodes[0]);
		}
	 };

	if ( typeof html==VALUE_STRING && html!='' ) {
		const div     = document.createElement('div');
		div.innerHTML = html;

		edit.call(this, div, after);
	}
	else if ( tipode(html)==VALUE_OBJECT && html.childNodes ) edit.call(this, html, after);

	return this;
 };
HTMLElement.prototype.sAttributes = function( attr, value      ) {/* AGREGAR ELEMENTO *//* ATRIBUTOS */
	const edit = ( att, val ) => {
		if ( val===undefined ) return this.getAttribute(att);
		else {
			if ( val===false ) this.removeAttribute(att);
			else               this.setAttribute(att,val);
		}
	 };

	if ( typeof attr==VALUE_STRING ) {
		if ( value===undefined ) return edit(attr       );
		else                            edit(attr, value);
	}
	else if ( tipode(attr)==VALUE_OBJECT )
		for(let x in attr) edit.call(this, x, attr[x]);

	return this;
 };
HTMLElement.prototype.sRemove     = function(                  ) {/* BORRAR ELEMENTOS */
	this.sEvents(false);
	this.parentNode.removeChild(this);

	return this;
 };
HTMLElement.prototype.sFind       = function( select           ) {/* BUSCAR HIJOS */
	return this.querySelectorAll(select);
 };
HTMLElement.prototype.sClass      = function( sclass, sdelete  ) {/* EDITAR CLASE */
	if ( typeof sclass==VALUE_STRING && sclass!='' ) {
		if ( sdelete==false ) {
			sclass = new RegExp(sclass, 'g');

			if      ( typeof this.className ==VALUE_STRING ) this.className         = this.className        .replace(sclass, '').replace(/  /g, ' ');
			else if ( tipode(this.className)==VALUE_OBJECT ) this.className.baseVal = this.className.baseVal.replace(sclass, '').replace(/  /g, ' ');
		}
		else {
			if ( typeof this.className==VALUE_STRING ) {
				if ( this.className.indexOf(sclass)==-1 )
					this.className+= ' ' + sclass;
			}
			else if ( tipode(this.className)==VALUE_OBJECT ) {
				if ( !this.className.baseVal.indexOf(sclass)==-1 )
					this.className.baseVal+= ' ' + sclass;
			}
		}
	}
	else if ( sclass===false )  {
		if      ( typeof this.className ==VALUE_STRING ) this.className         = '';
		else if ( tipode(this.className)==VALUE_OBJECT ) this.className.baseVal = '';
	}
	else if ( sclass===undefined ) return this.className;

	return this;
 };
HTMLElement.prototype.sCss        = function( property, value  ) {/* ESTILOS CSS */
	const edit=( property, value )=>{
		if (
			typeof value==VALUE_NUMBER &&
			property!='opacity' &&
			property!='zIndex'  &&
			property!='z-index' &&
			property!='order'
		) value+= 'px';

		for ( let x=this.length; x--; )
			this.style[property]!=undefined && (this.style[property]=value);
	 };

	if      ( typeof property==VALUE_STRING && value==undefined ) return window.getComputedStyle(this)[property];
	else if ( typeof property==VALUE_STRING && value!=undefined ) edit(property, value);
	else if ( tipode(property)==VALUE_OBJECT                    ) for(let x in property) edit(x, property[x]);

	return this;
 };
HTMLElement.prototype.sEvents     = function( events, idDelete ) {/* EVENTOS */
	const EditEvent = ( element, events, idDelete ) => {/* EDICION DE EVENTOS */
		if ( !element.bcEvents ) element.bcEvents=[];

		/*ELIMINAR TODOS LOS EVENTOS*/
		if ( events==false ) {
			if ( element.bcEvents ) {
				element.bcEvents.forEach(y=>element.removeEventListener(y.event, y.func));

				delete element.bcEvents;
			}
		 }

		/*VER LISTA DE EVENTOS*/
		else if ( events==undefined ) return element.bcEvents||[];

		/*LISTA DE EVENTOS CON ID*/
		else if ( typeof events==VALUE_STRING ) {
			/*ELIMINAR EVENTOS CON ID*/
			if ( idDelete===false ) {
				if ( element.bcEvents ) {
					for ( let y=element.bcEvents.length-1; y>=0; y-- ) {
						if ( element.bcEvents[y].id===events ) {
							element.removeEventListener(element.bcEvents[y].event, element.bcEvents[y].func);
							element.bcEvents.splice(y, 1);
						}
					}
				}
			 }

			/*SINGLE EVENT*/
			else if ( typeof idDelete===VALUE_FUNCTION ) {
				if ( !element.bcEvents ) element.bcEvents=[];

				element.bcEvents.push({ id:'', event:events, func:idDelete });
				element.addEventListener(events, idDelete);
			}

			/*VER LISTA DE EVENTOS CON ID*/
			else {
				let lis = [];
				if ( element.bcEvents ) element.bcEvents.forEach(x=>x.id===events && lis.push(x));
				return lis;
			 }
		 }

		/*MULTIPLES EVENTOS*/
		else if ( tipode(events)==VALUE_OBJECT ) {
			/*ELIMINAR EVENTO ESPESIFICO*/
			if ( idDelete==false ) {
				if ( element.bcEvents ) {
					events.forEach((x,y)=>{
						for ( let z=element.bcEvents.length-1; z>=0; z-- ) {
							if ( element.bcEvents[z].event===y && element.bcEvents[z].func===x ) {
								element.removeEventListener(element.bcEvents[z].event, element.bcEvents[z].func);
								element.bcEvents.splice(z, 1);
							}
						}
					});
				}
			 }

			/*AGREGAR EVENTOS*/
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id = '';

				if ( !element.bcEvents ) element.bcEvents = [];

				events.sForEach((x,y)=>{
					if ( y!='id' ) {
						element.bcEvents.push({ id:events.id, event:y, func:x });
						element.addEventListener(y, x);
					}
				});
			 }
		 }
	 }

	if      ( events===undefined                                  ) return EditEvent(this);
	else if ( typeof events==VALUE_STRING && idDelete===undefined ) return EditEvent(this, events);
	else                                                            EditEvent(this, events, idDelete);

	return this;
 };
HTMLElement.prototype.sPosition   = function( size             ) {/* TAMAÑO Y POSICION */
	if ( size===undefined ) {
		const re1 = this.getBoundingClientRect();
		const re2 = this.offsetParent!=null ?
			this.offsetParent.getBoundingClientRect():
			{top:0, left:0, right:0, bottom:0, width:0, height:0};

		return {
			top   : re1.top    - re2.top,
			left  : re1.left   - re2.left,
			right : re1.right  - re2.right,
			bottom: re1.bottom - re2.bottom,
			width : re1.width,
			height: re1.height,
		};
	}
	else if ( size=='off' ) {
		const re1 = this.getBoundingClientRect();

		return {
			top   : re1.top,
			left  : re1.left,
			right : re1.right,
			bottom: re1.bottom,
			width : re1.width,
			height: re1.height,
		};
	}
	else if ( tipode(size)==VALUE_OBJECT ) this.sCss(size);

	return gthis;
 };
HTMLElement.prototype.sTrigger    = function( event, extra     ) {/* GENERAR EVENTO */
	if ( typeof event==VALUE_STRING && event!='' ) {
		const evt = document.createEvent('HTMLEvents');

		evt.initEvent(event, false, true);

		evt.extra = extra;

		this.dispatchEvent(evt);
	}

	return this;
 };
HTMLElement.prototype.sWrapper    = function( html             ) {/* AGREGAR ELEMENTO ENVOLVENTE */
	if ( typeof html==VALUE_STRING && html!='' ) {
		let div = document.createElement('div'); div.innerHTML = html;
		let hij = div.childNodes[0];

		while ( this.childNodes.length )
			hij.appendChild(this.childNodes[0]);

		this.appendChild(hij);

		div = null;
	}

	return this;
 };
HTMLElement.prototype.sMove       = function( select, after    ) {/* MUEVE UNA ESTRUCTURA A OTRO NODO */
	if ( typeof select==VALUE_STRING ) {
		const obj = document.querySelectorAll(select)[0];

		if ( after==undefined || after ) obj.appendChild(this);
		else                             obj.insertBefore(this, obj.childNodes[0]);
	}
	else if ( typeof select.sClass==VALUE_STRING ) {
		if ( after==undefined || after ) select[0].appendChild(this);
		else                             select[0].insertBefore(this, select[0].childNodes[0]);
	}

	return this;
 };


















class lulu extends HTMLElement {
	__inerithed__() {
		super.__inerithed__ && super.__inerithed__();

		/*Herencia*/
		this.__props__ = {};
		this.id        = 12;

		/*Variables*/
		const gthis = this;
		const fs    = this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});
		const op    = {time:0};
		const va    = {};
		const co    = {};
		gthis.op    = op;
		gthis.va    = va;
		gthis.co    = co;

		/*HTML*/
		let shadowRoot       = this.attachShadow({mode: 'open'});
		shadowRoot.innerHTML = `
			<style>.lolo{color:red;}</style>
			<div id="i${gthis.id}-csec" class="lolo">Seconds:&nbsp;${op.time}</div><sell-button id="i${gthis.id}-ccol" hola="mundo"></sell-button>
		`;

		/*Variables - va*/
		va.timer = setInterval(()=>co.csec.innerHTML=('Seconds: ' + ++op.time), 1000);

		/*Funciones*/
		this.idsView     = function() {
			if ( fs.idsView ) fs.idsView();

			co[`csec`]=shadowRoot.querySelectorAll(`#i${gthis.id}-csec`)[0];
			co[`ccol`]=shadowRoot.querySelectorAll(`#i${gthis.id}-ccol`)[0];

			return this;
		 };
		this.destructor  = () => {
			clearInterval(va.timer);
		 };

		/*Creacion*/
		this.idsView();

		/*Inicio*/
		this.pClass = this.sClass;
		this.sClass = 'Timer';

		return shadowRoot;
	 }
	connectedCallback () {
		this.__inerithed__();
	 }
 }
window.customElements.define('c-lulu', lulu);


class lolo extends lulu {
	constructor() {
		super();
	 }
	__inerithed__() /*hola mundo*/ {
		let sha = super.__inerithed__ && super.__inerithed__();

		return sha;
	 }
	connectedCallback () {
		let sha   = this.__inerithed__();
		let style = document.createElement('style');

		style.appendChild(document.createTextNode('.lolo{color:blue;}'));
		sha  .appendChild(style);
	 }
 }
window.customElements.define('c-lolo', lolo);


setTimeout(
	x=>{
		console.log(55, document.querySelectorAll('c-lolo')[0].__inerithed__);
		console.log(66, document.querySelectorAll('body')[0].lololo);
	}
	, 1000
);