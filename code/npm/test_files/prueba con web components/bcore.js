'use strict';

/*Variables*/
window.__ids__   = 0;
const VALUE_FILE = 'file',
	VALUE_FOLDER   = 'folder',
	VALUE_INSERT   = 'insert',
	VALUE_UPDATE   = 'update',
	VALUE_DELETE   = 'delete',
	VALUE_GETTER   = 'getter',
	VALUE_FUNCTION = 'function',
	VALUE_OBJECT   = 'object',
	VALUE_ARRAY    = 'array',
	VALUE_STRING   = 'string',
	VALUE_NUMBER   = 'number',
	VALUE_FLOAT    = 'float',
	VALUE_BOOL     = 'bool',
	VALUE_INT      = 'int',
	VALUE_PASSWORD = 'password',
	VALUE_SPECIAL  = 'special',
	VALUE_HTML     = 'html',
	VALUE_CSS      = 'css',
	VALUE_JS       = 'js',
	VALUE_TAXONOMY = 'taxonomy',
	VALUE_JSON     = 'json',
	VALUE_TEXT     = 'text',
	VALUE_BIG      = 'big',
	VALUE_MED      = 'med',
	VALUE_MIN      = 'min',
	VALUE_SYNC     = 'sync',
	VALUE_ASYNC    = 'async',
	VALUE_NULL     = 'null',
	VALUE_WARN     = 'warn',
	VALUE_ERROR    = 'error',
	VALUE_VALID    = 'valid',
	VALUE_QUERY    = 'query',
	VALUE_ACCEPT   = 'accept',
	VALUE_NOTHING  = 'nothing';

/*Declaraciones*/
const tipode      = ( type                             ) => Object.prototype.toString.call(type).replace(/object|\[|\]| /g, '').toLowerCase();
const Hash        = ( data                             ) => {/* CREACION DE HASH hash */
	if ( !data || typeof data!=VALUE_STRING ) data = ((new Date()).toString() + __ids__++);

	let hash = 5381, num = data.length;

	while( num )
		hash = (hash * 33) ^ data.charCodeAt(--num);

	return hash>>>0;
 };
const Mime        = ( type                             ) => {/* REGRESA EL TIPO MIME DE UN TIPO DE ARCHIVO */
	switch ( type.toLowerCase() ) {
		case 'js'  : return 'application/javascript; charset=UTF-8';
		case 'bj'  : return 'application/javascript; charset=UTF-8';
		case 'css' : return 'text/css; charset=UTF-8';
		case 'ico' : return 'image/x-icon';
		case 'jpg' : return 'image/jpeg';
		case 'png' : return 'image/png';
		case 'svg' : return 'image/svg+xml';
		case 'woff': return 'font/woff';
		case 'html': return 'text/html; charset=UTF-8';
		case 'json': return 'application/json; charset=UTF-8';
		case 'txt' :
		default    : return 'text/plain';
	}
 };
const ParseUrl    = ( url                              ) => {/* ENTREGAR PATH DE LA URL */
	if ( typeof url!=VALUE_STRING ) url = document.URL;

	window.__pathUrl__ = new URL(url);

	let eur=/\b([\w\n\.\-\%]+)\b/g;
	let par=window.__pathUrl__.search, tur=window.__pathUrl__.pathname, mat;

	/*URL*/
	let dat=[];
	while ( (mat=eur.exec(tur))!==null ) {
		mat.index===eur.lastIndex && eur.lastIndex++;
		mat.forEach((match, gindex)=>gindex===1 && dat.push(match));
	}
	window.__pathUrl__.path = dat;

	/*Params*/
	let tem='', pas=[];
	while ( (mat=eur.exec(par))!==null ) {
		mat.index===eur.lastIndex && eur.lastIndex++;
		tem=='' ? tem=decodeURI(mat[0]) : (pas.push({[tem]:decodeURI(mat[0])}), tem='');
	}
	window.__pathUrl__.params = pas;

	return window.__pathUrl__;
 };
const Ajax        = ( url, params                      ) => {/* RECIBIR ARCHIVO DEL SERVIDOR CON AJAX */
	return new sProm((run,err)=>{
		fetch(url, Object.assign({mode:'cors', credentials:'include'}, params))
		.catch(err)
		.then(res=>{
			if ( res.ok ) {
				if ( /json/i.test( res.headers.get('content-type') ) ) res.json().catch(err).then(v=>run(v,res));
				else                                                   res.text().catch(err).then(v=>run(v,res));
			}
			else {
				if ( /json/i.test( res.headers.get('content-type') ) ) res.json().catch(err).then(v=>err(v,res));
				else                                                   res.text().catch(err).then(v=>err(v,res));
			}
		});
	});
 };
const Eval        = ( params                           ) => {/* EVALUAR SCRIPT */
	params = Object.assign({ id:'', type:VALUE_JS, data:'', src:'' }, params);

	if ( document.getElementById(params.id) ) return;

	if ( params.type==VALUE_CSS ) {
		let style = document.createElement('style');
		style.id  = params.id;

		style        .appendChild(document.createTextNode(params.data));
		document.head.appendChild(style);
	}
	else if ( params.type==VALUE_JS ) {
		let script  = document.createElement('script');
		script.id   = params.id;
		params.src  && (script.src =params.src);
		params.data && (script.text=params.data);

		document.head.appendChild(script);
	}
 };
const Exec        = ( clase, params                    ) => {/* EJECUCION DE MODULOS */
	return new sProm(function(run,err) {
		params    = Object.assign({ select:'body' }, params);
		let   ord = window.__orderbo__++;
		const exe = ()=>{
			if ( params.noExec===true ) return;
			params.css = Object.assign({order:ord}, params.css);
			params.eve = Object.assign({onConstructor:x=>run(x.extra)}, params.eve);
			new (eval(clase))(params);
		 };
		let   aja = ()=>{
			Ajax(params.url||`{{COM}}/${clase}.bj`, {headers:{token:'1', botype:'component', version:params.version||'+'}}).run(x=>{
				window.gcache && window.gcache.Add('cache_exec', { requestID:clase, data:x, hash:Hash(x), order:ord }).err(err);
				Eval({ id:'exec_'+clase, data:x, type:VALUE_JS });
				exe();
			 }).err(err);
		 };

		if      ( typeof window[clase]==VALUE_FUNCTION ) exe();
		else if ( window.gcache                        ){
			window.gcache.Get('cache_exec', 'requestID', clase)
			.run(x=>{
				if ( x ) {
					let nam = (typeof params.url==VALUE_STRING?params.url:clase+'_bj').split('/');
					nam     = (nam[nam.length-1]||'').replace(/[\.-]/g,'_').toLowerCase();

					if ( CACHE_HASH[nam]==x.hash ) {
						Eval({ id:'exec_'+clase, data:x.data, type:VALUE_JS });
						exe();
					}
					else window.gcache.Del('cache_exec', x.id).run(aja).err(err);
				}
				else aja();
			 })
			.err(err);
		 }
		else                                             aja();
	});
 };
const RGBaXYZ     = ( red, green, blue                 ) => {/* CONVERCION DE COLOR RGB A XYZ */
	red  /= 255.0;
	green/= 255.0;
	blue /= 255.0;

	if ( red   > 0.04045 ) red   = Math.pow( ( ( red   + 0.055 ) / 1.055 ), 2.4 ); else red   = red   / 12.92;
	if ( green > 0.04045 ) green = Math.pow( ( ( green + 0.055 ) / 1.055 ), 2.4 ); else green = green / 12.92;
	if ( blue  > 0.04045 ) blue  = Math.pow( ( ( blue  + 0.055 ) / 1.055 ), 2.4 ); else blue  = blue  / 12.92;

	red  *= 100.0;
	green*= 100.0;
	blue *= 100.0;

	return ( {
		x: red * 0.4124 + green * 0.3576 + blue * 0.1805,
		y: red * 0.2126 + green * 0.7152 + blue * 0.0722,
		z: red * 0.0193 + green * 0.1192 + blue * 0.9505,
	} );
 };
const RGBaLAB     = ( red, green, blue, alpha          ) => {/* CONVERCION DE COLOR RGB A LAB */
	let xyz = RGBaXYZ( red, green, blue );
	let rex = xyz.x / 95.047 ;
	let rey = xyz.y / 100.0  ;
	let rez = xyz.z / 108.883;

	if ( rex > 0.008856 ) rex = Math.pow( rex, ( 1.0 / 3.0 ) ); else rex = ( 7.787 * rex ) + ( 16.0 / 116.0 );
	if ( rey > 0.008856 ) rey = Math.pow( rey, ( 1.0 / 3.0 ) ); else rey = ( 7.787 * rey ) + ( 16.0 / 116.0 );
	if ( rez > 0.008856 ) rez = Math.pow( rez, ( 1.0 / 3.0 ) ); else rez = ( 7.787 * rez ) + ( 16.0 / 116.0 );

	return ( {
		l: ( 116.0 * rey ) - 16.0,
		a: 500.0 * ( rex - rey ) ,
		b: 200.0 * ( rey - rez ) ,
		p: alpha                 ,
	} );
 };
const LABaXYZ     = ( l, a, b                          ) => {/* CONVERCION DE COLOR LAB A XYZ */
	let y = ( l + 16.0 ) / 116.0;
	let x = ( a / 500.0 ) + y;
	let z = y - ( b / 200.0 );

	if ( Math.pow( y, 3.0 ) > 0.008856 ) y = Math.pow( y, 3.0 ); else y = ( y - 16.0 / 116.0 ) / 7.787;
	if ( Math.pow( x, 3.0 ) > 0.008856 ) x = Math.pow( x, 3.0 ); else x = ( x - 16.0 / 116.0 ) / 7.787;
	if ( Math.pow( z, 3.0 ) > 0.008856 ) z = Math.pow( z, 3.0 ); else z = ( z - 16.0 / 116.0 ) / 7.787;

	return ( {
		x: x * 95.047 ,
		y: y * 100.0  ,
		z: z * 108.883,
	} );
 };
const LABaRGB     = ( l, a, b                          ) => {/* CONVERCION DE COLOR LAB A RGB */
	let xyz = LABaXYZ( l, a, b );

	xyz.x/= 100.0;
	xyz.y/= 100.0;
	xyz.z/= 100.0;

	let rgb = {
		r: xyz.x *  3.2406 + xyz.y * -1.5372 + xyz.z * -0.4986,
		g: xyz.x * -0.9689 + xyz.y *  1.8758 + xyz.z *  0.0415,
		b: xyz.x *  0.0557 + xyz.y * -0.2040 + xyz.z *  1.0570,
	};

	if ( rgb.r > 0.0031308 ) rgb.r = 1.055 * Math.pow( rgb.r, ( 1.0 / 2.4 ) ) - 0.055; else rgb.r = 12.92 * rgb.r;
	if ( rgb.g > 0.0031308 ) rgb.g = 1.055 * Math.pow( rgb.g, ( 1.0 / 2.4 ) ) - 0.055; else rgb.g = 12.92 * rgb.g;
	if ( rgb.b > 0.0031308 ) rgb.b = 1.055 * Math.pow( rgb.b, ( 1.0 / 2.4 ) ) - 0.055; else rgb.b = 12.92 * rgb.b;

	rgb.r*= 255.0; rgb.r = rgb.r < 0.0 ? 0.0 : ( rgb.r > 255.0 ? 255.0 : rgb.r );
	rgb.g*= 255.0; rgb.g = rgb.g < 0.0 ? 0.0 : ( rgb.g > 255.0 ? 255.0 : rgb.g );
	rgb.b*= 255.0; rgb.b = rgb.b < 0.0 ? 0.0 : ( rgb.b > 255.0 ? 255.0 : rgb.b );

	return rgb;
 };
const Linear      = ( progres, start, final_start, max ) => final_start*progres/max+start;
const Back        = {
	In   : ( progres, start, final_start, max )=>final_start * ( progres/= max ) * progres * ( 2.70158 * progres - 1.70158 ) + start,
	Out  : ( progres, start, final_start, max )=>final_start * ( ( progres = ( progres / max ) - 1.0 ) * progres * ( 2.70158 * progres + 1.70158 ) + 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1 )
		return final_start / 2.0 * ( progres * progres * ( 3.5949095 * progres - 2.5949095 ) ) + start;
		return final_start / 2.0 * ( ( progres-= 2.0 ) * progres * ( 3.5949095 * progres + 2.5949095 ) + 2.0 ) + start;
	 },
 };
const Bounce      = {
	In   : ( progres, start, final_start, max )=>{
		progres = max - progres;

		if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) { progres = final_start * ( 7.5625 *   progres                       * progres            ); }
		else if (   progres         < ( 2.0 / 2.75 ) ) { progres = final_start * ( 7.5625 * ( progres -= ( 1.5   / 2.75 ) ) * progres + 0.75     ); }
		else if (   progres         < ( 2.5 / 2.75 ) ) { progres = final_start * ( 7.5625 * ( progres -= ( 2.25  / 2.75 ) ) * progres + 0.9375   ); }
		else                                           { progres = final_start * ( 7.5625 * ( progres -= ( 2.625 / 2.75 ) ) * progres + 0.984375 ); }

		return final_start - progres + start;
	 },
	Out  : ( progres, start, final_start, max )=>{
		if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) return final_start * ( 7.5625 *   progres                      * progres            ) + start;
		else if (   progres         < ( 2.0 / 2.75 ) ) return final_start * ( 7.5625 * ( progres-= ( 1.5   / 2.75 ) ) * progres + 0.75     ) + start;
		else if (   progres         < ( 2.5 / 2.75 ) ) return final_start * ( 7.5625 * ( progres-= ( 2.25  / 2.75 ) ) * progres + 0.9375   ) + start;
		else                                           return final_start * ( 7.5625 * ( progres-= ( 2.625 / 2.75 ) ) * progres + 0.984375 ) + start;
	 },
	InOut: ( progres, start, final_start, max )=>{
		if ( progres < max / 2.0 ) {
			progres = max - ( progres * 2.0 );

			if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) progres = final_start * ( 7.5625 *   progres                      * progres            );
			else if (   progres         < ( 2.0 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 1.5   / 2.75 ) ) * progres + 0.75     );
			else if (   progres         < ( 2.5 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 2.25  / 2.75 ) ) * progres + 0.9375   );
			else                                           progres = final_start * ( 7.5625 * ( progres-= ( 2.625 / 2.75 ) ) * progres + 0.984375 );

			return ( final_start - progres ) * 0.5 + start;
		}
		else {
			progres = progres * 2.0 - max;

			if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) progres = final_start * ( 7.5625 *   progres                      * progres            );
			else if (   progres         < ( 2.0 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 1.5   / 2.75 ) ) * progres + 0.75     );
			else if (   progres         < ( 2.5 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 2.25  / 2.75 ) ) * progres + 0.9375   );
			else                                           progres = final_start * ( 7.5625 * ( progres-= ( 2.625 / 2.75 ) ) * progres + 0.984375 );

			return progres * 0.5 + final_start * 0.5 + start;
		}
	 }
 };
const Circular    = {
	In   : ( progres, start, final_start, max )=>{
		progres = 1.0 - ( progres /= max ) * progres;

		if ( progres < 0 ) progres = 0;

		return -final_start * ( Math.sqrt( progres ) - 1.0 ) + start;
	 },
	Out  : ( progres, start, final_start, max )=>{
		progres = 1.0 - ( progres = ( progres / max ) - 1.0 ) * progres;

		if ( progres < 0 ) progres = 0;

		return final_start * Math.sqrt( progres ) + start;
	 },
	InOut: ( progres, start, final_start, max )=>{
		progres/= max / 2.0;

		if ( progres < 1.0 )
			return -final_start / 2.0 * ( Math.sqrt( 1.0 - progres * progres ) - 1.0 ) + start;

		progres-= 2.0;

		return final_start / 2.0 * ( Math.sqrt( 1.0 - progres * progres ) + 1.0 ) + start;
	 },
 };
const Cubic       = {
	In   : ( progres, start, final_start, max )=>final_start * ( progres/= max ) * progres * progres + start,
	Out  : ( progres, start, final_start, max )=>final_start * ( ( progres = ( progres/= max ) - 1 ) * progres * progres + 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		progres/= max / 2.0;

		if ( progres < 1.0 )
			return final_start / 2.0 * progres * progres * progres + start;

		progres-= 2;

		return final_start / 2.0 * ( progres * progres * progres + 2.0 ) + start;
	 },
 };
const Elastic     = {
	In   : ( progres, start, final_start, max )=>{
		if (   progres         == 0.0 ) return start              ;
		if ( ( progres/= max ) == 1.0 ) return start + final_start;

		let p       = max * 0.3;
		let a       = final_start;
		let s       = p / 4.0;
		let postFix = a * Math.pow( 2.0, 10.0 * ( progres-= 1.0 ) );

		return -( postFix * Math.sin( ( progres * max - s ) * ( 2.0 * 3.14159265 ) / p ) ) + start;
	 },
	Out  : ( progres, start, final_start, max )=>{
		if (   progres         == 0.0 ) return start              ;
		if ( ( progres/= max ) == 1.0 ) return start + final_start;

		let p = max * 0.3;
		let a = final_start;
		let s = p / 4.0;

		return ( a * Math.pow( 2.0, -10.0 * progres ) * Math.sin( ( progres * max - s ) * ( 2.0 * 3.14159265 ) / p ) + final_start + start );
	 },
	InOut: ( progres, start, final_start, max )=>{
		if (   progres               == 0.0 ) return start              ;
		if ( ( progres/= max / 2.0 ) == 2.0 ) return start + final_start;

		let p = max * ( 0.3 * 1.5 );
		let a = final_start;
		let s = p / 4.0;

		if ( progres < 1.0 ) {
			let postFix = a * Math.pow( 2.0, 10.0 * ( progres-= 1.0 ) );

			return -0.5 * ( postFix * Math.sin( ( progres * max - s ) * ( 2.0 * 3.14159265 ) / p ) ) + start;
		}

		let postFix = a * Math.pow( 2.0, -10.0 * ( progres-= 1.0 ) );

		return postFix * Math.sin( ( ( progres * max ) - s ) * ( 2.0 * 3.14159265 ) / p ) * 0.5 + final_start + start;
	 },
 };
const Exponential = {
	In   : ( progres, start, final_start, max )=>( progres == 0.0 ) ? start               : final_start *    Math.pow( 2.0,  10.0 * ( progres / max   - 1.0 ) ) + start,
	Out  : ( progres, start, final_start, max )=>( progres == max ) ? start + final_start : final_start * ( -Math.pow( 2.0, -10.0 *   progres / max ) + 1.0 )   + start,
	InOut: ( progres, start, final_start, max )=>{
		if (   progres == 0.0               ) return start;
		if (   progres == max               ) return start + final_start;
		if ( ( progres /= max / 2.0 ) < 1.0 ) return final_start / 2.0 * Math.pow( 2.0, 10.0 * ( progres - 1.0 ) ) + start;

		return final_start / 2.0 * ( -Math.pow( 2.0, -10.0 * --progres ) + 2.0 ) + start;
	 },
 };
const Quadratic   = {
	In   : ( progres, start, final_start, max )=> final_start * ( progres/= max ) *   progres         + start,
	Out  : ( progres, start, final_start, max )=>-final_start * ( progres/= max ) * ( progres - 2.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1.0 )
		return  final_start / 2.0 *     progres *   progres                 + start;
		return -final_start / 2.0 * ( --progres * ( progres - 2.0 ) - 1.0 ) + start;
	 },
 };
const Quartic     = {
	In   : ( progres, start, final_start, max )=> final_start * ( progres/= max ) * progres * progres * progres + start,
	Out  : ( progres, start, final_start, max )=>-final_start * ( ( progres = ( progres/= max ) - 1 ) * progres * progres * progres - 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1.0 )
		return  final_start / 2.0 *     progres         * progres * progres * progres         + start;
		return -final_start / 2.0 * ( ( progres-= 2.0 ) * progres * progres * progres - 2.0 ) + start;
	 },
 };
const Quintic     = {
	In   : ( progres, start, final_start, max )=>final_start * (   progres/= max                   ) * progres * progres * progres * progres         + start,
	Out  : ( progres, start, final_start, max )=>final_start * ( ( progres = ( progres/= max ) - 1 ) * progres * progres * progres * progres + 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1.0 )
		return final_start / 2.0 *     progres         * progres * progres * progres * progres         + start;
		return final_start / 2.0 * ( ( progres-= 2.0 ) * progres * progres * progres * progres + 2.0 ) + start;
	 },
 };
const Sinusoidal  = {
	In   : ( progres, start, final_start, max )=>-final_start       *   Math.cos( progres / max * 1.570796325 ) + start + final_start,
	Out  : ( progres, start, final_start, max )=> final_start       *   Math.sin( progres / max * 1.570796325 ) + start,
	InOut: ( progres, start, final_start, max )=>-final_start / 2.0 * ( Math.cos( 3.14159265 * progres / max ) - 1.0 ) + start,
 };

/*Extenciones*/
Object.defineProperty(Object.prototype, 'sMap'    , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return [];

	let res=[], ind=0;

	for ( let x in this )
		res.push(func.call(this, this[x], x, ind++));

	return res;
 }});
Object.defineProperty(Object.prototype, 'sReduce' , { enumerable:false, value:function( func, result ){
	if ( typeof func!=VALUE_FUNCTION ) return result;

	let ret=result, ant=null, ind=0;

	for ( let x in this ) {
		if ( ret==null ) ret = this[x];
		else             ret = func.call(this, ret, this[x], x, ant, ind++);

		ant = this[x];
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sFilter' , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return {};

	let ret={}, ind=0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++) ) {
			ret[x] = this[x];
		}
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind=0, res=null;

	for ( let x in this ) {
		res = func.call(this, this[x], x, ind++);

		if ( res ) return res;
	}
 }});
Object.defineProperty(Object.prototype, 'sForEach', { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind = 0;

	for ( let x in this ) func.call(this, this[x], x, ind++);
 }});
Object.defineProperty(Number.prototype, 'sFormat' , { enumerable:false, value:function( format       ){
	if ( format=='$' || format==',' ) {
		let num         = Math.round(parseFloat(this) * Math.pow(10, 2)) / Math.pow(10, 2);
		let regx        = /(\d+)(\d{3})/;
		let numSeparado = num.toString().split('.');
		let enteros     = numSeparado[0];
		let decimales   = numSeparado.length>1 ? '.'+numSeparado[1] : '.00';

		decimales = decimales + '00';
		decimales = decimales.substr(0, 3);

		while ( regx.test(enteros) ) {
			enteros = enteros.replace(regx, '$1,$2');
		}

		return enteros + (format=='$'?decimales:'');
	}
	else {
		let num = String(this);

		while ( num.length<format.length ) {
			num = '_' + num;
		}

		for ( let i=0; i<format.length; i++ ) {
			if ( format[i]==='0' && num[i]==='_' ) {
				num = num.replace(/_/, 0);
			}
		}

		return num;
	}
 }});
Object.defineProperty( Array.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let res=null;

	for ( let x=this.length; x--; ) {
		res = func.call(this, this[x], x);

		if ( res ) return res;
	}
 }});

HTMLElement.prototype.sAdd        = function( html, after      ) {/* AGREGAR ELEMENTO */
	const element = this.__shadow__ || this;

	const edit = ( _element, _after ) => {
		if ( _after==undefined || _after!=false ) {
			while ( _element.childNodes.length ) {
				element.appendChild(_element.childNodes[0]);
			}
		}
		else while ( _element.childNodes.length>0 ) {
			element.insertBefore(_element.childNodes[0], element.childNodes[0]);
		}
	 };

	if ( typeof html==VALUE_STRING && html!='' ) {
		const div     = document.createElement('div');
		div.innerHTML = html;

		edit.call(this, div, after);
	}
	else if ( tipode(html)==VALUE_OBJECT && html.childNodes ) edit.call(this, html, after);

	return this;
 };
HTMLElement.prototype.sAttributes = function( attr, value      ) {/* AGREGAR ELEMENTO *//* ATRIBUTOS */
	const edit = ( att, val ) => {
		if ( val===undefined ) return this.getAttribute(att);
		else {
			if ( val===false ) this.removeAttribute(att);
			else               this.setAttribute(att,val);
		}
	 };

	if ( typeof attr==VALUE_STRING ) {
		if ( value===undefined ) return edit(attr       );
		else                            edit(attr, value);
	}
	else if ( tipode(attr)==VALUE_OBJECT )
		for(let x in attr) edit.call(this, x, attr[x]);

	return this;
 };
HTMLElement.prototype.sRemove     = function(                  ) {/* BORRAR ELEMENTOS */
	this.sEvents(false);
	this.parentNode.removeChild(this);

	return this;
 };
HTMLElement.prototype.sFind       = function( select           ) {/* BUSCAR HIJOS */
	return this.querySelectorAll(select);
 };
HTMLElement.prototype.sClass      = function( sclass, sdelete  ) {/* EDITAR CLASE */
	if ( typeof sclass==VALUE_STRING && sclass!='' ) {
		if ( sdelete==false ) {
			sclass = new RegExp(sclass, 'g');

			if      ( typeof this.className ==VALUE_STRING ) this.className         = this.className        .replace(sclass, '').replace(/  /g, ' ');
			else if ( tipode(this.className)==VALUE_OBJECT ) this.className.baseVal = this.className.baseVal.replace(sclass, '').replace(/  /g, ' ');
		}
		else {
			if ( typeof this.className==VALUE_STRING ) {
				if ( this.className.indexOf(sclass)==-1 )
					this.className+= ' ' + sclass;
			}
			else if ( tipode(this.className)==VALUE_OBJECT ) {
				if ( !this.className.baseVal.indexOf(sclass)==-1 )
					this.className.baseVal+= ' ' + sclass;
			}
		}
	}
	else if ( sclass===false )  {
		if      ( typeof this.className ==VALUE_STRING ) this.className         = '';
		else if ( tipode(this.className)==VALUE_OBJECT ) this.className.baseVal = '';
	}
	else if ( sclass===undefined ) return this.className;

	return this;
 };
HTMLElement.prototype.sCss        = function( property, value  ) {/* ESTILOS CSS */
	const edit=( property, value )=>{
		if (
			typeof value==VALUE_NUMBER &&
			property!='opacity' &&
			property!='zIndex'  &&
			property!='z-index' &&
			property!='order'
		) value+= 'px';

		for ( let x=this.length; x--; )
			this.style[property]!=undefined && (this.style[property]=value);
	 };

	if      ( typeof property==VALUE_STRING && value==undefined ) return window.getComputedStyle(this)[property];
	else if ( typeof property==VALUE_STRING && value!=undefined ) edit(property, value);
	else if ( tipode(property)==VALUE_OBJECT                    ) for(let x in property) edit(x, property[x]);

	return this;
 };
HTMLElement.prototype.sEvents     = function( events, idDelete ) {/* EVENTOS */
	const EditEvent = ( element, events, idDelete ) => {/* EDICION DE EVENTOS */
		if ( !element.bcEvents ) element.bcEvents=[];

		/*ELIMINAR TODOS LOS EVENTOS*/
		if ( events==false ) {
			if ( element.bcEvents ) {
				element.bcEvents.forEach(y=>element.removeEventListener(y.event, y.func));

				delete element.bcEvents;
			}
		 }

		/*VER LISTA DE EVENTOS*/
		else if ( events==undefined ) return element.bcEvents||[];

		/*LISTA DE EVENTOS CON ID*/
		else if ( typeof events==VALUE_STRING ) {
			/*ELIMINAR EVENTOS CON ID*/
			if ( idDelete===false ) {
				if ( element.bcEvents ) {
					for ( let y=element.bcEvents.length-1; y>=0; y-- ) {
						if ( element.bcEvents[y].id===events ) {
							element.removeEventListener(element.bcEvents[y].event, element.bcEvents[y].func);
							element.bcEvents.splice(y, 1);
						}
					}
				}
			 }

			/*SINGLE EVENT*/
			else if ( typeof idDelete===VALUE_FUNCTION ) {
				if ( !element.bcEvents ) element.bcEvents=[];

				element.bcEvents.push({ id:'', event:events, func:idDelete });
				element.addEventListener(events, idDelete);
			}

			/*VER LISTA DE EVENTOS CON ID*/
			else {
				let lis = [];
				if ( element.bcEvents ) element.bcEvents.forEach(x=>x.id===events && lis.push(x));
				return lis;
			 }
		 }

		/*MULTIPLES EVENTOS*/
		else if ( tipode(events)==VALUE_OBJECT ) {
			/*ELIMINAR EVENTO ESPESIFICO*/
			if ( idDelete==false ) {
				if ( element.bcEvents ) {
					events.forEach((x,y)=>{
						for ( let z=element.bcEvents.length-1; z>=0; z-- ) {
							if ( element.bcEvents[z].event===y && element.bcEvents[z].func===x ) {
								element.removeEventListener(element.bcEvents[z].event, element.bcEvents[z].func);
								element.bcEvents.splice(z, 1);
							}
						}
					});
				}
			 }

			/*AGREGAR EVENTOS*/
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id = '';

				if ( !element.bcEvents ) element.bcEvents = [];

				events.sForEach((x,y)=>{
					if ( y!='id' ) {
						element.bcEvents.push({ id:events.id, event:y, func:x });
						element.addEventListener(y, x);
					}
				});
			 }
		 }
	 }

	if      ( events===undefined                                  ) return EditEvent(this);
	else if ( typeof events==VALUE_STRING && idDelete===undefined ) return EditEvent(this, events);
	else                                                            EditEvent(this, events, idDelete);

	return this;
 };
HTMLElement.prototype.sPosition   = function( size             ) {/* TAMAÑO Y POSICION */
	if ( size===undefined ) {
		const re1 = this.getBoundingClientRect();
		const re2 = this.offsetParent!=null ?
			this.offsetParent.getBoundingClientRect():
			{top:0, left:0, right:0, bottom:0, width:0, height:0};

		return {
			top   : re1.top    - re2.top,
			left  : re1.left   - re2.left,
			right : re1.right  - re2.right,
			bottom: re1.bottom - re2.bottom,
			width : re1.width,
			height: re1.height,
		};
	}
	else if ( size=='off' ) {
		const re1 = this.getBoundingClientRect();

		return {
			top   : re1.top,
			left  : re1.left,
			right : re1.right,
			bottom: re1.bottom,
			width : re1.width,
			height: re1.height,
		};
	}
	else if ( tipode(size)==VALUE_OBJECT ) this.sCss(size);

	return gthis;
 };
HTMLElement.prototype.sTrigger    = function( event, extra     ) {/* GENERAR EVENTO */
	if ( typeof event==VALUE_STRING && event!='' ) {
		const evt = document.createEvent('HTMLEvents');

		evt.initEvent(event, false, true);

		evt.extra = extra;

		this.dispatchEvent(evt);
	}

	return this;
 };
HTMLElement.prototype.sWrapper    = function( html             ) {/* AGREGAR ELEMENTO ENVOLVENTE */
	if ( typeof html==VALUE_STRING && html!='' ) {
		let div = document.createElement('div'); div.innerHTML = html;
		let hij = div.childNodes[0];

		while ( this.childNodes.length )
			hij.appendChild(this.childNodes[0]);

		this.appendChild(hij);

		div = null;
	}

	return this;
 };
HTMLElement.prototype.sMove       = function( select, after    ) {/* MUEVE UNA ESTRUCTURA A OTRO NODO */
	if ( typeof select==VALUE_STRING ) {
		const obj = document.querySelectorAll(select)[0];

		if ( after==undefined || after ) obj.appendChild(this);
		else                             obj.insertBefore(this, obj.childNodes[0]);
	}
	else if ( typeof select.sClass==VALUE_STRING ) {
		if ( after==undefined || after ) select[0].appendChild(this);
		else                             select[0].insertBefore(this, select[0].childNodes[0]);
	}

	return this;
 };

/*CLASES*/
function sProm( parent, func ) {
	/*Variables*/
	const gthis   = this;
	gthis.__sPromRun__ = [];
	gthis.__sPromErr__ = 0;
	gthis.__sPromAll__ = 0;
	gthis.__sPromTis__ = 0;
	gthis.fwait        = [];
	gthis.sClass       = 'sProm';

	/*Inicio*/
	if ( typeof parent==VALUE_FUNCTION ) {
		func   = parent;
		parent = undefined;
	}
	else for ( let x in parent ) {
		if ( typeof parent[x]==VALUE_FUNCTION )
			this[x]=(...params)=>ExecFunc(x, params);
	}

	/*Metodos*/
	gthis.run=( fnew )=>{gthis.__sPromRun__.push(fnew); return gthis; };
	gthis.err=( fnew )=>{gthis.__sPromErr__ = fnew;     return gthis; };
	gthis.all=( fnew )=>{gthis.__sPromAll__ = fnew;     return gthis; };
	gthis.tis=( para )=>{gthis.__sPromTis__ = para;     return gthis; };

	/*Funciones*/
	function Run( ...params ) {
		while ( gthis.__sPromRun__.length ) {
			if ( typeof gthis.__sPromRun__[0]==VALUE_FUNCTION ) {
				let res = gthis.__sPromRun__[0](...params);

				if ( res && res.sClass && res.sClass=='sProm' ) {
					res.err(gthis.__sPromErr__);
					gthis.__sPromRun__.splice(0, 1);
					for(let x=0;x<gthis.__sPromRun__.length;x++) res.run(gthis.__sPromRun__[x]);
					break;
				}
			}
			else if ( tipode(gthis.__sPromRun__[0])==VALUE_OBJECT && parent ) {
				if ( typeof gthis.__sPromRun__[0].func==VALUE_FUNCTION || typeof parent[gthis.__sPromRun__[0].func]==VALUE_FUNCTION ) {
					let res;

					if ( typeof gthis.__sPromRun__[0].func==VALUE_FUNCTION ) res = gthis.__sPromRun__[0].func(gthis.__sPromRun__[0].params);
					else                                                     res = parent[gthis.__sPromRun__[0].func](...gthis.__sPromRun__[0].params);

					if ( res!=undefined ) {
						if ( res.sClass && res.sClass=='sProm' ) {
							res.err(gthis.__sPromErr__);
							gthis.__sPromRun__.splice(0, 1);
							for(let x=0;x<gthis.__sPromRun__.length;x++) res.run(gthis.__sPromRun__[x]);
							break;
						}
						else {
							for ( let x=gthis.__sPromRun__.length; x--; ) {
								if      ( typeof gthis.__sPromRun__[x]     ==VALUE_FUNCTION ) gthis.__sPromRun__[x]        = { func:gthis.__sPromRun__[x], params:res };
								else if ( typeof gthis.__sPromRun__[x].func==VALUE_FUNCTION ) gthis.__sPromRun__[x].params = res;
							}
						}
					}
				}
			}

			gthis.__sPromRun__.splice(0, 1);
		}

		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION )
			gthis.__sPromAll__(...params);
	 }
	function Err( ...params ) {
		if ( typeof gthis.__sPromErr__==VALUE_FUNCTION ) gthis.__sPromErr__(...params);
		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION ) gthis.__sPromAll__(...params);
	 }
	function All( ...params ) {
		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION )
			gthis.__sPromAll__(...params);
	 }

	function ExecFunc( func, params ) {
		if ( params[0]==VALUE_ASYNC ){
			params.splice(0, 1);

			return parent[func](...params);
		}
		else {
			gthis.fwait.push(func);
			gthis.__sPromRun__.push({ func:func, params:params });
		}

		return gthis;
	 }

	/*Inicio*/
	if ( Promise ) new Promise((run,err)=>{run()}).then(func.bind(gthis, Run, Err, All));
	else           setTimeout(func.bind(gthis, Run, Err, All), 10);
 }

class sObject         extends HTMLElement {
	/*Creacion*/
	constructor() {
		super();

		this.__id__     = 'i' + window.__ids__++;
		this.__parent__ = this.__class__||'';
		this.__class__  = 'sObject';
	 }

	/*Metodos*/
	__render__ ( html, idsView ) {
		/*Herencia*/
		super.__render__ && super.__render__();

		/*Variables*/
		const gthis = this;
		const op    = this.__props__;
		const va    = {};
		const co    = {};
		gthis.op    = op;
		gthis.va    = va;
		gthis.co    = co;

		/*Inicio*/
		this.sAdd(html);

		for ( let x = idsView.length; x--; )
			this.co[ idsView[x] ] = this.__shadow__.querySelectorAll(`#${ idsView[x] }`)[0];
	 }
 }
class cPrueba         extends sObject     {
	/*Creacion*/
	constructor      ( props={} ) {
		super(props);

		this.__props__  = props;
		this.__parent__ = this.__class__||'';
		this.__class__  = 'c_prueba';
	 }
	connectedCallback(          ) {
		this.__shadow__ = this.attachShadow({ mode:'open' });

		this.__render__();
	 }

	/*Metodos*/
	__render__( html='', idsView=[] ) {
		/*Herencia*/
		super.__render__ &&
		super.__render__(
			`
				${html}
				<style>
					:host {
						--orange: #e67e22;
						--space: 1.5em;
					}
					.btn-container {
						border: 2px dashed var(--orange);
						padding: var(--space);
						text-align: center;
					}
					.btn {
						background-color: var(--orange);
						border: 0;
						border-radius: 5px;
						color: white;
						padding: var(--space);
						text-transform: uppercase;
					}
				</style>

				<div class="btn-container" id="cont">
					<button class="btn" id="titulo">Comprar Ahora</button>
				</div>
			`,
			idsView.concat(['titulo'])
		);
	 }
 } window.customElements.define('c-prueba', cPrueba);
class cPruebaHerencia extends cPrueba     {

	/*Metodos*/
	__render__( html='', idsView=[] ) {
		/*Herencia*/
		super.__render__ &&
		super.__render__(
			`
				${html}
				<style>
					.btn {
						background-color: blue;
						border: 0;
						border-radius: 5px;
						color: white;
						padding: var(--space);
						text-transform: uppercase;
					}
				</style>
				<div id="content">hola mundo</div>
			`,
			idsView.concat(['content'])
		);
	 }
 } window.customElements.define('c-prueba-herencia', cPruebaHerencia);





