# # Encabezado 1
## Encabezado 2
### Encabezado 3
#### Encabezado 4
##### Encabezado 5
###### Encabezado 6

### Citass
> Creo que los animales ven en el hombre un ser igual a ellos que ha perdido de forma extraordinariamente peligrosa el sano intelecto animal.
hola
mundo
>
hola
mundo
dos
y
tres
> Es decir, que ven en él al animal irracional, al animal que ríe, al animal que llora, al animal infeliz. — Friedrich Nietzsche

esto no es una cita

> Esto sería una cita como la que acabas de ver.
>
>> Dentro de ella puedes anidar otra cita.
>
> > > ssss
> > sssss
> > sss
>
> La cita principal llegaría hasta aquí.

### Listas
- Elemento de lista 1
**s**
a1

wew#e1

- Elemento de lista 2
* Elemento de lista 3
* Elemento de lista 4
+ Elemento de lista 5
+ Elemento de lista 6

- Elemento de lista a
- Elemento de lista b

- Elemento de lista aa1
	- Elemento de lista bb
		* Elemento de lista aaa
		* Elemento de lista bbb

#### Listas ordenadas
1. Elemento de lista o1
2. Elemento de lista o2
	- Elemento de lista o3
	- Elemento de lista o4
		1. Elemento de lista o5
		2. Elemento de lista o6

#### Lista de tareas
- [ ] tarea 1
- [ ] tarea 2
- [ ] tarea 3

### Bloque de Codigo
~~~
Creando códigos de bloque.
Puedes añadir tantas líneas y párrafos como quieras.
~~~
~~~js
for (let hola=123; hola<=0; hola++, lolo='esta es una cadena');
~~~
hola ```esto es``` el mundo
hola ~~~esto es~~~ el mundo

### Bloque de ecuaciones
$$
2+3^{(2-2)}-3
$$

### Reglas horizontales
***
---
___

### Elementos de línea
*cursiva*
_cursiva_
**negrita**
__negrita__
***cursiva y negrita***
___cursiva y negrita___
~~letra tachada~~
<!--comentario-->

### Links o enlaces

#### Links o enlaces en línea

[enlace en línea](http:www.tuenlace.com)

#### Links o enlaces como referencia

este es un ejemplo del [nombre que quieres darle a tu enlace][nombre de tu referencia]
[nombre de tu referencia]: http:www.tuenlace.com/11

#### Links automáticos
www.l o.lokhjkhkjhkjhkjhasda
asdasdwww.lo.lokhjkhkjhkjhkjh asdas
http://s.s
http://www.tuenlace.com
https://tuenlace.com
-_jesus@aldrete.run
-__jesus@aldrete.run
-___jesus@aldrete.run
<jesus@aldrete.run>

### Escapar Markdown

\*cursiva\*
\_cursiva\_
\*\*negrita\*\*
\_\_negrita\_\_

### Tablas

| titulo 1          | titulo 2          |
| ----------------- | ----------------- |
| columna 1, fila 1 | columna 2, fila 1 |
| columna 1, fila 2 | columna 2, fila 2 |
| columna 1, fila 3 | columna 2, fila 3 |

#### Tabla de contenidos
[TOC]

### Imagenes
![mi imagen](https://resources.blackkraken.run/title-readme-md.png)

#### Imagen con link

[![esta es mi imagen con link](https://resources.blackkraken.run/title-readme-md.png)](http:www.tuenlace.com)