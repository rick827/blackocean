'use strict';

/*Variables*/
window.__ids__          = 0;
window.__orderbo__      = 0;
window.__dependencies__ = [];
const VALUE_FILE        = 'file',
	VALUE_FOLDER   = 'folder',
	VALUE_INSERT   = 'insert',
	VALUE_UPDATE   = 'update',
	VALUE_DELETE   = 'delete',
	VALUE_GETTER   = 'getter',
	VALUE_FUNCTION = 'function',
	VALUE_OBJECT   = 'object',
	VALUE_ARRAY    = 'array',
	VALUE_STRING   = 'string',
	VALUE_NUMBER   = 'number',
	VALUE_FLOAT    = 'float',
	VALUE_BOOL     = 'bool',
	VALUE_INT      = 'int',
	VALUE_PASSWORD = 'password',
	VALUE_SPECIAL  = 'special',
	VALUE_HTML     = 'html',
	VALUE_CSS      = 'css',
	VALUE_JS       = 'js',
	VALUE_TAXONOMY = 'taxonomy',
	VALUE_JSON     = 'json',
	VALUE_TEXT     = 'text',
	VALUE_BIG      = 'big',
	VALUE_MED      = 'med',
	VALUE_MIN      = 'min',
	VALUE_SYNC     = 'sync',
	VALUE_ASYNC    = 'async',
	VALUE_NULL     = 'null',
	VALUE_WARN     = 'warn',
	VALUE_ERROR    = 'error',
	VALUE_VALID    = 'valid',
	VALUE_QUERY    = 'query',
	VALUE_ACCEPT   = 'accept',
	VALUE_NOTHING  = 'nothing';
const CACHE_STRUCT      = [
	{ name:'cache_ajax'    , cams:{ requestID:{ unique:true }, data:{}, hash:{}           } },
	{ name:'cache_exec'    , cams:{ requestID:{ unique:true }, data:{}, hash:{}, order:{} } },
	{ name:'cache_json'    , cams:{ requestID:{ unique:true }, data:{}, hash:{}           } },
	{ name:'cache_taxonomy', cams:{ requestID:{ unique:true }, data:{}, hash:{}           } },
	{ name:'cache_images'  , cams:{ requestID:{ unique:true }, data:{}                    } },
 ];

/*Declaraciones*/
const o           = ( select                           ) => {
	return document.querySelectorAll(select);
 };
const c           = ( sclass                           ) => coms.sFilter(v=>v.sClass==sclass && v);
const tipode      = ( type                             ) => Object.prototype.toString.call(type).replace(/object|\[|\]| /g, '').toLowerCase();
const Hash        = ( data                             ) => {/* CREACION DE HASH hash */
	if ( !data || !data.charCodeAt ) data = (new Date()).toString() + ++global.__ids__;

	let hash = 5381, num = (data||'').length;

	while ( num ) hash=(hash*33) ^ data.charCodeAt(--num);

	return hash>>>0;
 };
const Mime        = ( type                             ) => {/* REGRESA EL TIPO MIME DE UN TIPO DE ARCHIVO */
	switch (type.toLowerCase()) {
		case 'js'  : return 'application/javascript; charset=UTF-8';
		case 'bj'  : return 'application/javascript; charset=UTF-8';
		case 'css' : return 'text/css; charset=UTF-8';
		case 'ico' : return 'image/x-icon';
		case 'jpg' : return 'image/jpeg';
		case 'png' : return 'image/png';
		case 'svg' : return 'image/svg+xml';
		case 'woff': return 'font/woff';
		case 'html': return 'text/html; charset=UTF-8';
		case 'json': return 'application/json; charset=UTF-8';
		case 'txt' :
		default    : return 'text/plain';
	}
 };
const ParseUrl    = ( url                              ) => {/* ENTREGAR PATH DE LA URL */
	if ( typeof url!=VALUE_STRING ) url = document.URL;

	window.__pathUrl__ = new URL(url);

	let eur=/\b([\w\n\.\-\%]+)\b/g;
	let par=window.__pathUrl__.search, tur=window.__pathUrl__.pathname, mat;

	/*URL*/
	let dat=[];
	while ( (mat=eur.exec(tur))!==null ) {
		mat.index===eur.lastIndex && eur.lastIndex++;
		mat.forEach((match, gindex)=>gindex===1 && dat.push(match));
	}
	window.__pathUrl__.path = dat;

	/*Params*/
	let tem='', pas=[];
	while ( (mat=eur.exec(par))!==null ) {
		mat.index===eur.lastIndex && eur.lastIndex++;
		tem=='' ? tem=decodeURI(mat[0]) : (pas.push({[tem]:decodeURI(mat[0])}), tem='');
	}
	window.__pathUrl__.params = pas;

	return window.__pathUrl__;
 };
const Ajax        = ( url, params                      ) => {/* RECIBIR ARCHIVO DEL SERVIDOR CON AJAX */
	return new sProm((run,err)=>{
		fetch(url, Object.assign({mode:'cors', credentials:'include'}, params))
		.catch(err)
		.then(res=>{
			if ( res.ok ) {
				if ( /json/i.test( res.headers.get('content-type') ) ) res.json().catch(err).then(v=>run(v,res));
				else                                                   res.text().catch(err).then(v=>run(v,res));
			}
			else {
				if ( /json/i.test( res.headers.get('content-type') ) ) res.json().catch(err).then(v=>err(v,res));
				else                                                   res.text().catch(err).then(v=>err(v,res));
			}
		});
	});
 };
const Eval        = ( params                           ) => {/* EVALUAR SCRIPT */
	params = Object.assign({ id:'', type:VALUE_JS, data:'', src:'' }, params);

	if ( document.getElementById(params.id) ) return;

	if ( params.type==VALUE_CSS ) {
		let style = document.createElement('style');
		style.id  = params.id;

		style        .appendChild(document.createTextNode(params.data));
		document.head.appendChild(style);
	}
	else if ( params.type==VALUE_JS ) {
		let script  = document.createElement('script');
		script.id   = params.id;
		params.src  && (script.src =params.src);
		params.data && (script.text=params.data);

		document.head.appendChild(script);
	}
 };
const Exec        = ( clase, params                    ) => {/* EJECUCION DE MODULOS */
	return new sProm(function(run,err) {
		params    = Object.assign({ select:'body' }, params);
		let   ord = window.__orderbo__++;
		const cla = clase + (params.version ? `_${params.version.replace(/[\.-]/g, '_')}` : '');
		const exi = eval(`typeof ${cla}!='undefined'`);
		const exe = ()=>{
			if ( params.noExec ) return;
			params.__class__   = clase;
			params.__version__ = params.version || '';
			params.sCss        = Object.assign({order:ord}, params.sCss);
			params.sEvents     = Object.assign({onConstructor:x=>run(x.extra)}, params.sEvents);
			new (eval(cla))(params);
		 };
		const aja = ()=>{
			Ajax(params.url||`{{serverComponents}}/${cla}`, {headers:{botype:'component', bodependencies:JSON.stringify(__dependencies__)}}).run((x,y)=>{
				let jso = [];

				try {jso=JSON.parse(y.headers.get('boclass'))} catch(e) {}

				jso.forEach(v=>{
					let dat = ParseClass(v, x);

					gcache.Add('cache_exec', { requestID:v, data:dat, hash:Hash(dat), order:ord }).err(err);
					window.__dependencies__.push(v);
				});

				!exi && Eval({ id:'exec_'+cla, data:x, type:VALUE_JS });
				exe();
			 }).err(err);
		 };

		if ( exi ) exe();
		else {
			gcache.Get('cache_exec', 'requestID', cla)
			.run(x=>{
				if ( x ) {
					const ncl = cla.toLowerCase()+'_bj';

					Eval({ id:'exec_'+cla, data:x.data, type:VALUE_JS });
					exe();
				}
				else aja();
			 })
			.err(err);
		 }
	});
 };
const RGBaXYZ     = ( red, green, blue                 ) => {/* CONVERCION DE COLOR RGB A XYZ */
	red  /= 255.0;
	green/= 255.0;
	blue /= 255.0;

	if ( red   > 0.04045 ) red   = Math.pow( ( ( red   + 0.055 ) / 1.055 ), 2.4 ); else red   = red   / 12.92;
	if ( green > 0.04045 ) green = Math.pow( ( ( green + 0.055 ) / 1.055 ), 2.4 ); else green = green / 12.92;
	if ( blue  > 0.04045 ) blue  = Math.pow( ( ( blue  + 0.055 ) / 1.055 ), 2.4 ); else blue  = blue  / 12.92;

	red  *= 100.0;
	green*= 100.0;
	blue *= 100.0;

	return ( {
		x: red * 0.4124 + green * 0.3576 + blue * 0.1805,
		y: red * 0.2126 + green * 0.7152 + blue * 0.0722,
		z: red * 0.0193 + green * 0.1192 + blue * 0.9505,
	} );
 };
const RGBaLAB     = ( red, green, blue, alpha          ) => {/* CONVERCION DE COLOR RGB A LAB */
	let xyz = RGBaXYZ( red, green, blue );
	let rex = xyz.x / 95.047 ;
	let rey = xyz.y / 100.0  ;
	let rez = xyz.z / 108.883;

	if ( rex > 0.008856 ) rex = Math.pow( rex, ( 1.0 / 3.0 ) ); else rex = ( 7.787 * rex ) + ( 16.0 / 116.0 );
	if ( rey > 0.008856 ) rey = Math.pow( rey, ( 1.0 / 3.0 ) ); else rey = ( 7.787 * rey ) + ( 16.0 / 116.0 );
	if ( rez > 0.008856 ) rez = Math.pow( rez, ( 1.0 / 3.0 ) ); else rez = ( 7.787 * rez ) + ( 16.0 / 116.0 );

	return ( {
		l: ( 116.0 * rey ) - 16.0,
		a: 500.0 * ( rex - rey ) ,
		b: 200.0 * ( rey - rez ) ,
		p: alpha                 ,
	} );
 };
const LABaXYZ     = ( l, a, b                          ) => {/* CONVERCION DE COLOR LAB A XYZ */
	let y = ( l + 16.0 ) / 116.0;
	let x = ( a / 500.0 ) + y;
	let z = y - ( b / 200.0 );

	if ( Math.pow( y, 3.0 ) > 0.008856 ) y = Math.pow( y, 3.0 ); else y = ( y - 16.0 / 116.0 ) / 7.787;
	if ( Math.pow( x, 3.0 ) > 0.008856 ) x = Math.pow( x, 3.0 ); else x = ( x - 16.0 / 116.0 ) / 7.787;
	if ( Math.pow( z, 3.0 ) > 0.008856 ) z = Math.pow( z, 3.0 ); else z = ( z - 16.0 / 116.0 ) / 7.787;

	return ( {
		x: x * 95.047 ,
		y: y * 100.0  ,
		z: z * 108.883,
	} );
 };
const LABaRGB     = ( l, a, b                          ) => {/* CONVERCION DE COLOR LAB A RGB */
	let xyz = LABaXYZ( l, a, b );

	xyz.x/= 100.0;
	xyz.y/= 100.0;
	xyz.z/= 100.0;

	let rgb = {
		r: xyz.x *  3.2406 + xyz.y * -1.5372 + xyz.z * -0.4986,
		g: xyz.x * -0.9689 + xyz.y *  1.8758 + xyz.z *  0.0415,
		b: xyz.x *  0.0557 + xyz.y * -0.2040 + xyz.z *  1.0570,
	};

	if ( rgb.r > 0.0031308 ) rgb.r = 1.055 * Math.pow( rgb.r, ( 1.0 / 2.4 ) ) - 0.055; else rgb.r = 12.92 * rgb.r;
	if ( rgb.g > 0.0031308 ) rgb.g = 1.055 * Math.pow( rgb.g, ( 1.0 / 2.4 ) ) - 0.055; else rgb.g = 12.92 * rgb.g;
	if ( rgb.b > 0.0031308 ) rgb.b = 1.055 * Math.pow( rgb.b, ( 1.0 / 2.4 ) ) - 0.055; else rgb.b = 12.92 * rgb.b;

	rgb.r*= 255.0; rgb.r = rgb.r < 0.0 ? 0.0 : ( rgb.r > 255.0 ? 255.0 : rgb.r );
	rgb.g*= 255.0; rgb.g = rgb.g < 0.0 ? 0.0 : ( rgb.g > 255.0 ? 255.0 : rgb.g );
	rgb.b*= 255.0; rgb.b = rgb.b < 0.0 ? 0.0 : ( rgb.b > 255.0 ? 255.0 : rgb.b );

	return rgb;
 };
const Linear      = ( progres, start, final_start, max ) => final_start*progres/max+start;
const Back        = {
	In   : ( progres, start, final_start, max )=>final_start * ( progres/= max ) * progres * ( 2.70158 * progres - 1.70158 ) + start,
	Out  : ( progres, start, final_start, max )=>final_start * ( ( progres = ( progres / max ) - 1.0 ) * progres * ( 2.70158 * progres + 1.70158 ) + 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1 )
		return final_start / 2.0 * ( progres * progres * ( 3.5949095 * progres - 2.5949095 ) ) + start;
		return final_start / 2.0 * ( ( progres-= 2.0 ) * progres * ( 3.5949095 * progres + 2.5949095 ) + 2.0 ) + start;
	 },
 };
const Bounce      = {
	In   : ( progres, start, final_start, max )=>{
		progres = max - progres;

		if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) { progres = final_start * ( 7.5625 *   progres                       * progres            ); }
		else if (   progres         < ( 2.0 / 2.75 ) ) { progres = final_start * ( 7.5625 * ( progres -= ( 1.5   / 2.75 ) ) * progres + 0.75     ); }
		else if (   progres         < ( 2.5 / 2.75 ) ) { progres = final_start * ( 7.5625 * ( progres -= ( 2.25  / 2.75 ) ) * progres + 0.9375   ); }
		else                                           { progres = final_start * ( 7.5625 * ( progres -= ( 2.625 / 2.75 ) ) * progres + 0.984375 ); }

		return final_start - progres + start;
	 },
	Out  : ( progres, start, final_start, max )=>{
		if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) return final_start * ( 7.5625 *   progres                      * progres            ) + start;
		else if (   progres         < ( 2.0 / 2.75 ) ) return final_start * ( 7.5625 * ( progres-= ( 1.5   / 2.75 ) ) * progres + 0.75     ) + start;
		else if (   progres         < ( 2.5 / 2.75 ) ) return final_start * ( 7.5625 * ( progres-= ( 2.25  / 2.75 ) ) * progres + 0.9375   ) + start;
		else                                           return final_start * ( 7.5625 * ( progres-= ( 2.625 / 2.75 ) ) * progres + 0.984375 ) + start;
	 },
	InOut: ( progres, start, final_start, max )=>{
		if ( progres < max / 2.0 ) {
			progres = max - ( progres * 2.0 );

			if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) progres = final_start * ( 7.5625 *   progres                      * progres            );
			else if (   progres         < ( 2.0 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 1.5   / 2.75 ) ) * progres + 0.75     );
			else if (   progres         < ( 2.5 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 2.25  / 2.75 ) ) * progres + 0.9375   );
			else                                           progres = final_start * ( 7.5625 * ( progres-= ( 2.625 / 2.75 ) ) * progres + 0.984375 );

			return ( final_start - progres ) * 0.5 + start;
		}
		else {
			progres = progres * 2.0 - max;

			if      ( ( progres/= max ) < ( 1.0 / 2.75 ) ) progres = final_start * ( 7.5625 *   progres                      * progres            );
			else if (   progres         < ( 2.0 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 1.5   / 2.75 ) ) * progres + 0.75     );
			else if (   progres         < ( 2.5 / 2.75 ) ) progres = final_start * ( 7.5625 * ( progres-= ( 2.25  / 2.75 ) ) * progres + 0.9375   );
			else                                           progres = final_start * ( 7.5625 * ( progres-= ( 2.625 / 2.75 ) ) * progres + 0.984375 );

			return progres * 0.5 + final_start * 0.5 + start;
		}
	 }
 };
const Circular    = {
	In   : ( progres, start, final_start, max )=>{
		progres = 1.0 - ( progres /= max ) * progres;

		if ( progres < 0 ) progres = 0;

		return -final_start * ( Math.sqrt( progres ) - 1.0 ) + start;
	 },
	Out  : ( progres, start, final_start, max )=>{
		progres = 1.0 - ( progres = ( progres / max ) - 1.0 ) * progres;

		if ( progres < 0 ) progres = 0;

		return final_start * Math.sqrt( progres ) + start;
	 },
	InOut: ( progres, start, final_start, max )=>{
		progres/= max / 2.0;

		if ( progres < 1.0 )
			return -final_start / 2.0 * ( Math.sqrt( 1.0 - progres * progres ) - 1.0 ) + start;

		progres-= 2.0;

		return final_start / 2.0 * ( Math.sqrt( 1.0 - progres * progres ) + 1.0 ) + start;
	 },
 };
const Cubic       = {
	In   : ( progres, start, final_start, max )=>final_start * ( progres/= max ) * progres * progres + start,
	Out  : ( progres, start, final_start, max )=>final_start * ( ( progres = ( progres/= max ) - 1 ) * progres * progres + 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		progres/= max / 2.0;

		if ( progres < 1.0 )
			return final_start / 2.0 * progres * progres * progres + start;

		progres-= 2;

		return final_start / 2.0 * ( progres * progres * progres + 2.0 ) + start;
	 },
 };
const Elastic     = {
	In   : ( progres, start, final_start, max )=>{
		if (   progres         == 0.0 ) return start              ;
		if ( ( progres/= max ) == 1.0 ) return start + final_start;

		let p       = max * 0.3;
		let a       = final_start;
		let s       = p / 4.0;
		let postFix = a * Math.pow( 2.0, 10.0 * ( progres-= 1.0 ) );

		return -( postFix * Math.sin( ( progres * max - s ) * ( 2.0 * 3.14159265 ) / p ) ) + start;
	 },
	Out  : ( progres, start, final_start, max )=>{
		if (   progres         == 0.0 ) return start              ;
		if ( ( progres/= max ) == 1.0 ) return start + final_start;

		let p = max * 0.3;
		let a = final_start;
		let s = p / 4.0;

		return ( a * Math.pow( 2.0, -10.0 * progres ) * Math.sin( ( progres * max - s ) * ( 2.0 * 3.14159265 ) / p ) + final_start + start );
	 },
	InOut: ( progres, start, final_start, max )=>{
		if (   progres               == 0.0 ) return start              ;
		if ( ( progres/= max / 2.0 ) == 2.0 ) return start + final_start;

		let p = max * ( 0.3 * 1.5 );
		let a = final_start;
		let s = p / 4.0;

		if ( progres < 1.0 ) {
			let postFix = a * Math.pow( 2.0, 10.0 * ( progres-= 1.0 ) );

			return -0.5 * ( postFix * Math.sin( ( progres * max - s ) * ( 2.0 * 3.14159265 ) / p ) ) + start;
		}

		let postFix = a * Math.pow( 2.0, -10.0 * ( progres-= 1.0 ) );

		return postFix * Math.sin( ( ( progres * max ) - s ) * ( 2.0 * 3.14159265 ) / p ) * 0.5 + final_start + start;
	 },
 };
const Exponential = {
	In   : ( progres, start, final_start, max )=>( progres == 0.0 ) ? start               : final_start *    Math.pow( 2.0,  10.0 * ( progres / max   - 1.0 ) ) + start,
	Out  : ( progres, start, final_start, max )=>( progres == max ) ? start + final_start : final_start * ( -Math.pow( 2.0, -10.0 *   progres / max ) + 1.0 )   + start,
	InOut: ( progres, start, final_start, max )=>{
		if (   progres == 0.0               ) return start;
		if (   progres == max               ) return start + final_start;
		if ( ( progres /= max / 2.0 ) < 1.0 ) return final_start / 2.0 * Math.pow( 2.0, 10.0 * ( progres - 1.0 ) ) + start;

		return final_start / 2.0 * ( -Math.pow( 2.0, -10.0 * --progres ) + 2.0 ) + start;
	 },
 };
const Quadratic   = {
	In   : ( progres, start, final_start, max )=> final_start * ( progres/= max ) *   progres         + start,
	Out  : ( progres, start, final_start, max )=>-final_start * ( progres/= max ) * ( progres - 2.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1.0 )
		return  final_start / 2.0 *     progres *   progres                 + start;
		return -final_start / 2.0 * ( --progres * ( progres - 2.0 ) - 1.0 ) + start;
	 },
 };
const Quartic     = {
	In   : ( progres, start, final_start, max )=> final_start * ( progres/= max ) * progres * progres * progres + start,
	Out  : ( progres, start, final_start, max )=>-final_start * ( ( progres = ( progres/= max ) - 1 ) * progres * progres * progres - 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1.0 )
		return  final_start / 2.0 *     progres         * progres * progres * progres         + start;
		return -final_start / 2.0 * ( ( progres-= 2.0 ) * progres * progres * progres - 2.0 ) + start;
	 },
 };
const Quintic     = {
	In   : ( progres, start, final_start, max )=>final_start * (   progres/= max                   ) * progres * progres * progres * progres         + start,
	Out  : ( progres, start, final_start, max )=>final_start * ( ( progres = ( progres/= max ) - 1 ) * progres * progres * progres * progres + 1.0 ) + start,
	InOut: ( progres, start, final_start, max )=>{
		if ( ( progres/= max / 2.0 ) < 1.0 )
		return final_start / 2.0 *     progres         * progres * progres * progres * progres         + start;
		return final_start / 2.0 * ( ( progres-= 2.0 ) * progres * progres * progres * progres + 2.0 ) + start;
	 },
 };
const Sinusoidal  = {
	In   : ( progres, start, final_start, max )=>-final_start       *   Math.cos( progres / max * 1.570796325 ) + start + final_start,
	Out  : ( progres, start, final_start, max )=> final_start       *   Math.sin( progres / max * 1.570796325 ) + start,
	InOut: ( progres, start, final_start, max )=>-final_start / 2.0 * ( Math.cos( 3.14159265 * progres / max ) - 1.0 ) + start,
 };
const AnimStop    = ( element, idAnimation ) => {/* DETENER ANIMACION */
	if ( !element.__oanim__ ) element.__oanim__ = [];

	if      ( typeof idAnimation==VALUE_STRING ) {
		for ( let x=elementa.__oanim__.length; x--; ) {
			if ( element.__oanim__[x].id===idAnimation ) {
				clearTimeout(element.__oanim__[x].timerDelay);
				clearTimeout(element.__oanim__[x].timerFrame);
				element.__oanim__.splice(x, 1);
			}
		}
	 }
	else if ( tipode(idAnimation)==VALUE_OBJECT ) {
		for ( let x=element.__oanim__.length; x--; ) {
			if ( element.__oanim__[x].id===idAnimation.id ) {
				clearTimeout(element.__oanim__[x].timerDelay);
				clearTimeout(element.__oanim__[x].timerFrame);
				element.__oanim__.splice(x, 1);
			}
		}
	 }
	else {
		for ( let x=element.__oanim__.length; x--; ) {
			clearTimeout(element.__oanim__[x].timerDelay);
			clearTimeout(element.__oanim__[x].timerFrame);
		}

		element.__oanim__=[];
	 }
 };
const AnimJS      = ( element, options     ) => {/* ANIMACION DE TIPO JS */
	if ( !element.__oanim__ ) element.__oanim__ = [];

	return new sProm(element, (run,err)=>{
		options = Object.assign({ time:0.3, delay:0, run:null, err:null, type:Exponential.Out }, options);

		options.time<=0 && (options.time=0.001);
		options.id         = window.__ids__++;
		options.run        = run;
		options.err        = err;
		options.max        = options.time * 1000;
		options.delay     *= 1000;
		options.properties = AnimProps(element, options);

		element.__oanim__.push(options);

		options.timerDelay = setTimeout(()=>{
			options.date       = new Date().getTime();
			options.timerFrame = setInterval(()=>AnimFrame(element, options), 0);
		}, options.delay);
	});
 };
const AnimFrame   = ( element, options     ) => {/* ANIMACION DE ETIQUETA */
	let pro, pr1, pr2, pos = new Date().getTime() - options.date;

	if ( pos>=options.max ) {
		clearTimeout(options.timerDelay);
		clearTimeout(options.timerFrame);

		AnimStop(element, options);
		element.sCss(options.properties.property2);
		options.run(element);
	}
	else {
		pr1 = options.properties.property1, pr2 = options.properties.property2;

		for ( let nom in pr2 ) {
			pro = '';

			for ( let ind=0; ind<pr2[nom].length; ind++ ) {
				if ( typeof pr2[nom][ind]==VALUE_NUMBER )
					pro+= options.type(pos, pr1[nom][ind]||0, (pr2[nom][ind]||0) - (pr1[nom][ind]||0), options.max);

				else if ( typeof pr2[nom][ind]==VALUE_OBJECT ) {
					let tl = options.type(pos, pr1[nom][ind].l, pr2[nom][ind].l - pr1[nom][ind].l, options.max);
					let ta = options.type(pos, pr1[nom][ind].a, pr2[nom][ind].a - pr1[nom][ind].a, options.max);
					let tb = options.type(pos, pr1[nom][ind].b, pr2[nom][ind].b - pr1[nom][ind].b, options.max);

					tl = LABaRGB(tl, ta, tb);

					if ( typeof pr2[nom][ind].p==VALUE_NUMBER ) {
						ta  = options.type(pos, pr1[nom][ind].p, pr2[nom][ind].p - pr1[nom][ind].p, options.max);
						pro+= 'rgba('+ parseInt(tl.r) +','+ parseInt(tl.g) +','+ parseInt(tl.b) +','+ ta +')';
					}
					else pro = 'rgb('+ parseInt(tl.r) +','+ parseInt(tl.g) +','+ parseInt(tl.b) +')';
				}
				else pro+= pr2[nom][ind];
			}

			options.properties.element.style[nom] = pro;
		}
	}
 }
const AnimProps   = ( element, options     ) => {/* RESUELVE PROPIEDADES DE CSS PARA ANIMACION */
	// obtencion de propiedades
	let style1, style2, result;

	let clone            = document.createElement('div');
	result               = { element, property1:{}, property2:{} }; element.parentNode.appendChild(clone);
	clone.style.position = 'absolute';
	style1               = getComputedStyle(element);
	style2               = getComputedStyle(clone  );

	for ( let pro in options ) {
		if ( style1[pro]!=undefined ) {
			typeof options[pro]==VALUE_NUMBER && pro!='opacity' && (options[pro]+='px');
			clone.style[pro] = options[pro];

			if ( style2[pro]=='' ) {
				for ( let sub in style2 ) {
					if ( style2[sub]!='' && style2[sub]!=style1[sub] && sub.indexOf(pro)==0 && sub.indexOf('-')==-1 ) {
						result.property1[sub] = style1[sub];
						result.property2[sub] = style2[sub];
					}
				}
			}
			else if ( style1[pro]!=style2[pro] ) {
				result.property1[pro] = style1[pro];
				result.property2[pro] = style2[pro];

				if ( pro=='transform' ) {
					if ( style1[pro]=='none' ) result.property1[pro] = 'matrix(1, 0, 0, 1, 0, 0)';
					if ( style2[pro]=='none' ) result.property2[pro] = 'matrix(1, 0, 0, 1, 0, 0)';
				}
				else if ( pro=='boxShadow' ) {
					if ( style1[pro]=='none' ) result.property1[pro] = 'rgb(153, 153, 153) 0px 0px 0px 0px';
					if ( style2[pro]=='none' ) result.property2[pro] = 'rgb(153, 153, 153) 0px 0px 0px 0px';
				}
				else if ( pro=='lineHeight' ) {
					if ( style1[pro]=='normal' ) result.property1[pro] = '16px';
					if ( style2[pro]=='normal' ) result.property2[pro] = '16px';
				}
			}
		}
	}

	element.parentNode.removeChild(clone);
	style1 = style2 = clone = undefined;

	// separacion de groupos
	for ( let pro in result.property1 ) {
		result.property1[pro] = AnimParse(element, result.property1[pro]);
		result.property2[pro] = AnimParse(element, result.property2[pro]);
	}

	return result;
 }
const AnimParse   = ( element, options     ) => {/* RESOLVER PROPIEDAD */
	let result = [], next = 0, num = 0, tem = 0, ind = 0;

	for ( let pos=0; pos<options.length; pos++ ) {
		if ( options[pos]=='.' || options[pos]=='-' || isNaN(parseInt(options[pos]))==false )
			next==0 && (next=1), next!=1 && (next=1) && (num++);
		else if (
			 options[pos  ]=='r'                         &&
			 options[pos+1]=='g'                         &&
			 options[pos+2]=='b'                         &&
			(options[pos+3]=='a' || options[pos+3]=='(')
		) {
			next==1 && (result[num]=parseFloat(result[num])), next!=0 && (num++);
			next=2, tem=[], ind=0;

			for ( ;pos<options.length && options[pos]!='('; pos++ );
			for ( pos++; pos<options.length && options[pos]!=')'; ind++ ) {
				for ( ;pos<options.length && (options[pos]==' ' || options[pos]==',' || options[pos]==')'); pos++ );

				tem[ind] = '';

				for ( ;pos<options.length && options[pos]!=' ' && options[pos]!=',' && options[pos]!=')'; tem[ind]+=options[pos++] );
			}

			result[num]=RGBaLAB(tem[0], tem[1], tem[2], tem[3]!=undefined ? parseFloat(tem[3]) : undefined);
			continue;
		}
		else {
			next==0 && (next=3);

			if ( next!=3 ) {
				next==1 && (result[num]=parseFloat(result[num]));
				next=3, num++;
			}
		}

		result[num]=== undefined && (result[num]='');
		result[num]+=  options[pos];
	}

	next==1 && (result[num]=parseFloat(result[num]));

	return result;
 }

function isClassCom   ( clase, cad, pos ) {
	if (
		!(
			cad[pos  ]=='/' &&
			cad[pos+1]=='/' &&
			cad[pos+2]=='-' &&
			cad[pos+3]=='-' &&
			cad[pos+4]=='{' &&
			cad[pos+5]=='{'
		)
	) return false;

	pos+= 6;

	const len = cad.length;
	let   tem = '';

	for ( ;pos<len; pos++ ) {
		if ( cad[pos]=='}' && cad[pos+1]=='}' ) break;
		else                                    tem+= cad[pos];
	}

	return tem===clase;
 }
function isClassComFin( clase, cad, pos ) {
	if (
		!(
			cad[pos  ]=='\n' &&
			cad[pos+1]=='/' &&
			cad[pos+2]=='/' &&
			cad[pos+3]=='-' &&
			cad[pos+4]=='-' &&
			cad[pos+5]=='{' &&
			cad[pos+6]=='{'
		)
	) return false;

	pos+= 7;

	const len = cad.length;
	let   tem = '';

	for ( ;pos<len; pos++ ) {
		if ( cad[pos]=='}' && cad[pos+1]=='}' ) break;
		else                                    tem+= cad[pos];
	}

	return tem===clase;
 }
function ParseClass   ( clase, cad      ) {
	const len = cad.length;
	let   pos = 0, tem = '';

	for ( ;pos<len; pos++ ) {
		if ( isClassCom(clase, cad, pos) ) {
			for (;pos<len && cad[pos]!='\n'; pos++);
			for (tem='', pos++; pos<len && !isClassComFin(clase, cad, pos); tem+=cad[pos++]);

			return tem;
		}
	}
 }

/*Extenciones*/
Object.defineProperty(Object.prototype, 'sMap'    , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return [];

	let res=[], ind=0;

	for ( let x in this )
		res.push(func.call(this, this[x], x, ind++));

	return res;
 }});
Object.defineProperty(Object.prototype, 'sReduce' , { enumerable:false, value:function( func, result ){
	if ( typeof func!=VALUE_FUNCTION ) return result;

	let ret=result, ant=null, ind=0;

	for ( let x in this ) {
		if ( ret==null ) ret = this[x];
		else             ret = func.call(this, ret, this[x], x, ant, ind++);

		ant = this[x];
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sFilter' , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return {};

	let ret={}, ind=0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++) ) {
			ret[x] = this[x];
		}
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind=0, res=null;

	for ( let x in this ) {
		res = func.call(this, this[x], x, ind++);

		if ( res ) return res;
	}
 }});
Object.defineProperty(Object.prototype, 'sForEach', { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind = 0;

	for ( let x in this ) func.call(this, this[x], x, ind++);
 }});
Object.defineProperty(Number.prototype, 'sFormat' , { enumerable:false, value:function( format       ){
	if ( format=='$' || format==',' ) {
		let num         = Math.round(parseFloat(this) * Math.pow(10, 2)) / Math.pow(10, 2);
		let regx        = /(\d+)(\d{3})/;
		let numSeparado = num.toString().split('.');
		let enteros     = numSeparado[0];
		let decimales   = numSeparado.length>1 ? '.'+numSeparado[1] : '.00';

		decimales = decimales + '00';
		decimales = decimales.substr(0, 3);

		while ( regx.test(enteros) ) {
			enteros = enteros.replace(regx, '$1,$2');
		}

		return enteros + (format=='$'?decimales:'');
	}
	else {
		let num = String(this);

		while ( num.length<format.length ) {
			num = '_' + num;
		}

		for ( let i=0; i<format.length; i++ ) {
			if ( format[i]==='0' && num[i]==='_' ) {
				num = num.replace(/_/, 0);
			}
		}

		return num;
	}
 }});
Object.defineProperty( Array.prototype, 'sSome'   , { enumerable:false, value:function( func         ){
	if ( typeof func!=VALUE_FUNCTION ) return;

	let res=null;

	for ( let x=this.length; x--; ) {
		res = func.call(this, this[x], x);

		if ( res ) return res;
	}
 }});

HTMLElement.prototype.sAdd        = function( html, after          ) {/* AGREGAR ELEMENTO */
	const element = this.__shadow__ || this;
	const edit = ( _element, _after ) => {
		if ( _after==undefined || _after!=false ) {
			while ( _element.childNodes.length ) {
				element.appendChild(_element.childNodes[0]);
			}
		}
		else while ( _element.childNodes.length>0 ) {
			element.insertBefore(_element.childNodes[0], element.childNodes[0]);
		}
	 };

	if ( typeof html==VALUE_STRING && html!='' ) {
		const div     = document.createElement('div');
		div.innerHTML = html;

		edit(div, after);
	}
	else if ( tipode(html)==VALUE_OBJECT && html.childNodes ) edit(html, after);
	else                                                      element.appendChild(html);

	return this;
 };
HTMLElement.prototype.sAttributes = function( attr, value          ) {/* AGREGAR ELEMENTO *//* ATRIBUTOS */
	const edit = ( att, val ) => {
		if ( val===undefined ) return this.getAttribute(att);
		else {
			if ( val===false ) this.removeAttribute(att);
			else               this.setAttribute(att,val);
		}
	 };

	if ( typeof attr==VALUE_STRING ) {
		if ( value===undefined ) return edit(attr       );
		else                            edit(attr, value);
	}
	else if ( tipode(attr)==VALUE_OBJECT )
		for(let x in attr) edit.call(this, x, attr[x]);

	return this;
 };
HTMLElement.prototype.sRemove     = function(                      ) {/* BORRAR ELEMENTOS */
	this.sEvents(false);
	this.parentNode.removeChild(this);

	return this;
 };
HTMLElement.prototype.sFind       = function( select               ) {/* BUSCAR HIJOS */
	return this.querySelectorAll(select);
 };
HTMLElement.prototype.sClass      = function( sclass, sdelete      ) {/* EDITAR CLASE */
	if ( typeof sclass==VALUE_STRING && sclass!='' ) {
		if ( sdelete==false ) {
			sclass = new RegExp(sclass, 'g');

			if      ( typeof this.className ==VALUE_STRING ) this.className         = this.className        .replace(sclass, '').replace(/  /g, ' ');
			else if ( tipode(this.className)==VALUE_OBJECT ) this.className.baseVal = this.className.baseVal.replace(sclass, '').replace(/  /g, ' ');
		}
		else {
			if ( typeof this.className==VALUE_STRING ) {
				if ( this.className.indexOf(sclass)==-1 )
					this.className+= ' ' + sclass;
			}
			else if ( tipode(this.className)==VALUE_OBJECT ) {
				if ( !this.className.baseVal.indexOf(sclass)==-1 )
					this.className.baseVal+= ' ' + sclass;
			}
		}
	}
	else if ( sclass===false )  {
		if      ( typeof this.className ==VALUE_STRING ) this.className         = '';
		else if ( tipode(this.className)==VALUE_OBJECT ) this.className.baseVal = '';
	}
	else if ( sclass===undefined ) return this.className;

	return this;
 };
HTMLElement.prototype.sCss        = function( property, value      ) {/* ESTILOS CSS */
	const edit=( property, value )=>{
		if (
			typeof value==VALUE_NUMBER &&
			property!='opacity' &&
			property!='zIndex'  &&
			property!='z-index' &&
			property!='order'
		) value+= 'px';

		this.style[property]!=undefined && (this.style[property]=value);
	 };

	if      ( typeof property==VALUE_STRING && value==undefined ) return window.getComputedStyle(this)[property];
	else if ( typeof property==VALUE_STRING && value!=undefined ) edit(property, value);
	else if ( tipode(property)==VALUE_OBJECT                    ) for(let x in property) edit(x, property[x]);

	return this;
 };
HTMLElement.prototype.sEvents     = function( events, idDelete     ) {/* EVENTOS */
	const EditEvent = ( element, events, idDelete ) => {/* EDICION DE EVENTOS */
		if ( !element.bcEvents ) element.bcEvents=[];

		/*ELIMINAR TODOS LOS EVENTOS*/
		if ( events==false ) {
			if ( element.bcEvents ) {
				element.bcEvents.forEach(y=>element.removeEventListener(y.event, y.func));

				delete element.bcEvents;
			}
		 }

		/*VER LISTA DE EVENTOS*/
		else if ( events==undefined ) return element.bcEvents||[];

		/*LISTA DE EVENTOS CON ID*/
		else if ( typeof events==VALUE_STRING ) {
			/*ELIMINAR EVENTOS CON ID*/
			if ( idDelete===false ) {
				if ( element.bcEvents ) {
					for ( let y=element.bcEvents.length-1; y>=0; y-- ) {
						if ( element.bcEvents[y].id===events ) {
							element.removeEventListener(element.bcEvents[y].event, element.bcEvents[y].func);
							element.bcEvents.splice(y, 1);
						}
					}
				}
			 }

			/*SINGLE EVENT*/
			else if ( typeof idDelete===VALUE_FUNCTION ) {
				if ( !element.bcEvents ) element.bcEvents=[];

				element.bcEvents.push({ id:'', event:events, func:idDelete });
				element.addEventListener(events, idDelete);
			}

			/*VER LISTA DE EVENTOS CON ID*/
			else {
				let lis = [];
				if ( element.bcEvents ) element.bcEvents.forEach(x=>x.id===events && lis.push(x));
				return lis;
			 }
		 }

		/*MULTIPLES EVENTOS*/
		else if ( tipode(events)==VALUE_OBJECT ) {
			/*ELIMINAR EVENTO ESPESIFICO*/
			if ( idDelete==false ) {
				if ( element.bcEvents ) {
					events.forEach((x,y)=>{
						for ( let z=element.bcEvents.length-1; z>=0; z-- ) {
							if ( element.bcEvents[z].event===y && element.bcEvents[z].func===x ) {
								element.removeEventListener(element.bcEvents[z].event, element.bcEvents[z].func);
								element.bcEvents.splice(z, 1);
							}
						}
					});
				}
			 }

			/*AGREGAR EVENTOS*/
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id = '';

				if ( !element.bcEvents ) element.bcEvents = [];

				events.sForEach((x,y)=>{
					if ( y!='id' ) {
						element.bcEvents.push({ id:events.id, event:y, func:x });
						element.addEventListener(y, x);
					}
				});
			 }
		 }
	 }

	if      ( events===undefined                                  ) return EditEvent(this);
	else if ( typeof events==VALUE_STRING && idDelete===undefined ) return EditEvent(this, events);
	else                                                            EditEvent(this, events, idDelete);

	return this;
 };
HTMLElement.prototype.sPosition   = function( size                 ) {/* TAMAÑO Y POSICION */
	if ( size===undefined ) {
		const re1 = this.getBoundingClientRect();
		const re2 = this.offsetParent!=null ?
			this.offsetParent.getBoundingClientRect():
			{top:0, left:0, right:0, bottom:0, width:0, height:0};

		return {
			top   : re1.top    - re2.top,
			left  : re1.left   - re2.left,
			right : re1.right  - re2.right,
			bottom: re1.bottom - re2.bottom,
			width : re1.width,
			height: re1.height,
		};
	}
	else if ( size=='off' ) {
		const re1 = this.getBoundingClientRect();

		return {
			top   : re1.top,
			left  : re1.left,
			right : re1.right,
			bottom: re1.bottom,
			width : re1.width,
			height: re1.height,
		};
	}
	else if ( tipode(size)==VALUE_OBJECT ) this.sCss(size);

	return gthis;
 };
HTMLElement.prototype.sTrigger    = function( event, extra         ) {/* GENERAR EVENTO */
	if ( typeof event==VALUE_STRING && event!='' ) {
		const evt = document.createEvent('HTMLEvents');

		evt.initEvent(event, false, true);

		evt.extra = extra;

		this.dispatchEvent(evt);
	}

	return this;
 };
HTMLElement.prototype.sWrapper    = function( html                 ) {/* AGREGAR ELEMENTO ENVOLVENTE */
	if ( typeof html==VALUE_STRING && html!='' ) {
		let div = document.createElement('div'); div.innerHTML = html;
		let hij = div.childNodes[0];

		while ( this.childNodes.length )
			hij.appendChild(this.childNodes[0]);

		this.appendChild(hij);

		div = null;
	}

	return this;
 };
HTMLElement.prototype.sMove       = function( select, after        ) {/* MUEVE UNA ESTRUCTURA A OTRO NODO */
	if ( typeof select==VALUE_STRING ) {
		const obj = document.querySelectorAll(select)[0];

		if ( after==undefined || after ) obj.appendChild(this);
		else                             obj.insertBefore(this, obj.childNodes[0]);
	}
	else if ( typeof select.sClass==VALUE_STRING ) {
		if ( after==undefined || after ) select[0].appendChild(this);
		else                             select[0].insertBefore(this, select[0].childNodes[0]);
	}

	return this;
 };
HTMLElement.prototype.sAnimate    = function( options, idAnimation ) {/* MUEVE UNA ESTRUCTURA A OTRO NODO */
	if      ( options===false && idAnimation===undefined           ) AnimStop(this, idAnimation);
	else if ( options===false && tipode(idAnimation)==VALUE_OBJECT ) {
		AnimStop(this);
		return AnimJS(this, idAnimation);
	}
	else if ( tipode(options)===VALUE_OBJECT ) {
		options===false && AnimStop(idAnimation);
		typeof options.id==VALUE_STRING && AnimStop(this, options.id);
		return AnimJS(this, options);
	}

	return this;
 };

/*Clases*/
function sProm( parent, func ) {
	/*Variables*/
	const gthis        = this;
	gthis.__sPromRun__ = [];
	gthis.__sPromErr__ = 0;
	gthis.__sPromAll__ = 0;
	gthis.__sPromTis__ = 0;
	gthis.fwait        = [];
	gthis.sClass       = 'sProm';

	/*Inicio*/
	if ( typeof parent==VALUE_FUNCTION ) {
		func   = parent;
		parent = undefined;
	}
	else for ( let x in parent ) {
		if ( typeof parent[x]==VALUE_FUNCTION )
			this[x]=(...params)=>ExecFunc(x, params);
	}

	/*Metodos*/
	gthis.run=( fnew )=>{gthis.__sPromRun__.push(fnew); return gthis; };
	gthis.err=( fnew )=>{gthis.__sPromErr__ = fnew;     return gthis; };
	gthis.all=( fnew )=>{gthis.__sPromAll__ = fnew;     return gthis; };
	gthis.tis=( para )=>{gthis.__sPromTis__ = para;     return gthis; };

	/*Funciones*/
	function Run( ...params ) {
		while ( gthis.__sPromRun__.length ) {
			if ( typeof gthis.__sPromRun__[0]==VALUE_FUNCTION ) {
				let res = gthis.__sPromRun__[0](...params);

				if ( res && res.sClass && res.sClass=='sProm' ) {
					res.err(gthis.__sPromErr__);
					gthis.__sPromRun__.splice(0, 1);
					for(let x=0;x<gthis.__sPromRun__.length;x++) res.run(gthis.__sPromRun__[x]);
					break;
				}
			}
			else if ( tipode(gthis.__sPromRun__[0])==VALUE_OBJECT && parent ) {
				if ( typeof gthis.__sPromRun__[0].func==VALUE_FUNCTION || typeof parent[gthis.__sPromRun__[0].func]==VALUE_FUNCTION ) {
					let res;

					if ( typeof gthis.__sPromRun__[0].func==VALUE_FUNCTION ) res = gthis.__sPromRun__[0].func(gthis.__sPromRun__[0].params);
					else                                                     res = parent[gthis.__sPromRun__[0].func](...gthis.__sPromRun__[0].params);

					if ( res!=undefined ) {
						if ( res.sClass && res.sClass=='sProm' ) {
							res.err(gthis.__sPromErr__);
							gthis.__sPromRun__.splice(0, 1);
							for(let x=0;x<gthis.__sPromRun__.length;x++) res.run(gthis.__sPromRun__[x]);
							break;
						}
						else {
							for ( let x=gthis.__sPromRun__.length; x--; ) {
								if      ( typeof gthis.__sPromRun__[x]     ==VALUE_FUNCTION ) gthis.__sPromRun__[x]        = { func:gthis.__sPromRun__[x], params:res };
								else if ( typeof gthis.__sPromRun__[x].func==VALUE_FUNCTION ) gthis.__sPromRun__[x].params = res;
							}
						}
					}
				}
			}

			gthis.__sPromRun__.splice(0, 1);
		}

		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION )
			gthis.__sPromAll__(...params);
	 }
	function Err( ...params ) {
		if ( typeof gthis.__sPromErr__==VALUE_FUNCTION ) gthis.__sPromErr__(...params);
		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION ) gthis.__sPromAll__(...params);
	 }
	function All( ...params ) {
		if ( typeof gthis.__sPromAll__==VALUE_FUNCTION )
			gthis.__sPromAll__(...params);
	 }

	function ExecFunc( func, params ) {
		if ( params[0]==VALUE_ASYNC ){
			params.splice(0, 1);

			return parent[func](...params);
		}
		else {
			gthis.fwait.push(func);
			gthis.__sPromRun__.push({ func:func, params:params });
		}

		return gthis;
	 }

	/*Inicio*/
	new Promise((run,err)=>{run()}).then(func.bind(gthis, Run, Err, All));
 }

class sObjectNonVisual                 {
	/*Creacion*/
	constructor( props ) {
		this.__parent__     = '';
		this.__class__      = 'sObjectNonVisual';
		this.__variables__  = this.__variables__  || { events:[] };
		this.__components__ = this.__components__ || {};
		this.__properties__ = Object.assign(this.__properties__ || {}, props);

		this.__render__();
		this.__exec__  ( props );
		this.sTrigger('onConstructor', this);
	 }

	/*Metodos - Internos*/
	__render__(         ) {}
	__exec__  ( methods ) {
		if ( tipode(methods)==VALUE_OBJECT ) {
			for ( let x in methods ) {
				if ( this[x]!=undefined ) {
					if ( typeof this[x]==VALUE_FUNCTION ) {
						if ( tipode(methods[x])==VALUE_ARRAY ) this[x].apply(this, methods[x]);
						else                                  this[x](methods[x]);
					}
				}
				else if ( x[0]==='o' && x[1]==='n' && typeof methods[x]===VALUE_FUNCTION )
					this.sEvents(x, methods[x]);
			}
		}

		return this;
	 }

	/*Metodos*/
	sEvents ( events, idDelete ) {
		if      ( events===false              ) this.__variables__.events=[];
		else if ( events===undefined          ) return this.__variables__.events;
		else if ( typeof events==VALUE_STRING ) {
			if ( idDelete==false ) {
				for ( let y=this.__variables__.events.length; y--; )
					this.__variables__.events[y].id===events && this.__variables__.events.splice(y, 1);
			}
			else if ( typeof idDelete===VALUE_FUNCTION ) {
				this.__variables__.events.push({ id:'', event:events, func:idDelete });
			}
			else return this.__variables__.events.filter(x.id===events);
		 }
		else if ( tipode(events)==VALUE_OBJECT ) {
			if ( idDelete==false ) {
				events.sForEach((x,y)=>{
					for ( let z=this.__variables__.events.length; z--; )
						if ( this.__variables__.events[z].event===y && this.__variables__.events[z].func===x )
							this.__variables__.events.splice(z, 1);
				});
			 }
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id = '';
				if ( !this.__variables__.events                              ) this.__variables__.events = [];

				events.sForEach((x,y)=>y!='id' && this.__variables__.events.push({ id:events.id, event:y, func:x }));
			 }
		 }

		return this;
	 }
	sTrigger( event, extra     ) {
		if ( typeof event==VALUE_STRING && event!='' )
			this.__variables__.events.forEach(x=>x.event===event && typeof(x.func)==VALUE_FUNCTION && x.func.call(this,{extra}));

		return this;
	 }
 }
class sCache  extends sObjectNonVisual {
	/*Metodos*/
	__render__( methods ) {
		super.__render__ && super.__render__();

		/*Variables*/
		const gthis = this;
		const co    = this.__components__;
		const va    = this.__variables__;
		const op    = this.__properties__;

		/*Metodos*/
		gthis.Create = function(                            ) {
			try{CACHE_VERSION} catch(e){ window.CACHE_VERSION='NOCACHE_VERSION'; window.CACHE_HASH={} }

			if ( localStorage.CACHE_VERSION!=CACHE_VERSION ) {
				console.info('creacion de la cache');

				co.dbr           = indexedDB.deleteDatabase('BlackOcean.'+localStorage.CACHE_VERSION);
				co.dbr.onerror   = x =>gthis.sTrigger('onError'  , 'Error al borrar la cache');
				co.dbr.onsuccess = x =>gthis.sTrigger('onSuccess', 'la cache se ha borrado'  ) && CreateCache();

				localStorage.CACHE_VERSION = CACHE_VERSION;
			}
			else CreateCache();
		 };
		gthis.Clear  = function( cols                       ) {
			return new sProm(gthis,(run,err)=>{
				const res = co.dbp
				.transaction(cols, "readwrite")
				.objectStore(cols)
				.clear();

				res.onerror   = x =>err(x.target.result, x);
				res.onsuccess = x =>run(x.target.result, x);
			});
		 };
		gthis.Get    = function( cols, index, value         ) {
			return new sProm(gthis, (run, err)=>{
				if ( typeof index==VALUE_STRING ) {
					const res = co.dbp
					.transaction(cols, 'readonly')
					.objectStore(cols            )
					.index      (index           )
					.get        (value           );

					res.onerror   = x =>err(x.target.result, x);
					res.onsuccess = x =>run(x.target.result, x);
				}
				else {
					const arr = [], res = co.dbp
					.transaction(cols, 'readonly')
					.objectStore(cols            )
					.openCursor (                );

					res.onerror   = x =>err(x.target.result, x);
					res.onsuccess = x =>{
						if ( x.target.result ) {
							arr.push(x.target.result.value);
							x.target.result.continue();
						}
						else run(arr, res, x.target.result);
					 }
				}
			});
		 };
		gthis.Add    = function( cols,        value         ) {
			return new sProm(gthis, (run, err)=>{
				const res = co.dbp
				.transaction(cols, 'readwrite')
				.objectStore(cols             )
				.add        (value            );

				res.onsuccess = x =>run(x.target.result, x);
				res.onerror   = x =>{
					if ( !x.target.result ) run(x.target.result, x);
					else                    err(x.target.result, x);
				 };
			});
		 };
		gthis.Del    = function( cols, index, value         ) {
			return new sProm(gthis, (run, err)=>{
				const fde = idd =>{
					const res = co.dbp
					.transaction(cols, 'readwrite')
					.objectStore(cols             )
					.delete     (idd              );

					res.onsuccess = x =>run(x.target.result, x);
					res.onerror   = x =>{
						if ( !x.target.result ) run(x.target.result, x);
						else                    err(x.target.result, x);
					};
				 };

				if ( typeof index==VALUE_NUMBER ) fde(index);
				else {
					gthis
					.Get(cols, index, value)
					.run(x=>{
						if ( x.length ) run(x   );
						else            fde(x.id);
					})
					.err(err);
				}
			});
		 };
		gthis.Set    = function( cols, index, value, object ) {
			return new sProm(gthis, (run, err)=>{
				if ( typeof index ==VALUE_NUMBER && tipode(value)==VALUE_OBJECT ) object = value;
				if ( tipode(object)!=VALUE_OBJECT                               ) {
					err('Se requiere de un objeto que editar.');
					return this;
				}

				let isn = typeof index==VALUE_NUMBER;
				let res = co.dbp
				.transaction(cols, 'readwrite')
				.objectStore(cols             )
				.openCursor (                 );

				res.onerror   = x =>err(x.target.result, x);
				res.onsuccess = x =>{
					if ( x.target.result ) {
						if ( (x.target.result.value[isn?'id':index])===(isn?index:value) ) {
							object.id = x.target.result.value.id;

							res = x.target.result.update(object);

							res.onerror   = x =>err(x.target.result, x);
							res.onsuccess = x =>run(x.target.result, x);

							return;
						}

						x.target.result.continue();
					}
				 }
			});
		 };
		gthis.Mov    = function( cols, index, newindex      ) {
			return new sProm(gthis,(run,err)=>{
				gthis
				.Get.call(this, cols)
				.err(err)
				.run(rows=>{
					if ( index>=rows.length ) index = rows.length-1;
					if ( index<0            ) index = 0;

					if ( newindex>rows.length+1 ) newindex = rows.length;
					if ( newindex<0             ) newindex = 0;

					rows.splice(newindex, 0, rows.splice(index,1)[0]);

					gthis
					.Clear.call(this, cols)
					.err(err)
					.run(x=>{
						let ind = 0;
						let fes = ()=>{
							if ( ++ind>=rows.length )
							run(rows);
						 };

						rows.forEach(z=>{
							delete z.id;

							gthis.Add(cols, z).run(fes);
						});
					});
				});
			});
		 };

		/*Fuente*/
		function CreateCache() {
			co.dbr                =indexedDB.open('BlackOcean.'+CACHE_VERSION, 1);
			co.dbr.onerror        =x=>gthis.sTrigger('onError', 'Error al abrir la cache');
			co.dbr.onsuccess      =x=>{
				co.dbp = x.target.result;

				LoadModules().run(x=>gthis.sTrigger('onOpen', 'La cache se abrió correctamente'));
			 };
			co.dbr.onupgradeneeded=x=>{
				co.dbp = x.target.result;

				let   ind = 0;
				const run = () => {
					if ( ind<CACHE_STRUCT.length ) {
						let sre = co.dbp.createObjectStore(CACHE_STRUCT[ind].name, {keyPath:'id', autoIncrement:true});

						CACHE_STRUCT[ind].cams.sForEach((val,key)=>sre.createIndex(key, key, val));

						ind++;

						run();
					}
				 };

				run();
				gthis.sTrigger('onUpgradeneeded', 'La cache se actualizo correctamente');
			 };
		 }
		function LoadModules() {
			return new sProm(run=>{
				gthis.Get('cache_exec').run(x=>{
					x
					.sort((a,b)=>b.order-a.order)
					.forEach(y=>{
						if ( CACHE_HASH[y.requestID.toLowerCase()+'_bj']!=y.hash )
							return;

						window.__dependencies__.push(y.requestID);

						if ( eval(`typeof ${y.requestID}`)==='undefined' )
							Eval({ id:'exec_'+y.requestID, data:y.data, type:VALUE_JS });
					});

					run();
				}).err(run);
			});
		 }
	 }
 }
class sSocket extends sObjectNonVisual {
	/*Metodos*/
	__render__( methods ) {
		super.__render__ && super.__render__();

		/*Variables*/
		const gthis = this;
		const co    = this.co;
		const va    = this.va;
		const op    = this.op;

		/*Metodos*/
		gthis.Connect = ({ sock='' }) => {
			va.socket          =new WebSocket(sock);
			va.socket.onopen   =onSocketOpen;
			va.socket.onerror  =onSocketError;
			va.socket.onclose  =onSocketClose;
			va.socket.onmessage=onSocketData;
		 };

		/*Eventos*/
		function onSocketOpen ( socket ) {
			gthis.tri('onOpen', socket);
		 }
		function onSocketError( socket ) {
			gthis.tri('onError', socket);
		 }
		function onSocketClose( socket ) {
			gthis.tri('onClose', socket);
		 }
		function onSocketData ( socket ) {
			let jso, pro;

			try{jso=JSON.parse(socket.data)}catch(e){console.error(e); jso={}}

			if ( pro=va.proms[jso.id] ) {
				if ( jso.code<300 ) pro.run(jso.data);
				else                pro.err(jso.data);

				delete va.proms[jso.id];
			}
			else gthis.tri('onMessage', jso, socket);
		 }

		/*Funciones*/
		function LoadFunctions( functions, parent, onexec, onexeclevel ) {
			functions.sForEach((v,k)=>{
				gthis [k]=(...x)=>onexec(k, ...x);
				parent[k]=(...x)=>onexec(k, ...x);

				v.sForEach((v2,k2)=>{
					gthis [k][k2]=(...x)=>onexeclevel(k, k2, ...x);
					parent[k][k2]=(...x)=>onexeclevel(k, k2, ...x);
				});
			});
		 }

		function onExecRest     ( key, ...params       ) {
			return new sProm(gthis, (run,err)=>{
				let obj;

				if ( !params.length ) obj={method:'GET'};
				else                  obj={method:'POST', body:JSON.stringify(params)};

				Ajax(`{{API}}/${key}`, obj)
				.run(run)
				.err(e=>console.error('sSocket->onExecRest:', e));
			});
		 }
		function onExecSock     ( key, ...params       ) {}
		function onExecRestLevel( key, skey, ...params ) {
			return new sProm(gthis, (run,err)=>{
				Ajax(`{{API}}/${key}/${skey}`, {method:'POST', body:JSON.stringify(params)})
				.run(run)
				.err(e=>console.error('sSocket->onExecRestLevel:', e));
			});
		 }
		function onExecSockLevel( key, skey, ...params ) {
			return new sProm(gthis, (run, err)=>{
				const has=Hash(new Date()+va.ids++);

				va.proms[has]       ={};
				va.proms[has].id    =has;
				va.proms[has].class =key;
				va.proms[has].func  =skey;
				va.proms[has].run   =run;
				va.proms[has].err   =err;
				va.proms[has].params=params;

				va.socket.send( encodeURIComponent( JSON.stringify([ va.proms[has] ]) ) );
			});
		 }

		/*Inicio*/
		Ajax('{{API}}/boListEndPoints')
		.run(v=>{
			LoadFunctions(v.rest, gthis.rest={}, onExecRest, onExecRestLevel);
			LoadFunctions(v.sock, gthis.sock={}, onExecSock, onExecSockLevel);
		})
		.err( e=>console.error(e) );
	 }
 }

class sObject extends HTMLElement {
	/*Creacion*/
	constructor      ( props ) {
		super();

		this.__parentClass__ = this.__class__ || '';
		this.__class__       = 'sObject';
		this.__id__          = 'i' + window.__ids__++;
		this.__components__  = {};
		this.__variables__  = this.__variables__  || {};
		this.__components__ = this.__components__ || {};
		this.__properties__  = Object.assign(this.__properties__ || {}, props);
		this.__parentNode__  = this.parentNode;

		if ( this.parentNode===null ) {
			if ( !props.select                      ) props.select = document.body;
			if ( typeof props.select===VALUE_STRING ) props.select = document.querySelectorAll(props.select)[0];

			if ( /html(.+)element/.test( tipode(props.select) ) ) props.select.appendChild(this);

		}
	 }
	connectedCallback(       ) {
		const cla = this.__properties__.__class__||this.__class__;
		this.__render__();
		this.sTrigger('onConstructor', this);

		this.__parentNode__===null ?
		this.__exec__( this.__properties__ ):
		this.__execa__();

		if ( !coms[cla] ) coms[cla] = this;
		else {
			for ( var x=2; coms[cla+'_'+x]; x++ );

			coms[cla+'_'+x] = this;
		}
	 }

	/*Metodos*/
	__styles__( styles        ) {
		if ( !styles ) return;

		const cla = this.__properties__.__class__||this.__class__;

		Eval({ id:'style_'+cla, type:VALUE_CSS, data:styles });
	 }
	__render__( html, idsView ) {
		/*Herencia*/
		super.__render__ && super.__render__();

		/*Variables*/
		const gthis = this;
		const op    = this.__properties__;
		const va    = {};
		const co    = {};
		gthis.op    = op;
		gthis.va    = va;
		gthis.co    = co;

		/*Inicio*/
		this.innerHTML = html;

		for ( let x = idsView.length; x--; )
			this.__components__[ idsView[x] ] = this.querySelectorAll(`#${ idsView[x] }`)[0];

		// this.__components__.main = this.__shadow__.querySelectorAll(`main.${this.__properties__.__class__||this.__class__}`)[0];

		this.__exec__(this.__properties__);
	 }
	__exec__  ( methods       ) {
		if ( tipode(methods)==VALUE_OBJECT ) {
			for ( let x in methods ) {
				if ( this[x]!=undefined ) {
					if ( typeof this[x]==VALUE_FUNCTION ) {
						if ( tipode(methods[x])==VALUE_ARRAY ) this[x].apply(this, methods[x]);
						else                                   this[x](methods[x]);
					}
				}
				else if ( x[0]==='o' && x[1]==='n' && typeof methods[x]===VALUE_FUNCTION )
					this.sEvents(x, methods[x]);
			}
		}

		return this;
	 }
	__execa__ ( methods       ) {
		this.attributes.sForEach(v=>{
			if ( tipode(v)=='attr' ) {
				for ( let x in this ) {
					if ( typeof this[x]=='function' && x.toLowerCase()==v.nodeName ) {
						let val = v.nodeValue;

						try {val=JSON.parse(val);}
						catch(e) {}

						if ( tipode(val)==VALUE_ARRAY ) this[x].apply(this, val);
						else                            this[x](val);
					}
				}
			}
		});
	 }
 }

const coms   = {};
const sk     = new sSocket;
const gcache = new sCache({
	onOpen:x=>{
		Exec('cMain', {select:'body', noExec:true});
	},
	Create:1
 });




// if ( !window.lolo ) {
// 	window.lolo = class extends HTMLElement {}
// }
// !window.customElements.get('c-lolo') && window.customElements.define('c-lolo', window.lolo);


// if ( !window.lolo ) {
// 	window.lolo = class aaa {}
// }
// !window.customElements.get('c-lolo') && window.customElements.define('c-lolo', window.lolo);


// console.log(11111, window.lolo);



