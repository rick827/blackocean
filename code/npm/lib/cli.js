#! /usr/bin/env node
require('./main.js');

/*Inicio*/
new sCommands({
	Commands: FileTravel(__dirname+'/commands', true).reduce((r,v)=>{
		let com;

		if ( v.ext.toLowerCase()=='.js' && (com=require(v.path).command) )
			r[v.base]=com;

		return r;
	 }, {}),
	Default : function(x) {
		if      ( process.argv.length>2 ) console.log(`%frError:%r No se reconoce el comando '%fm${process.argv[2]}%r'`.cmd());
		else if ( !global.gprompt       ) this.Parse('help');
		else if ( x!=''                 ) console.log(`%frError:%r No se reconoce el comando '%fm${x}%r'`.cmd());
	 },
	Parse   : [process.argv],
 });
