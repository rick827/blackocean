/*Funciones*/
const exec = function( command ) {
	if ( this.commands[command] && this.commands[command].help ) console.log(this.commands[command].help||'');
	else {
		let wid = this.Commands().sReduce((x,v)=>x<v.command.length?v.command.length:x,0);
		let gke = key=>{while(key.length<wid)key+=' '; return key;};

		console.log(
		 `  %fmbo%r <command>

		  donde <command> es uno de:
			${this.Commands().sReduce((res,act,key)=>{
				if ( !act.description ) return res;

				return res+(res!=''?'\n':'')+`    %fm${gke(act.command)}:%r ${act.description}`;
			}, '')}

			    %cd?=Puede o no existir, $=Si existe se validará%r`
		 .inf('Uso del CLI'));
	}
 }

/*Definicion*/
module.exports.command = {
	command    : 'h -h --help help <command?>',
	description: 'Ayuda con los comandos de BlackOcean',
	help       : 'Ayuda con los comandos de BlackOcean',
	func       : exec,
 }
