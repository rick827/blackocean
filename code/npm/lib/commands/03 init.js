/*Declaraciones*/
const childp    = require('child_process');
const errorFile = '%fmstart%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj';

/*Funciones*/
const GetTypeProject    = function( type          ) {
	switch ( type.toLowerCase() ) {
		case 'com':
		case 'comp':
		case 'comps':
		case 'component': return VALUE_COM;
		case 'app':
		case 'application': return VALUE_APP;
		case 'k':
		case 'kra':
		case 'kraken':
		case 'KRAKEN': return 'KRAKEN';
	}

	return VALUE_SPA;
 };
const GetData           = function( path          ) {
	return new sProm(run=>{
		const res = {
			isPrompt     : /*prompt==null*/false,
			projectName  : path.base,
			description  : 'proyecto BlackOcean',
			version      : '0.0.1',
			gitRepository: '',
			keywords     : 'black, ocean',
			license      : 'MIT',
			ipLocalhost  : 'No',
			typeProject  : VALUE_SPA,
			host         : 'http://localhost',
			appPort      : '2000',
			resPort      : '2001',
			apiPort      : '2002',
			author       : (function(){
				try {
					return (
						childp.execSync('git config --get user.name' ).toString().trim() +' <'+
						childp.execSync('git config --get user.email').toString().trim() +'>'
					 );
				} catch (e) {}

				return '';
			 })(),
		 };

		(new sPrompt)
		.Message(' Tipo de Proyecto: ', res.typeProject).run( (ans,str)=>res.typeProject = GetTypeProject(ans) )
		.Message(' Descripción: '     , res.description).run( (ans,str)=>res.description = ans.toLowerCase()   )
		.Message(' Autor: '           , res.author     ).run( (ans,str)=>res.author      = ans.toLowerCase()   )
		.Message(' Version: '         , res.version    ).run( (ans,str)=>res.version     = ans.toLowerCase()   )
		.Message(' Host: '            , res.host       ).run( (ans,str)=>res.host        = ans.toLowerCase()   )
		.Message(' Puerto de APP: '   , res.appPort    ).run( (ans,str)=>res.appPort     = ans.toLowerCase()   )
		.Message(' Puerto de RES: '   , res.resPort    ).run( (ans,str)=>res.resPort     = ans.toLowerCase()   )
		.Message(' Puerto de API: '   , res.apiPort    ).run( (ans,str)=>res.apiPort     = ans.toLowerCase()   )
		.run(x=>run(res));
	});
 };
const GetBlackOceanJSON = function( options       ) {
	let ipp = options.host;

	if ( options.ipLocalhost.indexOf('n')==-1 && options.ipLocalhost.indexOf('o')==-1 ) {
		let oss = require('os');
		let int = oss.networkInterfaces();

		for ( let key in int ) {
			let adr = int[key];

			for ( let i=adr.length; i--; )
				if ( adr[i].internal===false && adr[i].family==='IPv4' )
					ipp = adr[i].address;
		}
	}

	return `{
    	-Project  : {
    		type       : '${options.typeProject}',
    		author     : '${options.author}',
    		version    : '${options.version}',
    		description: '${options.description}',
    	 },
    	-Variables: {
    		serverComponents: '${options.host}:${options.appPort}',
    		APP             : '${options.host}:${options.appPort}',
    		RES             : '${options.host}:${options.resPort}',
    		API             : '${options.host}:${options.apiPort}',
    	 },
    	-Compiler : {
    		protocol  : 'http',
    		cert      : {
    			key : './cert/ejemplo_key.pem',
    			cert: './cert/ejemplo_crt.pem',
    		 },
    		host      : '${options.host.replace(/http(s?):\/\//g, '')}',
    		port      : ${options.appPort},
    		pathSave  : './build',
    		pathPublic: './public',
    	 },
    	-Resources: {
    		protocol  : 'http',
    		host      : '${options.host.replace(/http(s?):\/\//g, '')}',
    		port      : ${options.resPort},
    		pathPublic: './resources',
    		extensions: ['.png','.ico','.jpg']
    	 },
    	-Api      : {
    		protocol  : 'http',
    		host      : '${options.host.replace(/http(s?):\/\//g, '')}',
    		port      : ${options.apiPort},
    		pathPublic: './api',
    		extensions: ['.json'],
    	 },
    	production: {
    		-Variables: {
    			serverComponents: 'https://kraken.blackocean.dev',
    			VAE             : process.env.variableEjemplo,
    			APP             : '.',
    			RES             : '.',
    			API             : '.',
    		 },
    		-Compiler : {
    			watchFiles: false,
    			pathSave  : './build',
    			ignore    : {
    				ext: ['.DS_Store'],
    				dir: ['/ejemplo/path'],
    				fil: ['/ejemplo/path.not'],
    			 },
    		 },
    		-Resources: {
    			watchFiles: false,
    			pathSave  : './build/resources',
    		 },
    	 },
     }`.replace(/^( *)/gm, '');
 };
const CopyProto         = function( path, options ) {
	const gen = ParsePath(__dirname + '/../../bin/general');
	const por = ParsePath(__dirname + `/../../bin/template-${options.typeProject}`);
	const flo = (_path, _replace)=>{
		FileTravel(_path).forEach(v=>{
			if ( v.type===VALUE_FILE && /\.DS_Store$/.test(v.path) ) return;

			if      ( v.type===VALUE_FOLDER ) flo(v.path, _replace);
			else if ( v.type===VALUE_FILE   ) {
				const tem = ParsePath( v.path.replace(_replace, path.path) );

				if ( tem.base[0]=='.' ) return;

				let con = FileRead(v.path, false);

				if ( /BlackOcean\.bj$/gi.test(v.path) ) con = GetBlackOceanJSON(options);

				CreateDir(tem.dir);
				FileWrite(tem.path, con);
			}
		});
	 }

	flo(gen.path, gen.path);
	flo(por.path, por.path);
 }
const Exec              = function( path          ) {
	if ( path==null ) path = '.';

	path = ParsePath(path);

	if ( path.type===VALUE_FOLDER ) {
		GetData(path).run(res=>{
			CopyProto(path, res);

			console.info(
				`  %fy*%r Para iniciar el proyecto solo escribe: '%fmbo -s%r'
				  %fy*%r Puedes leer más sobre BlackOcean en https://docs.blackocean.dev`
				.inf('BlackOcean se inicializo correctamente').replace(/\n$/g, ''));

			console.info();
			process.exit(0);
		 });
	}
	else console.log(`%fr*%r ${errorFile}`.err('Error'));
 }

/*Definicion*/
module.exports.command = {
	unique     : true,
	command    : 'i -i init <dir$>',
	description: 'Generador de proyectos según la plantilla general',
	error      : '%fminit%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo',
	func       : Exec,
	help       :
		`%fminit%r inicializa un proyecto según la plantilla de BlackOcean

		%fcuso:%r
		  %fc*%r bo %fminit%r (sin argumentos en el directorio de trabajo)
		  %fc*%r bo %fminit%r <carpeta donde se inicializara el proyecto BlackOcean>

		  %cdNota:%r %fminit%r puede ser iniciado en el CLI solo escribiendo %fminit%r o %fmi%r`.inf('Comando %bm%fwinit%r%bb%fy [alias: i]'),
 }
