/*Variables*/
const cluster   = require('cluster');
const sfiles    = {};
const errorFile = '%fmstart%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj';

/*Funciones*/
const GetOptions = ( file       ) => {
	if ( file.type!=VALUE_FILE ) return null;

	let envi = null;

	for (let x=process.argv.length; x--;) {
		if ( process.argv[x]=='-e' ) {
			envi = process.argv[x + 1];
			break;
		}
	}

	return new boOptions({ Path:[file.path, envi] });
 };
const ExecServer = ( type, path ) => {
	process.on('uncaughtException', e=>{
		process.send({ type:'error', error:e.stack });
		process.exit();
	});

	const file = ParsePath(path||'.', 'BlackOcean.bj');
	const opc  = GetOptions(file);

	new boStarts({ options:opc[type], GetStarts:ParsePath(file.dir, type.toLowerCase()).path });
	new boServer({
		onLoad : x=>process.send({ type:'load' }),
		Connect: opc[type],
	});
 };
const Exec       = ( path       ) => {
	const file = ParsePath(path||'.', 'BlackOcean.bj');

	if ( file.type!=VALUE_FILE ) {
		console.log(`%fr*%r ${errorFile}`.err('Error'));
		return;
	}

	const opc = GetOptions(file);

	if ( cluster.isMaster ) {
		const typ = ['Api', 'Resources', 'Compiler'];
		const men = (i,m,x)=>{
			switch( m.type ) {
				case 'load' : x===undefined && nwo(i+1); break;
				case 'error':
					console.error(m.error);
					nwo(i,0);
					break;
			}
		 };
		const nwo = (i,x  )=>{
			if ( i<0                                                   ) return;
			if ( i>=typ.length || (i>=1 && opc.Project.type=='KRAKEN') ) {
				console.info();
				console.info('%fmLog:%r'.cmd());
				return;
			}

			console.info();

			let wor=cluster.fork();

			wor.on( 'message', m=>men(i,m,x) );
			wor.send(typ[i]);
		 };

		log.info(`tipo de proyecto: %fu${opc.Project.type}%r`);
		nwo(0);
	}
	else process.on( 'message', m=>ExecServer(m, path) );
 };

/*Definicion*/
module.exports.command = {
	command    : 's -s start <dir$>',
	description: 'Inicia el servicio de BlackOcean',
	error      : errorFile,
	func       : Exec,
	help       :
		`%fmstart%r inicia el servicio de BlackOcean que se podrá visualizar en un navegador según la configuración en BlackOcean.bj

		%fcuso:%r
		  %fc*%r bo %fmstart%r (sin argumentos en el directorio que contenga un archivo BlackOcean.bj)
		  %fc*%r bo %fmstart%r <carpeta que contenga un archivo BlackOcean.bj>`.inf('Comando %bm%fwstart%r%bb%fy [alias: s]'),
 };
