/*Variables*/
const errorFile = '%fmbuild%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj';

/*Funciones*/
const Exec = function( path ) {
	let file=ParsePath(path||'.', 'BlackOcean.bj');

	if ( file.type===VALUE_FILE ) {
		const fre=( path, cfiles )=>{
			cfiles.filesComp.sForEach(v=>{
				let fil=ParsePath(path, v.url);

				CreateDir(fil.dir);
				FileWrite(fil.path, v.data);

				if ( IsFile(fil.path) )
					log.save(`%fy${v.url}%r %cd${fil.path}%r`);
			});
		 };
		const opc=new boOptions({ Path:[file.path, 'production'] });
		const fco=new boServer; fco.watchFiles=0, fco.viewLog=0;
		const frs=new boServer; frs.watchFiles=0, frs.viewLog=0;
		const fap=new boServer; fap.watchFiles=0, fap.viewLog=0;

		if ( opc.Compiler .pathSave ) fco.eve({ onLoad:g=>fre( ParsePath(opc.Compiler .pathSave).path, g ) }).Build(opc.Compiler );
		if ( opc.Resources.pathSave ) frs.eve({ onLoad:g=>fre( ParsePath(opc.Resources.pathSave).path, g ) }).Build(opc.Resources);
		if ( opc.Api      .pathSave ) fap.eve({ onLoad:g=>fre( ParsePath(opc.Api      .pathSave).path, g ) }).Build(opc.Api      );
	}
	else console.log(`%fr*%r ${errorFile}`.err('Error'));
 };

/*Definicion*/
module.exports.command = {
	command    : 'build <dir$>',
	description: 'Compila el proyecto BlackOcean',
	error      : '%fmbuild%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj',
	func       : Exec,
	help       :
		`%fmbuild%r Compila el proyecto BlackOcean

		%fcuso:%r
		  %fc*%r bo %fmbuild%r (sin argumentos en el directorio de trabajo)
		  %fc*%r bo %fmbuild%r <carpeta que contenga un archivo BlackOcean.bj>`.inf('Comando %bm%fwbuild%r%bb%fy'),
 };
