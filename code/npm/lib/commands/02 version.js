/*Definicion*/
module.exports.command = {
	command    : 'v -v --version version',
	description: 'Version de BlackOcean',
	help       : `%fmversion%r muestra la versión actual de BlackOcean`.inf('Comando %bm%fwversion%r%bb%fy [alias: v --version]'),
	func       : ()=>console.log(`${require(ParsePath(__dirname, '/../../package.json').path).version}`.inf(' BlackOcean - version ')),
 }