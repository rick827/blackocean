/*Variables*/
const errorFile = '%fmdeploy%r solo puede recibir como parámetro un directorio válido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.bj cuyo tipo de proyecto sea un componente o KRAKEN';

/*Funciones*/
const load_files   = cfiles  => {
	cfiles.files.sForEach(x=>{
		if (
			x.lower_url.indexOf('cmain.bj'   )===-1 &&
			x.lower_url.indexOf('core'       )===-1 &&
			x.lower_url.indexOf('index.html' )===-1 &&
			x.lower_url.indexOf('versions.js')===-1
		) {
			Ajax('http://localhost:8080', {protocol:'http', method:'POST', data:JSON.stringify(x), headers:{token:1, botype:'deploy'} })
			.run( x=>console.log(11, x) )
			.err( e=>console.log(22, e) );
		}
	});
 };
const read_options = options => {
	if ( options.Compiler.project.type===VALUE_COM ) {
		let fil = new sCompileFiles({
			eve       : { onLoad:load_files },
			viewLog   : false,
			watchFiles: false,
			Build     : Object.assign( {zip:1}, options.Compiler ),
		});
	}
	else console.error(`%fr*%r ${errorFile}`.err('Error'));
 };
const Exec         = path    => {
	let file = GetParsePath(path||'.', 'BlackOcean.bj');

	if ( IsExist(file.path) ) new sOptions({ Path:file.path, eve:{ onLoad:read_options } });
	else                          console.log(`%fr*%r ${errorFile}`.err('Error'));
 };

/*Definicion*/
module.exports.command = {
	command    : 'deploy <dir$>',
	description: 'Hace deploy del componente tipo BlackOcean o servicio KRAKEN',
	error      : '%fmdeploy%r solo puede recibir como parámetro un directorio valido o en su defecto ser ejecutado en un directorio de trabajo que contenga un archivo BlackOcean.json',
	func       : Exec,
	help       :
		`%fmdeploy%r Hace deploy del componente/servicio tipo BlackOcean

		%fcuso:%r
		  %fc*%r bo %fmdeploy%r (sin argumentos en el directorio de trabajo)
		  %fc*%r bo %fmdeploy%r <carpeta que contenga un archivo BlackOcean.json>`.inf('Comando %bm%fwdeploy%r%bb%fy'),
 };
