/*Variables*/
const _fs     = require('fs'    );
const _path   = require('path'  );
const _crypto = require('crypto');

/*Constantes*/
global.__ids__    = 0;
global.VALUE_FILE = 'file';
	global.VALUE_FOLDER    = 'folder';
	global.VALUE_PATH      = 'path';
	global.VALUE_FUNCTION  = 'function';
	global.VALUE_OBJECT    = 'object';
	global.VALUE_ARRAY     = 'array';
	global.VALUE_STRING    = 'string';
	global.VALUE_NUMBER    = 'number';
	global.VALUE_SPECIAL   = 'special';
	global.VALUE_ASYNC     = 'async';
	global.VALUE_VALID     = 'valid';
	global.VALUE_QUERY     = 'query';
	global.VALUE_API       = 'api';
	global.VALUE_COMPILER  = 'compiler';
	global.VALUE_RESOURCES = 'resources';
	global.VALUE_APP       = 'Application';
	global.VALUE_COM       = 'Component';
	global.VALUE_SPA       = 'SPA';
	global.VALUE_KRAKEN    = 'KRAKEN';

/*Funciones - GLOBALES*/
global.tipode    = ( type ) => {/* TIPO DE OBJETO */
	return Object.prototype.toString.call(type).replace(/object|\[|\]| /g, '').toLowerCase();
 };
global.Mime      = ( type ) => {/* TIPOS MIME DE ARCHIVOS */
	if ( tipode(type)==VALUE_STRING ) {
		switch (type.toLowerCase()) {
			case '.js'  : case 'js'  : return 'application/javascript';
			case '.bh'  : case 'bh'  : return 'text/html';
			case '.bj'  : case 'bj'  : return 'application/javascript';
			case '.css' : case 'css' : return 'text/css';
			case '.ico' : case 'ico' : return 'image/x-icon';
			case '.jpg' : case 'jpg' : return 'image/jpeg';
			case '.png' : case 'png' : return 'image/png';
			case '.svg' : case 'svg' : return 'image/svg+xml';
			case '.txt' : case 'txt' : return 'text/plain';
			case '.woff': case 'woff': return 'font/woff';
			case '.html': case 'html': return 'text/html';
			case '.json': case 'json': return 'application/json';
			case '.pdf' : case 'pdf' : return 'application/pdf';
		}
	}

	return 'text/txt; null';
 };
global.HttpCode  = ( code ) => {/* RESPUESTA DE CODIGOS HTTP */
	switch (code) {
		case 200: return 'Ok';
		case 201: return 'Created';
		case 202: return 'Accepted';
		case 203: return 'Non-Authoritative Information';
		case 204: return 'No Content';
		case 205: return 'Reset Content';
		case 206: return 'Partial Content';
		case 207: return 'Multi-Status';
		case 208: return 'Multi-Status';
		case 226: return 'IM Used';
		case 204: return '';

		case 300: return 'Multiple Choice';
		case 301: return 'Moved Permanently';
		case 302: return 'Found';
		case 303: return 'See Other';
		case 304: return 'Not Modified';
		case 305: return 'Use Proxy';
		case 306: return 'unused';
		case 307: return 'Temporary Redirect';
		case 308: return 'Permanent Redirect';

		case 400: return 'Bad Request';
		case 401: return 'Unauthorized';
		case 402: return 'Payment Required';
		case 403: return 'Forbidden';
		case 404: return 'Not Found';
		case 405: return 'Method Not Allowed';
		case 406: return 'Not Acceptable';
		case 407: return 'Proxy Authentication Required';
		case 408: return 'Request Timeout';
		case 409: return 'Conflict';
		case 410: return 'Gone';
		case 411: return 'Length Required';
		case 412: return 'Precondition Failed';
		case 413: return 'Payload Too Large';
		case 414: return 'URI Too Long';
		case 415: return 'Unsupported Media Type';
		case 416: return 'Requested Range Not Satisfiable';
		case 417: return 'Expectation Failed';
		case 418: return 'I\'m a teapot';
		case 421: return 'Misdirected Request';
		case 422: return 'Unprocessable Entity';
		case 423: return 'Locked';
		case 424: return 'Failed Dependency';
		case 426: return 'Upgrade Required';
		case 428: return 'Precondition Required';
		case 429: return 'Too Many Requests';
		case 431: return 'Request Header Fields Too Large';
		case 451: return 'Unavailable For Legal Reasons';

		case 500: return 'Internal Server Error';
		case 501: return 'Not Implemented';
		case 502: return 'Bad Gateway';
		case 503: return 'Service Unavailable';
		case 504: return 'Gateway Timeout';
		case 505: return 'HTTP Version Not Supported';
		case 506: return 'Variant Also Negotiates';
		case 507: return 'Insufficient Storage';
		case 508: return 'Loop Detected';
		case 510: return 'Not Extended';
		case 511: return 'Network Authentication Required';
	}

	return '';
 };

global.ExcelToInt = ( text                         ) => {/* CONVERSOR DE LETRAS TIPO EXCEL A NUMERO */
	let res = 0, pos = 0, len = text.length;

	while ( pos<len )
		res = (text[pos]>='a'?text[pos++]-32:text[pos++]) + (res*26) - 64;

	return res;
 };
global.IntToExcel = ( numb, lower=true             ) => {/* CONVERSOR DE NUMEROS A LETRAS TIPO EXCEL */
	let res = '', cas = lower?97:65;

	while ( parseInt(numb)>0 ) {
		res  = String.fromCharCode( ((numb-1) % 26) + cas ) + res;
		numb = (numb-1) / 26;
	}

	return res;
 };
global.Hash       = ( data                         ) => {/* HASH DE DATOS */
	if ( !data || !data.charCodeAt ) data = (new Date()).toString() + ++global.__ids__;

	let hash = 5381, num = (data||'').length;

	while ( num ) hash=(hash*33) ^ data.charCodeAt(--num);

	return hash>>>0;
 };
global.SHA256     = ( data, turns=1, base='base64' ) => {/* HASH DE DATOS */
	let res = data;

	while( turns-- ) res = _crypto.createHash('sha256').update(res).digest(base);

	return res;
 };
global.EncodeBOWT = ( json                         ) => {/* MANEJO DE TOKEN BO */
	let res = {};

	for ( let k in json ) {
		if ( k[0]==='_' )
			res[k]=json[k];
	}

	json._firm = SHA256( JSON.stringify(res)+(global.firmToken||'firm') );

	return json;
 };
global.VerifyBOWT = ( json                         ) => {/* MANEJO DE TOKEN BO */
	if ( !json || typeof json!=VALUE_OBJECT ) return false;

	let res = {};

	for ( let k in json ) {
		if ( k[0]==='_' && k!='_firm' )
			res[k]=json[k];
	}

	return json._firm===EncodeJWT(res)._firm;
 };

global.OpenLink = ( url, params ) => {
	if ( !global._child ) global._child = require('child_process');

	let com = '', arg = [];
	params  = Object.assign({}, params, {wait:false, app:false});

	if ( process.platform==='darwin' ) {
		com = 'open';

		params.wait && arg.push('-W');
		params.app  && arg.push('-a', params.app);
	}
	else if ( process.platform==='win32' ) {
		com = 'cmd';
		url = url.replace(/&/g, '^&');

		arg.push('/c', 'start', '""', '/b');

		params.wait && arg.push('/wait');
		params.app  && arg.push(params.app);
	}

	arg.push(url);

	const res = _child.spawn(com, arg);

	res.unref();

	return res;
 };

global.sProm = function( parent, func ) {
	/*Variables*/
	const gthis = this;
	const va    = { run:[], err:null, all:null };
	this.sClass = 'sProm';
	this.fwait  = [];
	this.funcs  = { run:Run, err:Err, all:All };

	if ( typeof parent==VALUE_FUNCTION ) {
		func = parent;
		parent = undefined;
	}
	else for ( let x in parent ) {
		if ( typeof parent[x]==VALUE_FUNCTION )
			this[x]=(...params)=>ExecFunc(x, params);
	}

	/*Metodos*/
	gthis.run = fnew => {va.run.push(fnew); return gthis}
	gthis.err = fnew => {va.err=fnew;       return gthis}
	gthis.all = fnew => {va.all=fnew;       return gthis}

	/*Funciones*/
	function Run( ...params ) {
		while ( va.run.length ) {
			if ( typeof va.run[0]==VALUE_FUNCTION ) {
				let res = va.run[0](...params);

				if ( res && res.sClass && res.sClass=='sProm' ) {
					res.err(va.err);
					va.run.splice(0, 1);
					for(let x=0;x<va.run.length;x++) res.run(va.run[x]);
					break;
				}
			}
			else if ( typeof va.run[0]==VALUE_OBJECT && parent ) {
				if ( typeof va.run[0].func==VALUE_FUNCTION || typeof parent[va.run[0].func]==VALUE_FUNCTION ) {
					let res;

					if ( typeof va.run[0].func==VALUE_FUNCTION ) res = va.run[0].func(va.run[0].params);
					else                                         res = parent[va.run[0].func](...va.run[0].params);

					if ( res!=undefined ) {
						if ( res.sClass && res.sClass=='sProm' ) {
							res.err(va.err);
							va.run.splice(0, 1);
							for(let x=0;x<va.run.length;x++) res.run(va.run[x]);
							break;
						}
						else {
							for ( let x=va.run.length; x--; ) {
								if      ( typeof va.run[x]     ==VALUE_FUNCTION ) va.run[x]        = { func:va.run[x], params:res };
								else if ( typeof va.run[x].func==VALUE_FUNCTION ) va.run[x].params = res;
							}
						}
					}
				}
			}

			va.run.splice(0, 1);
		}

		if ( typeof va.all==VALUE_FUNCTION )
			va.all(...params);
	 }
	function Err( ...params ) {
		if ( typeof va.err==VALUE_FUNCTION ) va.err(...params);
		if ( typeof va.all==VALUE_FUNCTION ) va.all('error', params);
	 }
	function All( ...params ) {
		if ( typeof va.all==VALUE_FUNCTION ) va.all(...params);
	 }

	function ExecFunc( func, params ) {
		if ( params[0]==VALUE_ASYNC ){
			params.splice(0, 1);

			return parent[func](...params);
		}
		else {
			gthis.fwait.push(func);
			va.run.push({ func:func, params:params });
		}

		return gthis;
	 }

	/*Inicio*/
	new Promise((run,err)=>{run()}).then(func.bind(gthis, Run, Err, All));
 };

/*Funciones - ARCHIVOS*/
global.IsExist   = ( path     ) => {
	try      { return _fs.statSync(path) }
	catch(e) { return false              }
 };
global.IsFile    = ( path     ) => {
	try      { return _fs.statSync(path).isFile() }
	catch(e) { return false                       }
 };
global.IsDir     = ( path     ) => {
	try      { return _fs.statSync(path).isDirectory() }
	catch(e) { return false                            }
 };
global.ParsePath = ( ...paths ) => {
	paths = paths.map(v=>{
		if( !v ) return'.';

		v.indexOf('~')>-1
		&&
		(v=v.replace(/^~/g, process.env.HOME));

		return v
	 });

	let pat  = paths.length==0 ? '.' : (paths.length==1?paths[0]:_path.join(...paths));
	let res  = _path.parse( _path.resolve(pat) );
	res.path = _path.join(res.dir, res.base);
	let sta  = IsExist(res.path);
	res.type = sta ? ( sta.isFile() ? VALUE_FILE : (sta.isDirectory() ? VALUE_FOLDER : '') ) : null;

	return res;
 };
global.CreateDir = ( path     ) => {
	if ( !global.IsExist(path) ) {
		global.CreateDir( global.ParsePath(path).dir );
		_fs.mkdirSync(path);
	}
 };

global.FileRead   = ( path, isText=true          ) => {
	try      { var fil = _fs.readFileSync(path, isText?'UTF8':undefined) }
	catch(e) { return null                                             }

	if      ( isText=='lower' ) return fil.toLowerCase();
	else if ( isText=='upper' ) return fil.toUpperCase();

	return fil;
 };
global.FileWrite  = ( path, data, options='UTF8' ) => {
	try      { _fs.writeFileSync(path, data, options); return true }
	catch(e) { return false                                        }
 };
global.FileWatch  = ( path, options              ) => {
	return new sProm((run, err, all)=>{
		if ( process.platform=='linux' ) {
			if ( tipode(options)==VALUE_OBJECT && options.recursive ) {
				delete options.recursive;

				(function fpa( _path ) {
					const fil=ParsePath(_path);

					if ( fil.type==VALUE_FOLDER ) {
						FileTravel(_path).forEach( f=>fpa(f.path) );
						FileWatch(_path, options).run(run).err(err).all(all);
					}
					else if ( fil.type==VALUE_FILE ) FileWatch(path,options).run(run).err(err).all(all);
				 })(path);
			}
			else _fs.watch(path, options, (eventType, filename)=>{
				const fil    =ParsePath(path, filename);
				fil.eventType=eventType==='rename' ? (fil.type===null?'delete':'change') : eventType;

				all(fil);
			});
		}
		else _fs.watch(path, options, (eventType, filename)=>{
			const fil     = ParsePath(path, filename);
			fil.eventType = eventType==='rename' ? (fil.type===null?'delete':'change') : eventType;

			all(fil);
		});
	});
 };
global.FileTravel = ( path, recursive            ) => {
	const sta=IsExist(path);

	if      ( !sta               ) return[];
	else if (  sta.isFile()      ) return[ ParsePath(path) ];
	else if (  sta.isDirectory() ) {
		const res=[];

		_fs.readdirSync(path).forEach(v=>{
			const par=ParsePath(path, v);

			res.push(par);

			if ( par.type===VALUE_FOLDER && recursive )
				par.childs=FileTravel(par.path);
		});

		return res;
	}

	return[];
 };

global.requireBJ = ( file, params ) => {/* INCLUCION DE ARCHIVOS BJ EN EL SERVIDOR */
	if ( typeof file!=VALUE_STRING || file=='' ) return;

	file = ParsePath(file);

	if ( file.type===VALUE_FILE ) {
		const nmo    = new (module.constructor)(file.path, module);
		nmo.paths    = (module.constructor)._nodeModulePaths(file.dir);
		nmo.id       = file.path;
		nmo.filename = file.path;

		nmo._compile(
			'module.return=eval('+
				JSON.stringify(
					Transpiler_BJN( FileRead(file.path)||'', Object.assign({isNode:1}, params) )
				)
			+')',
			file.path
		);
	}
	else throw `path no valido: "${file.path}"`;
 };

/*Funciones - AJAX*/
function GetHttp     ( cad, pos ) {
	const len = cad.length;
	let   res = '';

	for (;pos<len; pos++) {
		if   ( isSpecial(cad,pos) ) break;
		else                        res+= cad[pos];
	}

	return[res,pos+3];
 };
function GetHost     ( cad, pos ) {
	const len = cad.length;
	let   res = '';

	for (;pos<len && cad[pos]!='/' && cad[pos]!=':'; res+= cad[pos++] );

	return[res,pos];
 };
function GetPort     ( cad, pos ) {
	if ( cad[pos]!=':' ) return['',pos];

	const len = cad.length;
	let   res = '';

	for ( pos++; pos<len && isNumber(cad,pos); res+= cad[pos++] );

	return[res,pos];
 };
function GetPath     ( cad, pos ) {
	if ( cad[pos]!='/' ) return[[],pos,''];

	const len = cad.length, pat = [];
	let   tem = '', pas = '';

	for (;pos<len; pos++) {
		if ( cad[pos]=='/' ) {
			if ( tem!='' ) {
				pat.push(tem);
				tem = '';
			}
		}
		else if ( cad[pos]=='?' ) break;
		else                      tem+= cad[pos];

		pas+= cad[pos];
	}

	tem!='' && pat.push(tem);

	return[pat,pos,pas];
 };
function GetParams   ( cad, pos ) {
	if ( cad[pos]!='?' ) return[{},pos];

	const len = cad.length, res = {};
	let   tem = '', igu = '', isi = false;

	for ( pos++; pos<len; pos++ ) {
		if ( cad[pos]=='&' && isi ) {
			res[tem] = igu;
			tem      = '';
			igu      = '';
			isi      = 0;
		}
		else if ( cad[pos]=='=' ) isi = true;
		else if ( isi           ) igu+= cad[pos];
		else                      tem+= cad[pos];
	}

	isi && (res[tem]=igu);

	return[res,pos];
 };
function ParseURL    ( url      ) {
	if ( typeof url!=VALUE_STRING ) return{};

	const res = {};
	let   pos = 0;

	[res.protocol, pos          ] = GetHttp  (url,pos);
	[res.host    , pos          ] = GetHost  (url,pos);
	[res.port    , pos          ] = GetPort  (url,pos);
	[res.paths   , pos, res.path] = GetPath  (url,pos);
	[res.params  , pos          ] = GetParams(url,pos);

	const par=res.params.sReduce((r,v,k)=>{if(r=='')r+='?';else if(r!='')r+='&'; return r+k+'='+v},'');

	par && (res.path+= par);

	res.protocol = res.protocol||'https';
	res.port     = parseInt(res.port) || (res.protocol=='https'?443:80);
	res.original = url;

	return res;
 };
function ParseHeaders( headers  ) {
	const res = {};

	headers.sForEach((v,k) => {
		switch (k) {
			case 'referer': k='url'   ; break;
			case ':status': k='status'; break;
		}

		k=k.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, ( match, index ) => {
			if ( +match===0 ) return '';

			return index==0 ? match.toLowerCase() : match.toUpperCase();
		}).replace(/\s+|-/g, '');

		if ( k.toLowerCase()=='cookie' ) res.cookies=ParseCookie(v);

		res[k]=v;
	 });

	return res;
 };
function ParseCookie ( cookie   ) {
	const res = {}, cad = cookie, len = cad.length;
	let   pos = 0, tem = '', tit = '', val = '', ist = 1;

	for (;pos<len; pos++) {
		if      ( isStringCSS(cad,pos) )[tem,pos] = GetString(tem,cad,pos,{});
		else if ( cad[pos]=='=' ) {
			tit = tem;
			tem = '';
			ist = 0;
		}
		else if ( cad[pos]==';' ) {
			res[tit] = tem;
			tit      = '';
			tem      = '';
			ist      = 1;

			for (pos++; pos<len && isSpacesTabs(cad,pos); pos++);

			pos--;
		}
		else tem+= cad[pos];
	}

	tem!='' && (res[tit]=tem);

	return res;
 }

function AjaxHTTP ( props ) {
	!global._https && (global._https=require('https'));
	!global._http  && (global._http =require('http' ));

	return new sProm((run,err)=>{
		typeof props.data==VALUE_OBJECT && (props.data=JSON.stringify(props.data));

		const opc={
			host   : props.host,
			path   : props.path,
			port   : props.port,
			method : props.method ||'GET',
			headers: Object.assign({
				'Content-Type'  : 'application/x-www-form-urlencoded',
				'Content-Length': props.data ? Buffer.byteLength(props.data) : 0
			 }, props.headers),
		 };

		if ( props.manageCookie && global.__httpCookies__ )
		 	opc.headers.Cookie = global.__httpCookies__;

		const req=(props.protocol=='http' ? global._http : global._https).request(opc, res=>{
			let data='';

			if ( props.manageCookie )
				global.__httpCookies__=res.headers['set-cookie']||global._http_COOKIES;

			res.setEncoding('utf8');
			res.on('data', dat=>data+= dat);
			res.on('end' , x  =>{
				let jso;

				try{jso = JSON.parse(data)} catch(e){jso = {}}

				if ( res.statusCode<400 ) run({ response:res, headers:ParseHeaders(res.headers), code:res.statusCode, data:data, json:jso });
				else                      err({ response:res, headers:ParseHeaders(res.headers), code:res.statusCode, data:data, json:jso, error:'error en la consulta' });
			});
		});

		req.on( 'error',e=>err({ error:e }) );
		req.write(props.data||'');
		req.end();
	});
 }
function AjaxHTTP2( props ) {
	!co.http2 && (co.http2=require('http2'));

	return new sProm((run,err)=>{
		let   aut = process.env.NODE_TLS_REJECT_UNAUTHORIZED; process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';
		let   fer = error=>err({error}) || (process.env.NODE_TLS_REJECT_UNAUTHORIZED=aut);
		let   res = { data:'' };
		let   pat = ParseURL(url);
		const buf = new Buffer.from(params.data);
		const cli = co.http2.connect(`https://${pat.host}${pat.port===80 ? '':':'+pat.port}`); cli.on('error', fer);
		const req = cli.request(Object.assign(
			{
				[co.http2.constants.HTTP2_HEADER_SCHEME]: 'https',
				[co.http2.constants.HTTP2_HEADER_METHOD]: co.http2.constants.HTTP2_METHOD_POST,
				[co.http2.constants.HTTP2_HEADER_PATH  ]: pat.paths||'/',
				'Content-Type'                          : 'text/plain',
				'Content-Length'                        : buf.length,
			},
			params.headers
		));

		req.setEncoding('utf8');
		req.on('error'   , fer);
		req.on('response', headers=>{for(let k in headers) res[k] = headers[k]} );
		req.on('data'    , chunk  =>res.data+= chunk);
		req.on('end'     , x      =>cli.close() || run( ParseHeaders(res) || (process.env.NODE_TLS_REJECT_UNAUTHORIZED=aut) ) );
		req.write(buf);
		req.end();
	 });
 }

function ClassToTag( clase ) {
	if ( typeof clase!=VALUE_STRING ) return '';

	return clase.sReduce((r,v)=>{
		if ( typeof v=='string' ) {
			if      ( v.charCodeAt()>=65 && v.charCodeAt()<=90 ) r+= (r!='' ? '-' : '') + v.toLowerCase();
			else if ( v=='_' || v=='.'                         ) r+= '-';
			else                                                 r+= v;
		}

		return r;
	 }, '');
 }

global.Ajax = ( url, props ) => {/* PETICIONES AJAX */
	!props && (props={});

	url  += (props.params||{}).sReduce((r,v,k)=>{if(r=='')r+='?'; else if(r!='')r+='&'; return r+`${k}=${v}`;}, '');
	url   = encodeURI(url);
	props = Object.assign(ParseURL(url), props);

	switch ( props.protocol ) {
		case 'http2': return AjaxHTTP2(props);
		case 'https':
		case 'http' :
		default     : return AjaxHTTP(props);
	}
 };

/*Funciones - TRANSPILACION*/
global.isLetter      = ( cad, pos ) => {
	return (
		(cad[pos]>='a' && cad[pos]<='z') ||
		(cad[pos]>='A' && cad[pos]<='Z')
	 );
 };
global.isNumber      = ( cad, pos ) => {
	return (
		cad[pos]>='0' &&
		cad[pos]<='9'
	);
 };
global.isOperator    = ( cad, pos ) => {
	return (
		cad[pos]=='<' ||
		cad[pos]=='>' ||
		cad[pos]=='=' ||
		cad[pos]=='-' ||
		cad[pos]=='+' ||
		cad[pos]=='*' ||
		cad[pos]=='/' ||
		cad[pos]=='%' ||
		cad[pos]=='!' ||
		cad[pos]=='?'
	 );
 };
global.isSpaces      = ( cad, pos ) => {
	return (
		cad[pos]==' '  ||
		cad[pos]=='\n' ||
		cad[pos]=='\r' ||
		cad[pos]=='\t'
	 );
 };
global.isSpacesTabs  = ( cad, pos ) => {
	return (
		cad[pos]=='\t' ||
		cad[pos]==' '  ||
		cad[pos]=='⋅'
	 );
 };
global.isSpecial     = ( cad, pos ) => {
	return (
		cad[pos]==' '  ||
		cad[pos]==' '  ||
		cad[pos]=='⋅'  ||
		cad[pos]=='('  ||
		cad[pos]==')'  ||
		cad[pos]=='['  ||
		cad[pos]==']'  ||
		cad[pos]=='{'  ||
		cad[pos]=='}'  ||
		cad[pos]=='<'  ||
		cad[pos]=='>'  ||
		cad[pos]=='='  ||
		cad[pos]=='.'  ||
		cad[pos]==':'  ||
		cad[pos]==';'  ||
		cad[pos]==','  ||
		cad[pos]=='-'  ||
		cad[pos]=='+'  ||
		cad[pos]=='*'  ||
		cad[pos]=='/'  ||
		cad[pos]=='?'  ||
		cad[pos]=='#'  ||
		cad[pos]=='&'  ||
		cad[pos]=='!'  ||
		cad[pos]=='|'  ||
		cad[pos]=='@'  ||
		cad[pos]=='"'  ||
		cad[pos]=="'"  ||
		cad[pos]=='\n' ||
		cad[pos]=='\r' ||
		cad[pos]=='\t'
	);
 };
global.isReplace     = ( cad, pos ) => {
	return(
		cad[pos  ]=='{' &&
		cad[pos+1]=='{'
	);
 };
global.isStringJS    = ( cad, pos ) => {
	return (
		cad[pos]=="'" ||
		cad[pos]=='"' ||
		cad[pos]=='`'
	);
 };
global.isStringCSS   = ( cad, pos ) => {
	return (
		cad[pos]=="'" ||
		cad[pos]=='"'
	);
 };
global.isStringHTML  = ( cad, pos ) => {
	return (
		cad[pos]=="'" ||
		cad[pos]=='"'
	);
 };
global.isCommentJS   = ( cad, pos ) => {
	return (
		(cad[pos]=='/' && cad[pos+1]=='/') ||
		(cad[pos]=='/' && cad[pos+1]=='*')
	);
 };
global.isCommentCSS  = ( cad, pos ) => {
	return (
		cad[pos  ]=='/' &&
		cad[pos+1]=='*'
	);
 };
global.isCommentHTML = ( cad, pos ) => {
	return (
		cad[pos  ]=='<' &&
		cad[pos+1]=='!' &&
		cad[pos+2]=='-' &&
		cad[pos+3]=='-'
	);
 };
global.isReturn      = ( cad, pos ) => {
	return (
		cad[pos  ]=='r' &&
		cad[pos+1]=='e' &&
		cad[pos+2]=='t' &&
		cad[pos+3]=='u' &&
		cad[pos+4]=='r' &&
		cad[pos+5]=='n' &&
		isSpecial(cad,pos+6) &&
		(isSpecial(cad,pos-1)||pos-1<0)
	);
 };
global.isDefine      = ( cad, pos ) => {
	return(
		cad[pos  ]=='#' &&
		cad[pos+1]=='d' &&
		cad[pos+2]=='e' &&
		cad[pos+3]=='f' &&
		cad[pos+4]=='i' &&
		cad[pos+5]=='n' &&
		cad[pos+6]=='e' &&
		isSpacesTabs(cad,pos+7) &&
		(isSpecial(cad,pos-1)||pos-1<0)
	);
 };
global.isGDefine     = ( cad, pos ) => {
	return(
		cad[pos  ]=='#' &&
		cad[pos+1]=='g' &&
		cad[pos+2]=='d' &&
		cad[pos+3]=='e' &&
		cad[pos+4]=='f' &&
		cad[pos+5]=='i' &&
		cad[pos+6]=='n' &&
		cad[pos+7]=='e' &&
		isSpacesTabs(cad,pos+8) &&
		(isSpecial(cad,pos-1)||pos-1<0)
	);
 };

global.GetSpacesJS   = ( res, cad, pos, par                            ) => {
	if ( !par.zip ) return[res+cad[pos], pos];

	const len = cad.length;
	let   ler = res.length;

	if (
		pos+1<len &&
		--ler>=0 &&
		(isLetter(cad,pos+1) || isNumber(cad,pos+1) || cad[pos+1]=='_') &&
		(isLetter(res,ler  ) || isNumber(res,ler  ) || res[ler]=='"' || res[ler]=='_')
	) { res+= ' '; }

	return[res, pos];
 };
global.GetSpacesCSS  = ( res, cad, pos, par                            ) => {
	if ( !par.zip ) return[res+cad[pos], pos];

	const len = cad.length;
	let   ler = res.length;

	if (
		pos+1<len &&
		--ler>=0 &&
		(
			isLetter(cad,pos+1) ||
			isNumber(cad,pos+1) ||
			cad[pos+1]=='.' ||
			cad[pos+1]=='#' ||
			cad[pos+1]=='[' ||
			cad[pos+1]=='*' ||
			cad[pos+1]=='"' ||
			(isOperator(cad,pos+1) && cad[pos+1]!='>' && cad[pos+1]!='=')
		)
		&&
		(
			isLetter(res,ler) ||
			isNumber(res,ler) ||
			 res[ler]=='-' ||
			 res[ler]=='+' ||
			 res[ler]=='*' ||
			 res[ler]=='"' ||
			 res[ler]=='/'
		)
	){ res+= ' '; }

	return[res,pos];
 };
global.GetSpacesHTML = ( res, cad, pos, par                            ) => {
	if ( !par.zip ) return[res+cad[pos], pos];

	const len = cad.length;
	let   ler = res.length;

	if (
		pos+1<len &&
		--ler>=0 &&
		(
			isLetter(res,ler) ||
			isNumber(res,ler) ||
			res[ler]=='-'  ||
			res[ler]=='.'  ||
			res[ler]==','  ||
			res[ler]=='_'  ||
			res[ler]=='"'  ||
			res[ler]==':'  ||
			res[ler]==';'  ||
			res[ler]=='\''
		)
		&&
		(
			isLetter(cad,pos+1) ||
			isNumber(cad,pos+1) ||
			cad[pos+1]=='_' ||
			cad[pos+1]=='-' ||
			cad[pos+1]=='.' ||
			cad[pos+1]=='('
		)
	) { res+= ' '; }

	return[res,pos];
 };
global.GetComment    = ( res, cad, pos, par, inccom=true               ) => {
	const len = cad.length;
	let   ret = '';

	switch ( cad[pos+1] ) {
		case '*':
			for ( pos+=2; pos<len && (cad[pos]!='*' || cad[pos+1]!='/'); ret+=cad[pos++] );
			inccom && (ret='/*'+ret+'*/');
			pos++;
			break;
		case '/':
			for ( pos+=2; pos<len && cad[pos]!='\n'; ret+=cad[pos++] );
			inccom && (ret='//'+ret);
			pos--;
			break;
		case '!':
			for ( pos+=4; pos<len && !(cad[pos]=='-' && cad[pos+1]=='-' && cad[pos+2]=='>'); ret+=cad[pos++] );
			inccom && (ret='<!--'+ret+'-->');
			pos+= 2;
			break;
	}

	par.zip && (ret='');

	return[res+ret,pos];
 };
global.GetReplace    = ( res, cad, pos, par                            ) => {
	if ( !par.variables ) return[res+cad[pos], pos];

	if ( cad[pos+1]=='{' ) {
		const len = cad.length;
		let   tem = '';

		for (pos+=2; pos<len && (cad[pos]!='}' || cad[pos+1]!='}'); tem+=cad[pos++]);

		pos++;
		res+= par.variables[tem]||'not found variable';
	}
	else res+= cad[pos];

	return[res,pos];
 };
global.GetString     = ( res, cad, pos, par, rembjs=false, inccom=true ) => {
	const len = cad.length, com = cad[pos];

	inccom && (res+=com);

	for ( pos++; pos<len; pos++ ) {
		if      ( cad[pos]=='$' && cad[pos+1]=='{' )[res,pos] = Transpiler_JS(res,cad,pos,par);
		else if ( rembjs && cad[pos]=='{'          )[res,pos] = (par.isNode ? Transpiler_BJN : Transpiler_BJS)(res,cad,pos,Object.assign(par,{includeTemp:1}));
		else if ( cad[pos]=='\\'                   ) res+= cad[pos], res+=cad[++pos];
		else if ( isReplace(cad,pos)               )[res,pos] = GetReplace(res,cad,pos,par);
		else if ( cad[pos]==com                    ) break;
		else                                         res+=cad[pos];
	 }

	inccom && (res+=com);

	return[res,pos];
 }
global.GetAttrs      = ( res, cad, pos, par, inccom=true               ) => {
	const len = cad.length;
	const spa = par.cleanSpaces===undefined || par.cleanSpaces; delete par.cleanSpaces;
	const rem = par.replacekeys===undefined || par.replacekeys; delete par.replacekeys;

	for ( ;pos<len && cad[pos]!='['; res+=cad[pos++] );

	inccom && (res+=cad[pos]);

	pos++;

	for ( let key=0; pos<len; pos++ ) {
		if      ( cad[pos]=='['               ) res+= cad[pos], key++;
		else if ( cad[pos]==']' && key--==0   ) break;
		else if ( cad[pos]=='{'               )[res,pos] = Transpiler_BJ(res,cad,pos,Object.assign(par,{includeTemp:rem}));
		else if ( isCommentJS(cad,pos)        )[res,pos] = GetComment   (res,cad,pos,par);
		else if ( isSpaces   (cad,pos) && spa )[res,pos] = GetSpacesCSS (res,cad,pos,par);
		else if ( cad[pos]=='='               ){
			let ret = '';
			res+= cad[pos++];
			for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );
			for ( ;pos<len; pos++ ) {
				if      ( isStringCSS(cad,pos)        ){[ret,pos] = GetString(ret,cad,pos,par,true,false); break}
				else if ( isSpaces   (cad,pos) && spa ){pos--; break}
				else if ( isCommentJS(cad,pos)        )[ret,pos] = GetComment   (ret,cad,pos,par);
				else if ( cad[pos]=='{'               )[ret,pos] = Transpiler_BJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
				else if ( cad[pos]==']'               ){pos--; break}
				else                                    ret+= cad[pos];
			}

			res+= `"${ret}"`;
		}
		else res+= cad[pos];
	}

	inccom && (res+=cad[pos]);

	return[res,pos];
 }
global.GetRegExp     = ( res, cad, pos                                 ) => {
	const len = cad.length;
	let   ler = res.length;

	for ( ler--; ler>=0 && isSpaces(res,ler); ler-- );

	if (
		isLetter(res,ler) ||
		isNumber(res,ler) ||
		 res[ler]=='.' ||
		 res[ler]=='_' ||
		 res[ler]==')'
	) { return[res+cad[pos],pos]; }

	/*Leer Exprecion*/
	for (res+=cad[pos++]; pos<len; pos++) {
		if      ( cad[pos]=='\\'                  ) res+= cad[pos], res+= cad[++pos];
		else if ( cad[pos]=='['                   ){for(;pos<len&&cad[pos]!='\n'&&cad[pos]!=']'; res+=cad[pos++]); res+=cad[pos];}
		else if ( cad[pos]=='\n' || cad[pos]=='/' ){res+= cad[pos]; break;}
		else                                        res+= cad[pos];
	}

	return[res,pos];
 };

global.Transpiler_CMD  = ( data, clear=false              ) => {
	let cad=data, len=cad.length, pos=0, res='';

	for (;pos<len; pos++) {
		if ( cad[pos]=='%' ) {
			if      ( cad[pos+1]=='r'                    ) res+=(clear ? '' : '\x1b[0m'), pos+=1; // Reset

			else if ( cad[pos+1]=='c' && cad[pos+2]=='b' ) res+=(clear ? '' : '\x1b[1m'), pos+=2; // CBright
			else if ( cad[pos+1]=='c' && cad[pos+2]=='d' ) res+=(clear ? '' : '\x1b[2m'), pos+=2; // CDim
			else if ( cad[pos+1]=='c' && cad[pos+2]=='u' ) res+=(clear ? '' : '\x1b[4m'), pos+=2; // CUnderscore
			else if ( cad[pos+1]=='c' && cad[pos+2]=='l' ) res+=(clear ? '' : '\x1b[5m'), pos+=2; // CBlink
			else if ( cad[pos+1]=='c' && cad[pos+2]=='v' ) res+=(clear ? '' : '\x1b[7m'), pos+=2; // CReverse
			else if ( cad[pos+1]=='c' && cad[pos+2]=='h' ) res+=(clear ? '' : '\x1b[8m'), pos+=2; // CHidden

			else if ( cad[pos+1]=='f' && cad[pos+2]=='b' ) res+=(clear ? '' : '\x1b[30m'), pos+=2; // FgBlack
			else if ( cad[pos+1]=='f' && cad[pos+2]=='r' ) res+=(clear ? '' : '\x1b[31m'), pos+=2; // FgRed
			else if ( cad[pos+1]=='f' && cad[pos+2]=='g' ) res+=(clear ? '' : '\x1b[32m'), pos+=2; // FgGreen
			else if ( cad[pos+1]=='f' && cad[pos+2]=='y' ) res+=(clear ? '' : '\x1b[33m'), pos+=2; // FgYellow
			else if ( cad[pos+1]=='f' && cad[pos+2]=='u' ) res+=(clear ? '' : '\x1b[34m'), pos+=2; // FgBlue
			else if ( cad[pos+1]=='f' && cad[pos+2]=='m' ) res+=(clear ? '' : '\x1b[35m'), pos+=2; // FgMagenta
			else if ( cad[pos+1]=='f' && cad[pos+2]=='c' ) res+=(clear ? '' : '\x1b[36m'), pos+=2; // FgCyan
			else if ( cad[pos+1]=='f' && cad[pos+2]=='w' ) res+=(clear ? '' : '\x1b[37m'), pos+=2; // FgWhite

			else if ( cad[pos+1]=='b' && cad[pos+2]=='b' ) res+=(clear ? '' : '\x1b[40m'), pos+=2; // BgBlack
			else if ( cad[pos+1]=='b' && cad[pos+2]=='r' ) res+=(clear ? '' : '\x1b[41m'), pos+=2; // BgRed
			else if ( cad[pos+1]=='b' && cad[pos+2]=='g' ) res+=(clear ? '' : '\x1b[42m'), pos+=2; // BgGreen
			else if ( cad[pos+1]=='b' && cad[pos+2]=='y' ) res+=(clear ? '' : '\x1b[43m'), pos+=2; // BgYellow
			else if ( cad[pos+1]=='b' && cad[pos+2]=='u' ) res+=(clear ? '' : '\x1b[44m'), pos+=2; // BgBlue
			else if ( cad[pos+1]=='b' && cad[pos+2]=='m' ) res+=(clear ? '' : '\x1b[45m'), pos+=2; // BgMagenta
			else if ( cad[pos+1]=='b' && cad[pos+2]=='c' ) res+=(clear ? '' : '\x1b[46m'), pos+=2; // BgCyan
			else if ( cad[pos+1]=='b' && cad[pos+2]=='w' ) res+=(clear ? '' : '\x1b[47m'), pos+=2; // BgWhite

			else res+=cad[pos];
		}
		else res+=cad[pos];
	}

	return res;
 };
global.Transpiler_JS   = ( result, data, position, params ) => {
	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '', ini = '';
		let   inc = par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+= cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos] = GetComment (ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos] = GetString  (ret,cad,pos,par);
			else if ( isSpaces   (cad,pos)      )[ret,pos] = GetSpacesJS(ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos] = GetRegExp  (ret,cad,pos);
			else if ( cad[pos]=='{'             ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) break;
			else                                  ret+= cad[pos];
		}

		inc && (ret=ini+ret+cad[pos]);

		return[res+ret, pos];
	 }
	function Compile( cad, par           ) {
		const len = cad.length;
		let   pos = 0, res = '';

		for (;pos<len; pos++) {
			if      ( isStringJS (cad,pos) )[res,pos] = GetString  (res,cad,pos,par);
			else if ( isCommentJS(cad,pos) )[res,pos] = GetComment (res,cad,pos,par);
			else if ( isSpaces   (cad,pos) )[res,pos] = GetSpacesJS(res,cad,pos,par);
			else if ( cad[pos]=='/'        )[res,pos] = GetRegExp  (res,cad,pos);
			else                             res+= cad[pos];
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_JSON = ( result, data, position, params ) => {
	/*Is*/
	function isComaJSON( cad, pos ) {
		if ( cad[pos++]==',' ) {
			for ( let len=cad.length; pos<len; pos++ ) {
				if      (  isCommentJS(cad,pos) ) pos = GetComment('',cad,pos,{})[1];
				else if ( !isSpaces   (cad,pos) ) break;
			}

			return cad[pos]=='}' || cad[pos]==']';
		}

		return false;
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		const len = cad.length, ret = '', ini = '';
		let   inc = par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+= cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos] = GetRegExp (ret,cad,pos);
			else if ( cad[pos]=='{'             ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) break;
			else                                  ret+= cad[pos];
		}

		if ( inc ) ret = ini+ret+cad[pos];

		return[res+ret, pos];
	 }
	function Compile( cad, par           ) {
		const len = cad.length;
		let   pos = 0, res = '';

		for (;pos<len; pos++) {
			if      ( isStringJS (cad,pos) )[res,pos] = GetString  (res,cad,pos,par);
			else if ( isCommentJS(cad,pos) )pos       = GetComment (res,cad,pos,par)[1];
			else if ( isSpaces   (cad,pos) )[res,pos] = GetSpacesJS(res,cad,pos,par);
			else if ( isComaJSON (cad,pos) );
			else                             res+= cad[pos];
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_CSS  = ( result, data, position, params ) => {
	/*Is*/
	function isCalc( cad, pos ) {
		return(
			cad[pos  ]=='c' &&
			cad[pos+1]=='a' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='c' &&
			isSpecial(cad,pos+4) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }

	/*Funciones*/
	function GetCalc( res, cad, pos, par ) {
		const len = cad.length;

		for ( ;pos<len && isLetter(cad,pos); res+=cad[pos++] );
		for ( ;pos<len && (!isOperator(0) || cap(0)=='%' ); pos++ ) {
			if      ( isOperator(cad,pos) && cad[pos]!='%' ) break;
			else if ( isSpaces  (cad,pos)                  );
			else                                             res+= cad[pos];
		}

		res+= ' ' + cad[pos++] + ' ';

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]==')'     ){res+= cad[pos]; break}
			else if ( isSpaces(cad,pos) );
			else                          res+= cad[pos];
		}

		return[res,pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		res = Compile(cad,par);

		return[res,cad.length];
	 }
	function Compile( cad, par           ) {
		const len = cad.length;
		let   pos = 0, res = '';

		for ( ;pos<len; pos++ ) {
			if      ( isStringCSS (cad,pos) )[res,pos] = GetString   (res,cad,pos,par);
			else if ( isCommentCSS(cad,pos) )[res,pos] = GetComment  (res,cad,pos,par);
			else if ( isSpaces    (cad,pos) )[res,pos] = GetSpacesCSS(res,cad,pos,par);
			else if ( isReplace   (cad,pos) )[res,pos] = GetReplace  (res,cad,pos,par);
			else if ( isCalc      (cad,pos) )[res,pos] = GetCalc     (res,cad,pos,par);
			else                              res+= cad[pos];
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_HTML = ( result, data, position, params ) => {
	/*IS*/
	function isTagStyle    ( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			(cad[pos+1]=='s' || cad[pos+1]=='S') &&
			(cad[pos+2]=='t' || cad[pos+2]=='T') &&
			(cad[pos+3]=='y' || cad[pos+3]=='Y') &&
			(cad[pos+4]=='l' || cad[pos+4]=='L') &&
			(cad[pos+5]=='e' || cad[pos+5]=='E')
		);
	 }
	function isFinTagStyle ( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			 cad[pos+1]=='/' &&
			(cad[pos+2]=='s' || cad[pos+2]=='S') &&
			(cad[pos+3]=='t' || cad[pos+3]=='T') &&
			(cad[pos+4]=='y' || cad[pos+4]=='Y') &&
			(cad[pos+5]=='l' || cad[pos+5]=='L') &&
			(cad[pos+6]=='e' || cad[pos+6]=='E') &&
			 cad[pos+7]=='>'
		);
	 }
	function isTagScript   ( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			(cad[pos+1]=='s' || cad[pos+1]=='S') &&
			(cad[pos+2]=='c' || cad[pos+2]=='C') &&
			(cad[pos+3]=='r' || cad[pos+3]=='R') &&
			(cad[pos+4]=='i' || cad[pos+4]=='I') &&
			(cad[pos+5]=='p' || cad[pos+5]=='P') &&
			(cad[pos+6]=='t' || cad[pos+6]=='T')
		);
	 }
	function isFinTagScript( cad, pos ) {
		return(
			 cad[pos  ]=='<' &&
			 cad[pos+1]=='/' &&
			(cad[pos+2]=='s' || cad[pos+2]=='S') &&
			(cad[pos+3]=='c' || cad[pos+3]=='C') &&
			(cad[pos+4]=='r' || cad[pos+4]=='R') &&
			(cad[pos+5]=='i' || cad[pos+5]=='I') &&
			(cad[pos+6]=='p' || cad[pos+6]=='P') &&
			(cad[pos+7]=='t' || cad[pos+7]=='T') &&
			 cad[pos+8]=='>'
		);
	 }

	/*Funciones*/
	function GetTagStyle ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '', fin = '', num = 0;

		for ( ;pos<len; pos++ ) {
			if      ( isStringHTML (cad,pos) )[res,pos] = GetString (res,cad,pos,par);
			else if ( isCommentHTML(cad,pos) )[res,pos] = GetComment(res,cad,pos,par);
			else if ( cad[pos]=='>'          ){res+= cad[pos++]; break;}
			else                               res+= cad[pos];
		}

		for (;pos<len; pos++) {
			if      ( isFinTagStyle(cad,pos) ){for(;pos<len&&cad[pos]!='>';fin+=cad[pos++]); fin+=cad[pos]; break;}
			else if ( isStringCSS  (cad,pos) )[ret,pos] = GetString  (ret,cad,pos,par);
			else if ( isCommentCSS (cad,pos) )[ret,pos] = GetComment (ret,cad,pos,par);
			else                               ret+= cad[pos];
		}

		return[res+( Transpiler_CSS(ret,par) )+fin,pos];
	 }
	function GetTagScript( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '', fin = '', num = 0;

		for ( ;pos<len; pos++ ) {
			if      ( isStringHTML (cad,pos) )[res,pos] = GetString (res,cad,pos,par);
			else if ( isCommentHTML(cad,pos) )[res,pos] = GetComment(res,cad,pos,par);
			else if ( cad[pos]=='>'          ){res+= cad[pos++]; break;}
			else                               res+= cad[pos];
		}

		for ( ;pos<len; pos++ ) {
			if      ( isFinTagScript(cad,pos) ){for(;pos<len&&cad[pos]!='>';fin+=cad[pos++]); fin+=cad[pos]; break;}
			else if ( isStringJS    (cad,pos) )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( isCommentJS   (cad,pos) )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( cad[pos]=='/'           )[ret,pos] = GetRegExp (ret,cad,pos);
			else                                ret+= cad[pos];
		}

		return[res+( Transpiler_JS(ret,par) )+fin,pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		res = Compile(cad,par);

		return[res,cad.length];
	 }
	function Compile( cad, par           ) {
		const len = cad.length;
		let   pos = 0, res = '';

		for ( ;pos<len; pos++ ) {
			if      ( isStringHTML (cad,pos) )[res,pos] = GetString    (res,cad,pos,par);
			else if ( isCommentHTML(cad,pos) )[res,pos] = GetComment   (res,cad,pos,par);
			else if ( isSpaces     (cad,pos) )[res,pos] = GetSpacesHTML(res,cad,pos,par);
			else if ( isTagStyle   (cad,pos) )[res,pos] = GetTagStyle  (res,cad,pos,par);
			else if ( isTagScript  (cad,pos) )[res,pos] = GetTagScript (res,cad,pos,par);
			else if ( isReplace    (cad,pos) )[res,pos] = GetReplace   (res,cad,pos,par);
			else                               res+= cad[pos];
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_MD   = ( result, data, position, params ) => {
	/*Constantes*/
	const TYPE_TITLE = 1;
	const TYPE_QUOTE = 2;
	const TYPE_LIST  = 3;
	const TYPE_ORDER = 4;
	const TYPE_PCODE = 5;
	const TYPE_LINE  = 6;
	const TYPE_TABLE = 7;
	const TYPE_OTHER = 8;

	/*Is*/
	function isTitle( cad, pos ) {
		const len = cad.length;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return cad[pos]=='#';
	 }
	function isQuote( cad, pos ) {
		const len = cad.length;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return cad[pos]=='>';
	 }
	function isList ( cad, pos ) {
		const len = cad.length;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return (cad[pos]=='-' || cad[pos]=='*' || cad[pos]=='+') && isSpacesTabs(cad,pos+1);
	 }
	function isOrder( cad, pos ) {
		const len = cad.length;
		let   num = 0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);
		for (;pos<len && isNumber(cad,pos); pos++, num++);

		return num && cad[pos]=='.' && isSpacesTabs(cad,pos+1);
	 }
	function isPCode( cad, pos ) {
		const len = cad.length;
		let   num = 0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return (
			(cad[pos]=='`' && cad[pos+1]=='`' && cad[pos+2]=='`') ||
			(cad[pos]=='~' && cad[pos+1]=='~' && cad[pos+2]=='~')
		);
	 }
	function isLine ( cad, pos ) {
		const len = cad.length;
		let   num = 0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return (
			(cad[pos]=='-' && cad[pos+1]=='-' && cad[pos+2]=='-' && (cad[pos+3]=='\n' || pos+3>=len)) ||
			(cad[pos]=='_' && cad[pos+1]=='_' && cad[pos+2]=='_' && (cad[pos+3]=='\n' || pos+3>=len)) ||
			(cad[pos]=='*' && cad[pos+1]=='*' && cad[pos+2]=='*' && (cad[pos+3]=='\n' || pos+3>=len))
		);
	 }
	function isTable( cad, pos ) {
		const len = cad.length;
		let   num = 0;

		for (;pos<len && isSpacesTabs(cad,pos); pos++);

		return cad[pos]=='|';
	 }
	function isLink ( cad, pos ) {
		if ( cad[pos]!='[' ) return false;

		const len = cad.length;
		let   num = 0;

		for (pos++; pos<len; pos++) {
			if      ( cad[pos]=='\\'            ) pos++;
			else if ( cad[pos]=='\n'            ) break;
			else if ( cad[pos]=='('             ) num++;
			else if ( cad[pos]=='['             ) num++;
			else if ( cad[pos]==')' && num--==0 ) break;
			else if ( cad[pos]==']' && num--==0 ) break;
		}

		if ( cad[pos]!=']' ) return false;

		return cad[++pos]=='(';
	 }
	function isALink( cad, pos ) {
		if ( cad[pos]=='w' && cad[pos+1]=='w' && cad[pos+2]=='w' && cad[pos+3]=='.' ) {
			let len=cad.length, num=0;

			for (pos+=4; pos<len; pos++) {
				if      ( cad[pos]=='.'     ) num++;
				else if ( isSpaces(cad,pos) ) break;
			}

			return num>0;
		}
		else if ( cad[pos]=='h' && cad[pos+1]=='t' && cad[pos+2]=='t' && cad[pos+3]=='p' && cad[pos+4]==':' && cad[pos+5]=='/' && cad[pos+6]=='/' ) {
			let len=cad.length, num=0;

			for (pos+=7; pos<len; pos++) {
				if      ( cad[pos]=='.'     ) num++;
				else if ( isSpaces(cad,pos) ) break;
			}

			return num>0;
		}
		else if ( cad[pos]=='h' && cad[pos+1]=='t' && cad[pos+2]=='t' && cad[pos+3]=='p' && cad[pos+4]=='s' && cad[pos+5]==':' && cad[pos+6]=='/' && cad[pos+7]=='/' ) {
			let len=cad.length, num=0;

			for (pos+=8; pos<len; pos++) {
				if      ( cad[pos]=='.'     ) num++;
				else if ( isSpaces(cad,pos) ) break;
			}

			return num>0;
		}

		return false;
	 }
	function isAMeil( cad, pos ) {
		if (
			cad[pos]=='_' || cad[pos]=='-' || cad[pos]=='<' ||
			(cad[pos]>='a' && cad[pos]<='z') ||
			(cad[pos]>='A' && cad[pos]<='Z')
		) {
			let arr = 0, poi = 0;

			for (let len=cad.length; pos<len; pos++) {
				if      ( isSpaces(cad,pos) ) break;
				else if ( cad[pos]=='@'     ) arr++;
				else if ( cad[pos]=='.'     ) poi++;
			}

			return arr==1 && poi>=1;
		}

		return false;
	 }
	function isImage( cad, pos ) {
		if ( cad[pos]!='!' || cad[pos+1]!='[' ) return false;

		const len = cad.length;
		let   num = 0;

		for (pos+=2; pos<len; pos++) {
			if      ( cad[pos]=='\\'            ) pos++;
			else if ( cad[pos]=='\n'            ) break;
			else if ( cad[pos]=='('             ) num++;
			else if ( cad[pos]=='['             ) num++;
			else if ( cad[pos]==')' && num--==0 ) break;
			else if ( cad[pos]==']' && num--==0 ) break;
		}

		if ( cad[pos]!=']' ) return false;

		return cad[++pos]=='(';
	 }

	/*Funciones*/
	function GetChildsTitle( ite, cad, pos, par ) {
		const len = cad.length;
		let   num = 0, tem = '';

		for (;pos<len && isSpaces(cad,pos); pos++);
		for (;pos<len && cad[pos]=='#'; pos++, num++);
		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_TITLE, number:num, cont:tem });

		return pos;
	 }
	function GetChildsQuote( ite, cad, pos, par ) {
		const len = cad.length;
		let   tem = '';

		for (;pos<len && isSpaces(cad,pos); pos++);

		pos++;

		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_QUOTE, cont:tem });

		return pos;
	 }
	function GetChildsList ( ite, cad, pos, par ) {
		const len = cad.length;
		let   tem = '', com = '', lvl = 0;

		for (;pos<len; pos++) {
			if      ( cad[pos]=='\t' ) lvl+= 4;
			else if ( cad[pos]==' '  ) lvl++;
			else                       break;
		}

		lvl = parseInt(lvl/4);
		com = cad[pos++];

		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_LIST, level:lvl, list:com, cont:tem });

		return pos;
	 }
	function GetChildsOrder( ite, cad, pos, par ) {
		const len = cad.length;
		let   tem = '', lvl = 0;

		for (;pos<len; pos++) {
			if      ( cad[pos]=='\t' ) lvl+= 4;
			else if ( cad[pos]==' '  ) lvl++;
			else                       break;
		}

		lvl = parseInt(lvl/4);

		for (;pos<len && isNumber(cad,pos); pos++);

		pos++;

		if  (isSpacesTabs(cad,pos)) pos++;
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_ORDER, level:lvl, cont:tem });

		return pos;
	 }
	function GetChildsPCode( ite, cad, pos, par ) {
		const len = cad.length;
		let   tem = '', tip = '', com = '';

		for(;pos<len && isSpacesTabs(cad,pos); pos++);

		com = cad[pos];

		for (pos+=3; pos<len && cad[pos]!='\n'; tip+=cad[pos++]);
		for (pos++ ; pos<len; pos++) {
			if ( cad[pos]=='\n' && cad[pos+1]==com && cad[pos+2]==com && cad[pos+3]==com && cad[pos+4]=='\n' ){pos+= 4; break}
			else                                                                                               tem+= cad[pos];
		}

		ite.push({ type:TYPE_PCODE, code:tip, cont:tem });

		return pos;
	 }
	function GetChildsLine ( ite, cad, pos, par ) {
		const len = cad.length;

		for(;pos<len && isSpacesTabs(cad,pos); pos++);

		pos+= 3;

		ite.push({ type:TYPE_LINE });

		return pos;
	 }
	function GetChildsTable( ite, cad, pos, par ) {
		const len = cad.length;
		let   tem = '';

		for (;pos<len && isSpacesTabs(cad,pos); pos++);
		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_TABLE, cont:tem });

		return pos;
	 }
	function GetChildsOther( ite, cad, pos, par ) {
		const len = cad.length;
		let   tem = '';

		for (;pos<len && cad[pos]!='\n'; tem+=cad[pos++]);

		ite.push({ type:TYPE_OTHER, cont:tem });

		return pos;
	 }
	function GetChilds     ( cad, par           ) {
		const len = cad.length;
		let   pos = 0, ite = [];

		for ( ;pos<len; pos++ ) {
			if      ( isTitle(cad,pos) ) pos = GetChildsTitle(ite,cad,pos,par);
			else if ( isQuote(cad,pos) ) pos = GetChildsQuote(ite,cad,pos,par);
			else if ( isList (cad,pos) ) pos = GetChildsList (ite,cad,pos,par);
			else if ( isOrder(cad,pos) ) pos = GetChildsOrder(ite,cad,pos,par);
			else if ( isPCode(cad,pos) ) pos = GetChildsPCode(ite,cad,pos,par);
			else if ( isLine (cad,pos) ) pos = GetChildsLine (ite,cad,pos,par);
			else if ( isTable(cad,pos) ) pos = GetChildsTable(ite,cad,pos,par);
			else                         pos = GetChildsOther(ite,cad,pos,par);
		}

		return ite;
	 }

	/*Funciones*/
	function ConvertItalics( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '', com = cad[pos];

		for (pos++; pos<len; pos++) {
			if ( cad[pos]==com ) break;
			else                 ret+=cad[pos];
		}

		res+= `<span class="italics">${ret}</span>`;

		return[res,pos];
	 }
	function ConvertBolder ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '', com = cad[pos];

		for (pos+=2; pos<len; pos++) {
			if ( cad[pos]==com && cad[pos+1]==com ) break;
			else                                    ret+= cad[pos];
		}

		pos++;
		res+= `<span class="bolder">${ret}</span>`;

		return[res,pos];
	 }
	function ConvertIBolder( res, cad, pos, par ) {
		const len = cad.length, com = cad[pos];
		let   ret = '';

		for (pos+=3; pos<len; pos++) {
			if ( cad[pos]==com && cad[pos+1]==com && cad[pos+2]==com ) break;
			else                                                       ret+= cad[pos];
		}

		pos+= 2;
		res+= `<span class="italics bolder">${ret}</span>`;

		return[res,pos];
	 }
	function ConvertCrossed( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for (pos+=2; pos<len; pos++) {
			if ( cad[pos]=='~' && cad[pos+1]=='~' ) break;
			else                                    ret+= cad[pos];
		}

		pos++;
		res+= `<span class="crossed">${ret}</span>`;

		return[res,pos];
	 }
	function ConvertPCode  ( res, cad, pos, par ) {
		const len = cad.length, com = cad[pos];
		let   ret = '';

		for (pos+=3; pos<len; pos++) {
			if ( cad[pos]==com && cad[pos+1]==com && cad[pos+2]==com ) break;
			else ret+=cad[pos];
		}

		pos+= 2;
		res+= `<code>${ret}</code>`;

		return[res,pos];
	 }
	function ConvertLink   ( res, cad, pos, par ) {
		const len = cad.length, iss = cad.substring(pos-1, pos)===' ';
		let   num = 0, lin = '', tex = '';

		for (pos++; pos<len; pos++) {
			if      ( cad[pos]=='['             ) tex+=cad[pos], num++;
			else if ( cad[pos]==']' && num--==0 ) break;
			else                                  tex+=cad[pos];
		}

		for (num=0, pos+=2; pos<len; pos++) {
			if ( cad[pos]==')' ) break;
			else                 lin+=cad[pos];
		}

		tex = ConvertInlines(tex,par);
		res+= `${iss?'&nbsp;':''}<a href="${lin}">${tex}</a>`;

		return[res,pos];
	 }
	function ConvertALink  ( res, cad, pos, par ) {
		const len = cad.length, iss = cad.substring(pos-1, pos)===' ';
		let   ret = '';

		for (;pos<len; pos++) {
			if ( isSpaces(cad,pos) ){pos--; break;}
			else                     ret+= cad[pos];
		}

		res+= `${iss?'&nbsp;':''}<a href="${ret}">${ret}</a>`;

		return[res,pos];
	 }
	function ConvertAMeil  ( res, cad, pos, par ) {
		const len = cad.length, iss = cad.substring(pos-1, pos)===' ';
		let   ret = '';

		if ( cad[pos]=='<' ) pos++;

		for (;pos<len; pos++) {
			if      ( cad[pos]=='\n' ){pos--; break;}
			else if ( cad[pos]=='>'  ) break;
			else                       ret+= cad[pos];
		}

		res+= `${iss?'&nbsp;':''}<a href="mailto:${ret}">${ret}</a>`;

		return[res,pos];
	 }
	function ConvertImage  ( res, cad, pos, par ) {
		const len = cad.length;
		let   num = 0, lin = '', tex = '';

		for (pos+=2; pos<len; pos++) {
			if      ( cad[pos]=='['             ) tex+= cad[pos], num++;
			else if ( cad[pos]==']' && num--==0 ) break;
			else                                  tex+= cad[pos];
		}

		for (num=0, pos+=2; pos<len; pos++) {
			if ( cad[pos]==')' ) break;
			else                 lin+= cad[pos];
		}

		res+= `<img src="${lin}" alt="${tex}"/>`;

		return[res,pos];
	 }
	function ConvertInlines( cad, par           ) {
		const len = cad.length;
		let   res = '', pos = 0;

		for (;pos<len; pos++) {
			if      ( isLink (cad,pos) )[res,pos] = ConvertLink (res,cad,pos,par);
			else if ( isALink(cad,pos) )[res,pos] = ConvertALink(res,cad,pos,par);
			else if ( isAMeil(cad,pos) )[res,pos] = ConvertAMeil(res,cad,pos,par);
			else if ( isImage(cad,pos) )[res,pos] = ConvertImage(res,cad,pos,par);
			else if ( cad[pos]=='\\'   ) res+= cad[++pos];
			else if (
				(cad[pos]=='*' && cad[pos+1]=='*' && cad[pos+2]=='*') ||
				(cad[pos]=='_' && cad[pos+1]=='_' && cad[pos+2]=='_')
			)[res,pos] = ConvertIBolder(res,cad,pos,par);
			else if (
				(cad[pos]=='`' && cad[pos+1]=='`' && cad[pos+2]=='`') ||
				(cad[pos]=='~' && cad[pos+1]=='~' && cad[pos+2]=='~')
			)[res,pos] = ConvertPCode(res,cad,pos,par);
			else if (
				(cad[pos]=='*' && cad[pos+1]=='*') ||
				(cad[pos]=='_' && cad[pos+1]=='_')
			)                                           [res,pos] = ConvertBolder (res,cad,pos,par);
			else if ( cad[pos]=='~' && cad[pos+1]=='~' )[res,pos] = ConvertCrossed(res,cad,pos,par);
			else if ( cad[pos]=='*' || cad[pos]=='_'   )[res,pos] = ConvertItalics(res,cad,pos,par);
			else                                         res+= cad[pos];
		}

		return res;
	 }

	/*Funciones*/
	function SetCodeTitle( res, lis, pos, par         ) {
		const lit = lis[pos];
		let   tem = '';

		tem = ConvertInlines(lit.cont, par);
		tem = `<h${lit.number}>${lit.cont}</h${lit.number}>\n`;
		res+= tem

		return[res, pos];
	 }
	function SetCodeQuote( res, lis, pos, par         ) {
		let tem = '';

		do {
			if ( tem!='' ) tem+= '\n';

			tem+= lis[pos].cont;
		}
		while( lis[pos+1] && ( lis[pos+1].type==TYPE_QUOTE || (lis[pos+1].type==TYPE_OTHER && lis[pos+1].cont!='') ) && ++pos );

		// res+=`<blockquote>${Transpiler_MD(-1,tem,par)}</blockquote>\n`;
		res+= `<blockquote>${ConvertInlines(tem,par)}</blockquote>\n`;

		return[res,pos];
	 }
	function SetCodeList ( res, lis, pos, par, tab='' ) {
		let tem = '', inl = true;

		do {
			tes = '';

			do {
				if ( tes!='' ) tes+= '<br>';
				tes+= lis[pos].cont;
			}
			while( lis[pos+1] && (lis[pos+1].type==TYPE_OTHER && lis[pos+1].cont!='') && pos++ );

			if ( lis[pos+1] && lis[pos+1].type==TYPE_LIST && lis[pos].level<lis[pos+1].level ) {
				[tes,pos] = SetCodeList(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl       = false;
			}
			else if ( lis[pos+1] && lis[pos+1].type==TYPE_ORDER && lis[pos].level<lis[pos+1].level ) {
				[tes,pos] = SetCodeOrder(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl       = false;
			}

			tem+= `\t${tab}<li>${tes}${inl?'':'\t'+tab}</li>\n`;
		}
		while(
			lis[pos+1] &&
			(
				lis[pos+1].type ==TYPE_LIST       &&
				lis[pos  ].list ==lis[pos+1].list &&
				lis[pos  ].level==lis[pos+1].level
			) &&
			pos++
		);

		tem = ConvertInlines(tem,par);
		res+= `${tab}<ul>\n${tem}${tab}</ul>\n`;

		return[res, pos];
	 }
	function SetCodeOrder( res, lis, pos, par, tab='' ) {
		let tem = '', inl = true;

		do {
			tes = '';

			do {
				if ( tes!='' ) tes+= '<br>';
				tes+= lis[pos].cont;
			}
			while( lis[pos+1] && (lis[pos+1].type==TYPE_OTHER && lis[pos+1].cont!='') && pos++ );

			if ( lis[pos+1] && lis[pos+1].type==TYPE_LIST && lis[pos].level<lis[pos+1].level ) {
				[tes,pos] = SetCodeList(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl       = false;
			}
			else if ( lis[pos+1] && lis[pos+1].type==TYPE_ORDER && lis[pos].level<lis[pos+1].level ) {
				[tes,pos] = SetCodeOrder(tes+'\n', lis, pos+1, par, tab+'\t\t');
				inl       = false;
			}

			tem+= `\t${tab}<li>${tes}${inl?'':'\t'+tab}</li>\n`;
		}
		while(
			lis[pos+1] &&
			(
				lis[pos+1].type ==TYPE_ORDER      &&
				lis[pos  ].list ==lis[pos+1].list &&
				lis[pos  ].level==lis[pos+1].level
			) &&
			pos++
		);

		tem = ConvertInlines(tem,par);
		res+= `${tab}<ol>\n${tem}${tab}</ol>\n`;

		return[res, pos];
	 }
	function SetCodePCode( res, lis, pos, par         ) {
		res+= `<pre type="${lis[pos].code}">${lis[pos].cont}</pre>\n`;

		return[res, pos];
	 }
	function SetCodeLine ( res, lis, pos, par         ) {
		res+= `<line></line>\n`;

		return[res, pos];
	 }
	function SetCodeTable( res, lis, pos, par, tab='' ) {
		let ist = false, tem = '';

		if ( lis[pos+1] ) {
			const len = cad.length;
			let   cad = lis[pos+1].cont, pot=0;

			for (;pot<len && (cad[pot]=='|' || cad[pot]=='-' || isSpacesTabs(cad,pot)); pot++);

			lis[pos].isTitle = ist = pot>=len;
		}

		do {
			const cms = SetCodeCamps(res,lis,pos,par);

			if ( lis[pos].isTitle || (lis[pos-1] && !lis[pos-1].isTitle) ) {
				if ( tem!='' ) tem+= '\n';
				tem+='\t<tr>\n\t\t'+ cms.reduce((r,v)=>r+`<th>${v}</th>`, '') +'\n\t</tr>';
			}
		}
		while ( lis[pos+1] && lis[pos+1].type==TYPE_TABLE && ++pos );

		tem = ConvertInlines(tem,par);
		res+= `<table>\n${tem}\n</table>\n`;

		return[res,pos];
	 }
	function SetCodeCamps( res, lis, pos, par         ) {
		const len = cad.length, cms = [];
		let   cad = lis[pos].cont, pot = 0;

		do {
			let cam = '';

			for (pot++; pot<len && isSpacesTabs(cad,pot); pot++);
			for (;pot<len; pot++) {
				if ( isSpacesTabs(cad,pot) ) {
					let poc = pot, tem = '';

					for (;poc<len && isSpacesTabs(cad,poc); tem+=cad[poc++]);
					if ( cad[poc]!='|' ) {
						pot = poc-1;
						cam+= tem;
					}
					else {
						pot = poc;
						break;
					}
				}
				else if ( cad[pot]=='\\' ) cam+= cad[++pot];
				else                       cam+= cad[pot];
			}

			cam!='' && cms.push(cam);
		}
		while (cad[pot]=='|');

		return cms;
	 }
	function SetCodeOther( res, lis, pos, par         ) {
		res+= `<p>${ConvertInlines(lis[pos].cont,par)}</p>\n`;

		return[res,pos];
	 }
	function SetCode     ( lis, par={}                ) {
		const len = lis.length;
		let   res = '', pos = 0;

		for (;pos<len; pos++) {
			switch (lis[pos].type) {
				case TYPE_TITLE: [res,pos] = SetCodeTitle(res,lis,pos,par); break;
				case TYPE_QUOTE: [res,pos] = SetCodeQuote(res,lis,pos,par); break;
				case TYPE_LIST : [res,pos] = SetCodeList (res,lis,pos,par); break;
				case TYPE_ORDER: [res,pos] = SetCodeOrder(res,lis,pos,par); break;
				case TYPE_PCODE: [res,pos] = SetCodePCode(res,lis,pos,par); break;
				case TYPE_LINE : [res,pos] = SetCodeLine (res,lis,pos,par); break;
				case TYPE_TABLE: [res,pos] = SetCodeTable(res,lis,pos,par); break;
				default        : [res,pos] = SetCodeOther(res,lis,pos,par); break;
			}
		}

		return res;
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		res = Compile(cad,par);

		return[res,cad.length];
	 }
	function Compile( cad, par           ) {
		return Transpiler_HTML( SetCode(GetChilds(cad,par), par), par );
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };

global.Transpiler_DEF  = ( result, data, position, params ) => {
	/*Variables*/
	const TYPE_NORM = 1;
	const TYPE_FUNC = 2;

	/*Is*/
	function isReplace( cad, pos, def, ind ) {
		return (isSpecial(cad,pos) || pos==0) && def && def[ind] && def[ind].name!='' && cad[pos+1]==def[ind].name[0];
	 }

	/*Funciones - Define*/
	function GetDefineType ( cad, pos, par ) {
		const len = cad.length;

		for ( pos+=7; pos<len && cad[pos]!='(' && cad[pos]!='=' && cad[pos]!='\n'; pos++ );

		return cad[pos]=='(' ? TYPE_FUNC : TYPE_NORM;
	 }
	function GetDefineName ( cad, pos, par ) {
		const len = cad.length;
		let   res = '';

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len &&  isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]=='\n' || cad[pos]=='=' || cad[pos]=='(' ) break;
			else if ( isSpacesTabs(cad,pos)                            );
			else                                                         res+= cad[pos];
		}

		return res;
	 }
	function GetDefineAtts ( cad, pos, par ) {
		const len = cad.length, res = []
		let   tem = '';

		for ( ;pos<len && cad[pos]!='(' && cad[pos]!='\n' && cad[pos]!='='; pos++ );

		if ( cad[pos]=='(' ) {
			let key = 0;

			for ( pos++; pos<len; pos++ ) {
				if      ( cad[pos]=='('             ) key++, tem+= cad[pos];
				else if ( cad[pos]==')' && key--==0 ) break;
				else if ( cad[pos]==','             ) tem!=''&&res.push(tem)&&(tem='');
				else                                  tem+= cad[pos];
			}

			tem!='' && res.push(tem);
		}

		return res;
	 }
	function GetDefineValue( cad, pos, par ) {
		const len = cad.length;
		let   res = '';

		for ( ;pos<len && cad[pos]!='=' && cad[pos]!='\n'; pos++ );

		if ( cad[pos]=='=' ) {
			for ( pos++; isSpacesTabs(cad,pos); pos++ );
			for ( ;pos<len; pos++ ) {
				if      ( cad[pos]=='\n' ) break;
				else if ( cad[pos]=='\\' ) res+= cad[++pos];
				else                       res+= cad[pos];
			}
		}

		return res;
	 }
	function GetDefineFin  ( cad, pos, par ) {
		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]=='\n' ) break;
			else if ( cad[pos]=='\\' ) pos++;
		}

		return pos;
	 }
	function GetDefine     ( cad, par      ) {
		const len = cad.length, def = [];
		let   pos = 0, res = '', lin, glo;

		for ( ;pos<len; pos++ ) {
			if ( isDefine(cad,pos) || (glo=isGDefine(cad,pos)) ) {
				glo && pos++;

				(glo ? global.__defines__ : def).push({
					type : GetDefineType (cad,pos,par),
					name : GetDefineName (cad,pos,par),
					atts : GetDefineAtts (cad,pos,par),
					value: GetDefineValue(cad,pos,par),
				});

				pos = GetDefineFin(cad,pos,par);
				glo = false;
				res+= '\n';
			}
			else if ( isStringJS (cad,pos) )[res,pos] = GetString (res,cad,pos,par);
			else if ( isCommentJS(cad,pos) )[res,pos] = GetComment(res,cad,pos,par);
			else                             res+= cad[pos];
		}

		par.defines = [].concat(global.__defines__, def);

		return res;
	 }

	/*Funciones - Replace*/
	function GetReplaceFunc( res, cad, pos, def ) {
		const len = cad.length;
		let   psa = pos, tem='';

		for ( res+=cad[pos++]; pos<len && !isSpecial(cad,pos); tem+=cad[pos++] );

		if ( cad[pos]=='(' && def.name===tem ) {
			const led = def.atts.length;
			let   val = def.value, pod = 0, reg, att = GetDefineAtts(cad,pos), key = 0;

			for ( ;pod<led; pod++ )
				val=val.replace(new RegExp('\\b'+def.atts[pod]+'\\b','g'), att[pod]);

			for ( pos++; pos<len; pos++ ) {
				if      ( cad[pos]=='('             ) key++;
				else if ( cad[pos]==')' && key--==0 ) break;
			}

			return[res+val, pos, true];
		}

		return[res, psa, false];
	 }
	function GetReplaceNorm( res, cad, pos, def ) {
		const len = cad.length;
		let   tem = '', psa = pos++, rep = false;

		for ( ;pos<len && !isSpecial(cad,pos); tem+=cad[pos++] );

		if ( def.name===tem ) {
			res+= cad[psa] + def.value;
			pos--;
			rep = true;
		}
		else {
			pos = psa;
			res+= cad[pos];
		}

		return[res,pos,rep];
	 }
	function GetReplace    ( cad, par           ) {
		if ( !par.defines || !par.defines.length ) return cad;

		let len, pos, res = '', rep = true, def = par.defines;

		for ( let cou=0; rep && cou<100; cou++ ) {
			rep = false;

			for ( let i=def.length; i--; ) {
				len = cad.length, pos = 0, res = '';

				for ( ;pos<len; pos++ ) {
					if      ( isStringJS (cad,pos)       )[res,pos]	= GetString (res,cad,pos,par);
					else if ( isCommentJS(cad,pos)       )[res,pos] = GetComment(res,cad,pos,par);
					else if ( isReplace  (cad,pos,def,i) ){
						if ( def[i].type==TYPE_FUNC )[res,pos,rep] = GetReplaceFunc(res,cad,pos,def[i]);
						else                         [res,pos,rep] = GetReplaceNorm(res,cad,pos,def[i]);
					}
					else res+= cad[pos];
				}

				cad = res;
			}
		}

		return res;
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='=' && cad[pos]!='\n'; ret+=cad[pos++] );

		if ( cad[pos]=='=' ) {
			for ( ;pos<len; pos++ ) {
				if      ( cad[pos]=='\n'                     ){ret+= cad[pos]; break}
				else if ( cad[pos]=='\\' && cad[pos+1]=='\n' ) ret+= cad[pos], ret+= cad[++pos];
				else                                           ret+= cad[pos];
			}
		}

		return[res+ret, pos];
	 }
	function Compile( cad, par           ) {
		if ( !global.__defines__ ) global.__defines__ = [];

		return GetReplace(
			GetDefine(cad,par),
			par
		);
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_NEO  = ( result, data, position, params ) => {
	/*Variables*/
	function isNeo( cad, pos ) {
		return(
			cad[pos  ]=='n' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='o' &&
			isSpaces(cad,pos+3) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }

	/*Funciones*/
	function GetName  ( res, cad, pos, par ) {
		const len = cad.length;
		let   aco = true;

		for ( pos+=3; pos<len && isSpaces(cad,pos); pos++ );

		for ( ;pos<len; pos++ ) {
			if      ( isSpaces   (cad,pos) );
			else if ( isCommentJS(cad,pos) ) pos = GetComment('',cad,pos,par)[1];
			else if ( cad[pos]=='['        ) {
				aco = false;

				for ( pos++; pos<len; pos++ ) {
					if ( cad[pos]==']' ) break;
					else                 res+= cad[pos];
				}
			}
			else if ( isSpecial(cad,pos) ) break;
			else                           res+= cad[pos];
		}

		return[aco ? "'"+res+"'" : res,pos];
	 }
	function GetParent( res, cad, pos, par ) {
		if ( cad[pos]=='(' ) {
			const len = cad.length, spo = pos;
			let   num = 0;

			for (;pos<len && isSpecial(cad,pos) && !isStringJS(cad,pos); pos++);
			if ( !isStringJS(cad,pos) ) {
				for (;pos<len; pos++) {
					if      ( isCommentJS(cad,pos)                            ) pos = GetComment('',cad,pos,{})[1];
					else if ( cad[pos]==')' || cad[pos]==',' || cad[pos]==':' ) break;
				}
				if ( cad[pos]==':' ) return[res,spo];
			}

			pos = spo;

			for ( pos++; pos<len; pos++ ) {
				if      ( isSpaces   (cad,pos)            );
				else if ( isStringJS (cad,pos)            )[res,pos] = GetString(res,cad,pos,par);
				else if ( isCommentJS(cad,pos)            ) pos      = GetComment('',cad,pos,{} )[1];
				else if ( cad[pos]=='/'                   )[res,pos] = GetRegExp(res,cad,pos);
				else if ( cad[pos]=='('                   ) res+= cad[pos], num++;
				else if ( cad[pos]==')' && num            ) res+= cad[pos], num--;
				else if ( cad[pos]==')' || cad[pos]==','  ) break;
				else                                        res+= cad[pos];
			}
		}

		return[res,pos];
	 }
	function GetParams( res, cad, pos, par ) {
		if ( cad[pos]==',' || cad[pos]=='(' ) {
			const len = cad.length;

			for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

			if ( cad[pos]!=')' ) {
				for ( let num=0; pos<len; pos++ ) {
					if      ( cad[pos]=='('             ) res+= cad[pos], num++;
					else if ( cad[pos]==')' && num--==0 ) break;
					else if ( isSpaces   (cad,pos)      );
					else if ( isStringJS (cad,pos)      )[res,pos] = GetString (res,cad,pos,par);
					else if ( isCommentJS(cad,pos)      ) pos      = GetComment('' ,cad,pos,{} )[1];
					else if ( cad[pos]=='/'             )[res,pos] = GetRegExp (res,cad,pos,par);
					else                                  res+= cad[pos];
				}
			}
		}

		return[res,pos];
	 }
	function GetIgual ( res, cad, pos, par ) {
		const len = cad.length;

		for ( pos+=2; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos) || cad[pos]=='.'; !isSpaces(cad,pos) ? res+=cad[pos++] : pos++ );

		return[res,pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		return[cad,cad.length];
	 }
	function Compile( cad, par           ) {
		const len = cad.length;
		let   res = '', pos = 0;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos) )[res,pos] = GetComment(res,cad,pos,par);
			else if ( isStringJS (cad,pos) )[res,pos] = GetString (res,cad,pos,par);
			else if ( cad[pos]=='/'        )[res,pos] = GetRegExp (res,cad,pos,par);
			else if ( isNeo(cad,pos)       ) {
				let cln = '', pan = '', pam = '', bod = '', igu = '';

				[cln,pos] = GetName  ('',cad,pos,par);
				[pan,pos] = GetParent('',cad,pos,par);
				[pam,pos] = GetParams('',cad,pos,par);

				/*Finish*/
				if      ( cad[pos]=='-' && cad[pos+1]=='>' )[igu,pos] = GetIgual('',cad,pos,par);
				else if ( cad[pos]==')'                    ) {
					let len=cad.length, spo=++pos;

					for ( ;pos<len && isSpaces(cad,pos); pos++ );

					if      ( cad[pos]=='-' && cad[pos+1]=='>' )[igu,pos] = GetIgual      ('',cad,pos,par);
					else if ( cad[pos]=='{'                    )[bod,pos] = Transpiler_BJS('',cad,pos,Object.assign(par,{includeKeys:0,replaceThis:1})), pos++;
					else if ( cad[pos]!=';'                    ) pos = spo;
				}
				else if ( cad[pos]=='{' )[bod,pos] = Transpiler_BJS( '', cad, pos, Object.assign(par, {includeKeys:0, replaceThis:1}) ), pos++;

				res+=
				'Exec('+ cln +', {select:'+ (pan||'\'body\'') +','+ pam +'}).err(console.error).all(__x__=>{'+
					(igu? igu+'=__x__;' : bod) +
				'})'+
				(cad[pos]||'');
			}
			else res+= cad[pos];
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_INCF = ( result, data, position, params ) => {
	/*Is*/
	function isNIncludeFile( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='i' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='u' &&
				cad[pos+6]=='d' &&
				cad[pos+7]=='e' &&
				isSpacesTabs(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len = cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='f' &&
			cad[pos+1]=='i' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='e' &&
			isSpecial(cad,pos+4)
		);
	 }
	function isUIncludeFile( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				cad[pos+5]=='l' &&
				cad[pos+6]=='u' &&
				cad[pos+7]=='d' &&
				cad[pos+8]=='e' &&
				isSpacesTabs(cad,pos+9) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='f' &&
			cad[pos+1]=='i' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='e' &&
			isSpecial(cad,pos+4)
		);
	 }
	function isGIncludeFile( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='g' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				cad[pos+5]=='l' &&
				cad[pos+6]=='u' &&
				cad[pos+7]=='d' &&
				cad[pos+8]=='e' &&
				isSpacesTabs(cad,pos+9) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='f' &&
			cad[pos+1]=='i' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='e' &&
			isSpecial(cad,pos+4)
		);
	 }

	/*Funciones - GetInclude sincrona*/
	function GetNIncludeFile( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for (;pos<len && cad[pos]!='"' && cad[pos]!="'"; pos++);

		[ret,pos] = GetString('', cad, pos, par, false, false);
		ret       = ParsePath(ret);

		for (;pos<len && cad[pos]!='\n' && cad[pos]!=';'; pos++);
		if ( cad[pos]=='\n' ) pos--;

		/*ARCHIVO*/
		ret.id = Hash(ret.path.toLowerCase());

		if ( global.__included__[ret.id] ) ret = global.__included__[ret.id].data;
		else {
			global.__included__[ret.id] = ret;
			ret.includeGlobal           = false;
			ret.includeType             = 'file';
			ret.data                    = Transpiler_ALL(ret.path, par).data;
			ret                         = ret.data;
		}

		return[res+ret, pos];
	 }
	function GetUIncludeFile( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for (;pos<len && cad[pos]!='"' && cad[pos]!="'"; pos++);

		[ret,pos] = GetString('', cad, pos, par, false, false);
		ret       = ParsePath(ret);

		for (;pos<len && cad[pos]!='\n' && cad[pos]!=';'; pos++);
		if ( cad[pos]=='\n' ) pos--;

		/*ARCHIVO*/
		ret.id = Hash(ret.path.toLowerCase());

		if ( !par.__included__[ret.id] ) {
			par.__included__[ret.id] = ret;
			ret.includeType          = 'file';

			if ( global.__included__[ret.id] ) ret = global.__included__[ret.id].data;
			else {
				global.__included__[ret.id] = ret;
				ret.includeGlobal           = false;
				ret.includeType             = 'file';
				ret.data                    = Transpiler_ALL(ret.path, par).data;
				ret                         = ret.data;
			}
		}
		else ret = '';

		return[res+ret, pos];
	 }
	function GetGIncludeFile( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for (;pos<len && cad[pos]!='"' && cad[pos]!="'"; pos++);

		[ret,pos] = GetString('', cad, pos, par, false, false);
		ret       = ParsePath(ret);

		for (;pos<len && cad[pos]!='\n' && cad[pos]!=';'; pos++);
		if ( cad[pos]=='\n' ) pos--;

		/*ARCHIVO*/
		ret.id = Hash(ret.path.toLowerCase());

		if ( !global.__included__[ret.id] ) {
			global.__included__[ret.id] = ret;
			ret.includeGlobal           = true;
			ret.includeType             = 'file';
			ret.data                    = Transpiler_ALL(ret.path, par).data;
			ret                         = '';
		}
		else ret = '';

		return[res+ret, pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		return[cad, cad.length];
	 }
	function Compile( cad, par           ) {
		!global.__included__ && (global.__included__={});
		!par   .__included__ && (par   .__included__={});

		const len = cad.length;
		let   pos = 0, res = '';

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS   (cad,pos) )[res,pos] = GetComment     (res, cad, pos, par);
			else if ( isStringJS    (cad,pos) )[res,pos] = GetString      (res, cad, pos, par);
			else if ( isNIncludeFile(cad,pos) )[res,pos] = GetNIncludeFile(res, cad, pos, par);
			else if ( isUIncludeFile(cad,pos) )[res,pos] = GetUIncludeFile(res, cad, pos, par);
			else if ( isGIncludeFile(cad,pos) )[res,pos] = GetGIncludeFile(res, cad, pos, par);
			else                                res+= cad[pos];
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_INCC = ( result, data, position, params ) => {
	/*Declaraciones*/
	const arr = [];

	/*Is*/
	function isNIncludeClass( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='i' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='u' &&
				cad[pos+6]=='d' &&
				cad[pos+7]=='e' &&
				isSpacesTabs(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len = cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='c' &&
			cad[pos+1]=='l' &&
			cad[pos+2]=='a' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='s' &&
			isSpecial(cad,pos+5)
		);
	 }
	function isUIncludeClass( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				cad[pos+5]=='l' &&
				cad[pos+6]=='u' &&
				cad[pos+7]=='d' &&
				cad[pos+8]=='e' &&
				isSpacesTabs(cad,pos+9) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='c' &&
			cad[pos+1]=='l' &&
			cad[pos+2]=='a' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='s' &&
			isSpecial(cad,pos+5)
		);
	 }
	function isGIncludeClass( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='g' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				cad[pos+5]=='l' &&
				cad[pos+6]=='u' &&
				cad[pos+7]=='d' &&
				cad[pos+8]=='e' &&
				isSpacesTabs(cad,pos+9) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='c' &&
			cad[pos+1]=='l' &&
			cad[pos+2]=='a' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='s' &&
			isSpecial(cad,pos+5)
		);
	 }

	/*Funciones - GetInclude sincrona*/
	function GetIncludeClassName   ( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for (;pos<len &&  isSpecial(cad,pos) && cad[pos]!=')' && cad[pos]!=';' && cad[pos]!='\n'; pos++);
		for (;pos<len && !isSpecial(cad,pos); res+=cad[pos++]);

		return[res,pos];
	 }
	function GetIncludeClassVersion( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for (;pos<len && cad[pos]!='+' && isSpecial(cad,pos) && !isStringJS(cad,pos) && cad[pos]!=')' && cad[pos]!=';' && cad[pos]!='\n'; pos++);

		if ( cad[pos]=='+' || cad[pos]=='.' || isNumber(cad,pos) ) {
			for (;pos<len && (cad[pos]=='+' || cad[pos]=='.' || isNumber(cad,pos)); res+=cad[pos++]);
		}

		return[res,pos];
	 }
	function GetIncludeClassUrl    ( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for (;pos<len && isSpecial(cad,pos) && !isStringJS(cad,pos) && cad[pos]!=')' && cad[pos]!=';' && cad[pos]!='\n'; pos++);

		if ( isStringJS(cad,pos) ) [res,pos] = GetString('', cad, pos, par, false, false);

		return[res,pos];
	 }
	function GetIncludeClass       ( res, cad, pos, par ) {
		const len = cad.length;
		let   nam = '', ver = '', url = '';

		for (;pos<len && cad[pos]!='(' && cad[pos]!='\n'; pos++);

		pos++;

		[nam,pos] = GetIncludeClassName   (cad, pos, par);
		[ver,pos] = GetIncludeClassVersion(cad, pos, par);
		[url,pos] = GetIncludeClassUrl    (cad, pos, par);

		for (;pos<len && cad[pos]!=';' && cad[pos]!='\n'; pos++);
		if ( cad[pos]=='\n' ) pos--;

		/*GET CLASS*/
		const cla = {id:Hash(), nam, ver, url, type:'class'};

		arr.push(cla);

		return[res+`/*<%${cla.id}%>*/`, pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {}
	function Compile( cad, par           ) {
		!global.__included__ && (global.__included__={});

		const len = cad.length;
		let   pos = 0, res = '';

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS    (cad,pos) )[res,pos] = GetComment     (res, cad, pos, par);
			else if ( isStringJS     (cad,pos) )[res,pos] = GetString      (res, cad, pos, par);
			else if ( isNIncludeClass(cad,pos) )[res,pos] = GetIncludeClass(res, cad, pos, par);
			else if ( isUIncludeClass(cad,pos) )[res,pos] = GetIncludeClass(res, cad, pos, par);
			else if ( isGIncludeClass(cad,pos) )[res,pos] = GetIncludeClass(res, cad, pos, par);
			else                                res+= cad[pos];
		}

		for ( let x=arr.length; x--; ) {
			if ( arr[x].type!='class' || !global.__included__.sSome(v=>v.nam==arr[x].nam) ) {
				global.__included__[arr[x].id] = arr[x];
				arr[x].includeGlobal           = true;
				arr[x].includeType             = 'class';
			}
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_INCU = ( result, data, position, params ) => {
	/*Declaraciones*/
	const arr = [];

	/*Is*/
	function isNIncludeUrl( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='i' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='u' &&
				cad[pos+6]=='d' &&
				cad[pos+7]=='e' &&
				isSpacesTabs(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len = cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='u' &&
			cad[pos+1]=='r' &&
			cad[pos+2]=='l' &&
			isSpecial(cad,pos+3)
		);
	 }
	function isUIncludeUrl( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				cad[pos+5]=='l' &&
				cad[pos+6]=='u' &&
				cad[pos+7]=='d' &&
				cad[pos+8]=='e' &&
				isSpacesTabs(cad,pos+9) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='u' &&
			cad[pos+1]=='r' &&
			cad[pos+2]=='l' &&
			isSpecial(cad,pos+3)
		);
	 }
	function isGIncludeUrl( cad, pos ) {
		if (
			!(
				cad[pos  ]=='#' &&
				cad[pos+1]=='g' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				cad[pos+5]=='l' &&
				cad[pos+6]=='u' &&
				cad[pos+7]=='d' &&
				cad[pos+8]=='e' &&
				isSpacesTabs(cad,pos+9) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len=cad.length;

		for ( ;pos<len && !isSpacesTabs(cad,pos); pos++ );
		for ( ;pos<len && isSpacesTabs (cad,pos); pos++ );

		return (
			cad[pos  ]=='u' &&
			cad[pos+1]=='r' &&
			cad[pos+2]=='l' &&
			isSpecial(cad,pos+3)
		);
	 }

	/*Funciones - GetInclude sincrona*/
	function GetIncludeUrl( res, cad, pos, par, typ ) {
		const len = cad.length;
		let   url = '';

		for (;pos<len && cad[pos]!='(' && cad[pos]!=';' && cad[pos]!='\n'; pos++);

		if ( cad[pos]=='(' ) {
			for (pos++; pos<len && isSpecial(cad,pos) && !isStringCSS(cad,pos) && cad[pos]!=';' && cad[pos]!='\n'; pos++);
			if ( isStringCSS(cad,pos) ) [url,pos] = GetString('', cad, pos, par, false, false);
		}

		for (;pos<len && cad[pos]!=';' && cad[pos]!='\n'; pos++);
		if ( cad[pos]=='\n' ) pos--;

		/*GET URL CODE*/
		url = {id:Hash( url.toLowerCase() ), type:typ, url};

		if ( typ=='unique' ) {
			if ( !par.__included__[url.id] ) par.__included__[url.id] = url;
			else                             return[res, pos];
		}

		arr.push(url);

		return[res+`/*<%${url.id}%>*/`, pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {}
	function Compile( cad, par           ) {
		!global.__included__ && (global.__included__={});
		!par   .__included__ && (par   .__included__={});

		const len = cad.length;
		let   pos = 0, res = '';

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS  (cad,pos) )[res,pos] = GetComment   (res, cad, pos, par);
			else if ( isStringJS   (cad,pos) )[res,pos] = GetString    (res, cad, pos, par);
			else if ( isNIncludeUrl(cad,pos) )[res,pos] = GetIncludeUrl(res, cad, pos, par, 'normal');
			else if ( isUIncludeUrl(cad,pos) )[res,pos] = GetIncludeUrl(res, cad, pos, par, 'unique');
			else if ( isGIncludeUrl(cad,pos) )[res,pos] = GetIncludeUrl(res, cad, pos, par, 'global');
			else                               res+= cad[pos];
		}

		/*SAVE GLOBAL*/
		for ( let x=arr.length; x--; ) {
			global.__included__[arr[x].id] = arr[x];
			arr[x].includeGlobal           = arr[x].type=='global';
			arr[x].includeType             = 'url';
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_INCA = ( data                           ) => {
	/*Funciones - Remplazo*/
	function ReplaceUrl  ( data, included ) {
		return new sProm((run,err)=>{
			if ( included.data ) {
				data = data.replace(`/*<%${included.id}%>*/`, included.data);

				run(data);
			}
			else {
				Ajax(included.url)
				.err(e=>{
					if ( e.code===404 ) run(data);
					else                console.error(e);
				})
				.run(r=>{
					data = data.replace(`/*<%${included.id}%>*/`, r.data);

					run(data);
				});
			}
		});
	 }
	function ReplaceClass( data, included ) {
		return new sProm((run,err)=>{
			if ( included.includeGlobal ) {run(data); return}

			if ( included.data ) {
				data = data.replace(`/*<%${included.id}%>*/`, included.data);

				run(data);
			}
			else {
				Ajax(`${global.__serverComponents__}/${included.nam}.bj`, {headers:{token:'1', botype:'component', version:included.ver||'+'}})
				.err(e=>{
					if ( e.code===404 ) run(data);
					else                console.error(e);
				})
				.run(r=>{
					data = data.replace(`/*<%${included.id}%>*/`, r.data);

					run(data);
				});
			}
		});
	 }

	/*Funciones - Recorrer*/
	function TravelGlobal( data, index ) {
		return new sProm((run,err)=>{
			const key = Object.keys(global.__included__)[index], inc = global.__included__[key];

			if ( !inc ) run(data);
			else switch ( inc.includeType ) {
				case 'file' : TravelGlobal(data, ++index).err(err).run(run); break;
				case 'url'  : ReplaceUrl  (data, inc).err(err).run( d=>TravelGlobal(d, ++index).err(err).run(run) ); break;
				case 'class': ReplaceClass(data, inc).err(err).run( d=>TravelGlobal(d, ++index).err(err).run(run) ); break;
			}
		});
	 }

	/*Inicio*/
	return new sProm((run,err)=>{
		!global.__included__ && (global.__included__={});

		TravelGlobal(data, 0).err(err).run(run);
	});
 };
global.Transpiler_INCG = (                                ) => {
	/*Funciones - Remplazo*/
	function ReplaceClass( data, included ) {
		return new sProm((run,err)=>{
			if ( !included.includeGlobal ) {run(data); return}

			if ( included.data ) {
				data+= included.data;

				run(data);
			}
			else {
				Ajax(`${global.__serverComponents__}/${included.nam}.bj`, {headers:{token:'1', botype:'component', version:included.ver||'+'}})
				.err(e=>{
					if ( e.code!=404 ) console.error(e);

					run(data);
				})
				.run(r=>{
					data+= r.data;

					run(data);
				});
			}
		});
	 }

	/*Funciones - Recorrer*/
	function TravelGlobal( data, index ) {
		return new sProm((run,err)=>{
			const key = Object.keys(global.__included__)[index], inc = global.__included__[key];

			if ( !inc ) run(data);
			else switch ( inc.includeType ) {
				case 'url'  :
				case 'file' : TravelGlobal(data, ++index).err(err).run(run); break;
				case 'class': ReplaceClass(data, inc).err(err).run( d=>TravelGlobal(d, ++index).err(err).run(run) ); break;
			}
		});
	 }

	/*Inicio*/
	return new sProm((run,err)=>{
		!global.__included__ && (global.__included__={});

		TravelGlobal('', 0)
		.err(e=>{
			log.error(e);
			run('');
		})
		.run(run);
	});
 };

global.Transpiler_BC  = ( result, data, position, params ) => {
	/*Constantes*/
	const TYPE_TAG  = 1;
	const TYPE_TAGF = 2;
	const TYPE_TAGN = 3;
	const TYPE_TAGC = 4;
	const TYPE_KEYF = 5;
	const TYPE_MEDI = 6;
	const TYPE_ARRB = 7;
	const TYPE_PROP = 8;
	const TYPE_INCL = 9;

	/*Is*/
	function isStartTag( cad, pos ) {
		return isLetter(cad,pos) || cad[pos]=='-' || cad[pos]=='_';
	 }
	function isChild   ( cad, pos ) {
		return (
			cad[pos]==':' && (cad[pos+1]=='<' || cad[pos+1]=='>') ||
			cad[pos]==';'
		);
	 }
	function isBreak   ( cad, pos ) {
		return (
			cad[pos]=='}'  ||
			cad[pos]=='\n' ||
			isChild(cad,pos)
		);
	 }
	function isFrom    ( cad, pos ) {
		return(
			cad[pos  ]=='f' &&
			cad[pos+1]=='r' &&
			cad[pos+2]=='o' &&
			cad[pos+3]=='m' &&
			isSpecial(cad,pos+4) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isTo      ( cad, pos ) {
		return(
			cad[pos  ]=='t' &&
			cad[pos+1]=='o' &&
			isSpecial(cad,pos+2) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isTag     ( cad, pos ) {
		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isCommentJS (cad,pos) ) pos = GetComment    ('',cad,pos,{})[1];
			else if ( isChild     (cad,pos) ) pos++;
			else break;
		}

		if (
			cad[pos]=='.' || cad[pos]=='#' || cad[pos]=='<' || cad[pos]=='>' ||
			cad[pos]=='+' || cad[pos]=='*' || cad[pos]==':' || cad[pos]=='@' ||
			cad[pos]=='&' || cad[pos]=='$' || cad[pos]==','
		) return true;

		if ( isFrom(cad,pos) || isTo(cad,pos) ) return false;

		if ( isStartTag(cad,pos) ) {
			for ( ;pos<len; pos++ ) {
				if      ( isNumber(cad,pos) || isStartTag(cad,pos) );
				else if ( isCommentJS(cad,pos)                     ) pos = GetComment    ('',cad,pos,{})[1];
				else if ( cad[pos]=='{'                            ) pos = Transpiler_BJ ('',cad,pos,{})[1];
				else break;
			}

			for ( ;pos<len && isSpacesTabs(cad,pos); pos++ );

			if (
				isBreak(cad,pos) || cad[pos]=='>' || cad[pos]==',' ||
				cad[pos]=='+' || cad[pos]=='*' || cad[pos]=='@' ||
				cad[pos]=='[' || cad[pos]=='&'
			) return true;

			if      ( (cad[pos]=='.'||cad[pos]=='#') && isStartTag(cad,pos+1) ) return true;
			else if ( cad[pos]==':'                  && isStartTag(cad,pos+1) ) return true;
		}

		return false;
	 }
	function isProp    ( cad, pos ) {
		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isCommentJS (cad,pos) ) pos=GetComment('',cad,pos,{})[1];
			else if ( isChild     (cad,pos) ) pos++;
			else break;
		}

		if ( isFrom(cad,pos) || isTo(cad,pos) ) return false;

		for ( ;pos<len && (isNumber(cad,pos)||isStartTag(cad,pos)); pos++ );
		for ( ;pos<len && isSpacesTabs(cad,pos); pos++ );

		if      ( isBreak    (cad,pos)                                     ) return false;
		else if ( isStringCSS(cad,pos)                                     ) return true;
		else if ( cad[pos]=='{'                                            ) return true;
		else if ( cad[pos]==':'                  && !isStartTag(cad,pos+1) ) return true;
		else if ( cad[pos]=='$'                  && isLetter   (cad,pos+1) ) return true;
		else if ( (cad[pos]=='.'||cad[pos]=='#') && !isStartTag(cad,pos+1) ) return true;
		else if ( cad[pos]=='-' || cad[pos]=='_'                           ) return true;
		else if ( isLetter(cad,pos) || isNumber(cad,pos)                   ) return true;

		return false;
	 }
	function isPerc    ( cad, pos ) {
		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isCommentJS (cad,pos) ) pos = GetComment('',cad,pos,{})[1];
			else if ( isChild     (cad,pos) ) pos++;
			else break;
		}

		if ( cad[pos]=='.' || isNumber(cad,pos) || isFrom(cad,pos) || isTo(cad,pos) ) {
			for ( ;pos<len && !isSpaces   (cad,pos) && !isBreak(cad,pos); pos++ );
			for ( ;pos<len && isSpacesTabs(cad,pos) && !isBreak(cad,pos); pos++ );

			return isBreak(cad,pos);
		}

		return false;
	 }
	function isRgba    ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='r' &&
				cad[pos+1]=='g' &&
				cad[pos+2]=='b' &&
				cad[pos+3]=='a' &&
				isSpecial(cad,pos+4) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		const len = cad.length;

		for ( pos+=4; pos<len && isSpacesTabs(cad,pos); pos++ );

		return cad[pos]=='(';
	 }
	function isPropProc( cad, pos ) {
		return (
			cad=='top'            ||
			cad=='left'           ||
			cad=='right'          ||
			cad=='bottom'         ||
			cad=='width'          ||
			cad=='height'         ||
			cad=='margin'         ||
			cad=='margin-top'     ||
			cad=='margin-left'    ||
			cad=='margin-right'   ||
			cad=='margin-bottom'  ||
			cad=='padding'        ||
			cad=='padding-top'    ||
			cad=='padding-left'   ||
			cad=='padding-right'  ||
			cad=='padding-bottom' ||
			cad=='max-height'     ||
			cad=='max-width'      ||
			cad=='min-height'     ||
			cad=='min-width'      ||
			cad=='font-size'      ||
			cad=='box-shadow'     ||
			cad=='border-radius'
		);
	 }

	/*Funciones*/
	function GetProp        ( name, value, par        ) {
		let   val = '', pos = 0;
		const cad = value, len = value.length;

		if ( isPropProc(name) ) {
			for ( ;pos<len; pos++ ) {
				if ( isNumber(cad,pos) ) {
					for ( ;pos<len && (isNumber(cad,pos)||cad[pos]=='.'); val+=cad[pos++] );

					if      ( isSpecial(cad,pos) ) val+= 'px'+cad[pos];
					else if ( pos>=len           ) val+= 'px';
					else if ( pos<len            ) val+= cad[pos];
				}
				else if ( cad[pos]=='#' ) {
					val+= cad[pos++];

					for ( ;pos<len && (isLetter(cad,pos) || isNumber(cad,pos)); val+=cad[pos++] );

					pos--;
				}
				else if ( cad[pos]=='$' && cad[pos+1]!='{' )[val,pos] = GetChildsCssVar(val,cad,pos,par);
				else if ( isRgba(cad,pos) ) {
					for ( let key=-1; pos<len; pos++ ) {
						if      ( cad[pos]=='('                    ) val+= cad[pos], key++;
						else if ( cad[pos]==')' && key--==0        ){val+= cad[pos]; break}
						else if ( cad[pos]=='$' && cad[pos+1]!='{' )[val,pos] = GetChildsCssVar(val,cad,pos,par);
						else                                         val+= cad[pos];
					}
				}
				else if ( cad[pos]=='$' && cad[pos+1]=='{' )[val,pos] = Transpiler_JS(val,cad,pos,par);
				else                                         val+= cad[pos];
			}
		}
		else {
			for (;pos<len; pos++) {
				if ( cad[pos]=='$' && cad[pos+1]!='{' )[val,pos] = GetChildsCssVar(val,cad,pos,par);
				else                                    val+= cad[pos];
			}
		}

		return name+' '+val;
	 }
	function GetChildsCssVar( res, cad, pos, par      ) {
		const len = cad.length;
		let   tem = '';

		for ( pos++; pos<len && (isLetter(cad,pos) || isNumber(cad,pos) || cad[pos]=='-' || cad[pos]=='_'); tem+=cad[pos++] );

		res+= `var(--${tem})${cad[pos]?cad[pos]:''}`;

		return[res,pos];
	 }
	function GetChildsFin   ( cad, pos, par           ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos) ) pos      = GetComment('',cad,pos,{})[1];
			else if ( isStringCSS(cad,pos) )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( isBreak    (cad,pos) ) break;
			else if ( cad[pos]=='{'        )[ret,pos] = Transpiler_BJS(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='['        )[ret,pos] = GetAttrs      (ret,cad,pos,{zip:true});
			else                             ret+= cad[pos];
		}

		return[ret,pos];
	 }
	function GetChildsLvl   ( cad, pos, lva           ) {
		const len = cad.length;
		let   lvl = 0;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS (cad,pos) ){pos = GetComment('',cad,pos,{})[1]; cad[pos]=='\n'&&(lvl=0)}
			else if ( isSpacesTabs(cad,pos) ) lvl++;
			else break;
		}

		for ( let num=1; pos<len; pos++ ) {
			if      ( cad[pos]==':' && cad[pos+1]=='<' ) lvl = lva-(num++), pos++;
			else if ( cad[pos]==':' && cad[pos+1]=='>' ) lvl = lva+(num++), pos++;
			else if ( cad[pos]==';'                    ) lvl = lva;
			else if ( isSpacesTabs(cad,pos)            );
			else break;
		}

		return[lvl,pos];
	 }
	function GetChildRIncl  ( ite, cad, pos, par, lva ) {
		const len = cad.length
		let   lvl = 0, ret = '';
		[ret,pos] = Transpiler_INC('',cad,pos,par);

		ite.push({
			level: lvl,
			value: ret,
			type : TYPE_INCL,
		});

		return[ite,pos,lva];
	 }
	function GetChildsTag   ( ite, cad, pos, par, lva ) {
		const len = cad.length;
		let   lvl = 0, ret = '';

		[lvl,pos] = GetChildsLvl(cad,pos,lva);
		[ret,pos] = GetChildsFin(cad,pos,par);

		isChild(cad,pos) && pos--;

		if      ( ret.indexOf('@media'    )==0 ) tip = TYPE_MEDI;
		else if ( ret.indexOf('@keyframes')==0 ) tip = TYPE_KEYF;
		else if ( ret[0]=='@'                  ) tip = TYPE_ARRB;
		else if ( ret.indexOf(',')>-1          ) tip = TYPE_TAGC;
		else                                     tip = TYPE_TAG;

		ite.push({
			level: lvl,
			value: ret,
			type : tip,
		});

		return[ite,pos,lvl];
	 }
	function GetChildsProp  ( ite, cad, pos, par, lva ) {
		const len = cad.length;
		let   lvl = 0, ret = '', nam = '', val = '';

		[lvl,pos] = GetChildsLvl(cad,pos,lva);
		[ret,pos] = GetChildsFin(cad,pos,par);

		for ( let car=ret, por=0, ler=ret.length, isn=0; por<ler; por++ ) {
			if      ( isn==0 && isSpacesTabs(car,por) ) isn = 1;
			else if ( isn==0                          ) nam+= car[por];
			else if ( isn==1                          ) val+= car[por];
		}

		isChild(cad,pos) && pos--;

		ite.push({
			level: lvl,
			value: GetProp(nam,val,par),
			type : TYPE_PROP,
		});

		return[ite,pos,lvl];
	 }
	function GetChildsPerc  ( ite, cad, pos, par, lva ) {
		const len = cad.length;
		let   lvl = 0, ret = '';

		[lvl,pos] = GetChildsLvl(cad,pos,lva);
		[ret,pos] = GetChildsFin(cad,pos,par);

		if ( !isNaN(parseFloat(ret)) ) ite.push({ level:lvl, value:parseFloat(ret)+'%', type:TYPE_TAGN });
		else                           ite.push({ level:lvl, value:ret                , type:TYPE_TAGF });

		isChild(cad,pos) && pos--;

		return[ite,pos,lvl];
	 }
	function GetChildsIgnor ( ite, cad, pos, par, lva ) {
		for ( const len=cad.length; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos) ) pos = GetComment   ('',cad,pos,{})[1];
			else if ( cad[pos]=='\n'       ) break;
		}

		return[ite,pos,lva];
	 }
	function GetChilds      ( cad, par                ) {
		const len = cad.length;
		let   pos = 0, ite = [], lva = 0;

		for (;pos<len; pos++) {
			if      ( isCommentJS(cad,pos) ) pos          = GetComment    ('' ,cad,pos,{}     )[1];
			else if ( isTag      (cad,pos) )[ite,pos,lva] = GetChildsTag  (ite,cad,pos,par,lva);
			else if ( isProp     (cad,pos) )[ite,pos,lva] = GetChildsProp (ite,cad,pos,par,lva);
			else if ( isPerc     (cad,pos) )[ite,pos,lva] = GetChildsPerc (ite,cad,pos,par,lva);
			else                            [ite,pos,lva] = GetChildsIgnor(ite,cad,pos,par,lva);
		}

		return ite;
	 }

	/*Funciones*/
	function SetCodeSpa ( cad                               ) {
		const len = cad.length;
		let   res = '', pos = 0;

		for ( ;pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) )[res,pos] = GetSpacesCSS(res,cad,pos,{zip:true});
			else if ( cad[pos]=='&'         );
			else                              res+= cad[pos];
		}

		if ( res.indexOf('$')>-1 ) res = res.substring(res.indexOf('$')+1);

		return res;
	 }
	function SetCodePar ( pap, paa={}                       ) {
		let   res = [''], tem, isw = 0;
		const fes = _pap => {
			if ( _pap.type===TYPE_TAGC ) {
				tem = res, res = [];

				for (let y=0; y<tem.length; y++)
					for ( let z=0; z<_pap.arrayTagC.length; z++ )
						res.push(` ${_pap.arrayTagC[z]} ${tem[y]} `);
			}
			else for (let y=res.length; y--; res[y]=' '+_pap.value+' '+res[y]);
		 };

		pap.push(paa);
		fes(paa);

		let lvl = paa.level;

		for (let x=pap.length; x--;) {
			if ( pap[x].level<lvl ) {
				lvl = pap[x].level;

				fes(pap[x]);
			}
		}

		return SetCodeSpa( res.join(',') );
	 }
	function SetCodeTag ( res, chi, pos, pap, lva, tab, par ) {
		const pat = SetCodePar(pap, chi[pos]);

		/*Agregando*/
		if ( !chi[pos+1] || chi[pos+1].type!=TYPE_PROP ) return[res,pos,pap,lva];
		if ( res!=''                                   ) res+= `\n${tab} ${!chi[pos-1] || chi[pos-1].type!=TYPE_INCL?'}':''}\n`;

		return[res+`${tab}${SetCodeSpa(` ${pat} { `)}`,pos,pap,lva];
	 }
	function SetCodeTagc( res, chi, poc, pap, lva, tab, par ) {
		const prs = [], cad = chi[poc].value, len = cad.length;
		let   tem = '', pos = 0;

		for ( ;pos<len; pos++ ) {
			if ( cad[pos]==',' ){prs.push(tem); tem = ''}
			else                 tem+= cad[pos];
		}

		tem!='' && prs.push(tem);
		chi[poc].arrayTagC = prs;

		/*Parent*/
		const pat = SetCodePar(pap, chi[poc]);

		/*Agregando*/
		if ( pat=='' || !chi[poc+1] || chi[poc+1].type!=TYPE_PROP ) return[res,poc,pap,lva];
		if ( res!=''                                              ) res+=`\n${tab} ${!chi[pos-1] || chi[pos-1].type!=TYPE_INCL?'}':''}\n`;

		return[res+`${tab}${SetCodeSpa(` ${pat} { `)}`,poc,pap,lva];
	 }
	function SetCodeProp( res, chi, pos, pap, lva, tab, par ) {
		if ( !pap.length ) return[res,pos,pap,lva];

		if ( res=='' && pap.length==1 && pap[0].level==-1 )
			res+= pap[0].value+' {';

		const cad = chi[pos].value, len = cad.length;
		let   por = 0, ret = '', poi = false;

		for ( ;por<len; por++ ) {
			if      ( (cad[por]==' '||cad[por]=='\t') && !poi ) ret+= ':', poi = true;
			else if ( cad[por]==':' && !poi                   ) ret+= ':', poi = true;
			else if ( cad[por]==':' &&  poi                   );
			else if ( isSpaces   (cad,por)                    )[ret,por] = GetSpacesCSS(ret,cad,por,{zip:true});
			else if ( isStringCSS(cad,por)                    )[ret,por] = GetString   (ret,cad,por,par);
			else                                                ret+= cad[por];
		}

		return[`${res}${tab}\n${tab}\t${ret};`,pos,pap,lva];
	 }
	function SetCodeKeyf( res, chi, poc, pap, lva, tab, par ) {
		const cad = chi[poc].value, lec = chi.length, len = cad.length, cht = [];
		let   pos = 0, ret = '', psa = poc;

		for ( poc++; poc<lec; poc++ ) {
			if (
				chi[poc].type===TYPE_TAG  ||
				chi[poc].type===TYPE_KEYF ||
				chi[poc].type===TYPE_MEDI ||
				chi[poc].type===TYPE_ARRB
			) {poc--;break}
			else cht.push(chi[poc]);
		}

		for ( ;pos<len && !isSpaces(cad,pos); pos++ );
		for ( ;pos<len &&  isSpaces(cad,pos); pos++ );

		if ( cad[pos]=='.' || isNumber(cad,pos) ) {
			let val = '', nam = '';

			for ( ;pos<len && !isBreak(cad,pos); val+=cad[pos++] );

			nam = IntToExcel( Hash(new Date().valueOf()+'a'+global.VALUE_IDS++), false );
			res = SetCodeProp(res,[{type:TYPE_PROP,value:`animation ${nam} ${val}`}],0,pap,0,tab)[0];

			if ( res!='' ) res+=`\n${tab} }\n`;

			ret = tab + SetCodeSpa(` @keyframes ${nam} {`);
		}
		else {
			if ( res!='' ) res+= `\n${tab} ${chi[poc-1].type!=TYPE_INCL?'}':''}\n`;
			ret = tab + SetCodeSpa(` ${chi[psa].value} {`);
		}

		ret+= '\n';
		ret+= SetCode(cht, tab+'\t');

		return[res+ret,poc,pap,lva];
	 }
	function SetCodeMedi( res, chi, poc, pap, lva, tab, par ) {
		const cht = [], lec = chi.length, psa = poc;

		for ( poc++; poc<lec; poc++ ) {
			if ( chi[poc].level<=chi[psa].level && chi[poc].type!=TYPE_PROP ){poc--;break}
			else                                                              cht.push(chi[poc]);
		}

		if ( res!='' ) res+= `\n${tab} ${chi[psa-1].type!=TYPE_INCL?'}':''}\n`;

		res+= tab + SetCodeSpa(` ${chi[psa].value} {`) +'\n';
		res+= SetCode(cht, tab+'\t', par);

		return[res,poc,pap,lva];
	 }
	function SetCodeArro( res, chi, poc, pap, lva, tab, par ) {
		const cht = [], lec = chi.length, psa = poc;

		for ( poc++; poc<lec; poc++ ) {
			if ( chi[poc].level<=chi[psa].level && chi[poc].type!=TYPE_PROP ){poc--;break}
			else                                                              cht.push(chi[poc]);
		}
		if ( res!='' ) res+=`\n${tab} ${chi[psa-1].type!=TYPE_INCL?'}':''}\n`;

		res+= tab + SetCodeSpa(` ${chi[psa].value} {`);

		cht.forEach((x,i)=>res+=SetCodeProp('', cht, i, [chi[psa]], chi[psa].level, tab, par )[0]);

		return[res,poc,pap,lva];
	 }
	function SetCodeIncl( res, chi, poc, pap, lva, tab, par ) {
		res+= chi[poc].value;

		return[res,poc,pap,lva];
	 }
	function SetCode    ( chi, tab=-1, par={}               ) {
		const len = chi.length;
		let   pos = 0, res = '', lva = 0, pap = [];

		if ( tab===-1 ) {
			tab = '';

			for ( let pol=0; len && pol<chi[0].level; tab+='\t', pol++ );
		}

		if ( typeof par.sClass==VALUE_STRING && par.sClass!='' )
			pap.push({ level:-1, type:TYPE_TAG, value:par.sClass });

		for ( ;pos<len; pos++ ) {
			switch ( chi[pos].type ) {
				case TYPE_TAGF:
				case TYPE_TAGN:
				case TYPE_TAG : [res,pos,pap,lva] = SetCodeTag (res,chi,pos,pap,lva,tab,par); break;
				case TYPE_TAGC: [res,pos,pap,lva] = SetCodeTagc(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_PROP: [res,pos,pap,lva] = SetCodeProp(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_KEYF: [res,pos,pap,lva] = SetCodeKeyf(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_MEDI: [res,pos,pap,lva] = SetCodeMedi(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_ARRB: [res,pos,pap,lva] = SetCodeArro(res,chi,pos,pap,lva,tab,par); break;
				case TYPE_INCL: [res,pos,pap,lva] = SetCodeIncl(res,chi,pos,pap,lva,tab,par); break;
			}
		}

		if ( res!='' ) res+= `\n${tab} }`;

		return res;
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '', ini = '';
		let   inc = par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let   ite = par.includeTemp                               ; delete par.includeTemp;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+= cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos) )[ret,pos] = GetComment    (ret,cad,pos,par);
			else if ( isStringCSS(cad,pos) )[ret,pos] = GetString     (ret,cad,pos,par,true);
			else if ( cad[pos]=='{'        )[ret,pos] = Transpiler_BJS(ret,cad,pos,par);
			else if ( cad[pos]=='}'        ) break;
			else if ( cad[pos]=='\\'       ) ret+= cad[pos], ret+= cad[++pos];
			else                             ret+= cad[pos];
		}

		ret = Compile(ret,par);

		if ( inc ) ret = ini+ret+cad[pos];

		return[res+ret, pos];
	 }
	function Compile( cad, par           ) {
		cad = Transpiler_DEF (cad,par);
		cad = Transpiler_INCF(cad,par);
		cad = Transpiler_CSS (
			SetCode(
				GetChilds(cad, par),
				-1,
				par
			),
			par
		);

		return cad;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_BH  = ( result, data, position, params ) => {
	/*Is*/
	function isBreak     ( cad, pos, lin ) {
		return(
			(cad[pos]==':' && (cad[pos+1]=='<'||cad[pos+1]=='>')) ||
			cad[pos]==';' ||
			(lin && lin.body=='' && cad[pos]=='/') ||
			cad[pos]=='\n'
		);
	 }
	function isChild     ( cad, pos      ) {
		return(
			cad[pos]==';'
			||
			(
				cad[pos]==':'
				&&
				(cad[pos+1]=='<' || cad[pos+1]=='>')
			)
		);
	 }
	function isTag       ( cad, pos      ) {
		return (
			isLetter(cad,pos) ||
			isNumber(cad,pos) ||
			cad[pos]=='-'     ||
			cad[pos]=='_'
		);
	 }
	function isFinTag    ( cad, pos      ) {
		return(
			cad[pos]=='.' ||
			cad[pos]=='#' ||
			cad[pos]=='(' ||
			isSpacesTabs(cad,pos) ||
			isBreak(cad,pos)
		);
	 }
	function isSp        ( cad, pos      ) {
		if ( cad[pos]!='&'  ) return false;

		const len = cad.length;

		for ( pos++; pos<len && (isLetter(cad,pos) || isNumber(cad,pos) || cad[pos]=='#'); pos++ );

		return cad[pos]==';';
	 }
	function isHtmlTag   ( cad, pos      ) {
		if ( cad[pos]!='<' || (!isLetter(cad,pos+1) && !isNumber(cad,pos+1) && cad[pos+1]!='/') ) return false;

		return true;
	 }
	function isBody      ( cad, pos, lin ) {
		return(
			(cad[pos]==' ' || cad[pos]=='\t')
			&&
			lin.tag!=''
		);
	 }
	function isVersionCom( cad, pos      ) {
		return (
			cad[pos  ]=='v' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='r' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='o' &&
			cad[pos+6]=='n' &&
			isSpecial(cad,pos+7) &&
			(isSpecial(cad,pos-1) || pos-1<0)
		);
	 }

	/*Funciones*/
	function GetVersionComponent( cad, pos ) {
		const len = cad.length;

		for (pos++; pos<len && !isSpecial(cad,pos) && cad[pos]!='.' && cad[pos]!='#'; pos++);

		if ( cad[pos]=='(' ) {
			for (pos++; pos<len; pos++) {
				if      ( cad[pos]==')'         ) break;
				else if ( isVersionCom(cad,pos) ) {
					for (;pos<len && cad[pos]!='='; pos++);

					let ver = '';

					for ( pos++; pos<len && (isNumber(cad,pos) || cad[pos]=='.'); ver+=cad[pos++] );

					return ver;
				}
			}
		}

		return '';
	 }

	function GetChildsSp        ( res, cad, pos, par      ) {
		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if ( cad[pos]==';' ){res+=cad[pos]; break}
			else                 res+=cad[pos];
		}

		return[res,pos];
	 }
	function GetChildsHtmlTag   ( res, cad, pos, par      ) {
		const len = cad.length;

		for ( ;pos<len && cad[pos]!='>'; res+=cad[pos++] );

		res+= cad[pos];

		return[res,pos];
	 }
	function GetChildsSubb      ( res, cad, pos, par      ) {
		let ret   = '';
		[ret,pos] = GetAttrs(ret,cad,pos,Object.assign(par, {cleanSpaces:0, replacekeys:0}),false);
		res      += Transpiler_BH(ret,par);

		return[res,pos];
	 }
	function GetChildsLvl       ( lin, cad, pos, par, lva ) {
		const len = cad.length;
		let   lvl = 0, lvn = 0, isc = false;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS (cad,pos) ){pos = GetComment('',cad,pos,{})[1]; cad[pos]=='\n'&&(lvl=0)}
			else if ( isSpacesTabs(cad,pos) ) lvl++;
			else break;
		}

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]==':' && cad[pos+1]=='<' ) isc = true, lvn--, pos++;
			else if ( cad[pos]==':' && cad[pos+1]=='>' ) isc = true, lvn++, pos++;
			else if ( cad[pos]==';'                    ) isc = true;
			else if ( isSpacesTabs(cad,pos)            );
			else break;
		}

		lin.level = (isc?lva:0)+lvl+lvn;
		lin.level<0 && (lin.level=0);
		pos--;

		return pos;
	 }
	function GetChildsBody      ( lin, cad, pos, par      ) {
		const len = cad.length;
		let   bod = '';

		for ( pos++; pos<len; pos++ ) {
			if      ( isBreak    (cad,pos,{body:bod})    ){pos--; break}
			else if ( isCommentJS(cad,pos)               ) pos      = GetComment      ('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos)               )[bod,pos] = GetString       (bod,cad,pos,par,true);
			else if ( isHtmlTag  (cad,pos)               )[bod,pos] = GetChildsHtmlTag(bod,cad,pos,par);
			else if ( isSp       (cad,pos)               )[bod,pos] = GetChildsSp     (bod,cad,pos,par);
			else if ( cad[pos]=='['                      )[bod,pos] = GetChildsSubb   (bod,cad,pos,par);
			else if ( cad[pos]=='{'                      )[bod,pos] = Transpiler_BJS  (bod,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='\\' && cad[pos+1]=='n'  ) bod+= '<br>'  , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='t'  ) bod+= '&emsp;', pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='s'  ) bod+= '&nbsp;', pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=="'"  ) bod+= '&#39;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='"'  ) bod+= '&#34;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='<'  ) bod+= '&#60;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='>'  ) bod+= '&#62;' , pos++;
			else if ( cad[pos]=='\\' && cad[pos+1]=='\\' ) bod+= '\\'    , pos++;
			else if ( cad[pos]=='\\'                     ) bod+= cad[++pos];
			else                                           bod+= cad[pos];
		}

		lin.body = bod;

		return pos;
	 }
	function GetChildsTag       ( lin, cad, pos, par      ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len; pos++ ) {
			if      ( isFinTag   (cad,pos) ){pos--; break}
			else if ( isCommentJS(cad,pos) ) pos      = GetComment   ('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos) )[ret,pos] = GetString    (ret,cad,pos,par);
			else if ( cad[pos]=='\r'       );
			else if ( cad[pos]=='{'        )[ret,pos] = Transpiler_BJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='\\'       ) ret+=cad[++pos];
			else                             ret+=cad[pos];
		}

		lin.tag = ret;

		if ( ret.indexOf('-')>-1 ) {
			const ver = GetVersionComponent(cad,pos);
			let   rep = '';
			ver && (lin.tag+= '-' + ver.replace(/[\._]/g, '-'));

			for ( let pop = 0, lep = ret.length; pop<lep; pop++ ) {
				if ( ret[pop]=='-' ) {
					pop++;
					rep+= ret[pop].toUpperCase();
				}
				else rep+= ret[pop];
			}

			par.__classDependencies__[rep] = {type:'tag', version_dependencie:ver};
		}

		return pos;
	 }
	function GetChildsIds       ( lin, cad, pos, par      ) {
		const len = cad.length;
		let   ret = '', not = false;

		for ( pos++; pos<len; pos++ ) {
			if      ( isFinTag   (cad,pos) ){pos--; break}
			else if ( isCommentJS(cad,pos) ) pos      = GetComment   ('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos) )[ret,pos] = GetString    (ret,cad,pos,par);
			else if ( isSpaces   (cad,pos) );
			else if ( cad[pos]=='{'        )[ret,pos] = Transpiler_BJS(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='!'        ) not      = true;
			else if ( cad[pos]=='\\'       ) ret+= cad[++pos];
			else                             ret+= cad[pos];
		}

		lin.id = ret;

		if ( lin.tag=='' ) lin.tag          = 'div';
		if (  not        ) lin.notView      = true;
		if ( !not        ) par.idsView[ret] = 1;

		return pos;
	 }
	function GetChildsAttsString( res, cad, pos, par      ) {
		let ret, por = 0;

		[ret,pos] = GetString('',cad,pos,par,true,false);
		const len = ret.length;

		for ( ;por<len; por++ ) {
			if      ( ret[por]=='\\' ) res+= ret[por], res+= ret[++por];
			else if ( ret[por]=='"'  ) res+= '\\"';
			else                       res+= ret[por];
		}

		return[res, pos];
	 }
	function GetChildsAttsEvent ( res, cad, pos, par      ) {
		const len = cad.length;
		let   eve = '', fun = '';

		for ( pos++; pos<len && cad[pos]!=')'; eve+=cad[pos++] );
		for ( ;pos<len && cad[pos]!='='; pos++ );
		for ( pos++; pos<len && !isSpecial(cad,pos); fun+=cad[pos++] );

		if ( cad[pos]==')' ) pos--;

		res({ key:eve, val:fun, type:'event' });

		return pos;
	 }
	function GetChildsAtts      ( lin, cad, pos, par      ) {
		let   isk = 0, key = '', val = '';
		const len = cad.length, ret = [];
		const fre = obj => {ret.push(obj); isk=0; key=''; val='';};

		let col = '';
		for ( pos++; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)                       )[col,pos] = GetComment        ('', cad, pos, par);
			else if ( cad[pos]=='('                              ) pos      = GetChildsAttsEvent(fre, cad, pos, par);
			else if ( cad[pos]==')'                              ) break;
			else if ( isk==1 && cad[pos]=='{'                    )[val,pos] = Transpiler_BJS(val,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( isk==1 && isStringCSS (cad,pos)            )[val,pos] = GetChildsAttsString(val,cad,pos,par), fre({key,val});
			else if ( isk==1 && isSpacesTabs(cad,pos) && val=='' );
			else if ( isk==1 && isSpacesTabs(cad,pos)            ) fre({key,val});
			else if ( isk==1                                     ) val     += cad[pos];
			else if ( isk==0 && cad[pos]=='='                    ) isk      = 1;
			else if ( isk==0 && cad[pos]=='{'                    )[key,pos] = Transpiler_BJS(key,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( isk==0 && isSpacesTabs(cad,pos) && key=='' );
			else if ( isk==0 && isSpacesTabs(cad,pos)            ){
				for( ;pos<len && isSpacesTabs(cad,pos); pos++);

				if ( cad[pos]!='=' ) fre({key,val});

				pos--;
			}
			else if ( isk==0 && cad[pos]!='\n' ) key+= cad[pos];
		}

		if ( key!='' ) ret.push({key,val});

		for ( let x=ret.length; x--; ) {
			if ( ret[x].key==='class' ) {
				if ( lin.class!='' ) lin.class+= ' ';

				lin.class+= ret[x].val;
				ret.splice(x,1);
			}
		}

		lin.atts = ret;

		if ( lin.tag=='' ) lin.tag = 'div';

		return pos;
	 }
	function GetChildsClass     ( lin, cad, pos, par      ) {
		const len = cad.length;
		let   ret = '';

		for ( pos++; pos<len; pos++ ) {
			if      ( isFinTag   (cad,pos) ){pos--; break}
			else if ( isCommentJS(cad,pos) ) pos      = GetComment   ('' ,cad,pos,par)[1];
			else if ( isStringCSS(cad,pos) )[ret,pos] = GetString    (ret,cad,pos,par);
			else if ( cad[pos]=='{'        )[ret,pos] = Transpiler_BJ(ret,cad,pos,Object.assign(par,{includeTemp:1}));
			else if ( cad[pos]=='\\'       ) ret+= cad[++pos];
			else                             ret+= cad[pos];
		}

		if ( lin.tag  =='' ) lin.tag   = 'div';
		if ( lin.class!='' ) lin.class+= ' ';

		lin.class+= ret;

		return pos;
	 }
	function GetChildsRInclude  ( lin, cad, pos, par      ) {
		[lin.body,pos] = Transpiler_INC('', cad, pos, Object.assign(par, {includeKeys:0}));
		lin.onlyBody   = true;

		return pos;
	 }
	function GetChilds          ( cad, par                ) {
		const fli = () => { return{level:0, id:'', tag:'', atts:[], class:'', body:'', aclose:false} };
		const res = [], len = cad.length;
		let   pos = 0, lva = 0, lin = fli();

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)                        ) pos        = GetComment       ('' ,cad,pos,{} )[1];
			else if ( isBody     (cad,pos,lin)                    ) pos        = GetChildsBody    (lin,cad,pos,par);
			else if ( isTag      (cad,pos)                        ) pos        = GetChildsTag     (lin,cad,pos,par);
			else if ( cad[pos]=='#'                               ) pos        = GetChildsIds     (lin,cad,pos,par);
			else if ( cad[pos]=='('                               ) pos        = GetChildsAtts    (lin,cad,pos,par);
			else if ( cad[pos]=='.'                               ) pos        = GetChildsClass   (lin,cad,pos,par);
			else if ( cad[pos]=='/'  && lin.body==''              ) lin.aclose = true;
			else if ( cad[pos]=='\n' && isBody(' ',0,lin)         ){lva        = lin.level; res.push(lin); lin = fli()}
			else if ( (isSpacesTabs(cad,pos) || isChild(cad,pos)) ){
				if ( lin.tag!='' ) {
					lva = lin.level;

					res.push(lin);
					lin=fli();
				}

				pos = GetChildsLvl(lin,cad,pos,par,lva);
			}
		}

		isBody(' ',0,lin) && res.push(lin);

		return res;
	 }

	/*Funciones*/
	function GetStruct( res, lin, pos, par ) {
		const len = lin.length;

		if ( pos>=len           ) return[res+''  , pos];
		if ( lin[pos].level==-1 ) return[res+'\n', pos];

		const line = lin[pos]
		let   ret = '', chi = '';

		for ( let x=line.level; x--; ) ret+= '\t';

		if ( line.onlyBody ) {
			res+= line.body;

			return[res+'\n', pos];
		}

		line.atts.forEach(v=>{
			if ( v.type=='event' ) {
				if ( !line.id                     ) line.id                     = 'i' + Hash(), par.idsView[line.id] = 1;
				if ( !par.__htmlEvents__[line.id] ) par.__htmlEvents__[line.id] = [];

				par.__htmlEvents__[line.id].push({ key:v.key, val:v.val, id:line.id });
			}
		})

		ret+= '<'+
			 line.tag +
			(line.id   !=''   ? ' id="'    + line.id + '"' : '') +
			(line.class!=''   ? ' class="' + line.class + '"' : '') +
			(line.atts.length ? ' '        + line.atts.reduce((r,v)=>!v.type ? `${r} ${v.key}="${v.val}"` : r,'') : '') +
			(line.aclose      ? '/' : '') +
		'>'+ line.body;

		for ( pos++; pos<len; pos++ ) {
			if ( line.level>=lin[pos].level ){pos--; break;}
			else                             [chi,pos] = GetStruct(chi,lin,pos,par);
		}

		if ( chi!='' ) {
			ret+= '\n' + chi;
			for ( let x=line.level; x--; ) ret+= '\t';
		}

		if ( !line.aclose )
			ret+= '</'+ line.tag +'>';

		return[res+ret+'\n', pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '', ini = '';
		let   inc = par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let   ite = par.includeTemp                               ; delete par.includeTemp;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+= cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos) )[ret,pos] = GetComment    (ret,cad,pos,par);
			else if ( isStringCSS(cad,pos) )[ret,pos] = GetString     (ret,cad,pos,par,true);
			else if ( cad[pos]=='{'        )[ret,pos] = Transpiler_BJS(ret,cad,pos,par);
			else if ( cad[pos]=='}'        ) break;
			else if ( cad[pos]=='\\'       ) ret+= cad[pos], ret+= cad[++pos];
			else                             ret+= cad[pos];
		}

		ret = Compile(ret,par);

		if ( inc ) ret = ini+ret+cad[pos];

		return[res+ret, pos];
	 }
	function Compile( cad, par           ) {
		if ( !par.__htmlEvents__        ) par.__htmlEvents__        = {};
		if ( !par.__classDependencies__ ) par.__classDependencies__ = {};

		cad         = Transpiler_DEF (cad,par);
		cad         = Transpiler_INCF(cad,par);
		par.idsView = Object.assign({}, par.idsView);

		const ite = GetChilds(cad,par);
		let   len = cad.length, tem = '', pos = 0;

		for ( pos=0, tem='', len=ite.length; pos<len; pos++ )
			[tem,pos] = GetStruct(tem,ite,pos,par);

		while ( tem[tem.length-1]=='\n' )
			tem = tem.substring(0, tem.length-1);

		return Transpiler_HTML(tem, par);
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_BJS = ( result, data, position, params ) => {
	/*Variables*/
	const TYPE_FUNC = 1;

	/*Is*/
	function isView         ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='-' &&
				cad[pos+1]=='v' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='e' &&
				cad[pos+4]=='w' &&
				isSpecial(cad,pos+5) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=5;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isStyle        ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='-' &&
				cad[pos+1]=='s' &&
				cad[pos+2]=='t' &&
				cad[pos+3]=='y' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='e' &&
				isSpecial(cad,pos+6) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 6;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isBH           ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='b' &&
				cad[pos+1]=='h' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 2;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isBC           ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='b' &&
				cad[pos+1]=='c' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 2;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isMD           ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='m' &&
				cad[pos+1]=='d' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 2;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isClass        ( cad, pos ) {
		if (
			cad[pos  ]=='c' &&
			cad[pos+1]=='l' &&
			cad[pos+2]=='a' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='s' &&
			isSpacesTabs(cad,pos+5) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		) {
			const len = cad.length;

			for (pos+=5; pos<len && isSpaces(cad,pos); pos++);

			return cad[pos]=='-' || cad[pos]=='_' || isLetter(cad,pos);
		}

		return false;
	 }
	function isCompiledClass( cad, pos ) {
		return (
			cad[pos  ]=='/' &&
			cad[pos+1]=='*' &&
			cad[pos+2]=='@' &&
			cad[pos+3]=='*' &&
			cad[pos+4]=='/' &&
			cad[pos+5]=='c' &&
			cad[pos+6]=='l' &&
			cad[pos+7]=='a' &&
			cad[pos+8]=='s' &&
			cad[pos+9]=='s' &&
			isSpacesTabs(cad,pos+10) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isFunction     ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpaces(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		for ( pos+=8; pos<len && isSpaces(cad,pos); pos++ );

		return isLetter(cad,pos) || cad[pos]=='_';
	 }
	function isFunctionS    ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='a' &&
				cad[pos+1]=='s' &&
				cad[pos+2]=='y' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				isSpaces(cad,pos+5) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		for ( pos+=5; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpecial(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		for ( pos+=8; pos<len && isSpaces(cad,pos); pos++ );

		return isLetter(cad,pos) || cad[pos]=='_';
	 }
	function isFunctionA    ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if ( cad[pos]=='=' || (isSpecial(cad,pos) && !isSpaces(cad,pos) && cad[pos]!='.') ) break;
		}

		if ( cad[pos]!='=' ) return false;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpecial(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 8;

		return cad[pos]=='(';
	 }
	function isArrowP       ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)                                        ) pos = GetComment('',cad,pos,{})[1];
			else if ( isSpecial  (cad,pos) && !isSpaces(cad,pos) && cad[pos]!='.' ) break;
		}

		if ( cad[pos]!='=' ) return false;
		for ( pos++; pos<len && isSpacesTabs(cad,pos); pos++ );

		if ( cad[pos]=='(' ) {
			for ( let key=-1; pos<len; pos++ ) {
				if      ( cad[pos]=='('             ) key++;
				else if ( cad[pos]==')' && key--==0 ){pos++; break}
				else if ( isCommentJS(cad,pos)      ) pos = GetComment('',cad,pos,{})[1];
				else if ( isStringJS (cad,pos)      ) pos = GetString ('',cad,pos,{})[1];
			}
		}
		else for ( ;pos<len && (isLetter(cad,pos) || isNumber(cad,pos) || isSpacesTabs(cad,pos) || cad[pos]=='_'); pos++ );

		if ( cad[pos]!='=' || cad[pos+1]!='>' ) return false;

		pos+= 2;

		for ( ;pos<len && isSpaces(cad,pos); pos++ );

		return cad[pos]=='{';
	 }
	function isArrowN       ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( pos++; pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isCommentJS (cad,pos) ) pos = GetComment('',cad,pos,{})[1];
			else break;
		}

		for ( ;pos<len && cad[pos]!='\n' && cad[pos]!='='; pos++ );

		if ( cad[pos]!='=' ) return false;

		for ( pos++; pos<len; pos++ ) {
			if ( cad[pos]=='(' ) {
				let key=0;
				for (pos++; pos<len; pos++) {
					if      ( cad[pos]=='('             ) key++;
					else if ( cad[pos]==')' && key--==0 ){pos++; break}
				}
			}
			else if ( cad[pos]=='\n' || cad[pos]=='=' ) break;
		}

		return cad[pos]=='=' && cad[pos+1]=='>';
	 }
	function isVaIgual      ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( ;pos<len && isSpaces(cad,pos); pos++ );

		if ( cad[pos]!='v' || cad[pos+1]!='a' || cad[pos+2]!='.' ) return false;

		for ( ;pos<len && (!isSpecial(cad,pos) || isSpaces(cad,pos) || cad[pos]=='.'); pos++ );

		return cad[pos]=='=';
	 }
	function isBefore       ( cad, pos ) {
		return(
			cad[pos  ]=='-' &&
			cad[pos+1]=='b' &&
			cad[pos+2]=='e' &&
			cad[pos+3]=='f' &&
			cad[pos+4]=='o' &&
			cad[pos+5]=='r' &&
			cad[pos+6]=='e' &&
			isSpecial(cad,pos+7) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isConstructor  ( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='c' &&
			cad[pos+2 ]=='o' &&
			cad[pos+3 ]=='n' &&
			cad[pos+4 ]=='s' &&
			cad[pos+5 ]=='t' &&
			cad[pos+6 ]=='r' &&
			cad[pos+7 ]=='u' &&
			cad[pos+8 ]=='c' &&
			cad[pos+9 ]=='t' &&
			cad[pos+10]=='o' &&
			cad[pos+11]=='r' &&
			isSpecial(cad,pos+12) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDestructor   ( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='d' &&
			cad[pos+2 ]=='e' &&
			cad[pos+3 ]=='s' &&
			cad[pos+4 ]=='t' &&
			cad[pos+5 ]=='r' &&
			cad[pos+6 ]=='u' &&
			cad[pos+7 ]=='c' &&
			cad[pos+8 ]=='t' &&
			cad[pos+9 ]=='o' &&
			cad[pos+10]=='r' &&
			isSpecial(cad,pos+11) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDefinition   ( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='d' &&
			cad[pos+2 ]=='e' &&
			cad[pos+3 ]=='f' &&
			cad[pos+4 ]=='i' &&
			cad[pos+5 ]=='n' &&
			cad[pos+6 ]=='i' &&
			cad[pos+7 ]=='t' &&
			cad[pos+8 ]=='i' &&
			cad[pos+9 ]=='o' &&
			cad[pos+10]=='n' &&
			isSpecial(cad,pos+12) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDefif        ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='d' &&
			cad[pos+2]=='e' &&
			cad[pos+3]=='f' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='f' &&
			cad[pos+6]==' '
		);
	 }
	function isElseif       ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='e' &&
			cad[pos+5]=='i' &&
			cad[pos+6]=='f' &&
			(isSpecial(cad,pos+7)||pos+7>=cad.length)
		);
	 }
	function isEndif        ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='n' &&
			cad[pos+3]=='d' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='f' &&
			(isSpecial(cad,pos+6)||pos+6>=cad.length)
		);
	 }
	function isThis         ( cad, pos ) {
		return(
			cad[pos  ]=='t' &&
			cad[pos+1]=='h' &&
			cad[pos+2]=='i' &&
			cad[pos+3]=='s' &&
			isSpecial(cad,pos+4) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isVarResJSON   ( cad, pos ) {
		return (
			cad[pos]=='-' &&
			(
				(cad[pos+1]=='P' && cad[pos+2]=='r' && cad[pos+3]=='o' && cad[pos+4]=='j' && cad[pos+5]=='e' && cad[pos+6]=='c' && cad[pos+7]=='t') ||
				(cad[pos+1]=='V' && cad[pos+2]=='a' && cad[pos+3]=='r' && cad[pos+4]=='i' && cad[pos+5]=='a' && cad[pos+6]=='b' && cad[pos+7]=='l' && cad[pos+8]=='e' && cad[pos+9]=='s') ||
				(cad[pos+1]=='S' && cad[pos+2]=='e' && cad[pos+3]=='r' && cad[pos+4]=='v' && cad[pos+5]=='e' && cad[pos+6]=='r') ||
				(cad[pos+1]=='P' && cad[pos+2]=='a' && cad[pos+3]=='t' && cad[pos+4]=='h' && cad[pos+5]=='s') ||
				(cad[pos+1]=='C' && cad[pos+2]=='o' && cad[pos+3]=='m' && cad[pos+4]=='p' && cad[pos+5]=='i' && cad[pos+6]=='l' && cad[pos+7]=='e' && cad[pos+8]=='r') ||
				(cad[pos+1]=='R' && cad[pos+2]=='e' && cad[pos+3]=='s' && cad[pos+4]=='o' && cad[pos+5]=='u' && cad[pos+6]=='r' && cad[pos+7]=='c' && cad[pos+8]=='e' && cad[pos+9]=='s') ||
				(cad[pos+1]=='A' && cad[pos+2]=='p' && cad[pos+3]=='i')
			) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isVersionDef   ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='v' &&
				cad[pos+1]=='e' &&
				cad[pos+2]=='r' &&
				cad[pos+3]=='s' &&
				cad[pos+4]=='i' &&
				cad[pos+5]=='o' &&
				cad[pos+6]=='n' &&
				isSpecial(cad,pos+7) &&
				(isSpecial(cad,pos-1) || pos-1<0)
			)
		) return false;

		const len = cad.length;

		for (pos+=7; pos<len && isSpaces(cad,pos); pos++);

		if ( cad[pos]!=':' ) return false;

		for (pos++; pos<len && isSpaces(cad,pos); pos++);

		return isNumber(cad,pos);
	 }

	/*Funciones - Proc*/
	function GetProcFunction ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( res+=cad[pos++]; pos<len; pos++ ) {
			if  ( cad[pos]=='{'     ) break;
			else                      res+= cad[pos];
		}

		def         = par.defines;
		par.defines = {};
		[ret,pos]   = Transpiler_BJS('',cad,pos,par);
		par.defines = def;

		return[res+ret+'\n',pos];
	 }
	function GetProcArrowN   ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && (cad[pos]!='=' || cad[pos+1]!='>'); res+=cad[pos++] );

		res+= cad[pos++];//=
		res+= cad[pos++];//>

		for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );

		for ( let cor=0, key=0, par=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos] = GetRegExp (ret,cad,pos,par);
			else if ( cad[pos]=='('             ) ret+= cad[pos], par++;
			else if ( cad[pos]==')' && par--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='['             ) ret+= cad[pos], cor++;
			else if ( cad[pos]==']' && cor--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='{'             ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) ret+= cad[pos];
			else if (
				(cad[pos]=='\n' || cad[pos]==';')
				&&
				cor==0 && key==0 && par==0
			){
				ret+= cad[pos];
				break;
			}
			else ret+= cad[pos];
		}

		def         = par.defines;
		par.defines = {};
		ret         = Transpiler_BJS(ret,par);
		par.defines = def;

		return[res+ret+'\n',pos];
	 }
	function GetVaIgual      ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='='; res+=cad[pos++] );

		res+= cad[pos++];

		for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );

		for ( let cor=0, key=0, par=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos] = GetRegExp (ret,cad,pos,par);
			else if ( cad[pos]=='('             ) ret+= cad[pos], par++;
			else if ( cad[pos]==')' && par--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='['             ) ret+= cad[pos], cor++;
			else if ( cad[pos]==']' && cor--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='{'             ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) ret+= cad[pos];
			else if (
				(cad[pos]=='\n' || cad[pos]==';')
				&&
				cor==0 && key==0 && par==0
			){
				ret+= cad[pos];
				break;
			}
			else ret+= cad[pos];
		}

		return[res+ret+(ret[ret.length-1]!=';'?';':'')+'\n',pos];
	 }
	function GetBC           ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = Transpiler_BC(ret,cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetStyle        ( res, cad, pos, par ) {
		let ret       = '';
		let zip       = par.zip;
		par.zip       = !par.noZipDebug;
		[ret,pos]     = Transpiler_BC(ret,cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip       = zip;
		par.hashStyle = Hash(ret);
		par.codeStyle = ret;

		return[ret, pos];
	 }
	function GetView         ( res, cad, pos, par ) {
		let ret      = '';
		let zip      = par.zip;
		par.zip      = !par.noZipDebug;
		[ret,pos]    = Transpiler_BH('',cad,pos,Object.assign(par, {includeKeys:0}));
		par.zip      = zip;
		par.yaView   = true;
		par.hashView = Hash(ret);
		par.codeView = ret;

		return[ret, pos];
	 }
	function GetBH           ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = Transpiler_BH('',cad,pos,Object.assign({},par,{includeKeys:0, idsView:{}}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetMD           ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = Transpiler_MD('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetBefore       ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		def         = par.defines;
		par.defines = {};
		[ret,pos]   = Transpiler_BJS('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines = def;

		return[ret, pos];
	 }
	function GetConstructor  ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		def               = par.defines;
		par.defines       = {};
		[ret,pos]         = Transpiler_BJS('',cad,pos,Object.assign(par,{includeKeys:0, __clearLevelDependencies__:true}));
		par.defines       = def;
		par.yaConstructor = true;

		delete par.__clearLevelDependencies__;

		return[ret, pos];
	 }
	function GetDestructor   ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		def              = par.defines;
		par.defines      = {};
		[ret,pos]        = Transpiler_BJS('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines      = def;
		par.yaDestructor = true;

		return[res+`
        		this.destructor  = () => {
        			for( let x in co )
        				if( tipode(co[x])==VALUE_OBJECT && typeof co[x].destructor==VALUE_FUNCTION )
        					co[x].destructor();

        			${ret}
        		 };
            `.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetDefinition   ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		let def     = par.defines;
		par.defines = {};
		[ret,pos]   = Transpiler_BJS('',cad,pos,Object.assign(par,{includeKeys:0, typeDefinition:1}));
		par.defines = def;

		try {eval(`def={${ret}}`)}
		catch(e) {def = {}}

		par.definition = def;

		return[res,pos];
	 }
	function GetDefif        ( res, cad, pos, par ) {
		const len = cad.length;
		let   nco = '', col = '', elc = '';

		for (      ; pos<len && !isSpaces(cad,pos); pos++           );
		for ( pos++; pos<len && !isSpaces(cad,pos); nco+=cad[pos++] );

		for( col=''; pos<len && !isElseif(cad,pos) && !isEndif(cad,pos); col+=cad[pos++] );

		if ( isElseif(cad,pos) ) {
			for(;pos<len && cad[pos]!='\n'; pos++);
			for( elc='', pos++; pos<len && !isEndif(cad,pos); elc+=cad[pos++] );
		}

		for ( ;pos<len && !isEndif(cad,pos); pos++ );

		pos+= 6;

		if ( nco==(par.environment||'debug') ) col = Transpiler_BJ(col, par) + (cad[pos]||'');
		else                                   col = Transpiler_BJ(elc, par) + (cad[pos]||'');

		return[res+col,pos];
	 }
	function GetThis         ( res, cad, pos, par ) {
		const len = cad.length;

		for (;pos<len && !isSpecial(cad,pos); pos++);

		return[res+'__x__',--pos];
	 }
	function GetVarResJSON   ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for (pos++; pos<len && !isSpecial(cad,pos); ret+=cad[pos++]);

		res+= ret;

		return[res,--pos];
	 }
	function GetEvents       ( res, cad, pos, par ) {
		(par.__htmlEvents__||{}).sForEach((v,k)=>{
			res+= `
        		co.${k}.sEvents({
        			${v.reduce((rr,vv)=>rr+`'${vv.key}':${vv.val},`, '')}
        		});
             `.replace(/^( *)/gm, '');
		});

		return res;
	 }
	function GetCompiledClass( res, cad, pos, par ) {
		const len = cad.length;

		for ( ;pos<len &&  isSpecial(cad,pos); res+=cad[pos++] );
		for ( ;pos<len && !isSpecial(cad,pos); res+=cad[pos++] );

		return[res,--pos];
	 }
	function GetVersionDef   ( res, cad, pos, par ) {
		const len = cad.length;
		let   ver = '';

		for (pos+=7; pos<len && (isSpaces(cad,pos) || cad[pos]==':'); pos++);
		for (;pos<len && (isNumber(cad,pos) || cad[pos]=='.'); ver+=cad[pos++]);

		res+= `"version": "${ver}"` + cad[pos];

		return[res,pos];
	 }

	/*Funciones - GET*/
	function GetClassName   ( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for ( pos+=5; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos); res+=cad[pos++] );

		par.sClass = '.'+res;

		return res;
	 }
	function GetClassParent ( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for ( ;pos<len && cad[pos]!=':' && cad[pos]!='{'; pos++ );
		if ( cad[pos]!=':' ) return'';
		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos); res+=cad[pos++] );

		return res;
	 }
	function GetClassVersion( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for ( ;pos<len && cad[pos]!='(' && cad[pos]!='{'; pos++ );
		if ( cad[pos]!='(' ) return '';
		for (;pos<len && cad[pos]!='='; pos++);
		for (pos++; pos<len && (isNumber(cad,pos) || cad[pos]=='.'); res+=cad[pos++]);

		return res;
	 }
	function GetClassBody   ( cad, pos, par      ) {
		const len = cad.length;
		let   res = '', sty = '', vie = '', des = '', bef = '', con = '', fun = '', vas = '', key = 0;

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		// Proc
		for ( pos++; pos<len; pos++ ) {
			if      ( cad[pos]=='{'                                                           ) res+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0                                               ) break;
			else if ( isCompiledClass(cad,pos)                                                )[res,pos] = GetCompiledClass(res,cad,pos,par);
			else if ( isCommentJS    (cad,pos)                                                )[res,pos] = GetComment      (res,cad,pos,par);
			else if ( isStringJS     (cad,pos)                                                )[res,pos] = GetString       (res,cad,pos,par);
			else if ( isStyle        (cad,pos)                                                )[sty,pos] = GetStyle        (sty,cad,pos,par);
			else if ( isView         (cad,pos)                                                )[vie,pos] = GetView         (vie,cad,pos,par);
			else if ( isBefore       (cad,pos)                                                )[bef,pos] = GetBefore       (bef,cad,pos,par);
			else if ( isConstructor  (cad,pos)                                                )[con,pos] = GetConstructor  (con,cad,pos,par);
			else if ( isDestructor   (cad,pos)                                                )[des,pos] = GetDestructor   (des,cad,pos,par);
			else if ( isDefinition   (cad,pos)                                                )[con,pos] = GetDefinition   (con,cad,pos,par);
			else if ( isBH           (cad,pos)                                                )[res,pos] = GetBH           (res,cad,pos,par);
			else if ( isBC           (cad,pos)                                                )[res,pos] = GetBC           (res,cad,pos,par);
			else if ( isMD           (cad,pos)                                                )[res,pos] = GetMD           (res,cad,pos,par);
			else if ( isDefine       (cad,pos) || isGDefine  (cad,pos)                        )[res,pos] = Transpiler_DEF  (res,cad,pos,par);
			else if ( isFunctionA    (cad,pos) || isFunctionS(cad,pos) || isFunction(cad,pos) ) res+= cad[pos++], [fun,pos] = GetProcFunction(fun,cad,pos,par);
			else if ( isArrowP       (cad,pos)                                                ) res+= cad[pos++], [fun,pos] = GetProcFunction(fun,cad,pos,par);
			else if ( isArrowN       (cad,pos)                                                ) res+= cad[pos++], [fun,pos] = GetProcArrowN  (fun,cad,pos,par);
			else if ( isVaIgual      (cad,pos)                                                ) res+= cad[pos++], [vas,pos] = GetVaIgual     (vas,cad,pos,par);
			else                                                                              res+= cad[pos];
		}

		if ( !par.yaDestructor ) con = GetDestructor(con,'',0,{})[0];

		delete par.yaView;

		return[`
        	/*Creacion*/
        	constructor( props={} ) {
        		super(props);

        		this.__parent__ = this.__class__ || '';
        		this.__class__  = '${par.bjClassName}';
        	 }

        	/*Metodos - Internos*/
        	__render__( html='', idsView=[] ) {
        		/*Variables*/
        		const gthis = this;
        		const op    = this.__properties__;
        		const va    = this.__variables__;
        		const co    = this.__components__;

        		this.className+= ' ${par.bjClassName}';

        		${bef}

        		/*Variables*/
        		${vas}

        		/*Herencia*/
        		super.__styles__ &&
        		super.__styles__('${sty}');
        		super.__render__ &&
        		super.__render__(\`
        			${vie}
        			\${html}
        		\`,
        		idsView.concat( [${(par.idsView||{}).sReduce((r,v,k)=>{if(r!='')r+=','; return r+`'${k}'`}, '')}]) );

        		${fun}
        		${des}
        		${res}
        		${con}
        		${GetEvents('',cad,pos,par)}
        	 }
		`.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetClass       ( res, cad, pos, par ) {
		let bod;
		let nam           = GetClassName   (cad,pos,par);
		let pad           = GetClassParent (cad,pos,par); pad=='' && (pad='sObject');
		let ver           = GetClassVersion(cad,pos,par);
		par.bjHtmlTagName = ClassToTag     (nam);
		[bod,pos]         = GetClassBody   ( cad,pos,Object.assign(par, {bjClassName:nam, bjTagName:par.bjHtmlTagName, bjClassParent:pad}) );

		pad!='sObject' && (par.__classDependencies__[pad]={type:'extends', version_dependencie:ver});
		pad && ver && (pad+= '_' + ver.replace(/[\.-]/g, '_'));

		return[res+
			`
            if ( !window.${nam} ) {
            /*@*/window.${nam} = class extends ${pad} {
            	${bod}
            }
            }
            !window.customElements.get('${par.bjHtmlTagName}') && window.customElements.define('${par.bjHtmlTagName}', window.${nam});
			`.replace(/^( *)/gm, ''),
			pos
		];
	 }

	function GetTagDependencies( key, dependencie ) {
		return `
            Exec('${key}', {noExec:true, ${dependencie.version_dependencie ? `version:'${dependencie.version_dependencie}'` : ''}});
		`.replace(/^( *)/gm, '');
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		if ( !par.__classDependencies__ ) par.__classDependencies__ = {};
		const len = cad.length
		let   ret = '', ini = '', com = false, reu = false;
		let   inc = par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let   ite = par.includeTemp                               ; delete par.includeTemp;
		let   rep = par.replaceThis                               ; delete par.replaceThis;
		let   tid = par.typeDefinition                            ; delete par.typeDefinition;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+= cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)          )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)          )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'                 )[ret,pos] = GetRegExp (ret,cad,pos);
			else if ( cad[pos]=='{'                 ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0     ) break;
			else if ( !com && cad[pos]==';'         ) ret+= cad[pos], com = true;
			else if ( !reu && isReturn(cad,pos)     ) ret+= cad[pos], reu = true;
			else if (  tid && isVersionDef(cad,pos) )[ret,pos] = GetVersionDef(ret,cad,pos,par);
			else if ( isView (cad,pos)              )[ret,pos] = GetView      (ret,cad,pos,par);
			else if ( isBH   (cad,pos)              )[ret,pos] = GetBH        (ret,cad,pos,par);
			else if ( isBC   (cad,pos)              )[ret,pos] = GetBC        (ret,cad,pos,par);
			else if ( isStyle(cad,pos)              )[ret,pos] = GetStyle     (ret,cad,pos,par);
			else if ( isThis (cad,pos) && rep       )[ret,pos] = GetThis      (ret,cad,pos,par);
			else                                      ret+= cad[pos];
		}

		ret = Compile(ret,par);

		if ( ite ) {
			if      ( !reu && com ) ret = "(function(){return "+ ret +"})()";
			else if (  reu        ) ret = "(function(){"       + ret +"})()";

			ret='${'+ ret +'}';
		}
		else if ( inc ) ret = ini+ret+cad[pos];

		return[res+ret, pos];
	 }
	function Compile( cad, par           ) {
		let dep = null;

		if ( !par.__classDependencies__     ) par.__classDependencies__ = {};
		if ( par.__clearLevelDependencies__ ) dep = { __classDependencies__:{} };

		cad = Transpiler_DEF (cad,par);
		cad = Transpiler_INCF(cad,par);
		cad = Transpiler_INCC(cad,par);
		cad = Transpiler_INCU(cad,par);
		cad = Transpiler_NEO (cad,par);

		const len = cad.length;
		let   res = '', pos = 0;

		for ( ;pos<len; pos++ ) {
			if      ( isCompiledClass(cad,pos) )[res,pos] = GetCompiledClass(res,cad,pos,par);
			else if ( isCommentJS    (cad,pos) )[res,pos] = GetComment      (res,cad,pos,par);
			else if ( isStringJS     (cad,pos) )[res,pos] = GetString       (res,cad,pos,par);
			else if ( isClass        (cad,pos) )[res,pos] = GetClass        (res,cad,pos,par);
			else if ( isDefif        (cad,pos) )[res,pos] = GetDefif        (res,cad,pos,par);
			else if ( isBH           (cad,pos) )[res,pos] = GetBH           (res,cad,pos,par);
			else if ( isBC           (cad,pos) )[res,pos] = GetBC           (res,cad,pos,par);
			else if ( isVarResJSON   (cad,pos) )[res,pos] = GetVarResJSON   (res,cad,pos,par);
			else if ( cad[pos]=='{'            )[res,pos] = Transpiler_BJS  (res,cad,pos,Object.assign({}, par, {includeTemp:0}, dep));
			else                                 res+= cad[pos];
		}

		res            = Transpiler_JS(res, par);
		par.hashScript = Hash(res);

		if ( !dep ) {
			par.__classDependencies__.sForEach((v,k)=>{
				if ( v.type=='tag' ) {
					res = GetTagDependencies(k, v) + res;
				}
			});
		}

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };
global.Transpiler_BJN = ( result, data, position, params ) => {
	/*Variables*/
	const TYPE_FUNC = 1;

	/*Is*/
	function isView       ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='-' &&
				cad[pos+1]=='v' &&
				cad[pos+2]=='i' &&
				cad[pos+3]=='e' &&
				cad[pos+4]=='w' &&
				isSpecial(cad,pos+5) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+=5;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isStyle      ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='-' &&
				cad[pos+1]=='s' &&
				cad[pos+2]=='t' &&
				cad[pos+3]=='y' &&
				cad[pos+4]=='l' &&
				cad[pos+5]=='e' &&
				isSpecial(cad,pos+6) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 6;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isBH         ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='b' &&
				cad[pos+1]=='h' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 2;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isBC         ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='b' &&
				cad[pos+1]=='c' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 2;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isMD         ( cad, pos ) {
		if (
			!(
				cad[pos  ]=='m' &&
				cad[pos+1]=='d' &&
				isSpecial(cad,pos+2) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 2;

		for (const len=cad.length; pos<len && isSpaces(cad,pos); pos++);

		return cad[pos]=='{';
	 }
	function isClass      ( cad, pos ) {
		if (
			cad[pos  ]=='c' &&
			cad[pos+1]=='l' &&
			cad[pos+2]=='a' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='s' &&
			isSpacesTabs(cad,pos+5) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		) {
			const len = cad.length;

			for (pos+=5; pos<len && isSpaces(cad,pos); pos++);

			return cad[pos]=='-' || cad[pos]=='_' || isLetter(cad,pos);
		}

		return false;
	 }
	function isFunction   ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpaces(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		for ( pos+=8; pos<len && isSpaces(cad,pos); pos++ );

		return isLetter(cad,pos) || cad[pos]=='_';
	 }
	function isFunctionS  ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='a' &&
				cad[pos+1]=='s' &&
				cad[pos+2]=='y' &&
				cad[pos+3]=='n' &&
				cad[pos+4]=='c' &&
				isSpaces(cad,pos+5) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		for ( pos+=5; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpecial(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		for ( pos+=8; pos<len && isSpaces(cad,pos); pos++ );

		return isLetter(cad,pos) || cad[pos]=='_';
	 }
	function isFunctionA  ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if ( cad[pos]=='=' || (isSpecial(cad,pos) && !isSpaces(cad,pos) && cad[pos]!='.') ) break;
		}

		if ( cad[pos]!='=' ) return false;

		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );

		if (
			!(
				cad[pos  ]=='f' &&
				cad[pos+1]=='u' &&
				cad[pos+2]=='n' &&
				cad[pos+3]=='c' &&
				cad[pos+4]=='t' &&
				cad[pos+5]=='i' &&
				cad[pos+6]=='o' &&
				cad[pos+7]=='n' &&
				isSpecial(cad,pos+8) &&
				(isSpecial(cad,pos-1)||pos-1<0)
			)
		) return false;

		pos+= 8;

		return cad[pos]=='(';
	 }
	function isArrowP     ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)                                        ) pos = GetComment('',cad,pos,{})[1];
			else if ( isSpecial  (cad,pos) && !isSpaces(cad,pos) && cad[pos]!='.' ) break;
		}

		if ( cad[pos]!='=' ) return false;
		for ( pos++; pos<len && isSpacesTabs(cad,pos); pos++ );

		if ( cad[pos]=='(' ) {
			for ( let key=-1; pos<len; pos++ ) {
				if      ( cad[pos]=='('             ) key++;
				else if ( cad[pos]==')' && key--==0 ){pos++; break}
				else if ( isCommentJS(cad,pos)      ) pos = GetComment('',cad,pos,{})[1];
				else if ( isStringJS (cad,pos)      ) pos = GetString ('',cad,pos,{})[1];
			}
		}
		else for ( ;pos<len && (isLetter(cad,pos) || isNumber(cad,pos) || isSpacesTabs(cad,pos) || cad[pos]=='_'); pos++ );

		if ( cad[pos]!='=' || cad[pos+1]!='>' ) return false;

		pos+= 2;

		for ( ;pos<len && isSpaces(cad,pos); pos++ );

		return cad[pos]=='{';
	 }
	function isArrowN     ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( pos++; pos<len; pos++ ) {
			if      ( isSpacesTabs(cad,pos) );
			else if ( isCommentJS (cad,pos) ) pos = GetComment('',cad,pos,{})[1];
			else break;
		}

		for ( ;pos<len && cad[pos]!='\n' && cad[pos]!='='; pos++ );

		if ( cad[pos]!='=' ) return false;

		for ( pos++; pos<len; pos++ ) {
			if ( cad[pos]=='(' ) {
				let key=0;
				for (pos++; pos<len; pos++) {
					if      ( cad[pos]=='('             ) key++;
					else if ( cad[pos]==')' && key--==0 ){pos++; break}
				}
			}
			else if ( cad[pos]=='\n' || cad[pos]=='=' ) break;
		}

		return cad[pos]=='=' && cad[pos+1]=='>';
	 }
	function isVaIgual    ( cad, pos ) {
		if ( !isSpecial(cad,pos) ) return false;

		const len = cad.length;

		for ( ;pos<len && isSpaces(cad,pos); pos++ );

		if ( cad[pos]!='v' || cad[pos+1]!='a' || cad[pos+2]!='.' ) return false;

		for ( ;pos<len && (!isSpecial(cad,pos) || isSpaces(cad,pos) || cad[pos]=='.'); pos++ );

		return cad[pos]=='=';
	 }
	function isConstructor( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='c' &&
			cad[pos+2 ]=='o' &&
			cad[pos+3 ]=='n' &&
			cad[pos+4 ]=='s' &&
			cad[pos+5 ]=='t' &&
			cad[pos+6 ]=='r' &&
			cad[pos+7 ]=='u' &&
			cad[pos+8 ]=='c' &&
			cad[pos+9 ]=='t' &&
			cad[pos+10]=='o' &&
			cad[pos+11]=='r' &&
			isSpecial(cad,pos+12) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDestructor ( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='d' &&
			cad[pos+2 ]=='e' &&
			cad[pos+3 ]=='s' &&
			cad[pos+4 ]=='t' &&
			cad[pos+5 ]=='r' &&
			cad[pos+6 ]=='u' &&
			cad[pos+7 ]=='c' &&
			cad[pos+8 ]=='t' &&
			cad[pos+9 ]=='o' &&
			cad[pos+10]=='r' &&
			isSpecial(cad,pos+11) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDefinition ( cad, pos ) {
		return(
			cad[pos   ]=='-' &&
			cad[pos+1 ]=='d' &&
			cad[pos+2 ]=='e' &&
			cad[pos+3 ]=='f' &&
			cad[pos+4 ]=='i' &&
			cad[pos+5 ]=='n' &&
			cad[pos+6 ]=='i' &&
			cad[pos+7 ]=='t' &&
			cad[pos+8 ]=='i' &&
			cad[pos+9 ]=='o' &&
			cad[pos+10]=='n' &&
			isSpecial(cad,pos+12) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isDefif      ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='d' &&
			cad[pos+2]=='e' &&
			cad[pos+3]=='f' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='f' &&
			cad[pos+6]==' '
		);
	 }
	function isElseif     ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='l' &&
			cad[pos+3]=='s' &&
			cad[pos+4]=='e' &&
			cad[pos+5]=='i' &&
			cad[pos+6]=='f' &&
			(isSpecial(cad,pos+7)||pos+7>=cad.length)
		);
	 }
	function isEndif      ( cad, pos ) {
		return(
			cad[pos  ]=='#' &&
			cad[pos+1]=='e' &&
			cad[pos+2]=='n' &&
			cad[pos+3]=='d' &&
			cad[pos+4]=='i' &&
			cad[pos+5]=='f' &&
			(isSpecial(cad,pos+6)||pos+6>=cad.length)
		);
	 }
	function isThis       ( cad, pos ) {
		return(
			cad[pos  ]=='t' &&
			cad[pos+1]=='h' &&
			cad[pos+2]=='i' &&
			cad[pos+3]=='s' &&
			isSpecial(cad,pos+4) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }
	function isVarResJSON ( cad, pos ) {
		return (
			cad[pos]=='-' &&
			(
				(cad[pos+1]=='P' && cad[pos+2]=='r' && cad[pos+3]=='o' && cad[pos+4]=='j' && cad[pos+5]=='e' && cad[pos+6]=='c' && cad[pos+7]=='t') ||
				(cad[pos+1]=='V' && cad[pos+2]=='a' && cad[pos+3]=='r' && cad[pos+4]=='i' && cad[pos+5]=='a' && cad[pos+6]=='b' && cad[pos+7]=='l' && cad[pos+8]=='e' && cad[pos+9]=='s') ||
				(cad[pos+1]=='S' && cad[pos+2]=='e' && cad[pos+3]=='r' && cad[pos+4]=='v' && cad[pos+5]=='e' && cad[pos+6]=='r') ||
				(cad[pos+1]=='P' && cad[pos+2]=='a' && cad[pos+3]=='t' && cad[pos+4]=='h' && cad[pos+5]=='s') ||
				(cad[pos+1]=='C' && cad[pos+2]=='o' && cad[pos+3]=='m' && cad[pos+4]=='p' && cad[pos+5]=='i' && cad[pos+6]=='l' && cad[pos+7]=='e' && cad[pos+8]=='r') ||
				(cad[pos+1]=='R' && cad[pos+2]=='e' && cad[pos+3]=='s' && cad[pos+4]=='o' && cad[pos+5]=='u' && cad[pos+6]=='r' && cad[pos+7]=='c' && cad[pos+8]=='e' && cad[pos+9]=='s') ||
				(cad[pos+1]=='A' && cad[pos+2]=='p' && cad[pos+3]=='i')
			) &&
			(isSpecial(cad,pos-1)||pos-1<0)
		);
	 }

	/*Funciones - Proc*/
	function GetProcFunction( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( res+=cad[pos++]; pos<len; pos++ ) {
			if  ( cad[pos]=='{'     ) break;
			else                      res+= cad[pos];
		}

		def         = par.defines;
		par.defines = {};
		[ret,pos]   = Transpiler_BJN('',cad,pos,par);
		par.defines = def;

		return[res+ret+'\n',pos];
	 }
	function GetProcArrowN  ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && (cad[pos]!='=' || cad[pos+1]!='>'); res+=cad[pos++] );

		res+= cad[pos++];//=
		res+= cad[pos++];//>

		for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );

		for ( let cor=0, key=0, par=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos] = GetRegExp (ret,cad,pos,par);
			else if ( cad[pos]=='('             ) ret+= cad[pos], par++;
			else if ( cad[pos]==')' && par--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='['             ) ret+= cad[pos], cor++;
			else if ( cad[pos]==']' && cor--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='{'             ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) ret+= cad[pos];
			else if (
				(cad[pos]=='\n' || cad[pos]==';')
				&&
				cor==0 && key==0 && par==0
			){
				ret+= cad[pos];
				break;
			}
			else ret+= cad[pos];
		}

		def         = par.defines;
		par.defines = {};
		ret         = Transpiler_BJ(ret,par);
		par.defines = def;

		return[res+ret+'\n',pos];
	 }
	function GetVaIgual     ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='='; res+=cad[pos++] );

		res+= cad[pos++];

		for ( ;pos<len && isSpaces(cad,pos); res+=cad[pos++] );

		for ( let cor=0, key=0, par=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos] = GetRegExp (ret,cad,pos,par);
			else if ( cad[pos]=='('             ) ret+= cad[pos], par++;
			else if ( cad[pos]==')' && par--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='['             ) ret+= cad[pos], cor++;
			else if ( cad[pos]==']' && cor--==0 ) ret+= cad[pos];
			else if ( cad[pos]=='{'             ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) ret+= cad[pos];
			else if (
				(cad[pos]=='\n' || cad[pos]==';')
				&&
				cor==0 && key==0 && par==0
			){
				ret+= cad[pos];
				break;
			}
			else ret+= cad[pos];
		}

		return[res+ret+(ret[ret.length-1]!=';'?';':'')+'\n',pos];
	 }
	function GetStyle       ( res, cad, pos, par ) {
		let ret       = '';
		let zip       = par.zip;
		par.zip       = !par.noZipDebug;
		[ret,pos]     = Transpiler_BC(ret,cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip       = zip;
		par.hashStyle = Hash(ret);
		par.codeStyle = ret;

		if ( par.isNode ) return[res,pos];

		return[res+`
        	this.style   = () => {
        		fs.style();

        		Eval({ id:'style_${par.bjClassName}', data:\`${ret}\`, type:VALUE_CSS });

        		return gthis;
        	 }`.replace(/^( *)/gm, '')
		, pos];
	 }
	function GetBC          ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = Transpiler_BC(ret,cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetView        ( res, cad, pos, par ) {
		let ret      = '';
		let zip      = par.zip;
		par.zip      = !par.noZipDebug;
		[ret,pos]    = Transpiler_BH('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip      = zip;
		par.yaView   = true;
		par.hashView = Hash(ret);
		par.codeView = ret;

		if ( par.isNode ) return[res,pos];

		return[res+`
        	this.html    = ( _html='', _class='' ) => fs.html ( _html + \`${ret}\`, '${par.bjClassName} '+_class );
        	this.idsView = (                     ) => {
        		if ( fs.idsView ) fs.idsView();

        		${par.idsView.sReduce((r,v,k)=>r+`\n\t\tco[\`${k}\`]=gthis.fin(\`#\${gthis.id}-${k}\`);`, '')}

        		return this;
        	 };
            `.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetBH          ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = Transpiler_BH('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetMD          ( res, cad, pos, par ) {
		let ret   = '';
		let zip   = par.zip;
		par.zip   = !par.noZipDebug;
		[ret,pos] = Transpiler_MD('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.zip   = zip;

		return[res+'`'+ ret +'`',pos];
	 }
	function GetConstructor ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		def               = par.defines;
		par.defines       = {};
		[ret,pos]         = Transpiler_BJN('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines       = def;
		par.yaConstructor = true;

		return[res+`
        	this.constructor = ( _props, noexec ) => {
        		fs.constructor(_props, 1);

        		${
        		par.isNode?
        		''
        		:
        		`if ( !noexec && _props && _props.eve ) {
        			gthis.eve(_props.eve);
        			delete _props.eve;
        		}`
        		}


        		${par.isNode?'':'this.idsView();'}

        		${ret}

        		!noexec && gthis.exe(_props).tri('onConstructor', gthis);

        		return gthis;
        	 }`.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetDestructor  ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		def              = par.defines;
		par.defines      = {};
		[ret,pos]        = Transpiler_BJN('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines      = def;
		par.yaDestructor = true;

		return[res+`
        	this.destructor  = (        ) => {
        		for( let x in co )
        			if( tipode(co[x])==VALUE_OBJECT && typeof co[x].destructor==VALUE_FUNCTION )
        				co[x].destructor();

        		${ret}

        		fs.destructor();
        	 };
            `.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetDefinition  ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		const def   = par.defines;
		par.defines = {};
		[ret,pos]   = Transpiler_BJ('',cad,pos,Object.assign(par,{includeKeys:0}));
		par.defines = def;

		eval(`def={${ret}}`);

		par.definition = def;

		return[res,pos];
	 }
	function GetDefif       ( res, cad, pos, par ) {
		const len = cad.length;
		let   nco = '', col = '', elc = '';

		for (      ; pos<len && !isSpaces(cad,pos); pos++           );
		for ( pos++; pos<len && !isSpaces(cad,pos); nco+=cad[pos++] );

		for( col=''; pos<len && !isElseif(cad,pos) && !isEndif(cad,pos); col+=cad[pos++] );

		if ( isElseif(cad,pos) ) {
			for(;pos<len && cad[pos]!='\n'; pos++);
			for( elc='', pos++; pos<len && !isEndif(cad,pos); elc+=cad[pos++] );
		}

		for ( ;pos<len && !isEndif(cad,pos); pos++ );

		pos+= 6;

		if ( nco==(par.environment||'debug') ) col = Transpiler_BJ(col, par) + (cad[pos]||'');
		else                                   col = Transpiler_BJ(elc, par) + (cad[pos]||'');

		return[res+col,pos];
	 }
	function GetThis        ( res, cad, pos, par ) {
		const len = cad.length;

		for (;pos<len && !isSpecial(cad,pos); pos++);

		return[res+'__x__',--pos];
	 }
	function GetVarResJSON  ( res, cad, pos, par ) {
		const len = cad.length;
		let   ret = '';

		for (pos++; pos<len && !isSpecial(cad,pos); ret+=cad[pos++]);

		res+= ret;

		return[res,--pos];
	 }

	/*Funciones - GET*/
	function GetClassName  ( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for ( pos+=5; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos); res+=cad[pos++] );

		par.sClass = '.'+res;

		return res;
	 }
	function GetClassParent( cad, pos, par      ) {
		const len = cad.length;
		let   res = '';

		for ( ;pos<len && cad[pos]!=':' && cad[pos]!='{'; pos++ );
		if ( cad[pos]!=':' ) return'';
		for ( pos++; pos<len && isSpaces(cad,pos); pos++ );
		for ( ;pos<len && !isSpecial(cad,pos); res+=cad[pos++] );

		return res;
	 }
	function GetClassBody  ( cad, pos, par      ) {
		const len = cad.length;
		let   res = '', con = '', fun = '', vas = '', key = 0;

		for ( ;pos<len && cad[pos]!='{'; pos++ );

		// Proc
		for ( pos++; pos<len; pos++ ) {
			if      ( cad[pos]=='{'                                                         ) res+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0                                             ) break;
			else if ( isCommentJS  (cad,pos)                                                )[res,pos] = GetComment    (res,cad,pos,par);
			else if ( isStringJS   (cad,pos)                                                )[res,pos] = GetString     (res,cad,pos,par);
			else if ( isStyle      (cad,pos)                                                )[con,pos] = GetStyle      (con,cad,pos,par);
			else if ( isView       (cad,pos)                                                )[con,pos] = GetView       (con,cad,pos,par);
			else if ( isConstructor(cad,pos)                                                )[con,pos] = GetConstructor(con,cad,pos,par);
			else if ( isDestructor (cad,pos)                                                )[con,pos] = GetDestructor (con,cad,pos,par);
			else if ( isDefinition (cad,pos)                                                )[con,pos] = GetDefinition (con,cad,pos,par);
			else if ( isBH         (cad,pos)                                                )[res,pos] = GetBH         (res,cad,pos,par);
			else if ( isBC         (cad,pos)                                                )[res,pos] = GetBC         (res,cad,pos,par);
			else if ( isMD         (cad,pos)                                                )[res,pos] = GetMD         (res,cad,pos,par);
			else if ( isDefine     (cad,pos) || isGDefine  (cad,pos)                        )[res,pos] = Transpiler_DEF(res,cad,pos,par);
			else if ( isFunctionA  (cad,pos) || isFunctionS(cad,pos) || isFunction(cad,pos) ) res+= cad[pos++], [fun,pos] = GetProcFunction(fun,cad,pos,par);
			else if ( isArrowP     (cad,pos)                                                ) res+= cad[pos++], [fun,pos] = GetProcFunction(fun,cad,pos,par);
			else if ( isArrowN     (cad,pos)                                                ) res+= cad[pos++], [fun,pos] = GetProcArrowN  (fun,cad,pos,par);
			else if ( isVaIgual    (cad,pos)                                                ) res+= cad[pos++], [vas,pos] = GetVaIgual     (vas,cad,pos,par);
			else                                                                              res+= cad[pos];
		}

		if ( !par.yaView        ) con = GetView       (con,'',0,{bjClassName:par.bjClassName, isNode:par.isNode})[0];
		if ( !par.yaConstructor ) con = GetConstructor(con,'',0,{isNode:par.isNode})[0];
		if ( !par.yaDestructor  ) con = GetDestructor (con,'',0,{})[0];

		delete par.yaView;

		return[`
        	/*Herencia*/
        	this.__props__=props;
        	this.${par.bjClassParent}=${par.bjClassParent};
        	this.${par.bjClassParent}(0,props);

        	/*Variables*/
        	const gthis = this;
        	const fs    = this.sReduce((r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {});
        	const op    = props||_ops||{};
        	const va    = {};
        	const co    = {};
        	gthis.op    = op;
        	gthis.va    = va;
        	gthis.co    = co;

        	/*Variables - va*/
        	${vas}

        	/*Creacion*/
        	${con}

        	/*Funciones*/
        	${fun}

        	/*Inicio*/
        	this.pClass = this.sClass;
        	this.sClass = '${par.bjClassName}';

        	props!=0 && this.constructor(props);

        	/*Body*/
        	${res}
            `.replace(/^( *)/gm, '')
		,pos];
	 }
	function GetClass      ( res, cad, pos, par ) {
		let bod;
		let win   = par.isNode?'global':'window';
		let nam   = GetClassName  (cad,pos,par);
		let pad   = GetClassParent(cad,pos,par); pad=='' && (pad=par.isNode?'sObject':'sDomVisual');
		[bod,pos] = GetClassBody  ( cad,pos,Object.assign(par, {bjClassName:nam, bjClassParent:pad}) );

		return[
		`(function(){
            ${res}
            ${win}.${nam} = function( props, _ops ) {
            	${bod}
            };${win}.${nam}.prototype=new ${pad}(0);
         })();`.replace(/^( *)/gm, '')
		, pos];
	 }

	/*Compile*/
	function Escaped( res, cad, pos, par ) {
		const len = cad.length
		let   ret = '', ini = '', com = false, reu = false;
		let   inc = par.includeKeys===undefined || par.includeKeys; delete par.includeKeys;
		let   ite = par.includeTemp                               ; delete par.includeTemp;
		let   rep = par.replaceThis                               ; delete par.replaceThis;

		for ( ;pos<len && cad[pos]!='{'; ini+=cad[pos++] );

		ini+= cad[pos++];

		for ( let key=0; pos<len; pos++ ) {
			if      ( isCommentJS(cad,pos)      )[ret,pos] = GetComment(ret,cad,pos,par);
			else if ( isStringJS (cad,pos)      )[ret,pos] = GetString (ret,cad,pos,par);
			else if ( cad[pos]=='/'             )[ret,pos] = GetRegExp (ret,cad,pos);
			else if ( cad[pos]=='{'             ) ret+= cad[pos], key++;
			else if ( cad[pos]=='}' && key--==0 ) break;
			else if ( !com && cad[pos]==';'     ) ret+= cad[pos], com = true;
			else if ( !reu && isReturn(cad,pos) ) ret+= cad[pos], reu = true;
			else if ( isView (cad,pos)          )[ret,pos] = GetView (ret,cad,pos,par);
			else if ( isBH   (cad,pos)          )[ret,pos] = GetBH   (ret,cad,pos,par);
			else if ( isBC   (cad,pos)          )[ret,pos] = GetBC   (ret,cad,pos,par);
			else if ( isStyle(cad,pos)          )[ret,pos] = GetStyle(ret,cad,pos,par);
			else if ( isThis (cad,pos) && rep   )[ret,pos] = GetThis (ret,cad,pos,par);
			else                                  ret+= cad[pos];
		}

		ret = Compile(ret,par);

		if ( ite ) {
			if      ( !reu && com ) ret = "(function(){return "+ ret +"})()";
			else if (  reu        ) ret = "(function(){"       + ret +"})()";

			ret='${'+ ret +'}';
		}
		else if ( inc ) ret = ini+ret+cad[pos];

		return[res+ret, pos];
	 }
	function Compile( cad, par           ) {
		cad = Transpiler_DEF (cad,par);
		cad = Transpiler_INCF(cad,par);
		cad = Transpiler_INCC(cad,par);
		cad = Transpiler_INCU(cad,par);
		cad = Transpiler_NEO (cad,par);

		const len = cad.length;
		let   res = '', pos = 0;

		for ( ;pos<len; pos++ ) {
			if      ( isCommentJS (cad,pos) )[res,pos] = GetComment    (res,cad,pos,par);
			else if ( isStringJS  (cad,pos) )[res,pos] = GetString     (res,cad,pos,par);
			else if ( isClass     (cad,pos) )[res,pos] = GetClass      (res,cad,pos,par);
			else if ( isDefif     (cad,pos) )[res,pos] = GetDefif      (res,cad,pos,par);
			else if ( isBH        (cad,pos) )[res,pos] = GetBH         (res,cad,pos,par);
			else if ( isBC        (cad,pos) )[res,pos] = GetBC         (res,cad,pos,par);
			else if ( isVarResJSON(cad,pos) )[res,pos] = GetVarResJSON (res,cad,pos,par);
			else if ( cad[pos]=='{'         )[res,pos] = Transpiler_BJN(res,cad,pos,Object.assign(par,{includeTemp:0}));
			else                              res+= cad[pos];
		}

		res            = Transpiler_JS(res, par);
		par.hashScript = Hash(res);

		return res;
	 }

	/*Inicio*/
	if ( position!=undefined ) return Escaped(result,data,position,params);
	else                       return Compile(result,data);
 };

global.Transpiler_ALL = ( path, params, compileAsync ) => {
	let file        = ParsePath(path);
	file.lower_ext  = file.ext .toLowerCase();
	file.lower_path = file.path.toLowerCase();
	file.mime       = Mime(file.ext);

	if ( compileAsync ) {
		return new sProm((run,err)=>{
			try {file=Transpiler_ALL(path, params)}
			catch(e) {log.error(e)}

			Transpiler_INCA(file.data).err(err)
			.run(d=>{
				file.data = d;

				run(file);
			});

		});
	}

	if ( file.type===VALUE_FILE ) {
		file = Object.assign({}, params, file);

		switch (file.ext) {
			case '.svg' :
			case '.htm' :
			case '.html': file.original_data = FileRead(file.path); file.data = Transpiler_HTML(file.original_data, file); file.hash = Hash(file.data); break;
			case '.md'  : file.original_data = FileRead(file.path); file.data = Transpiler_MD  (file.original_data, file); file.hash = Hash(file.data); break;
			case '.css' : file.original_data = FileRead(file.path); file.data = Transpiler_CSS (file.original_data, file); file.hash = Hash(file.data); break;
			case '.json': file.original_data = FileRead(file.path); file.data = Transpiler_JSON(file.original_data, file); file.hash = Hash(file.data); break;
			case '.js'  : file.original_data = FileRead(file.path); file.data = Transpiler_JS  (file.original_data, file); file.hash = Hash(file.data); break;
			case '.bc'  : file.original_data = FileRead(file.path); file.data = Transpiler_BC  (file.original_data, file); file.hash = Hash(file.data); break;
			case '.bh'  : file.original_data = FileRead(file.path); file.data = Transpiler_BH  (file.original_data, file); file.hash = Hash(file.data); break;
			case '.bj'  : file.original_data = FileRead(file.path); file.data = Transpiler_BJS (file.original_data, file); file.hash = Hash(file.data); break;
			default     : file.data = FileRead(file.path,false); file.hash=Hash(file.data);
		}
	}

	return file
 };

/*Extenciones*/
Object.defineProperty(Object.prototype, 'sMap'    , {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return [];

	let res=[], ind=0;

	for ( let x in this )
		res.push(func.call(this, this[x], x, ind++));

	return res;
 }});
Object.defineProperty(Object.prototype, 'sReduce' , {enumerable:false, value:function( func, result ) {
	if ( typeof func!=VALUE_FUNCTION ) return result;

	let ret=result, ant=null, ind=0;

	for ( let x in this ) {
		if ( ret==null ) ret = this[x];
		else             ret = func.call(this, ret, this[x], x, ant, ind++);

		ant = this[x];
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sFilter' , {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return {};

	let ret={}, ind=0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++) ) {
			ret[x] = this[x];
		}
	}

	return ret;
 }});
Object.defineProperty(Object.prototype, 'sForEach', {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind = 0;

	for ( let x in this ) {
		if ( func.call(this, this[x], x, ind++)==false )
			break;
	}
 }});
Object.defineProperty(Object.prototype, 'sSome'   , {enumerable:false, value:function( func         ) {
	if ( typeof func!=VALUE_FUNCTION ) return;

	let ind=0, res=null;

	for ( let x in this ) {
		res = func.call(this, this[x], x, ind++);

		if ( res ) return res;
	}
 }});

String.prototype.cmd = function( all=true ) {
	return this
	.replace(/%r/g,  all?'\x1b[0m':'') // Reset

	.replace(/%cb/g, all?'\x1b[1m':'') // CBright
	.replace(/%cd/g, all?'\x1b[2m':'') // CDim
	.replace(/%cu/g, all?'\x1b[4m':'') // CUnderscore
	.replace(/%cl/g, all?'\x1b[5m':'') // CBlink
	.replace(/%cv/g, all?'\x1b[7m':'') // CReverse
	.replace(/%ch/g, all?'\x1b[8m':'') // CHidden

	.replace(/%fb/g, all?'\x1b[30m':'') // FgBlack
	.replace(/%fr/g, all?'\x1b[31m':'') // FgRed
	.replace(/%fg/g, all?'\x1b[32m':'') // FgGreen
	.replace(/%fy/g, all?'\x1b[33m':'') // FgYellow
	.replace(/%fu/g, all?'\x1b[34m':'') // FgBlue
	.replace(/%fm/g, all?'\x1b[35m':'') // FgMagenta
	.replace(/%fc/g, all?'\x1b[36m':'') // FgCyan
	.replace(/%fw/g, all?'\x1b[37m':'') // FgWhite

	.replace(/%bb/g, all?'\x1b[40m':'') // BgBlack
	.replace(/%br/g, all?'\x1b[41m':'') // BgRed
	.replace(/%bg/g, all?'\x1b[42m':'') // BgGreen
	.replace(/%by/g, all?'\x1b[43m':'') // BgYellow
	.replace(/%bu/g, all?'\x1b[44m':'') // BgBlue
	.replace(/%bm/g, all?'\x1b[45m':'') // BgMagenta
	.replace(/%bc/g, all?'\x1b[46m':'') // BgCyan
	.replace(/%bw/g, all?'\x1b[47m':'') // BgWhite
	;
 }
String.prototype.inf = function( title='' ) {
	let gui='';

	for(let x=title.cmd(false).length+2; x--; gui+='-');

	return `
		%bb%fy ${title} %r
		%fy${gui}%r
		${this}
	`.replace(/\t/g, '').cmd();
 }
String.prototype.war = function( title='' ) {
	let gui='';

	for(let x=title.cmd(false).length+2; x--; gui+='-');

	return `
		%fy%cv ${title} %r
		%fy${gui}%r
		${this}
	`.replace(/\t/g, '').cmd();
 }
String.prototype.err = function( title='' ) {
	let gui = '';

	for(let x=title.cmd(false).length+2; x--; gui+='-');

	return `
		%br%fw ${title} %r
		%fr${gui}%r
		${this}
	`.replace(/\t/g, '').cmd();
 }

/*Clases*/
global.sObject = function( props ) {
	/*Variables*/
	const co={};
	const va={bcEvents:[]};

	/*Creacion*/
	this.constructor = ( _props, noexec ) => {
		co.events=new (require('events').EventEmitter)();
		!noexec && this.exe(_props);

		return this;
	 };

	/*Metodos*/
	this.eve = ( events, idDelete ) => {
		if      ( events===false               ) {
			va.bcEvents.forEach(x=>co.events.removeListener(x.event, x.func));
			va.bcEvents=[];
		 }
		else if ( events===undefined           ) return va.bcEvents;
		else if ( typeof events===VALUE_STRING ) {
			if ( idDelete===false ) {
				for ( let x=va.bcEvents.length; x--; ) {
					if ( va.bcEvents[x].id===events ) {
						co.events.removeListener(va.bcEvents[x].event, va.bcEvents[x].func);
						va.bcEvents.splice(x, 1);
					}
				}
			 }
			else if ( typeof idDelete===VALUE_FUNCTION ) {
				va.bcEvents.push({ id:'', event:events, func:idDelete });
				co.events.on(events, idDelete);
			}
			else return va.bcEvents.reduce((r,v)=>{ x.id===events && r.push(v); return r; },[]);
		 }
		else if ( tipode(events)==VALUE_OBJECT ) {
			if ( idDelete===false ) {
				for ( let x in events ) {
					for ( let y=va.bcEvents.length; y--; ) {
						if ( va.bcEvents[y].event===x && va.bcEvents[y].func===events[x] ) {
							co.events.removeListener(va.bcEvents[y].event, va.bcEvents[y].func);
							va.bcEvents.splice(y, 1);
						}
					}
				}
			 }
			else {
				if ( !events.id || typeof events.id!=VALUE_STRING ) events.id='';

				for ( let x in events ) {
					if ( x!='id' ) {
						va.bcEvents.push({ id:events.id, event:x, func:events[x] });
						co.events.on(x, events[x]);
					}
				}
			 }
		 }

		return this;
	 };
	this.tri = ( event , ...extra ) => {
		typeof event==VALUE_STRING && event!='' && co.events.emit(event, ...extra);

		if ( typeof this[event]===VALUE_FUNCTION ) this[event](...extra);

		return this;
	 };
	this.exe = ( method           ) => {
		if ( tipode(method)==VALUE_OBJECT ) {
			for ( let x in method ) {
				if ( this[x]!=undefined && x[0]!='o' && x[1]!='n' ) {
					if ( typeof this[x]==VALUE_FUNCTION ) {
						if ( tipode(method[x])==VALUE_ARRAY ) this[x].apply(this, method[x]);
						else                                  this[x](method[x]);
					}
				}
				else if ( x[0]==='o' && x[1]==='n' && typeof method[x]===VALUE_FUNCTION ) this.eve(x, method[x]);
			}
		}

		return this;
	 };
	this.run = ( func             ) => {
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };
	this.err = ( func             ) => {
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };
	this.all = ( func             ) => {
		if ( typeof func!=VALUE_FUNCTION ) return this;

		func.call(this);

		return this;
	 };

	/*Inicio*/
	this.sClass='sObject';
	props && this.constructor(props);
 };

global.sLog      = function( props ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0);

	/*Variables*/
	const gthis=this;
	const fs   =this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const co   ={};
	const op   ={};

	/*Creacion*/
	this.constructor = ( _props, noexec ) => {
		fs.constructor(_props, 1);

		/*Asignaciones*/
		co.fs=require('fs');

		!noexec && gthis.exe(_props);

		return gthis;
	 };

	/*Metodos*/
	gthis.File   = ( file      ) => {
		if ( file===undefined ) return op.file;

		co.output=co.fs.createWriteStream(ParsePath(file).path);
		co.logger=new (require('console').Console)(co.output);
		op.file  =file;

		return gthis;
	 };
	gthis.info   = ( ...params ) => PrintLog(params, 'Info'  );
	gthis.load   = ( ...params ) => PrintLog(params, 'Load'  );
	gthis.save   = ( ...params ) => PrintLog(params, 'Save'  );
	gthis.stack  = ( ...params ) => PrintLog(params, `%r%fb%bg${ /(\s+at\s)([A-z.]+)/.exec(new Error().stack.split('\n')[2])[2] }%r%cd` );
	gthis.error  = ( ...params ) => PrintLog(params, '%fb%brError%r%cd' );
	gthis.delete = ( ...params ) => PrintLog(params, 'Delete');

	/*Funciones*/
	function PrintLog( params, tag ) {
		if ( params.length===0 ) {
			console.error();
			co.logger && co.logger.error();
		}
		else if ( params.length===1 ){
			let err=`%cd[PID:${process.pid} - ${ (new Date).toLocaleTimeString("es-MX") }] ${tag}:%r`;

			if ( typeof params[0]==VALUE_STRING ) console.error( Transpiler_CMD(err+' '+params[0]) );
			else                                  console.error(Transpiler_CMD(err), params[0]);

			co.logger && co.logger.error( Transpiler_CMD(err+' '+params[0], 1) );
		}
		else {
			console.error(...params);
			co.logger && co.logger.error(...params);
		}
	 }

	/*Inicio*/
	this.sClass='sLog';
	props!=0 && this.constructor(props);
 };sLog.prototype=new sObject(0);
global.sPrompt   = function( props ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0);

	/*Variables*/
	const gthis=this;
	const fs   =this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const va   ={};

	/*Metodos*/
	gthis.Message     = ( message='', rdef='' ) => {
		return new sProm(gthis, run=>{
			let ca1=`%fy?%r${message}`, ca2=`%cd(${rdef})%r `;


			if ( !va.readline ) va.readline=require('readline');
			if ( !va.stream   ) va.stream  =va.readline.createInterface({ input:process.stdin, output:process.stdout });

			va.stream.question((ca1+ca2).cmd(), answer=>{
				process.stdout.moveCursor(0, -1-parseInt((ca1+ca2).cmd(false).length/process.stdout.columns));
				process.stdout.cursorTo(0);
				process.stdout.clearScreenDown();

				if ( answer=='' ) answer=rdef;

				console.info(`${ca1} %fc${answer}%r`.cmd());
				run(answer);
			});
		});
	 };
	gthis.PlaySpinner = ( message=''          ) => {
		clearTimeout(va.timer);

		let spi='⣾⣽⣻⢿⡿⣟⣯⣷';
		let len=spi.length, pos=-1;
		let tic=()=>{
			process.stdout.moveCursor(0, 0);
			process.stdout.cursorTo(0);
			process.stdout.clearScreenDown();
			process.stdout.write( message.replace( /%spinner/g, spi[ (pos=++pos%len) ] ).cmd() );
		 };

		va.timer=setInterval(tic, 80);

		return gthis;
	 };
	gthis.StopSpinner = ( clear=false         ) => {
		clearInterval(va.timer);

		if ( clear!=false ) {
			process.stdout.moveCursor(0, 0);
			process.stdout.cursorTo(0);
			process.stdout.clearScreenDown();

			if ( typeof clear==VALUE_STRING ) process.stdout.write( clear.cmd() );
		}

		return gthis;
	 };

	/*Inicio*/
	this.sClass='sPrompt';
	props!=0 && this.constructor(props);
 };sPrompt.prototype=new sObject(0);
global.sCommands = function( props ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0);

	/*Variables*/
	const gthis  =this;
	const fs     =this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const va     ={};
	this.commands={};

	/*Metodos*/
	gthis.Commands = ( commands                   ) => {
		if ( commands===undefined ) return va.commands;

		va.commands=commands;

		tipode(commands)==VALUE_OBJECT && commands.sForEach(v=>this.Param(v));

		return gthis;
	 };
	gthis.Param    = ( command, desc, error, func ) => {
		if ( command==undefined ) return;

		let help='', unique=false;

		if ( tipode(command)==VALUE_OBJECT ) {
			desc    = typeof command.description==VALUE_FUNCTION ? command.description() : command.description ||'';
			error   = typeof command.error      ==VALUE_FUNCTION ? command.error      () : command.error       ||'';
			help    = typeof command.help       ==VALUE_FUNCTION ? command.help       () : command.help        ||'';
			func    = command.func;
			unique  = command.unique||false;
			command = command.command||'';
		}

		if ( typeof error==VALUE_FUNCTION ) func =error;
		if ( typeof error!=VALUE_STRING   ) error='';
		if ( typeof desc ==VALUE_FUNCTION ) func =desc;
		if ( typeof desc !=VALUE_STRING   ) desc ='';
		if ( func==='GetHelp'             ) func = GetHelp;

		let all      = command.split(' ').filter(x=>x!='');
		let params   = all.filter(x=>x[0]=='<');
		let commands = all.filter(x=>x[0]!='<');

		commands.forEach(x=>this.commands[x]={params,desc,error,func,help,unique});

		return this;
	 };
	gthis.Parse    = ( commandLine                ) => {
		if ( typeof commandLine ==VALUE_STRING ) commandLine=ParseString(commandLine);
		if ( tipode(commandLine)!=VALUE_ARRAY  ) commandLine=process.argv;

		va.scommandLine = commandLine.join(' ');
		commandLine     = commandLine.slice();

		let com, res={}, key;

		for ( let x=0; x<commandLine.length; x++ ) {
			if ( (com=this.commands[commandLine[x]]) && (key=commandLine[x]) ) {
				if ( com.params.length ) {
					if (!res[key]) res[key] = {accept:0, ok:com.params.length, params:[], func:com.func, error:com.error, unique:com.unique};

					com.params.forEach(par=>{
						let trq = par.indexOf('?>')==par.length-2 ? VALUE_QUERY : (par.indexOf('$>')==par.length-2 ? VALUE_VALID : true);

						switch (par.replace(/[<?$>]/g, '')) {
							case 'path'   : ParseParam(commandLine, x, res[key], trq, VALUE_PATH   ); break;
							case 'dir'    : ParseParam(commandLine, x, res[key], trq, VALUE_FOLDER ); break;
							case 'string' : ParseParam(commandLine, x, res[key], trq, VALUE_STRING ); break;
							case 'number' : ParseParam(commandLine, x, res[key], trq, VALUE_NUMBER ); break;
							case 'command': ParseParam(commandLine, x, res[key], trq, VALUE_SPECIAL); break;
						}
					});
				}
				else if ( !res[commandLine[x]] ) res[commandLine[x]] = {func:com.func};
			}
		}

		ExecResult(res);

		return this;
	 };
	gthis.Default  = ( func                       ) => {
		if ( typeof func==VALUE_FUNCTION ) va.default = func.bind(this);
		else                               va.default = null;

		return this;
	 };

	/*Funciones*/
	function ParseString ( cad                                        ) {
		let pos=0, len=cad.length, res=[], tem='';

		for ( ;pos<len; pos++ ) {
			if      ( cad[pos]=='\\'                 ) tem+=cad[++pos];//, tem+=cad[pos];
			else if ( cad[pos]==' ' && tem!=''       ) res.push(tem), tem='';
			else if ( cad[pos]=='"' || cad[pos]=="'" ){
				let com=cad[pos];
				for ( pos++; pos<len; pos++ ) {
					if      ( cad[pos]==com  ) break;
					else if ( cad[pos]=='\\' ) tem+=cad[pos++], tem+=cad[pos];
					else                       tem+=cad[pos];
				}
			}
			else tem+=cad[pos];
		}

		if ( tem!='' ) res.push(tem);

		return res;
	 };
	function ParseCommand( command                                    ) {
		if ( typeof command!=VALUE_STRING || command=='' ) return '';

		return command.split(' ').filter(x=>x!='');
	 };
	function ParseParam  ( commandLine, index, result, required, type ) {
		let ace=false, res, ind=index+1;

		for (;ind<commandLine.length; ind++) {
			const fil=ParsePath(commandLine[ind]);

			if (
				( type==VALUE_PATH    && fil.type                             ) ||
				( type==VALUE_FILE    && fil.type==VALUE_FILE                 ) ||
				( type==VALUE_FOLDER  && fil.type==VALUE_FOLDER               ) ||
				( type==VALUE_NUMBER  && !isNaN(parseFloat(commandLine[ind])) ) ||
				( type==VALUE_STRING  && !gthis.commands[commandLine[ind]]    ) ||
				( type==VALUE_SPECIAL &&  gthis.commands[commandLine[ind]]    )
			) {
				type==VALUE_NUMBER && (commandLine[ind]=parseFloat(commandLine[ind]));
				ace = true;
				result.accept++;
				result.params.push(commandLine[ind]);
				commandLine.splice(ind,1);
				break;
			}
			else if ( commandLine[ind][0]=='-' ) break;
		}

		required==VALUE_VALID && !ace && index+1==ind && ++result.accept && result.params.push(null);
		required==VALUE_QUERY && !ace                 && ++result.accept && result.params.push(null);
	 };
	function ExecResult  ( result                                     ) {
		let err = result.sFilter(x=>x.accept!=x.ok);

		if ( Object.keys(err).length ) console.error(err.sReduce((r,v)=>r+`  %fr*%r ${v.error}`,'').err('Error'));
		else {
			let uni = result.sSome((x,k)=>x.unique&&(x.key=k)&&x);
			uni && (result={}) && (result[uni.key]=uni);

			result.sForEach(x=>typeof x.func==VALUE_FUNCTION && x.func.apply(gthis, x.params));

			if ( typeof va.default==VALUE_FUNCTION && !result.sReduce((r,v)=>r||typeof v.func==VALUE_FUNCTION, false) ) {
				va.default(va.scommandLine);
			}
		}

		if ( props && typeof props.onFinish==VALUE_FUNCTION ) props.onFinish();
	 };

	/*Inicio*/
	this.sClass='sCommands';
	props!=0 && this.constructor(props);
 };sCommands.prototype=new sObject(0);
global.sServer   = function( props ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0);

	/*Variables*/
	const gthis=this;
	const fs   =this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const op   ={};
	const va   ={};
	const co   ={ crypto:require('crypto') };

	/*Metodos*/
	gthis.Connect     = ({ host='0.0.0.0', port=8080, cert={}, protocol='http' }) => {
		gthis.Disconnect();

		if ( IsFile(cert.cert) ) cert.cert=FileRead(cert.cert);
		if ( IsFile(cert.key ) ) cert.key =FileRead(cert.key );

		switch (protocol) {
			case 'http2': !co.http2&&(co.http2=require('http2')); co.server=co.http2.createSecureServer(cert); co.server.on('stream', requ2_this); break;
			case 'https': !co.https&&(co.https=require('https')); co.server=co.https.createServer(cert, requs_this).on('upgrade', updat_socket); break;
			default     : !co.http &&(co.http =require('http' )); co.server=co.http .createServer(      reque_this).on('upgrade', updat_socket); break;
		}

		co.server.listen( port, host, x=>gthis.tri('onConnect') );

		return gthis;
	 };
	gthis.Disconnect  = (                                                       ) => {
		if ( co.server ) {
			if ( gthis.response ) gthis.response.end();
			if ( gthis.request  ){gthis.request.connection.end(); gthis.request.connection.destroy;}

			co.server.close( x=>gthis.tri('onDisconnect') );
		}

		return gthis;
	 };
	gthis.EnabledCors = ( enabledcors                                           ) => {
		if ( enabledcors===undefined ) return op.enabledcors===undefined ? op.enabledcors=false : op.enabledcors;

		op.enabledcors=enabledcors;

		return gthis;
	 };

	/*Eventos - Metodos*/
	gthis.onConnect = (         ) => {};
	gthis.onRequest = ( headers ) => 200;

	gthis.onSocketOpen  = ( socket                ) => {};
	gthis.onSocketClose = ( socket                ) => {};
	gthis.onSocketData  = ( socket, json, message ) => {};

	/*Eventos*/
	function reque_this( request, response ) {
		if ( op.enabledcors && request.method==='OPTIONS' ) {Response({code:204}, { petition:{request, response} }); return}

		let body='', json, isJson=false;

		request
		.on('error', e=>console.error(e))
		.on('data' , c=>body+=c )
		.on('end'  , x=>{
			try{json=JSON.parse(body); isJson=true} catch(e) {json={}}

			let hea=ParseHttpHeaders(
				Object.assign(
					{method:request.method},
					{protocol:'http'},
					{petition:{request, response}},
					request.headers,
					{headers:request.headers},
					{origin:(request.headers.referer||'').replace(/\/$/,'')},
					{referer:'http://'+request.headers.host+request.url},
					{url_path:request.url},
					{body, json, isJson},
				)
			);

			hea.Response=(o)=>Response(o, hea);

			gthis.tri('onRequest', hea);
		});
	 }
	function requs_this( request, response ) {
		if ( request.method==='OPTIONS' ) {Response({code:204}, { petition:{request, response} }); return}

		let body='', json, isJson=false;

		request
		.on('error', e=>console.error(e))
		.on('data' , c=>body+=c )
		.on('end'  , x=>{
			try{json=JSON.parse(body); isJson=true} catch(e) {json={}}

			let hea=ParseHttpHeaders(
				Object.assign(
					{method:request.method},
					{protocol:'https'},
					{petition:{request, response}},
					request.headers,
					{headers:request.headers},
					{referer: 'https://'+request.headers.host+request.url},
					{url_path:request.url},
					{body, json, isJson},
				)
			);

			hea.Response=o=>Response(o, hea);

			gthis.tri('onRequest', hea);
		});
	 };
	function requ2_this( stream , headers  ) {
		// if ( request.method==='OPTIONS' ) {Response({code:204}, { petition:{stream} }); return}

		let body='', json, isJson;
		stream.setEncoding('utf8');
		stream
		.on('error', e=>console.error(e))
		.on('data' , c=>body+=c )
		.on('end'  , x=>{
			try{json=JSON.parse(body); isJson=true} catch(e) {json={}}

			let obj={};

			for (let x in headers) obj[x.replace(/:/g, '')]=headers[x];

			obj.protocol='http2';
			obj.referer ='https://'+headers[':authority']+headers[':path'];
			// obj.url_path=request.url;
			obj.petition={stream};
			obj.method  =headers[':method'];
			obj         =ParseHttpHeaders(obj);
			obj.Response=o=>Response(o, obj);

			gthis.tri('onRequest', obj);
		});
	 };

	function updat_socket( request, socket ) {
		if ( !va.sockets ) va.sockets=[], va.idsSockets=0;

		// Conectar Socket
		if ( !(gthis.Key=request.headers['sec-websocket-key']) ) {
			Reject(400, 'El cliente no proporciono la llave publica WebSocket.');
			return;
		}

		if ( !(gthis.Version=parseInt(request.headers['sec-websocket-version'], 10)) || isNaN(gthis.Version) ) {
			Reject(400, 'El cliente no proporciono la version del protocolo.');
			return;
		}

		if      ( gthis.Version===13 ) gthis.Origin=request.headers['origin'              ];
		else if ( gthis.Version===8  ) gthis.Origin=request.headers['sec-websocket-origin'];
		else                           {Reject(400, 'Conexion rechazada por no ser version 8 o 13'); return}

		socket.write(
			'HTTP/1.1 101 Switching Protocols\r\n' +
			'Upgrade: websocket\r\n'               +
			'Connection: Upgrade\r\n'              +
			'Sec-WebSocket-Accept: '               + co.crypto.createHash('sha1').update(gthis.Key + '258EAFA5-E914-47DA-95CA-C5AB0DC85B11').digest('base64') + '\r\n' +
			'Sec-WebSocket-protocolo: chat\r\n'    +
			'\r\n'
		, 'ascii');

		/*Configurando socket*/
		socket.setTimeout(0);

		let tem={ id:va.idsSockets++, socket, request };

		gthis.tri('onSocketOpen', tem);
		socket.on('close'  , function(x){ close_socket.call(this, x, tem) });
		socket.on('data'   , function(x){ datas_socket.call(this, x, tem) });
		socket.on('timeout', function(x){ timeo_socket.call(this, x, tem) });
		va.sockets.push(tem);

		socket.Json         = response => {
			if ( typeof response===VALUE_NUMBER ) response={code:response};
			if ( !response.code                 ) response.code=200;
			if ( !response.data                 ) response.data=HttpCode(response.code);
			if ( response.data && tipode(response.data)===VALUE_OBJECT && response.data.code && response.data.data===undefined )
				response.data.data=HttpCode(response.data.code);

			socket.write( StrToDat( JSON.stringify(response) ) );
		 };
		socket.Send         = response => socket.write( StrToDat(response) );
		gthis.SocketSendAll = json     => va.sockets.forEach( s=>s.socket.write( StrToDat(json) ) );
		tem.Json            = socket.Json;
		tem.Send            = socket.Send;
	 };
	function timeo_socket( data, tsoc      ) {
		gthis.tri('onSocketTimeout', tsoc);
	 };
	function close_socket( data, tsoc      ) {
		gthis.tri('onSocketClose', tsoc);

		for ( let x=va.sockets.length; x--; ) {
			if ( va.sockets[x].id===tsoc.id ) {
				va.sockets.splice(x, 1);
				break;
			}
		}
	 };
	function datas_socket( data, tsoc      ) {
		if ( data[0]===0x88 ) this.end();
		else {
			let res=DatToStr(data);
			let jso=null;

			try{jso=JSON.parse(res)} catch(e){}

			gthis.tri('onSocketData', tsoc, jso, res);
		}
	 };

	/*Funciones*/
	function Response(response, headers) {
		if      ( typeof response ==VALUE_NUMBER ) response={data:HttpCode(response), code:response, type:'txt', headers:{}};
		else if ( tipode(response)==VALUE_OBJECT ) {
			response={
				code   : response._code   ||200,
				type   : response._type   ||'json',
				data   : response._data!=undefined ? response._data : response,
				cookie : response._cookie,
				headers: response._headers||{},
			};

			delete response.data._code;
			delete response.data._type;
			delete response.data._data;
			delete response.data._cookie;
			delete response.data._headers;

			if ( tipode(response.data)==VALUE_OBJECT && !Object.keys(response.data).length )
				response.data=HttpCode(response.code);
		}
		else response={code:200, type:tipode(response)==VALUE_ARRAY?'json':'txt', headers:{}, data:response};

		if ( tipode(response.data)==VALUE_OBJECT || tipode(response.data)==VALUE_ARRAY )
			response.data=JSON.stringify(response.data);

		if ( headers.petition.response ) {
			headers.petition.response.writeHead(
				response.code,
				Object.assign(
					(!op.enabledcors?{}:{
						'Accept'                          : '*/*',
						'Access-Control-Allow-Origin'     : headers.petition.request.headers.origin || headers.origin || '*',
						'Access-Control-Allow-Headers'    : 'Authorization, token, version, botype, Content-Type, Cache-Control, Content-Length, Date, Expires, Server, Transfer-Encoding, x-goog-meta-foo1, Accept-Language, Origin, X-Requested-With, Accept, *',
						'Access-Control-Allow-Credentials': 'true',
						'Access-Control-Expose-Headers'   : '*',
					}),
					{'Content-Type':Mime(response.type)},
					(response.cookie ? {'set-cookie':response.cookie} : null),
					response.headers||{},
				)
			);

			headers.petition.response.end(response.data);
		}
		else if ( headers.petition.stream ) {
			headers.petition.stream.respond(
				Object.assign(
					(!op.enabledcors?{}:{
						'Access-Control-Allow-Origin'     : '*',
						'Access-Control-Allow-Methods'    : 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
						'Access-Control-Allow-Headers'    : 'Origin, X-Requested-With, Content-Type, X-Auth-Token, Accept',
						'Access-Control-Allow-Credentials': 'true',
					}),
					{':status':response.code,'content-type':Mime(response.type)},
					response.headers||{},
				)
			);
			headers.petition.stream.end(response.data);
		}
	 }

	function ParseHttpHeaders( headers ) {
		let res={};
		let pat=(headers.referer||'').replace(/^http(s?):\/\/[A-z0-9:.]+/g, '');
		let par='';

		if ( pat.indexOf('?')>-1 ) {
			par=pat.substring(pat.indexOf('?')+1);
			pat=pat.substring(0, pat.indexOf('?'));
		}

		pat=pat.split('/').splice(1);
		par=par.split('&');
		par=par.reduce((r,v)=>{
			let t=v.split('=');

			if ( t[0] && t[1] && t[0]!='' && t[1]!='' )
				r[t[0]||'']=(t[1]||'');

			return r;
		}, {});

		res=ParseHeaders( headers );

		if ( pat.length==1 && pat[0]==='' ) pat=[];

		res.path  =pat;
		res.params=par;

		return res;
	 }

	function Reject  ( socket, code, message ) {
		console.info( message );
		socket.end(`HTTP/1.1 ${code} ${va.codes[code]}\r\nConnection: close\r\n`, 'ascii');
	 }
	function DatToStr( data                  ) {
		let buf = '';
		let dat = 0;

		if ( data[1]&0x80 ) {
			let mlen = data[1] & 0x7F;

			if ( mlen<126 ) {
				for ( let i=0; i<mlen; i++ ) {
					buf+= String.fromCharCode(data[6+i] ^ data[i%4+2]);
				}
			}
			else if( mlen===126 ) {
				mlen = data[2] * 256 + data[3];

				for ( let i=0; i<mlen; i++ ) {
					buf+= String.fromCharCode(data[8+i] ^ data[i%4+4]);
				}
			}
		}

		return decodeURIComponent(buf);
	 }
	function StrToDat( data                  ) {
		data = String(data);

		let length = Buffer.byteLength(data);
		let index  = 2 + (length>65535 ? 8 : (length>125 ? 2 : 0));
		let buffer = new Buffer.alloc(index+length);

		buffer[0] = 129;

		if ( length>65535 ) {
			buffer[1] = 127;

			buffer.writeUInt32BE(0     , 2);
			buffer.writeUInt32BE(length, 6);
		}
		else if ( length>125 ) {
			buffer[1] = 126;

			buffer.writeUInt16BE(length, 2);
		}
		else buffer[1] = length;

		buffer.write(data, index);

		return buffer;
	 }

	/*Inicio*/
	this.sClass='sServer';
	props!=0 && this.constructor(props);
 };sServer.prototype=new sObject(0);

global.boStarts  = function( props ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0);

	/*Variables*/
	const gthis  =this;
	const fs     =this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const va     ={};

	/*Creacion*/
	gthis.constructor = function( _props, noexec ) {
		fs.constructor(_props, 1);

		/*Inicio*/
		if ( !noexec ) gthis.exe(_props) && (!_props || _props.GetStarts===undefined) && gthis.GetStarts( ParsePath().path );

		/*Inicio*/
		return gthis;
	 };

	/*Metodos*/
	gthis.GetStarts=path=>{
		if ( path===undefined ) return va.path;

		(function tpa( _path ) {
			FileTravel(_path).forEach(v=>{
				if      ( v.type===VALUE_FILE && v.base.toLowerCase()==='oninit.bj' ) try{requireBJ( v.path, Object.assign({}, props && props.options||{}) )}catch(e){console.error(e)}
				else if ( v.type===VALUE_FOLDER                                     ) tpa(v.path);
			});
		})(path);

		va.path=path;

		return gthis;
	 };

	/*Inicio*/
	this.sClass='boStarts';
	props!=0 && this.constructor(props);
 };boStarts.prototype=new sObject(0);
global.boOptions = function( props ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0);

	/*Variables*/
	const gthis=this;
	const fs   =this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const op   ={};

	/*Creacion*/
	gthis.constructor = function( _props, noexec ) {
		fs.constructor(_props, 1);

		this.Project  ={};
		this.Variables={};
		this.Server   ={};
		this.Paths    ={};
		this.Compiler ={project:this.Project, variables:this.Variables};
		this.Resources={project:this.Project, variables:this.Variables};
		this.Api      ={project:this.Project, variables:this.Variables};

		this.Project.type    = VALUE_APP;
		this.Project.author  = '';
		this.Project.version = '';

		this.Server.debug     = false;
		this.Server.protocol  = 'http';
		this.Server.cert      = {};
		this.Server.host      = 'localhost';
		this.Server.port      = 8080;
		this.Server.rest      = './rest';
		this.Server.sock      = './sock';
		this.Server.public    = './public';
		this.Server.resources = './resources';

		this.Paths.public = './public';
		this.Paths.rest   = './rest'  ;
		this.Paths.sock   = './sock'  ;

		this.Compiler.type             = VALUE_COMPILER;
		this.Compiler.watchFiles       = true;
		this.Compiler.autoRefresh      = true;
		this.Compiler.protocol         = 'http';
		this.Compiler.cert             = {};
		this.Compiler.host             = 'localhost';
		this.Compiler.port             = 8080;
		this.Compiler.pathSave         = './build';
		this.Compiler.pathPublic       = './public';
		this.Compiler.extensions       = ['.html','.htm','.svg','.css','.js','.json', '.bc','.bh','.bj'];
		this.Compiler.serverComponents = {host:process.env.hostComponents||'BlackKraken', protocol:process.env.protocolComponents||'https'};

		this.Resources.type       = VALUE_RESOURCES;
		this.Resources.watchFiles = true;
		this.Resources.protocol   = 'http';
		this.Resources.cert       = {};
		this.Resources.host       = 'localhost';
		this.Resources.port       = 8081;
		this.Resources.pathPublic = './public';
		this.Resources.extensions = ['.png','.ico','.jpg'];

		this.Api.type       = VALUE_API;
		this.Api.watchFiles = true;
		this.Api.protocol   = 'http';
		this.Api.cert       = {};
		this.Api.host       = 'localhost';
		this.Api.port       = 8082;
		this.Api.pathPublic = './public';
		this.Api.sock       = {protocol:'web'};

		!noexec && gthis.exe(_props);
	 };

	/*Metodos*/
	gthis.Path = function( path, environment ) {
		if ( path===undefined ) return op.path===undefined ? op.path='' : op.path;
		if ( environment      ) op.environment=environment;
		if ( !op.environment  ) op.environment='debug';

		let dat=Transpiler_BJS(FileRead(path), {});

		try{eval('gthis._options='+dat)}catch(e){ console.error(e); gthis._options={} }

		Compile(gthis._options);

		if ( op.environment && gthis._options[op.environment] )
			Compile(gthis._options[op.environment]);

		gthis.environment           = op.environment;
		global.__serverComponents__ = gthis.Variables.serverComponents;
	 };

	/*Funciones*/
	function Compile( options ) {
		if ( !options ) return;

		options.sForEach((v,k)=>{
			if ( k==='Project' ) {
				gthis.Project.type        = v.type   ||gthis.Project.type   ||VALUE_APP;
				gthis.Project.author      = v.author ||gthis.Project.author ||'';
				gthis.Project.version     = v.version||gthis.Project.version||'';
				gthis.Project.environment = op.environment;
			}
			else if ( k==='Server' ) {
				gthis.Server.debug       = v.debug===undefined ? gthis.Server.debug : v.debug;
				gthis.Server.protocol    = v.protocol ||gthis.Server.protocol ||'http';
				gthis.Server.cert        = v.cert     ||gthis.Server.cert     ||{};
				gthis.Server.host        = v.host     ||gthis.Server.host     ||'localhost';
				gthis.Server.port        = v.port     ||gthis.Server.port     ||8080;
				gthis.Server.rest        = v.rest     ||gthis.Server.rest     ||'./rest';
				gthis.Server.sock        = v.sock     ||gthis.Server.sock     ||'./sock';
				gthis.Server.public      = v.public   ||gthis.Server.public   ||'./public';
				gthis.Server.resources   = v.resources||gthis.Server.resources||'./resources';
				gthis.Server.environment = op.environment;
			}
			else if ( k==='Paths' ) {
				gthis.Paths.public = v.public||gthis.Paths.public||'./public';
				gthis.Paths.rest   = v.rest  ||gthis.Paths.rest  ||'./rest'  ;
				gthis.Paths.sock   = v.sock  ||gthis.Paths.sock  ||'./sock'  ;
			}
			else if ( k==='Compiler' ) {
				gthis.Compiler.watchFiles       = v.watchFiles ===undefined ? gthis.Compiler.watchFiles  : v.watchFiles ;
				gthis.Compiler.autoRefresh      = v.autoRefresh===undefined ? gthis.Compiler.autoRefresh : v.autoRefresh;
				gthis.Compiler.protocol         = v.protocol        ||gthis.Compiler.protocol        ||'http';
				gthis.Compiler.cert             = v.cert            ||gthis.Compiler.cert            ||{};
				gthis.Compiler.host             = v.host            ||gthis.Compiler.host            ||'localhost';
				gthis.Compiler.port             = v.port            ||gthis.Compiler.port            ||8080;
				gthis.Compiler.pathSave         = v.pathSave        ||gthis.Compiler.pathSave        ||'./build';
				gthis.Compiler.pathPublic       = v.pathPublic      ||gthis.Compiler.pathPublic      ||'./public';
				gthis.Compiler.extensions       = v.extensions      ||gthis.Compiler.extensions      ||['.html','.htm','.svg','.css','.js','.json', '.bc','.bh','.bj'];
				gthis.Compiler.serverComponents = v.serverComponents||gthis.Compiler.serverComponents||{host:'BlackKraken', protocol:'https'};
				gthis.Compiler.environment      = op.environment;

				gthis.Compiler.protocolText                 =   gthis.Compiler.protocol.replace(/2/g, 's');
				gthis.Compiler.serverText                   =`${gthis.Compiler.protocolText}://${gthis.Compiler.host+(gthis.Compiler.port && gthis.Compiler.port!=80?':'+gthis.Compiler.port:'')}`;
				gthis.Compiler.serverComponents.protocolText=   gthis.Compiler.serverComponents.protocol.replace(/2/g, 's');
				gthis.Compiler.serverComponents.serverText  =`${gthis.Compiler.serverComponents.protocolText}://${gthis.Compiler.serverComponents.host+(gthis.Compiler.serverComponents.port && gthis.Compiler.serverComponents.port!=80?':'+gthis.Compiler.serverComponents.port:'')}`;
			}
			else if ( k==='Resources' ) {
				gthis.Resources.watchFiles  = v.watchFiles===undefined ? gthis.Resources.watchFiles : v.watchFiles;
				gthis.Resources.protocol    = v.protocol  ||gthis.Resources.protocol  ||'http';
				gthis.Resources.cert        = v.cert      ||gthis.Resources.cert      ||{};
				gthis.Resources.host        = v.host      ||gthis.Resources.host      ||'localhost';
				gthis.Resources.port        = v.port      ||gthis.Resources.port      ||8081;
				gthis.Resources.pathSave    = v.pathSave;
				gthis.Resources.pathPublic  = v.pathPublic||gthis.Resources.pathPublic||'./public';
				gthis.Resources.extensions  = v.extensions||gthis.Resources.extensions||['.png','.ico','.jpg'];
				gthis.Resources.environment = op.environment;
			}
			else if ( k==='Api' ) {
				gthis.Api.watchFiles  = v.watchFiles===undefined ? gthis.Api.watchFiles : v.watchFiles;
				gthis.Api.protocol    = v.protocol  ||gthis.Api.protocol  ||'http';
				gthis.Api.cert        = v.cert      ||gthis.Api.cert      ||{};
				gthis.Api.host        = v.host      ||gthis.Api.host      ||'localhost';
				gthis.Api.port        = v.port      ||gthis.Api.port      ||8082;
				gthis.Api.pathSave    = v.pathSave;
				gthis.Api.pathPublic  = v.pathPublic||gthis.Api.pathPublic||'./public';
				gthis.Api.extensions  = v.extensions||gthis.Api.extensions||['.json'];
				gthis.Api.environment = op.environment;
				gthis.Api.sock        = Object.assign({protocol:'web'}, v.sock||gthis.Api.sock);
			}
			else if ( k==='Variables' ) v.sForEach((vv,kk)=>gthis.Variables[kk]=vv);
		});
	 };

	/*Inicio*/
	this.sClass='boOptions';
	props!=0 && this.constructor(props);
 };boOptions.prototype=new sObject(0);
global.boServer  = function( props ) {
	/*Herencia*/
	this.sServer = sServer;
	this.sServer(0);

	/*Variables*/
	const gthis = this;
	const fs    = this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const op    = props||{};
	const va    = {
		nfiles        : 0,
		filesRest     : {},
		filesSock     : {},
		filesCompiler : {},
		filesResources: {},
	 };

	/*Creacion*/
	gthis.constructor = function( _props, noexec ) {
		fs.constructor(_props, 1);

		if ( noexec ) return;

		gthis.exe(_props);
		gthis.EnabledCors(true);

		gthis.filesRest      = va.filesRest     ;
		gthis.filesSock      = va.filesSock     ;
		gthis.filesCompiler  = va.filesCompiler ;
		gthis.filesResources = va.filesResources;
	 };

	/*Metodos - CARGA DE ARCHIVOS*/
	gthis.Build = ( build ) => {
		if ( build===undefined ) return op.build===undefined ? op.build={} : op.build;

		switch ( build.type ) {
			case 'api'      : ApiLoad      (build); break;
			case 'compiler' : CompilerLoad (build); va.filesResources=va.filesCompiler; break;
			case 'resources': ResourcesLoad(build); break;
		}

		op.build = build;

		return gthis;
	 };

	/*Funciones - CARGA DE ARCHIVOS - API*/
	function ApiBuild ( build, file, clean ) {
		const
		tip      = file.dir.indexOf('sock')>-1,
		tif      = (tip ? va.filesSock : va.filesRest),
		nam      = file.name.replace(/^[0-9-_ ]+/g, '');
		file.url = '/'+nam;

		if ( tif[file.url] ) delete tif[file.url].instance;
		if ( global[nam]   ) delete global[nam];

		requireBJ( file.path, Object.assign({},build) );

		let ver = false;

		if ( global[nam] ) {
			file.instance          = new (eval(nam))({ files:tif });
			file.instance.__path__ = file.path;
		}
		else (ver=true) && log.error(`%fy${file.url}%r %cd${file.path}, Error: clase "${nam}" no encontrada%r`);

		file.lower_url = file.url.toLowerCase();
		tif[file.url]  = file;

		if ( !ver && gthis.viewLog!=undefined ? gthis.viewLog : (build.viewLog!=undefined ? build.viewLog : 1) )
			log.load(`%fy${file.url}%r%cd-${tip ? 'SOCK' : 'REST'}%r %cd${file.path}%r`);
	 }
	function ApiChange( build, file, clean ) {
		if ( file.ext.toLowerCase()!='.bj' ) return;

		if ( file.eventType==='delete' ) {
			const
			tif = (file.dir.indexOf('sock')>-1 ? va.filesSock : va.filesRest),
			url = file.url || (file.dir.replace( new RegExp(clean,'gi'), '' ) + '/' + file.name);

			delete tif[url];

			log.delete(`%fy${url}%r %cd${file.path}%r`);
		}
		else ApiBuild(build, file, clean);
	 }
	function ApiLoad  ( build              ) {
		const
		dre = ParsePath(build.pathPublic, 'rest'),
		dso = ParsePath(build.pathPublic, 'sock'),
		rec = (_path, _clean)=>{
			FileTravel(_path).forEach(v=>{
				if      ( v.type===VALUE_FOLDER && v.base.toLowerCase()!='node_modules' ) rec(v.path, _clean);
				else if ( v.type===VALUE_FILE   && v.ext .toLowerCase()=='.bj'          ) ApiBuild(build, v, _clean);
			});
		 };

		if ( dre.type!=VALUE_FOLDER ){log.error(`"${dre.path}" no es un directorio valido`)}
		if ( dso.type!=VALUE_FOLDER ){log.error(`"${dso.path}" no es un directorio valido`)}

		rec(dre.path, dre.path);
		rec(dso.path, dso.path);

		if ( gthis.watchFiles!=undefined ? gthis.watchFiles : (build.watchFiles!=undefined ? build.watchFiles : 1) ) {
			FileWatch(dre.path, {recursive:true}).all( v=>ApiChange(build, v, dre.path) );
			FileWatch(dso.path, {recursive:true}).all( v=>ApiChange(build, v, dso.path) );
		}

		gthis.tri('onLoad', gthis);
	 }

	/*Funciones - CARGA DE ARCHIVOS - COMPILER*/
	function GetDataVersion     (                    ) {
		return 'const CACHE_HASH = {'

			+va.filesCompiler.sReduce((res,act)=>{
				if ( act.url=='/versions.js' ) return res;

				return res + `\n\t"${
					act.name
						.replace(/^[0-9-_ ]+/g, '').toLowerCase() +
					(act.ext||'').replace(/[.]/g,'_').toLowerCase()
				}": ${Hash(act.data)},`;
			}, '')

		+'\n };';
	 }
	function CompilerIncludeJS  (                    ) {
		return new sProm((run,err)=>{
			Transpiler_INCG().run(x=>{
				// const
				// inc                             = ParsePath('/includes.js');
				// inc.url                         = '/includes.js';
				// inc.lower_url                   = '/includes.js';
				// inc.lower_path                  = inc.path.toLowerCase();
				// inc.clean                       = inc.dir;
				// inc.data                        = x;
				// inc.hash                        = Hash(inc.data);
				// inc.mime                        = Mime('.js');
				// va.filesCompiler[inc.lower_url] = inc;
				run();
			})
		});
	 }
	function CompilerVersionJS  ( build              ) {
		const
		// cor                             = va.filesCompiler['/bcore.js'],
		cor                             = va.filesCompiler['/bcore.bj'],
		ver                             = ParsePath('/versions.js');
		ver.url                         = '/versions.js';
		ver.lower_url                   = '/versions.js';
		ver.lower_path                  = ver.path.toLowerCase();
		ver.clean                       = ver.dir;
		ver.data                        = GetDataVersion();
		ver.hash                        = Hash(ver.data);
		ver.data                        = Transpiler_JS(`const CACHE_VERSION='${cor ? cor.hash : ver.hash}';\n${ver.data}`, Object.assign({},build));
		ver.mime                        = Mime('.js');
		va.filesCompiler[ver.lower_url] = ver;
	 };
	function CompilerRefresh    ( build, file        ) {
		if (
			gthis.autoRefresh!=undefined ? gthis.autoRefresh : (build.autoRefresh!=undefined ? build.autoRefresh : 1)
			&&
			file.lower_url=='/index.html'
		) {
			file.data+= `<script src="./bcore_refresh.bj"></script>`;
		}
	 };
	function CompilerInclude    ( build, file, clean ) {
		let isi = false;

		va.filesCompiler.sForEach(v=>{
			if ( v.__included__ && v.__included__.sSome( (vv,kk)=>kk.toLowerCase().indexOf(file.base.toLowerCase())>-1 ) ) {
				let par = ParsePath(v.path);
				par.ext = par.ext.toLowerCase();
				isi     = true;

				CompilerBuild(build, par, clean);
			}
		});

		return isi;
	 };
	function CompilerGetUrl     ( file, clean        ) {
		let url =
			file.path
			.replace(/\\/g, '/')
			.replace(clean.replace(/\\/g, '/'), '');

		if ( url.indexOf('/modules/' )>-1 ) url = '/modules/'  + file.base.replace(/^[0-9-_ ]+/g, '');
		if ( url.indexOf('/json/'    )>-1 ) url = '/json/'     + file.base.replace(/^[0-9-_ ]+/g, '');
		if ( url.indexOf('/taxonomy/')>-1 ) url = '/taxonomy/' + file.base.replace(/^[0-9-_ ]+/g, '');

		return url;
	 }
	function CompileDependencies(                    ) {
		va.filesCompiler.sForEach((v,k)=>{
			v.__classDependencies__ && v.__classDependencies__.sForEach((vv,kk)=>{
				if ( typeof vv===VALUE_OBJECT ) {
					const fil = va.filesCompiler.sSome((vvv,kkk)=>vvv.lower_url===`/modules/${kk.toLowerCase()}.bj` ? vvv : null);

					if ( fil ) {
						v.__classDependencies__[kk].name         = fil.name;
						v.__classDependencies__[kk].version_file = fil.definition && fil.definition.version || '';
					}
				}
			});
		});
	 }
	function CompileAllVersions ( file               ) {
		for ( let x in va.filesCompiler ) {
			if ( va.filesCompiler[x].name===file.name && va.filesCompiler[x].isAllVersion ) {
				delete va.filesCompiler[x];
				break;
			}
		}

		if ( file.definition && file.definition.version ) {
			const nve                          = `${file.name}_${file.definition.version.replace(/[\.-]/g, '_')}`;
			const npa                          = `/modules/${nve.toLowerCase()}.bj`;
			va.filesCompiler[npa]              = Object.assign({},file);
			va.filesCompiler[npa].isAllVersion = true;
			va.filesCompiler[npa].name         = nve;
			va.filesCompiler[npa].lower_url    = npa;
			va.filesCompiler[npa].data         = va.filesCompiler[npa].data
				.replace(new RegExp(file.name,'g'), nve)
				.replace(new RegExp(ClassToTag(file.name),'g'), ClassToTag(nve));
		}
	 }

	function CompilerBuild ( build, file, clean ) {
		return new sProm((run,err)=>{
			va.nfiles++;

			const uha = '/modules/'+file.base.replace(/^[0-9-_ ]/g, '').toLowerCase();
			let   vha = null, sha = null, jha = null;

			if ( va.filesCompiler[uha] ) {
				vha = va.filesCompiler[uha].hashView;
				sha = va.filesCompiler[uha].hashStyle;
				jha = va.filesCompiler[uha].hashScript;
			}

			Transpiler_ALL(file.path, Object.assign({}, file, build), true)
			.err(log.error)
			.run(x=>{
				x.url                         = CompilerGetUrl(x, clean);
				x.clean                       = clean;
				x.lower_url                   = x.url.toLowerCase();
				va.filesCompiler[x.lower_url] = x;

				CompileAllVersions(x);

				/*Compile Includes*/
				va.filesCompiler.sForEach(x=>{
					if ( !x.__included__ || !x.__included__[x.lower_path] ) return;
					if ( x.lower_path!=x.lower_path                       ) CompilerBuild( GetParsePath(x.path), x.clean, 1 );
				});

				CompilerIncludeJS()
				.run(xx=>{
					CompilerVersionJS(build);
					CompilerRefresh  (build, x);

					const isi = CompilerInclude(build, x, clean);

					if ( gthis.viewLog!=undefined ? gthis.viewLog : (build.viewLog!=undefined ? build.viewLog : 1) )
						log.load(`%fy${x.url}%r %cd${x.path}%r`);

					if ( --va.nfiles==0 && !isi ) {
						if      ( sha && sha!=x.hashStyle  ) gthis.tri('onChanges', { sender:gthis, class:x.name, style  :x.codeStyle, data:x.data });
						else if ( vha && vha!=x.hashView   ) gthis.tri('onChanges', { sender:gthis, class:x.name, view   :x.codeView , data:x.data });
						else if ( jha && jha!=x.hashScript ) gthis.tri('onChanges', { sender:gthis, class:x.name, script :x.data     , data:x.data });
						else                                 gthis.tri('onChanges', { sender:gthis, class:x.name, browser:1                        });

						if ( !va.yaLoad ) gthis.tri('onLoad', gthis) && (va.yaLoad=1);
					}

					CompileDependencies();
					run();
				});
			});
		});
	 }
	function CompilerChange( build, file, clean ) {
		if ( build.extensions.indexOf( file.ext.toLowerCase() )===-1 ) return;

		if ( file.eventType==='delete' ) {
			const url = CompilerGetUrl(file, clean);

			delete va.filesCompiler[url];

			log.delete(`%fy${url}%r %cd${file.path}%r`);
		}
		else CompilerBuild(build, file, clean);
	 }
	function CompilerLoad  ( build              ) {
		const
		fil = ParsePath(build.pathPublic),
		rec = ( _path, _clean ) => {
			FileTravel(_path).forEach(v=>{
				if      ( v.type===VALUE_FOLDER && v.base.toLowerCase()!='node_modules'               ) rec          (v.path, _clean);
				else if ( v.type===VALUE_FILE   && build.extensions.indexOf( v.ext.toLowerCase() )>-1 ) CompilerBuild(build, v, _clean);
			});
		 };

		if ( fil.type!=VALUE_FOLDER ) return;

		rec(fil.path         , fil.path         );
		rec(__dirname+'/core', __dirname+'/core');

		if ( gthis.watchFiles!=undefined ? gthis.watchFiles : (build.watchFiles!=undefined ? build.watchFiles : 1) ) {
			FileWatch(fil.path         , {recursive:true}).all( d=>CompilerChange(build, d, fil.path         ) );
			FileWatch(__dirname+'/core', {recursive:true}).all( d=>CompilerChange(build, d, __dirname+'/core') );
		}
	 }

	/*Funciones - CARGA DE ARCHIVOS - Resources*/
	function ResourcesBuild ( build, file, clean ) {
		file                        = Transpiler_ALL(file.path, Object.assign({}, file, build));
		file.url                    = file.path.replace(clean, '').replace(/\\/g, '/');
		va.filesResources[file.url] = file;

		if ( gthis.viewLog!=undefined ? gthis.viewLog : (build.viewLog!=undefined ? build.viewLog : 1) )
			log.load(`%fy${file.url}%r %cd${file.path}%r`);
	 }
	function ResourcesChange( build, file, clean ) {
		if ( build.extensions.indexOf( file.ext.toLowerCase() )===-1 ) return;

		if ( file.eventType==='delete' ) {
			const url = file.path.replace(clean, '').replace(/\\/g, '/');

			delete va.filesResources[url];

			log.delete(`%fy${url}%r %cd${file.path}%r`);
		}
		else ResourcesBuild(build, file, clean);
	 }
	function ResourcesLoad  ( build              ) {
		const
		fil = ParsePath(build.pathPublic),
		rec = ( _path, _clean ) => {
			FileTravel(_path).forEach(v=>{
				if      ( v.type===VALUE_FOLDER && v.base.toLowerCase()!='node_modules'             ) rec           (v.path, _clean);
				else if ( v.type===VALUE_FILE && build.extensions.indexOf( v.ext.toLowerCase() )>-1 ) ResourcesBuild(build, v, _clean);
			});
		 };

		if ( fil.type!=VALUE_FOLDER ) return;

		rec(fil.path, fil.path);

		if ( gthis.watchFiles!=undefined ? gthis.watchFiles : (build.watchFiles!=undefined ? build.watchFiles : 1) )
			FileWatch(fil.path, {recursive:true}).all( v=>ResourcesChange(build, v, fil.path) );

		gthis.tri('onLoad', gthis);
	 };

	/*Metodos - SERVIDOR*/
	gthis.Connect = ( connect ) => {
		op.connect = connect;

		if ( !connect ) throw new Error('void property connect');

		fs.Connect(connect);
	 };

	/*Eventos - Metodos*/
	gthis.onConnect     = (                       ) => {
		log.info(`servidor %fy"${op.connect.type}"%r %fgconectado%r %cd(p:${op.connect.protocol||'http'}, h:${op.connect.host||'0.0.0.0'}, u:${op.connect.port||8080}, w: ${op.connect.protocol}://${op.connect.host}:${op.connect.port} )%r`);
		gthis.Build(op.connect);
	 };
	gthis.onRequest     = ( headers               ) => {
		switch (headers.botype||op.build.type) {
			case 'api'      : ResponseAPI(headers); break;
			case 'component': ResponseBOC(headers); break;
			default         : ResponseWEB(headers); break;
		}

		return gthis;
	 };
	gthis.onChanges     = ( changes               ) => {
		if ( !gthis.SocketSendAll ) return;

		if ( changes.style ) gthis.SocketSendAll( JSON.stringify({ type:'style', class:changes.class, code:changes.style }) );
		else                 gthis.SocketSendAll( JSON.stringify({ type:'code' , class:changes.class, code:changes.data  }) );
	 };
	gthis.onSocketOpen  = ( socket                ) => {
		va.filesSock.sForEach(v=>{
			if ( v.instance && v.instance.onOpen )
				try{EvalResultSock(v.instance.onOpen(socket), socket, 0)} catch(e){log.error(e)}
		});
	 };
	gthis.onSocketClose = ( socket                ) => {
		va.filesSock.sForEach(v=>{
			if ( v.instance && v.instance.onClose )
				try{EvalResultSock(v.instance.onClose(socket), socket, 0)} catch(e){log.error(e)}
		});
	 };
	gthis.onSocketData  = ( socket, json, message ) => {
		if ( !json || tipode(json)!=VALUE_ARRAY ) {socket.Json(400); return}

		for ( let x=0; x<json.length; x++ ) {
			const fil=va.filesSock.sSome((v,k)=>v.lower_url==='/'+json[x].class.toLowerCase() ? v : false);

			if ( fil && typeof fil.instance==VALUE_OBJECT ) {
				if ( json[x].func ) {
					const func=fil.instance.sSome((v,k)=>k===json[x].func ? v : false);

					if ( func && typeof func===VALUE_FUNCTION ) {
						if ( tipode(json[x].params)===VALUE_ARRAY ) {
							json[x].params.push(socket);
							EvalResultSock(func.apply(socket,json[x].params), socket, json[x].id);
						}
						else EvalResultSock(func.call (socket,json[x].params,socket), socket, json[x].id);
					}
					else socket.Json(404);
				}
				else if ( typeof fil.instance.onData===VALUE_FUNCTION ) {
					if ( tipode(json[x].params)===VALUE_ARRAY ) {
						json[x].params.push(socket);
						EvalResultSock(fil.instance.onData.apply(socket,json[x].params), socket, json[x].id);
					}
					else EvalResultSock(fil.instance.onData.call (socket,json[x].params,socket), socket, json[x].id);
				}
				else socket.Json(404);
			}
			else socket.Json(404);
		}
	 };

	/*Funciones - SERVIDOR*/
	function ResponseBOC( headers ) {
		let   cad = '', dep = [], cla = [];
		const sea = ( classSeach, add ) => {
			const fil = va.filesCompiler.sSome((v,k)=>v.lower_url===`/modules/${classSeach.toLowerCase()}.bj` ? v : null);

			if ( fil ) {
				fil.__classDependencies__ && fil.__classDependencies__.sForEach(
					(v,k)=>{
						if ( v.type=='extends' ) {
							const nam = v.name + (v.version_dependencie ? '_' + v.version_dependencie.replace(/[\.-]/g, '_') : '');
							sea(nam, dep.indexOf(nam)===-1);
						}
					}
				);

				if ( add ) {
					cad+= `\n//--{{${classSeach}}}\n${fil.data}\n//--{{${classSeach}}}\n`;

					cla.push(classSeach);
				}
			}
		 };

		try {dep = JSON.parse(headers.bodependencies)} catch(e) {}

		sea(headers.path[headers.path.length-1], true);

		if ( cad!='' ) headers.Response({ _code:200, _data:cad, _type:'js', _headers:{boclass:JSON.stringify(cla)} });
		else {
			headers.Response(404);
			return;










			Ajax(
				`${op.build.serverComponents.serverText}/${headers.path[0]}`,
				{ protocol:op.build.serverComponents.protocol, headers:{token:'1', version:headers.version, botype:'component'}
			})
			.run(x=>{
				if ( x.status===200 ) {
					headers.Response({ _code:200, _data:x.data });

					va.filesCompiler[`/modules/${headers.path[0]}.bj`]={
						data     : x.data,
						base     : `${headers.path[0]}.bj`,
						hash     : Hash(x.data),
						lower_url: `/modules/${headers.path[0]}.bj`.toLowerCase()
					};
				}
				else headers.Response(404);
			})
			.err( e=>headers.Response(404) && console.error(e) );
		}
	 };
	function ResponseAPI( headers ) {
		let res=null, par, uri='';

		if ( headers.path.length && headers.path[0]==='boListEndPoints' ) {
			EvalResultRest( headers, GetStruct() );
			return;
		}

		for ( let x=0; x<headers.path.length; x++ ) {
			uri+='/'+headers.path[x].toLowerCase();
			fil =va.filesRest.sSome((v,k)=>v.lower_url===uri ? v : false);

			if ( fil && fil.instance ) {
				headers.sClass=fil.instance.sClass;

				if ( !SetGateway(headers) ) {headers.Response(401); return}

				if ( headers.path.length>=2 && typeof fil.instance[headers.path[1]]==VALUE_FUNCTION ) {
					res=fil.instance[headers.path[1]];
					par=[...headers.json,headers];
				}
				else {
					if ( typeof fil.instance['on'+headers.method]==VALUE_FUNCTION ) res=fil.instance['on'+headers.method];
					else                                                            res=fil.instance.onRequest;

					par=[headers];
				}

				break;
			}
		}

		if ( typeof res==VALUE_FUNCTION ) EvalResultRest( headers, res(...par) );
		else                              headers.Response(404);
	 };
	function ResponseWEB( headers ) {
		let
		url = headers.url_path.toLowerCase(); url=='/' && (url='/index.html'),
		fil = va.filesResources.sSome((v,k)=>k.toLowerCase()===url ? v : null);

		if      ( fil                              ) headers.Response({ _code:200, _data:fil.data, _type:fil.ext });
		else if ( op.build.project.type==VALUE_SPA ) {
			va.filesResources.sSome((v,k)=>{
				if ( k.toLowerCase()=='/index.html' ) {
					headers.Response({ _code:200, _data:v.data, _type:v.ext });
					return true;
				}
			}) || headers.Response(404);
		}
		else headers.Response(404);
	 };

	/*Funciones - Respuesta*/
	function SetGateway( headers ) {
		const fil=va.filesRest.sSome((v,k)=>k.toLowerCase()==='/gateway' ? v : null);

		if ( fil && fil.instance ) {
			let res=null;

			if ( headers.path.length>=2 && typeof fil.instance[headers.path[1]]==VALUE_FUNCTION ) res=fil.instance[headers.path[1]];
			else {
				if ( typeof fil.instance['on'+headers.method]==VALUE_FUNCTION ) res=fil.instance['on'+headers.method];
				else                                                            res=fil.instance.onRequest;
			}

			if ( typeof res==VALUE_FUNCTION ) return res(headers);

			return false;
		}

		return true;
	 }

	function EvalResultRest( headers, result      ) {
		if ( result===undefined ) return;

		let sda=data=>headers.Response(data);

		if ( tipode(result)===VALUE_OBJECT && result.sClass==='sProm' ) {
			result.run( r=>sda(r) );
			result.err( e=>sda(e) );
		}
		else sda(result);
	 }
	function EvalResultSock( result, sock, jsonID ) {
		if ( result===undefined ) return;

		let sda=data=>sock.Json({ id:jsonID, type:tipode(data), data });

		if ( tipode(result)===VALUE_OBJECT && result.sClass==='sProm' ) {
			result.run( r=>sda(r) );
			result.err( e=>{console.error(e); sda(e)} );
		}
		else sda(result);
	 }

	function GetStruct() {
		return {
			sock: va.filesSock.sReduce((r,v,k)=>{
				r[v.name.replace(/^[0-9-_ ]+/g, '')] = v.instance.sReduce((r,v,k)=>{
					k[0]>='A' && k[0]<='Z' && (r[k]=1);
					return r;
				}, {});

				return r;
			}, {}),

			rest: va.filesRest.sReduce((r,v,k)=>{
				r[v.name.replace(/^[0-9-_ ]+/g, '')] = v.instance.sReduce((r,v,k)=>{
					k[0]>='A' && k[0]<='Z' && (r[k]=1);
					return r;
				}, {});

				return r;
			}, {}),
		};
	 }

	/*Inicio*/
	this.sClass='boServer';
	props!=0 && this.constructor(props);
 };boServer.prototype=new sServer(0);
global.boRest    = function( props ) {
	/*Herencia*/
	this.sObject=sObject;
	this.sObject(0);

	/*Variables*/
	const gthis=this;
	const fs   =this.sReduce( (r,v,k)=>{typeof v==VALUE_FUNCTION && (r[k]=v); return r;}, {} );
	const op   ={};

	/*Inicio*/
	this.sClass='boRest';
	props!=0 && this.constructor(props);
 };boRest.prototype=new sObject(0);

/*Inicio*/
global.log = new sLog;




// const par = {};
// FileWrite(
// 	'/Users/and/Desktop/BlackOcean/code/npm/test_files/file_compile0',
// 	Transpiler_BJS(
// 		FileRead('/Users/and/Desktop/BlackOcean/code/npm/test_files/bj/test0.bj'),
// 		par
// 	)
// )
// console.log(99, par.__htmlEvents__);
