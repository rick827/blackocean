

[![BlackOcean.js](https://i.imgur.com/ppQX9DZ.png)](https://BlackOcean.run)

BlackOcean.js es un Framework Javascript fuertemente enfocado en la verdadera reutilización de código en función de componentes completamente encapsulados e independientes del entorno de trabajo.

BlackOcean.js es precursor en el concepto de la nube de componentes, en donde los componentes web se pueden consumir directamente de la web o de un servicio concreto, en lugar de tener que copiarlos, ajustarlos y reconstruirlos para cada proyecto.

BlackOcean.js construye sitios, SPA, PWA y sistema ultra ligeros y eficientes, con velocidades de descarga superiores a cualquier otro framework.

BlackOcean.js también está pensado para aplicaciones off line, pues carga y actualiza cada componente en su propia cache.


## Inicio

En este apartado se explica cómo se puede comenzar a trabajar con BlackOcean.js y las herramientas de las que depende.

Para una mayor información, por favor consulta: https://BlackOcean.run/docs

###Requisitos Previos

Este Framework está desarrollado en Javascript ES6+ con [Node.js](https://nodejs.org/es/) [v8.11.2](https://nodejs.org/dist/v8.11.2/node-v8.11.2.pkg), por lo que se requerirá tener instalada una versión de Node.js igual o superior.

### Instalación

BlackOcean.js se encuentra disponible como un paquete [npm](https://www.npmjs.com/package/black-ocean), de manera que para instalar y usarlo solo hay que ejecutar el comando para instalar el Framework de manera global: ```npm i -g black-ocean```

### Primer Proyecto

1. Prepara una carpeta para alojar nuestro proyecto.
2. Dirígete a esa carpeta en la consola.
3. Ejecuta el comando del CLI: ```bo i```, que inicializara el proyecto. 
4. Ejecuta el comando del CLI: ```bo s```, que iniciara los servicios.
5. Visita en el navegador: [http://localhost:8080](http://localhost:8080/)
6. Visita nuestra documentación en: https://blackocean.run/docs


## Autores

* Jesús E. Aldréte Hernández <br><jesus@aldrete.pro>


## License

Este proyecto está licenciado bajo la Licencia MIT - Véase el archivo [LICENSE](https://bitbucket.org/jesus-aldrete/blackocean/src/master/LICENSE.md) para más detalles.